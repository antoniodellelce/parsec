c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Reads file wfn.dat and produces output to be visualizes with
c     OpenDX (or DataExplorer). Output functions are: self-consistent
c     potential, density, or wave-functions.
c
c     input file : wfn.dat
c     output file: [ out_file ]
c
c     Murilo L Tiago, UTexas-Austin, August 2005, mtiago@ices.utexas.edu
c
c     ===============================================================
      program plotdx

      implicit none

      integer, parameter :: nmax = 100

      character (len=200) :: outfile
      character (len=26) :: label
      integer :: ipr, ispflag, wfnflag,neig, pbcflag
      integer :: ndim, ntrans, nspin, nstate
      integer :: indxeig(nmax), indxspn(nmax)
      integer :: ii, jj, kk, is, jr, nw(3), rmin(3), rmax(3)
      double precision :: hh(3), rsize(3), shift(3), vec(3)
      integer, allocatable :: rgrid(:,:), rgrid_full(:,:), irep(:),
     1     chi(:,:)
      double precision, allocatable :: wfn(:),trans(:,:,:),
     1     plotbox(:,:,:)
c
      open(20,file='wfn.dat',form='unformatted',status='old')
      ipr = 0
      ispflag = 0
      wfnflag = 0
      pbcflag = 0

      write(6,*) 'Output file ? '
      read(5,'(a)') outfile
      open(30,file=trim(outfile),form='formatted')
      write(6,*) 'Reading input file wfn.dat'
      write(6,*) 'writing output to file ',trim(outfile)

      write(6,*) 'Plot density (1), potential (2), or ',
     1     'wave-functions (3) ? '
      read(5,*) ipr
      select case(ipr)
      case (1)
         write(6,*) 'Printing electron density [ 1/bohr^3 ]'
      case (2)
         write(6,*) 'Printing self-consistent potential [Ry]'
      case (3)
         write(6,*) 'Printing wave-functions '
         write(6,*)
     1        'Print real part (1) or absolute value squared (2) ?'
         read(5,*) wfnflag
      end select

      write(6,*) ' Periodic (1) or non-periodic (2) system ?'
      read(5,*) pbcflag
      select case(pbcflag)
      case (1)
         write(6,*) ' system is periodic on all dimensions'
      case (0)
         write(6,*) ' system is confined (non-periodic)'
      end select

      read(20) label
      write(6,*) ' wfn.dat file created on ',label
      if (pbcflag .eq. 1) then
         read(20) hh(:),rsize(:),ii,ndim,ntrans,nspin
      else
         read(20) hh(1),rsize(1),ii,ndim,ntrans,nspin
         hh(2:3) = hh(1)
         rsize(2:3) = rsize(1)
      endif

      ispflag = 1
      if (ipr .eq. 1 .and. nspin .eq. 2) then
         write(6,*) ' System is spin polarized'
         write(6,*) ' Plot up component(1),',
     1        ' down component (2) or total density (3) ?'
         read(5,*) ispflag
      endif
      if (ipr .eq. 2 .and. nspin .eq. 2) then
         write(6,*) ' System is spin polarized'
         write(6,*) ' Plot up component(1) or', 
     1        ' down component (2) of potential?'
         read(5,*) ispflag
      endif

      if (ipr .eq. 3 .and. nspin .eq. 1) then
         write(6,*) ' Which eigenstates will be printed out ?'
         write(6,*) ' End list with 0. Output is added up '
         neig = 0
         do
            neig = neig + 1
            if (neig .gt. nmax) then
               write(6,*)
     1              'ERROR: too many eigenstates. Increase nmax'
               write(6,*) ' STOP'
               stop
            endif
            read(5,*) indxeig(neig)
            indxspn(neig) = 1
            if (indxeig(neig) .eq. 0) exit
         enddo
         neig = neig - 1
      endif
      if (ipr .eq. 3 .and. nspin .eq. 2) then
         write(6,*) ' Which eigenstates/spins will be printed out ?'
         write(6,*) ' End list with 0 0. Output is added up '
         neig = 0
         do
            neig = neig + 1
            if (neig .gt. nmax) then
               write(6,*)
     1              'ERROR: too many eigenstates. Increase nmax'
               write(6,*) ' STOP'
               stop
            endif
            read(5,*) indxeig(neig), indxspn(neig)
            if (indxeig(neig) .eq. 0) exit
         enddo
         neig = neig - 1
      endif

      read(20) shift
      read(20)
      allocate(trans(3,3,ntrans))
      read(20) trans
      read(20)
      read(20)
      allocate(chi(ntrans,ntrans))
      read(20) ((chi(ii,jj),ii=1,ntrans),jj=1,ntrans)

      allocate(rgrid(3,ndim))
      allocate(rgrid_full(3,ntrans*ndim))
      read(20) ( (rgrid(ii,jr),ii=1,3), jr=1,ndim )
      call unfoldgrid(rgrid,rgrid_full,ndim,ntrans,shift,trans)
      rmin = minval(rgrid_full,2)
      rmax = maxval(rgrid_full,2)
      nw = rmax - rmin + 1
      allocate(plotbox(nw(1),nw(2),nw(3)))
      plotbox = 0.0

      call dx_latticevec(hh,nw)

      allocate(wfn(ndim))
c
      do is = 1, nspin
         read(20) nstate
         allocate(irep(nstate))

         read(20) (irep(jj), jj=1,nstate)
         if (wfnflag .eq. 2) irep = 1

         read(20)
         read(20)
c     potential, trivial representation
         read(20) (wfn(jr), jr=1,ndim)
         if (ipr .eq. 2 .and. is .eq. ispflag) call
     1        addbox(nw,wfn,ndim,ntrans,chi(1,1),rgrid_full,plotbox)
c     charge density, trivial representation
         read(20) (wfn(jr), jr=1,ndim)
         if (ipr .eq. 1 .and. ispflag .eq. is) call
     1        addbox(nw,wfn,ndim,ntrans,chi(1,1),rgrid_full,plotbox)
         if (ipr .eq. 1 .and. ispflag .eq. 3) call
     1        addbox(nw,wfn,ndim,ntrans,chi(1,1),rgrid_full,plotbox)

c     wave-functions, must add characters from the corresponding representation
         do jj = 1,nstate
            read(20) (wfn(jr), jr=1,ndim)
            if (ipr .eq. 3) then
               if (wfnflag .eq. 2) then
                  do jr = 1, ndim
                     wfn(jr) = wfn(jr) * wfn(jr)
                  enddo
               endif
               do ii = 1,neig
                  if (indxeig(ii) .eq. jj .and. indxspn(ii) .eq. is)
     1                 call addbox(nw,wfn,ndim,ntrans,chi(1,irep(jj))
     2                 ,rgrid_full,plotbox)
               enddo
            endif
         enddo                  ! jj = 1,nstate
         deallocate(irep)
      enddo                     ! is = 1, nspin
      deallocate(wfn)
      close(20)
      write(6,*) ' Done! sum = ',sum(plotbox)*hh(1)*hh(2)*hh(3)

      write(30, 5)              ! write the header for the array
      write(30, 10) nw(1) * nw(2) * nw(3)

 5    format(/'# this is the object defining the data values')  
 10   format('object 1 class array type float rank 0 items ',i8,
     1     ' data follows')
c
      write(30,'(5g15.7)') (((plotbox(ii,jj,kk),ii=1,nw(1))
     1     ,jj=1,nw(2)),kk=1,nw(3))
c
      write(30, 20) nw(3), nw(2),  nw(1)
 20   format(/'# this is the object defining the grid connections',
     1     /'object 2 class gridconnections counts ',3i4)
                                !     write header for gridpositions
      write(30, 30) nw(3), nw(2), nw(1)
 30   format(//'# this is the object defining the grid ',
     1     /'object 3 class gridpositions counts ',3i4 )

      do ii = 1,3
         vec(ii) = ( minval(rgrid_full(ii,:)) + shift(ii) ) * hh(ii)
      enddo
      write(30,'(a,3f20.10)') 'origin',vec

      write(30,50) 0.0,0.0,hh(3)
      write(30,50) 0.0,hh(2),0.0
      write(30,50) hh(1),0.0,0.0
 50   format('delta ', 3f20.10)  
                                !     write header for field object
      write(30, 60)  
 60   format(/'# this is the collective object, one for each grid',
     1     /'object 4 class field',
     2     /'component "positions"   value 3',
     3     /'component "connections" value 2',
     4     /'component "data"        value 1')
      write(30,*)
      write(30,'(2a)') '# Date label : ',label
      if (ipr .eq. 1) then
         write(30,'(a)') '# electron density'
         if (nspin .eq. 2 .and. ispflag .eq. 3) then
            write(30,'(a)') '# total density'
         elseif (nspin .eq. 2 .and. ispflag .eq. 2) then
            write(30,'(a)') '# spin up component'
         elseif (nspin .eq. 2 .and. ispflag .eq. 1) then
            write(30,'(a)') '# spin down component'
         endif
      elseif (ipr .eq. 2) then
         write(30,'(a)') '# self-consistent potential'
         if (nspin .eq. 2 .and. ispflag .eq. 1) then
            write(30,'(a)') '# spin up component'
         elseif (nspin .eq. 2 .and. ispflag .eq. 2) then
            write(30,'(a)') '# spin down component'
         endif
      elseif (ipr .eq. 3) then
         write(30,'(a)') '# electron wave-functions'
         if (wfnflag .eq. 1) then
            write(30,'(a)') '# real part'
         elseif (wfnflag .eq. 2) then
            write(30,'(a)') '# absolute value squared'
         endif
         if (nspin .eq. 1) then
            write(30,'(a)') '# printed eigenstates : '
            write(30,70) (indxeig(ii),ii=1,neig)
         elseif (nspin .eq. 2) then
            write(30,'(a)') '# printed eigenstates : '
            write(30,80) (indxeig(ii),indxspn(ii),ii=1,neig)
         endif
      endif
 70   format('# ',10i4)
 80   format(10('# ',3('( ',i4,' , ',i1,' ) '),/))

      close(30)

      end program plotdx
c     ===============================================================
      subroutine unfoldgrid(rgrid,rgrid_full,ndim,ntrans,shift,trans)

      implicit none
      integer, intent(in) :: ndim,ntrans
      double precision, intent(in) :: shift(3),trans(3,3,ntrans)
      integer, intent(in) :: rgrid(3,ndim)
      integer, intent(out) :: rgrid_full(3,ntrans*ndim)

      integer ir, jr, kt
      double precision rvec(3), tvec(3)

      do ir = 1, ndim
         do kt = 1, ntrans
            jr = kt + (ir-1)*ntrans
            rvec = rgrid(:,ir) + shift
            tvec = matmul(rvec,trans(:,:,kt)) - shift
            rgrid_full(:,jr) = nint(tvec)
         enddo
      enddo

      end subroutine unfoldgrid
c     ===============================================================
      subroutine addbox(nw,wfn,ndim,ntrans,chi,rgrid_full,plotbox)

      implicit none
      integer, intent(in) :: ndim,ntrans,nw(3)
      integer, intent(in) :: chi(ntrans),rgrid_full(3,ntrans*ndim)
      double precision, intent(in) :: wfn(ndim)
      double precision, intent(inout) :: plotbox(nw(1),nw(2),nw(3))

      integer ir, jr, kt, rmin(3), i1, i2, i3
      double precision rvec(3), tvec(3), wfn_tmp

      rmin = minval(rgrid_full,2)
      do ir = 1, ndim
         do kt = 1, ntrans
            jr = kt + (ir-1)*ntrans
            wfn_tmp = wfn(ir) * chi(kt)
            i1 = rgrid_full(1,jr) - rmin(1) + 1
            i2 = rgrid_full(2,jr) - rmin(2) + 1
            i3 = rgrid_full(3,jr) - rmin(3) + 1
            plotbox(i1,i2,i3) = plotbox(i1,i2,i3) + wfn_tmp
         enddo
      enddo

      end subroutine addbox
c     ===============================================================
      subroutine dx_latticevec(hh,nw)
c
c     writes files LATTICE_VEC.dx and UCELL_FRAME.dx for the
c     IBM data explorer, a visualization software package.
c     1996 Bernd Pfrommer, UC Berkeley.
c
      implicit none
      double precision, intent(in) :: hh(3)
      integer, intent(in) :: nw(3)

      integer :: i, j
      double precision :: avec(3,3)
 
      avec = 0.0
      do i = 1, 3
         avec(i,i) = hh(i) * nw(i)
      enddo
      open(19, file = 'LATTICE_VEC.dx', form= 'formatted')
      write(19, 10)  
      write(19, 20) 1, 3  
      write(19, 30) ((avec(i, j), i = 1, 3), j = 1, 3)
      write(19, 20) 2, 3  
      write(19, 35)  
      write(19, 40) 3, 1, 2  
      write(19, 50)  
      close(19)
  
      open(19, file= 'UCELL_FRAME.dx',form = 'formatted')
      write(19, 60)  
      write(19, 15) 3, 12  
      write(19,'(2i3)') 0, 1, 0, 2, 0, 3, 1, 4, 1, 5, 3, 5, 3, 6, 2,
     1     6, 2, 4, 7, 5, 7, 6, 7, 4
      write(19, *) 'attribute "element type" string "lines"'  
      write(19, 20) 4, 8  
      write(19, 30) 0.0,0.0,0.0
      write(19, 30) ((avec(i, j), i = 1, 3), j = 1, 3)
      write(19, 30) (avec(i, 1) + avec(i, 2), i = 1, 3)  
      write(19, 30) (avec(i, 1) + avec(i, 3), i = 1, 3)  
      write(19, 30) (avec(i, 2) + avec(i, 3), i = 1, 3)  
      write(19, 30) (avec(i, 1) + avec(i, 2) + avec(i, 3), i = 1, 3)
      write(19, 70) 5, 12  
      write(19, '(12(''1.0''/))')  
      write(19, *) 'attribute "dep" string "connections"'  
      write(19, 45) 6, 5, 4, 3  
      write(19, 50)  
      close(19)  

 10   format(2('#',/),'#    LATTICE VECTOR INFO:',2(/'#'))  
 15   format('object ',i2,' class array type '
     1     ,'int rank 1 shape 2 items ',i4,' data follows')
 20   format('object ',i2,' class array type '
     1     ,'float rank 1 shape 3 items ',i4,' data follows')
 30   format(3(f15.8))  
 35   format(3('0 0 0'/))  
 40   format('object ',i3,' class field',/'component "data" value '
     1     ,i2,/'component "positions" value ',i2)
 45   format('object ',i3,' class field',/'component "data" value '
     1     ,i2,/'component "positions" value ',i2,/'component
     2     "connections" value ',i2)

 50   format('end')  
 60   format(2('#',/),'#   UNIT CELL FRAME:',2(/'#'))  

 70   format('object ',i4,' array type float rank 0 items ',i4
     1     ,' data follows')

      end subroutine dx_latticevec
c     ===============================================================
