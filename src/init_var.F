c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Define a series of quantities over the processors.
c     Coefficients for the numerical derivatives are also defined.
c
c     ---------------------------------------------------------------
      subroutine init_var(clust,elec_st,mixer,solver,rsymm,parallel
     1     ,nloc_p_pot,grid,p_pot,pbc,ipr,ierr)

      use constants
      use cluster_module
      use electronic_struct_module
      use mixer_module
      use eigen_solver_module
      use parallel_data_module
      use non_local_psp_module
      use grid_module
      use pseudo_potential_module
      use pbc_module
      use symmetry_module

      implicit none

c     include mpi definitions
#ifdef MPI
      include 'mpif.h'
#endif
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
c     mixer related data
      type (mixer_data), intent(inout) :: mixer
c     eigen solver data
      type (eigen_solver), intent(inout) :: solver
c     parallel data
      type (parallel_data), intent(in) :: parallel
c     non local pseudopotential related data
      type (nonloc_pseudo_potential), intent(inout) :: nloc_p_pot 
c     grid related data
      type (grid_data), intent(inout) :: grid
c     pseudopotential related data
      type (pseudo_potential), intent(inout) :: p_pot 
c     periodic boundary conditions data
      type (pbc_data), intent(inout) :: pbc
c     symmetry operations in the reduced group
      type (symmetry), intent(inout) :: rsymm
c     printing flag
      integer, intent(inout) :: ipr
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     communicator
      integer comm
c     rank of the master in the above communicator
      integer masterid
c     exit code for mpi calls
      integer mpinfo
c     counters
      integer ity,ii,irp,nmax

c     ---------------------------------------------------------------

c
c     Master updates some variables.
c
      if (parallel%iammaster) then
c     change ekbi into h^3/ekbi, to be used from now on 
         do ity  = 1, clust%type_num
            do ii = 1, p_pot%nlocp(ity)
               if (ii .ne. p_pot%loc(ity)) p_pot%ekbi(ii,ity) = 
     1              grid%hcub / p_pot%ekbi(ii,ity)
            enddo
         enddo
c     for confined system, define grid%step; grid%step(1) will be
c     used in place of grid%stepin
         if (.not. pbc%is_on) then
            grid%step = grid%stepin
            grid%n1 = idint(two*grid%rmax/grid%stepin) + 2
            grid%n2 = grid%n1
            grid%n3 = grid%n1
         endif
c     transport important information from rsymm to elec_st
         elec_st%nrep = rsymm%ntrans
         allocate(elec_st%chi(elec_st%nrep,elec_st%nrep))
         elec_st%chi = rsymm%chi
      endif

#ifdef MPI
      masterid = parallel%masterid
      comm = parallel%comm
c
c     Broadcast info to all PEs
c
      call MPI_BCAST(pbc%is_on,
     1               1,MPI_LOGICAL,masterid,comm,mpinfo)
      call MPI_BCAST(pbc%box_size,
     1               3,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(rsymm%ntrans,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(rsymm%alatt,
     1               9,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(rsymm%invlat,
     1               9,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(clust%atom_num,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(grid%norder,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%nstate,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(solver%lpole,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%nspin,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%nrep,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%dioniz,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%icorr,
     1               2,MPI_CHARACTER,masterid,comm,mpinfo)
      call MPI_BCAST(nloc_p_pot%l_max,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(solver%maxmv,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(solver%kss0,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(solver%winsize,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(solver%nadd,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(mixer%name,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(mixer%memory,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(mixer%block,
     1               1,MPI_INTEGER,masterid,comm,mpinfo)
      call MPI_BCAST(mixer%param,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%xele,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm, mpinfo)
      call MPI_BCAST(elec_st%tfermi,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm, mpinfo)
      call MPI_BCAST(elec_st%ncharge,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm, mpinfo)
      call MPI_BCAST(solver%toler,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(solver%toler_in,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
          call MPI_BCAST(grid%hcub,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
          call MPI_BCAST(grid%hcub2,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
          call MPI_BCAST(grid%h_2,
     1               1,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(solver%name,6,MPI_CHARACTER,masterid,comm,mpinfo)
      call MPI_BCAST(ipr, 1,MPI_INTEGER,masterid,comm,mpinfo)

c     send the step sizes (NOTE: grid%stepin is not broadcasted)
      call MPI_BCAST(grid%step, 
     1     3,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
#endif
c
c     Get coefficients for numerical first derivative based on the
c     order specified by the user. These coefficients are used only
c     for GGA purposes.
c
      allocate(grid%coe1(-grid%norder:grid%norder,3))
      call fornberg(1,grid%norder,grid%coe1(-grid%norder,1),ierr)
#ifdef MPI
      call MPI_ALLREDUCE(ierr,ii,1,MPI_INTEGER,MPI_SUM,comm,mpinfo)
      ierr = ii
#endif

      if (ierr .ne. 0) then
         write(6,*) ' ERROR: Wrong parameter in call of Fornberg'
         write(6,*) ' STOP in init_var'
         return
      endif

c     renormalize coes with respect to the grid spacing
      grid%coe1(:,3) = grid%coe1(:,1)/grid%step(3)
      grid%coe1(:,2) = grid%coe1(:,1)/grid%step(2)
      grid%coe1(:,1) = grid%coe1(:,1)/grid%step(1)
c
c     Get coefficients for numerical second derivative based on the 
c     order specified by the user.
c
      allocate(grid%coe2(-grid%norder:grid%norder,3))
      call fornberg(2,grid%norder,grid%coe2(-grid%norder,1),ierr)
#ifdef MPI
      call MPI_ALLREDUCE(ierr,ii,1,MPI_INTEGER,MPI_SUM,comm,mpinfo)
      ierr = ii
#endif

      if (ierr .ne. 0) then
         write(6,*) ' ERROR: Wrong parameter in call of Fornberg'
         write(6,*) ' STOP in init_var'
         return
      endif

c     renormalize coekes with respect to the grid spacing
      grid%coe2(:,3) = -grid%coe2(:,1)/(grid%step(3)*grid%step(3))
      grid%coe2(:,2) = -grid%coe2(:,1)/(grid%step(2)*grid%step(2))
      grid%coe2(:,1) = -grid%coe2(:,1)/(grid%step(1)*grid%step(1))
c
c     All PEs (except master) define/allocate variables
c
      if (.not. parallel%iammaster) then

c     allocate arrays
         nloc_p_pot%maxnlm = (nloc_p_pot%l_max+1)**2
         nloc_p_pot%m_max = 2*nloc_p_pot%l_max+1
         allocate (nloc_p_pot%skbi (nloc_p_pot%maxnlm,clust%atom_num))
         allocate (nloc_p_pot%nlatom (clust%atom_num))
         allocate (nloc_p_pot%nlmatm (clust%atom_num))
         allocate
     1        (elec_st%occ_in(elec_st%nstate,elec_st%nspin))
         allocate (elec_st%ntotal (elec_st%nspin))
         allocate (elec_st%ifmax (elec_st%nspin))
         allocate (elec_st%sre (elec_st%nspin))
         allocate(elec_st%chi(elec_st%nrep,elec_st%nrep))
         nloc_p_pot%atom_num = clust%atom_num
         call create_symmetry (rsymm%ntrans,rsymm)
      endif
#ifdef MPI
      call MPI_BCAST(p_pot%type_num, 1,MPI_INTEGER
     1     ,masterid,comm,mpinfo)
      call MPI_BCAST(clust%type_num, 1,MPI_INTEGER
     1     ,masterid,comm,mpinfo)
      if (.not. parallel%iammaster) then
         allocate (p_pot%nlocp (1:p_pot%type_num))
         allocate (p_pot%loc (1:p_pot%type_num))
         allocate(clust%natmi(clust%type_num))
      endif
      call MPI_BCAST(p_pot%nlocp, p_pot%type_num,MPI_INTEGER
     1     ,masterid,comm,mpinfo)
      call MPI_BCAST(p_pot%loc, p_pot%type_num,MPI_INTEGER
     1     ,masterid,comm,mpinfo)
      call MPI_BCAST(clust%natmi, clust%type_num,MPI_INTEGER
     1     ,masterid,comm,mpinfo)
      call MPI_BCAST(elec_st%chi, elec_st%nrep**2,MPI_INTEGER
     1     ,masterid,comm,mpinfo)
      call MPI_BCAST(rsymm%trans,3*3*rsymm%ntrans
     1               ,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(rsymm%tnp,3*rsymm%ntrans
     1               ,MPI_DOUBLE_PRECISION,masterid,comm,mpinfo)
      call MPI_BCAST(rsymm%chi,rsymm%ntrans*rsymm%ntrans
     1               ,MPI_INTEGER,masterid,comm,mpinfo)
#endif

      allocate(elec_st%eig(elec_st%nrep,elec_st%nspin))
      nmax = (solver%winsize + solver%nadd + elec_st%nstate)
     1     *elec_st%nrep
      allocate(elec_st%irep(nmax,elec_st%nspin))
c     create eigenstate structures with empty arrays
      do ii = 1, elec_st%nspin
         do irp = 1, elec_st%nrep
            allocate(elec_st%eig(irp,ii)%en(1))
            allocate(elec_st%eig(irp,ii)%occ(1))
            elec_st%eig(irp,ii)%occ = zero
            allocate(elec_st%eig(irp,ii)%wf(1,1))
            call destroy_eigenstate(elec_st%eig(irp,ii))
         enddo
      enddo
      elec_st%eig(:,:)%nn = -1
      elec_st%eig(:,:)%nec = -1

      end subroutine init_var
c     ===============================================================
