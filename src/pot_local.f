c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Computes the local potential vscr on the real space using
c     fast fourier transforms.
c
c     Adapted from plane-wave programs written by S. Froyen and
c     J. L. Martins.
c
c     ---------------------------------------------------------------
      subroutine pot_local(pbc,ipr,veff)

      use constants
      use pbc_module
      implicit none
c
c     Input/Output variables:
c
c     pbc related data
      type (pbc_data), intent(inout) :: pbc
c     local potential in reciprocal space
      complex(dpc), dimension (pbc%ng), intent(in) :: veff
c     printout flag
      integer, intent(in) :: ipr
c
c     Work variables:
c
      integer id,ntot,i,j,k,ijk2,k1,k2,k3,kd
      integer iadd,ierr

      real(dp) :: dmax,dmin,cmax,abschd,vreal,vimag
      complex(dpc) :: phase

      real(dp), parameter :: small = 1.0d-9

c     ---------------------------------------------------------------

c     print out local potential if requested
      if (ipr .ge. 6) write(7,10) 
     1     (real(veff(i)),aimag(veff(i)),i=1,pbc%ng)

      ntot = pbc%n1 * pbc%n2 * pbc%n3
      write(7,14) pbc%n1,pbc%n2,pbc%n3
c
c     Initialize charge density array and enter symmetrized
c     charge. Must add phase from displacement of FFT origin:
c     coordinates of corner of FFT box are not (0,0,0).
c
      pbc%vscr4 = zero

      do i=1,pbc%ng
         k1 = 1 + pbc%kgv(1,i)
         if (k1 .le. 0) k1 = k1 + pbc%n1
         k2 = 1 + pbc%kgv(2,i)
         if (k2 .le. 0) k2 = k2 + pbc%n2
         k3 = 1 + pbc%kgv(3,i)
         if (k3 .le. 0) k3 = k3 + pbc%n3
         call get_address(pbc,k1,k2,k3,iadd,phase)
         pbc%vscr4(iadd) = pbc%vscr4(iadd)  + veff(i) * phase
      enddo

c     fourier transform to real space
      call cfftw (pbc%vscr4(1),pbc%n1,pbc%n2,pbc%n3,-1)

      dmax = real( pbc%vscr4(1) ,dp)
      dmin = dmax
      cmax = zero
      ierr = 0
      do k=1,pbc%n3
         do j=1,pbc%n2
            do i=1,pbc%n1
               ijk2 = ((k-1)*pbc%n2 + j-1)*pbc%n1 + i
               pbc%vscr2(ijk2) = real( pbc%vscr4(ijk2) ,dp)
               if (pbc%vscr2(ijk2) .gt. dmax) dmax=pbc%vscr2(ijk2)
               if (pbc%vscr2(ijk2) .lt. dmin) dmin=pbc%vscr2(ijk2)
               abschd = abs( aimag( pbc%vscr4(ijk2) ) )
               if (abschd .gt. cmax) cmax=abschd
               if (abschd .gt. small) ierr=ierr+1
            enddo
         enddo
      enddo

      write(7,16) dmax,dmin
      if (ierr .ne. 0) write(7,18) cmax

 10   format(/,'  local potential in g-space',/,100(/,1x,20f6.2))
 14   format(/,'  in fft for local potential n = ',3i4)
 16   format(/,'  max and min of potential ',2f10.4)
 18   format(/,'  complex potential: max value:',e12.4)

      end subroutine pot_local
c     ===============================================================
