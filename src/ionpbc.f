c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Calculates the ionic local potential (vion) on the real space
c     mesh for a given ionic configuration (xatm,yatm,zatm).
c
c     WARNING: this subroutine is not fully parallelized!
c
c     ---------------------------------------------------------------
      subroutine ionpbc(clust,grid,pbc,parallel,vion_d,ipr)

      use constants
      use cluster_module
      use grid_module
      use pbc_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type(cluster), intent(in) :: clust
c     grid related data
      type(grid_data), intent(in) :: grid
c     pbc related data
      type(pbc_data), intent(inout) :: pbc
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel
c     local part of ionic potential
      real(dp), intent(inout) :: vion_d(parallel%mydim)
c     printout flag
      integer, intent(in) :: ipr
c
c     Work variables:
c
c     counters
      integer i, j, k, ijk, ii, ir
c     allocation check
      integer alcstat
c     work arrays for the master processor
      real(dp), allocatable :: vion(:)
      complex(dpc),dimension(:), allocatable :: vionz

c     ---------------------------------------------------------------

      if (parallel%iammaster) then

c     initialize potentials
         allocate(vion(grid%nwedge),stat=alcstat)
         call alccheck('vion',grid%nwedge,alcstat)
         vion = zero
         allocate(vionz(pbc%ng),stat=alcstat)
         call alccheck('vionz',pbc%ng,alcstat)

c     Define coordinates of atoms with respect to the corner of the
c     periodic cell. This is needed for the FFTs and structure factor.
         ii = 0
         do i = 1, clust%type_num
            do j = 1, clust%natmi(i)
               ii = ii + 1
               pbc%rat(1,j,i) = clust%xatm(ii) * twopi / pbc%box_size(1)
               pbc%rat(2,j,i) = clust%yatm(ii) * twopi / pbc%box_size(2)
               pbc%rat(3,j,i) = clust%zatm(ii)*  twopi / pbc%box_size(3)
            enddo
         enddo

c     obtain the local ionic potential on the reciprocal space mesh
         call v_first(pbc,clust%natmi,vionz)
         vionz(1) = cmplx(zero,zero,dp)

c     transfer the local ionic potential to the real space grid ...
         call pot_local(pbc,ipr,vionz)

c     ... and finally store it according to mapping
         do ir = 1, grid%nwedge
            i = grid%kx(ir)
            j = grid%ky(ir)
            k = grid%kz(ir)
            ijk = ((k-pbc%mz)*grid%n2 + j-pbc%my)*grid%n1 + i-pbc%mx+1
            ii = grid%indexw(i,j,k)
            vion(ii) = pbc%vscr2(ijk)*two
         enddo
         parallel%ftmp = vion
         deallocate(vionz, vion)
      endif                     ! parallel%iammaster

c     master PE distributes the ion potential
      call export_function(parallel,vion_d)

      end subroutine ionpbc
c     ===============================================================
