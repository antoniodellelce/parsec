c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Define "reciprocal space" information and constructs the
c     reciprocal space.
c
c     ---------------------------------------------------------------
      subroutine inipbc(grid,pbc,symm,ipr)

      use constants
      use grid_module
      use pbc_module
      use symmetry_module

      implicit none
c
c     Input/Output variables:
c
      type (grid_data), intent(inout) :: grid
      type (pbc_data), intent(inout) :: pbc 
      type (symmetry), intent(in) :: symm

      integer, intent(in) :: ipr
c
c     Work variables:
c
      integer ii, kmax(3)
      real(dp) :: htemp

c     ---------------------------------------------------------------

      write(7,*) 'Reciprocal Space Data:'
      write(7,*) '----------------------'

      do ii=1,3
         pbc%adot(ii) = pbc%box_size(ii)*pbc%box_size(ii)
         pbc%bvec(ii) = twopi/pbc%box_size(ii)
         pbc%bdot(ii) = pbc%bvec(ii)*pbc%bvec(ii)
      enddo

      grid%n1 = 2*int(pbc%box_size(1)/(two*grid%stepin))
      grid%n2 = 2*int(pbc%box_size(2)/(two*grid%stepin))
      grid%n3 = 2*int(pbc%box_size(3)/(two*grid%stepin))

      pbc%n1 = grid%n1
      pbc%n2 = grid%n2
      pbc%n3 = grid%n3

      htemp = grid%stepin

      grid%step(1) = pbc%box_size(1)/(real(grid%n1,dp))
      grid%step(2) = pbc%box_size(2)/(real(grid%n2,dp))
      grid%step(3) = pbc%box_size(3)/(real(grid%n3,dp))
      grid%hcub =grid%step(1)*grid%step(2)*grid%step(3)
      grid%hcub2 = 2/grid%hcub
c
c     kmax = n/2 if n is even; kmax = (n+1)/2 otherwise
c
      kmax(1) = nint( (real(grid%n1,dp) + one/four)/ two )
      kmax(2) = nint( (real(grid%n2,dp) + one/four)/ two )
      kmax(3) = nint( (real(grid%n3,dp) + one/four)/ two )

      pbc%vcell = pbc%box_size(1)*pbc%box_size(2)*pbc%box_size(3)

c     rescale alpha energy
      pbc%ealpha = pbc%ealpha/pbc%vcell

c     construct reciprocal space
      call g_space(pbc,symm,kmax,ipr)

      write(7,*)
      write(7,21) htemp
      write(7,22) grid%step
      write(7,24) grid%n1,grid%n2,grid%n3
      write(7,*)

 21   format(' Input grid spacing:', 1x, f6.3, 1x, 'bohr')
 22   format(' Final grid spacings:', 1x, 3(f6.3, 1x), 'bohr')
 24   format(' Number of grid points along each direction:',3(1x,i3))

      end subroutine inipbc
c     ===============================================================
c
c     For input coordinates (i,j,k) in FFT grid, calculate the address
c     and phase associated to these coordinates. Input coordinates are
c     of the form 0 < i =< n1, and similarly for j, k. Output phase is
c     exp( -i G . V ) where V is the shift vector: coordinates of the
c     corner point in the real-space grid.

c     ---------------------------------------------------------------
      subroutine get_address(pbc,i,j,k,iadd,phase)
      use constants
      use pbc_module
      implicit none
c
c     Input/Output variables:
c
c     pbc related data
      type (pbc_data), intent(inout) :: pbc
c     coordinates of g-vector in FFT grid
      integer, intent(in) :: i, j, k
c     address of g-vector in FFT mesh
      integer, intent(out) :: iadd
c     phase associated to this g-vector
      complex(dpc), intent(out) :: phase
c
c     Work variables:
c
c     exponent of phase
      real(dp) :: phi
c     coordinates of g-vector
      integer :: gv(3)
c     ---------------------------------------------------------------
      gv(1) = i - 1 - pbc%n1
      gv(2) = j - 1 - pbc%n2
      gv(3) = k - 1 - pbc%n3
      if (gv(1) < pbc%mx) gv(1) = gv(1) + pbc%n1
      if (gv(2) < pbc%my) gv(2) = gv(2) + pbc%n2
      if (gv(3) < pbc%mz) gv(3) = gv(3) + pbc%n3
      iadd = ( (k-1)*pbc%n2 + j-1 )*pbc%n1 + i
      phi = dot_product(gv,pbc%shift)
      phase = cmplx(cos(phi),sin(phi))

      end subroutine get_address

c     ===============================================================
