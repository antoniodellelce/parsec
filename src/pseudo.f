c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Sets up the pseudopotentials and computes associated quantities.
c
c     ---------------------------------------------------------------
      subroutine pseudo(clust,p_pot,nloc_p_pot,nstate,elval1
     1     ,icorr,oldformat,ierr)

      use constants
      use cluster_module
      use pseudo_potential_module
      use non_local_psp_module
      implicit none
c
c     Input/Output variables:
c
      type (cluster), intent(inout) :: clust
      type (pseudo_potential), intent(inout) :: p_pot
      type(nonloc_pseudo_potential), intent (out) :: nloc_p_pot

c     User specified number of states
      integer, intent(in) :: nstate

c     two different ways for calculating the # of valence electrons
c     for a neutral system, from # of electrons in the core:
c     elval1 - according to # in ptable
c     elval2 - see work variables
      real(dp), intent(out) :: elval1

c     correlation type
      character(len=2), intent(in) :: icorr

c     oldformat = .true.   --> read *_POTR.DAT files (no header info)
c     oldformat = .false.  --> read *_POTRE.DAT files (with header info)
      logical, intent(in) :: oldformat

c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     order for the pseudopotential derivative (number of points
c     on each side used); maximum value is mor = 10
      integer, parameter :: mor = 9

c     Lower boundary index for the potential and wavefunction
c     for the convenience of derivative calculations near the origin
      integer mxp

c     number of different atom types
      integer naty

c     number of components in the pseudopotential (per given atom)
c     read from pseudopotential file
      integer lll

c     indices of pseudopotential components used when
c     reading the pseudopotential file
      integer l1, l2, l3, l4

c     maximum number of grid points in given pseudopotential files
c     maxpot = max (p_pot%ns)
      integer maxpot

c     number of spin-polarized pseudopotential components
      integer nlso

c     counters, temporary variables
      integer i, iorb, j, ity, i0, j0, kk, k, nold
      integer dummi1,dummi2,dummi3
      integer npoint
      real(dp) :: a, b, temp0, fspd, delr
      integer, allocatable :: nloc(:)

c     temporary core cutoff parameters
      real(dp) :: rc, tcore,cfac,rcfac

c     from # electrons in each pseudopotential component (user input)
      real(dp) :: elval2

c     temporary storage of the atomic mass of each atom type
      real(dp) :: am

c     list of coefficients for numerical first derivative
      real(dp) ::  coe(-mor:mor)

c     <psi_l|psi_l> - check that the input pseudowavefunction is
c     properly normalized on the radial grid
      real(dp) :: psinorm(4)

c     pseudopotential data read from pseudopotential file(s):
c     vspd - pseudopotential (up to four components)
c     wfspd - pseudo wavefunction (up to four components)
      real(dp), dimension (:,:,:), allocatable :: vspd
      real(dp), dimension (:,:,:), allocatable :: wfspd

c     tolerance for identifying ekbi as zero)
      real(dp), parameter :: small = 1.0d-16

c     occupancy of orbitals (from pseudopotential file)
      real(dp) :: eleatm_psp(4)

c     extension that turns atom name into the pseudopotential file name
      character(len=90), dimension(:), allocatable :: fname

c     atom names
      character(len=2) :: name(1:clust%type_num)

c     strings read from pseudopotential file
      character(len=2) :: nameat,icorrt,icorrt2
      character(len=3) :: irel
      character(len=4) :: nicore
      character(len=200) :: linestr

c     ---------------------------------------------------------------

      naty = p_pot%type_num
      name = clust%name

      write(7,*)
      write(7,*)
      write(7,*) 'Pseudopotential messages'
      write(7,*) '------------------------'
      write(7,*)
c     initialize counters
c     for # of valence electrons
      elval1 = zero
      elval2 = zero
c     for number of atoms
      k=1

      allocate(fname(naty))

c     go over all atom types
      do ity = 1, naty  

c     get number of valence electrons and atomic mass from periodic
c     table
         call ptable(name(ity),temp0,am,ierr)
         if (oldformat) p_pot%zion(ity) = temp0
         if (ierr .ne. 0) return
c     spread atomic mass value to all atoms of this type
         do kk=k,k+clust%natmi(ity)-1
            clust%amass(kk) = am*1836.d0
         enddo
         k = k + clust%natmi(ity)
c     open potential & wave function input files 
         if (oldformat) then
            fname(ity) = trim(name(ity))//'_POTR.DAT'
         else
            fname(ity) = trim(name(ity))//'_POTRE.DAT'
         endif

         open(unit=20,file=trim(fname(ity)),
     1        status='old',form='formatted', err=50)
c
c     Read in  Pseudopotential data
c
c     pseudopotential parameters read from first line of
c     pseudopotential file:
c     (icore = 0 - no core, = 1 - core correction)
         if (oldformat) then
            read(20,*) p_pot%ns(ity), lll, p_pot%icore(ity)

            if (p_pot%loc(ity) .gt. p_pot%nlocp(ity)) then
               write(7,*)
               write(7,*) 'ERROR: problem with atom type', ity
               write(7,*) 'local pseudopot. beyond # of components'
               write(7,*) p_pot%loc(ity),p_pot%nlocp(ity)
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif

            if (lll .ne. p_pot%nlocp(ity)) then
               write(7,*) 'ERROR: # of pseudopot. components from user'
               write(7,*) 'incompatible with # from pseudopot. file'
               write(7,*) ' atom type ',ity,lll,p_pot%nlocp(ity)
               write(7,*) 'STOP in pseudo '
               ierr = 1
               return
            endif

            if (lll .gt. 4) then 
               write(7,*) 'ERROR: More than 4 pseudopot. components'
               write(7,*) 'Do you really need to go beyond f orbitals?'
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif
         
            if (lll .lt. 1) then 
               write(7,*) 'ERROR: less than 1 pseudopot. components'
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif
         else                   !  oldformat
            read(20,'(1x,a2,1x,a2,1x,a3,1x,a4)') 
     1           nameat, icorrt, irel, nicore
            read(20,'(a200)',end=68) linestr
 68         continue
            read(20,'(a200)',end=69) linestr
 69         continue
            nold = p_pot%nlocp(ity)
            read(20,*) p_pot%nlocp(ity), i, p_pot%ns(ity)

            if ((nold .ne. 0) .and. (nold .ne. p_pot%nlocp(ity))) then
               write(7,*)
               write(7,*) 'ERROR: problem with atom type', ity
               write(7,*) 'number of angular momentum components on '
     1              ,'input different from data on pseudopotential '
     2              ,'file ',nold,p_pot%nlocp(ity)
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif
            if (p_pot%loc(ity) .gt. p_pot%nlocp(ity)) then
               write(7,*)
               write(7,*) 'ERROR: problem with atom type', ity
               write(7,*) 'local pseudopot. beyond # of components: '
     1              ,p_pot%loc(ity),p_pot%nlocp(ity)
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif
            if (p_pot%nlocp(ity) .gt. 4) then
               write(7,*)'ERROR: More than 4 pseudopot. components'
               write(7,*)
     1              'Do you really need to go beyond f orbitals?'
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif
         endif                  !  oldformat

c     the increase by one is in order to "push in" the point at r=0 -
c     see below
         p_pot%ns(ity) = p_pot%ns(ity)+1
         close(20)
      enddo                     ! ity = 1, naty (first loop)

c     creating the 'empty' non_local_psp module
      kk = maxval(p_pot%nlocp) - 1
      call create_nonloc_pseudo_pot (kk,clust%atom_num,nloc_p_pot) 

c     getting the maximum number of lines in the given
c     pseudopotential files
      maxpot = maxval(p_pot%ns)
      call pseudo_potential_set_mxpot (maxpot,p_pot)

c     allocate working arrays
      allocate (vspd (-p_pot%norder:maxpot, p_pot%type_num, 4))
      allocate (wfspd (-p_pot%norder:maxpot, p_pot%type_num, 4))
      vspd = zero
      wfspd = zero

      do ity = 1, naty
         open(unit=20,file=trim(fname(ity)),
     1        status='old',form='formatted', err=50)
         write(7,55) ity, trim(fname(ity))
 55      format(1x,'Pseudopotential file for atom type',1x,i2,1x,
     1        'is',1x,a)

         if (oldformat) then
            read(20,*) dummi1,dummi2 ,dummi3

c     first, read in s,p,d,f pseudopotentials
c     then, read in s,p,d pseudo wave functions
c     all reading starts from j=2 to leave room for r=0 at j=1
            allocate(nloc(p_pot%nlocp(ity)))
            read(20,*) (nloc(j),j=1,p_pot%nlocp(ity))
            do j = 2, p_pot%ns(ity)
               read(20,*) p_pot%rs(j,ity),(vspd(j,ity,nloc(i)),i=1
     1              ,p_pot%nlocp(ity))
            enddo
            do  j = 2,p_pot%ns(ity)
               read(20,*) p_pot%rs(j,ity),(wfspd(j,ity,nloc(i)),i=1
     1              ,p_pot%nlocp(ity))
            enddo
            deallocate(nloc)
c
c     Calculate the radius step parameters - the pseudopotential is
c     given on a log grid !
c             r(i) = par_a*(exp(par_b*(i-1)) - 1)
c     The calculation is based on the results at two grid points that
c     are quite far away, for accuracy.
c
            i0 = 101
            j0 = 2*(i0-1) + 1
            if(j0.gt.p_pot%ns(ity)) then
               write(7,*) 
     1              'ERROR: choose smaller i0 to get par_a & par_b'
               write(7,*) 'STOP in pseudo'
               ierr = 1
               return
            endif
            p_pot%par_a(ity) = p_pot%rs(i0,ity)**2/
     1           (p_pot%rs(j0,ity)-two*p_pot%rs(i0,ity))
            p_pot%par_b(ity) = log(p_pot%rs(j0,ity)/
     1           p_pot%par_a(ity) + one)/real(j0-1,dp)

            write(7,*)
            write(7,*) 
     1           'logarithmic parameters of pseudopot. radial grid'
            write(7,*) 'r(i) = a*[Exp(b(i-1)) - 1)]'
            write(7,93) name(ity), p_pot%par_a(ity), p_pot%par_b(ity)
 93         format(1x,a2,':',3x,'a =',1x,e12.6,1x,' b =',1x,e12.6)
c
c     Read in the Partial-Core Charge Density, if present.
c
            do j=2,p_pot%ns(ity)
               p_pot%denc(j,ity) = zero
            enddo
            if (p_pot%icore(ity).gt.0) then
               write(7,'(/,a,/)') 'NOTICE: Using Core-Correction !'
               do j=2,p_pot%ns(ity)
                  read(20,*,END=80) p_pot%rs(j,ity), p_pot%denc(j,ity)
               enddo
 80            continue
            else
               write(7,'(/,a,/)') 'NOTICE: No Core-Correction !'
            endif
         else                   ! oldoformat
            read(20,'(1x,a2,1x,a2,1x,a3,1x,a4)') 
     1           nameat, icorrt, irel, nicore
            write(7,82) nameat,icorrt,irel,nicore
 82         format(/,a2,2x,a2,2x,a3,2x,a4)
            read(20,'(a200)',end=84) linestr
 84         continue
            write(7,*) trim(linestr)
            read(20,'(a200)',end=86) linestr
 86         continue
            write(7,'(a,/)') trim(linestr)

            if (trim(nicore).eq.'nc') then
               read(20,*) p_pot%nlocp(ity), nlso, i,
     1              p_pot%par_a(ity),p_pot%par_b(ity), p_pot%zion(ity)
            else
               read(20,*) p_pot%nlocp(ity), nlso, i,
     1              p_pot%par_a(ity),p_pot%par_b(ity), p_pot%zion(ity),
     2              cfac,rcfac
            endif

            if (icorrt .eq. 'ca') icorrt2 = 'CA'
            if (icorrt .eq. 'xa') icorrt2 = 'XA'
            if (icorrt .eq. 'wi') icorrt2 = 'WI'
            if (icorrt .eq. 'hl') icorrt2 = 'HL'
            if (icorrt .eq. 'pb') icorrt2 = 'PB'
            if (icorrt .eq. 'CA') icorrt2 = 'ca'
            if (icorrt .eq. 'XA') icorrt2 = 'xz'
            if (icorrt .eq. 'WI') icorrt2 = 'wi'
            if (icorrt .eq. 'HL') icorrt2 = 'hl'
            if (icorrt .eq. 'PB') icorrt2 = 'pb'
            if ((icorrt .ne. icorr).and.(icorrt2 .ne. icorr))
     1           write(7,56)
 56         format('  *** warning in pseudo   correlation potential '
     1           ,'does not match')

            write(7,'(a,g10.4)') ' ion charge = ',p_pot%zion(ity)
            write(7,*) 
     1           'logarithmic parameters of pseudopot. radial grid'
            write(7,*) 'r(i) = a*[Exp(b(i-1)) - 1)]'
            write(7,58) name(ity), p_pot%par_a(ity), p_pot%par_b(ity)
     1           ,p_pot%ns(ity)
 58         format(1x,a2,':',3x,'a =',1x,e12.6,1x,' b =',1x,e12.6,
     1           '    nr = ',i8)

            if (irel.eq.'rel')
     1           write(7,*) ' RELATIVISTIC POTENTIALS : '
     2           ,'IGNORING SPIN-ORBIT PSEUDOPOTENTIAL COMPONENT'
            if (irel.eq.'isp') write(7,*)
     1           ' SPIN POLARIZED POTENTIALS : '
     2           ,'IGNORING SECOND PSEUDOPOTENTIAL COMPONENT'

            if (trim(nicore).eq.'nc') then
               write(7,'(/,a,/)') 'NOTICE: No Core-Correction !'
            else
               write(7,'(/,a)') 'NOTICE: Using Core-Correction !'
               write(7,'(a,f10.6,a,f10.6,/)') ' cfac = ',cfac
     1              ,' rcfac = ',rcfac
            endif

c     read in s,p,d,f pseudopotentials
c     all reading starts from j=2 to leave room for r=0 at j=1
            read(20,*)
            read(20,*) (p_pot%rs(j,ity),j=2,p_pot%ns(ity))
            do i = 1,p_pot%nlocp(ity)
               read(20,*)
               read(20,*) i0
               if (i0 .gt. 4) then
                  write(7,*)'ERROR: More than 4 pseudopot. components'
                  write(7,*)
     1                 'Do you really need to go beyond f orbitals?'
                  write(7,*) 'STOP in pseudo'
                  ierr = 1
                  return
               endif
               read(20,*) (vspd(j,ity,i0+1),j=2,p_pot%ns(ity))

               do j=2,p_pot%ns(ity)
                  vspd(j,ity,i0+1) = vspd(j,ity,i0+1)/p_pot%rs(j,ity)
               enddo

            enddo
c     ignore second component of relativistic/spin polarized
c     potentials
            do i = 1,nlso
               read(20,*)
               read(20,*) j
               read(20,*) (temp0,j=2,p_pot%ns(ity))
            enddo
c
c     Read in the Partial-Core Charge Density, if present.
c
            read(20,*)
            p_pot%denc(:,ity) = zero
            p_pot%ddenc(:,ity) = zero
            if (trim(nicore).eq.'nc') then
               read(20,*) (temp0,j=2,p_pot%ns(ity))
               p_pot%icore(ity) = 0
            else
               read(20,*) (p_pot%denc(j,ity),j=2,p_pot%ns(ity))
               do j=2,p_pot%ns(ity)
                  p_pot%denc(j,ity) = p_pot%denc(j,ity)/(four*pi
     1                 *p_pot%rs(j,ity)*p_pot%rs(j,ity))
               enddo
               p_pot%icore(ity) = 1
            endif
c
c     Ignore valence charge density.
c
            read(20,*)
            read(20,*) (temp0,j=2,p_pot%ns(ity))
c
c     read in s,p,d pseudo wave functions
c     all reading starts from j=2 to leave room for r=0 at j=1
c     for cutoff radius, choose the maximum cutoff in file
c
            tcore = zero
            do i = 1,p_pot%nlocp(ity)
               read(20,*)
               read(20,*) i0,eleatm_psp(i),rc
               read(20,*) (wfspd(j,ity,i0+1),j=2,p_pot%ns(ity))
               if (tcore .lt. rc) tcore = rc
            enddo

c     if occupancy of orbitals was already read from parsec.in, use
c     that and ignore the occupancy array from pseudopotential file
            if (sum(p_pot%eleatm(ity,:)) .eq. zero) then
               p_pot%eleatm(ity,1:p_pot%nlocp(ity)) =
     1              eleatm_psp(1:p_pot%nlocp(ity))
            endif
c
c     Move core radius to the next grid point.
c
            do i = 2,p_pot%ns(ity)
               if (p_pot%rs(i,ity) .gt. tcore) exit
            enddo
            tcore = p_pot%rs(i,ity)
            p_pot%rcore(ity) = tcore
         endif                  !  oldformat

         write(7,'(a,/,4f8.3,/)') ' Occupancy of orbitals : '
     1        ,p_pot%eleatm(ity,1:p_pot%nlocp(ity))

         write(7,14) p_pot%nlocp(ity), p_pot%loc(ity)-1,
     1        p_pot%rcore(ity)
 14      format(' # of pseudopot= ', i1, ',', 
     1        ' Local component is l = ',i1,',',' Core radius= ',f5.2)
         write(7,*) 

         close(unit=20)
c
c     Filling in the value at r<=0 for the pseudopotentials and wave
c     functions. Value at r=0 NOT provided by pseudopotential file!
c     For negative r, set rs arbitrarily be j-1 for preventing
c     division by zero. This only affects several points near the
c     atom center, which is a negligible effect.
c
         do j = -p_pot%norder,1
            p_pot%rs(j,ity) = real(j-1,dp)
            vspd(j,ity,:) = vspd(2,ity,:)
            wfspd(j,ity,:) = zero
            p_pot%denc(j,ity) = p_pot%denc(2,ity)
         enddo
c
c     Calculate number of valence electrons in two ways - once based
c     on the number of valence electrons found in ptable, and once
c     based on the number of electrons per pseudopotential orbital
c     provided by the user.
c
         elval1 = elval1 + real(clust%natmi(ity),dp)*p_pot%zion(ity)
         do j = 1, p_pot%nlocp(ity)
            elval2 = elval2 + real(clust%natmi(ity),dp)*
     1           real(p_pot%eleatm(ity,j),dp)
         end do
      enddo                     !  ity = 1, naty (second loop)

c     the two values for number of valence electrons must agree!
      if (elval1 .ne. elval2) then
         write(7,*) 'ERROR: # of electrons from pseudopot.'
         write(7,*) 'incompatible with # from ptable'
         write(7,*) 'STOP in pseudo'
         ierr = 1
         return
      endif
c
c     Make sure the user specified enough states to contain all electrons
c     in the system + two empty states to facilitate convergence.
c
      if (real(nstate,dp) .lt. elval1*0.5d0+2.d0) then
         write(7,*) 'ERROR: Increase number of states to at least',
     1        idint(elval1*0.5d0+2.d0) + 1
         write(7,*) 'Stop in pseudo'
         ierr = 1
         return
      endif
c
c     Change non-local components of the pseudopotential into the
c     difference between them and the local component, i.e., give
c     pseudopotentials to the main program already in Kleinman-
c     Bylander form.
c
      do ity = 1, naty
         do j0 = 1, p_pot%nlocp(ity)
            if (j0 .ne. p_pot%loc(ity)) then
               do i0 = -p_pot%norder,p_pot%ns(ity)
                  vspd(i0,ity,j0) = 
     1                 vspd(i0,ity,j0) - vspd(i0,ity,p_pot%loc(ity))
               enddo 
            endif
         enddo 
      enddo 
c
c     Calculate the derivative of the ionic (=local) pseudopotential,
c     wfspd*vspd/r, its derivative, and the derivative of the 
c     core charge density.
c
c     Comment: the wavefunctions and pseudopotentials are specified
c     on a logarithmic grid, c but the numerical derivative assumes a
c     uniform grid, therefore: d/dr = di/dr*d/di. 
c     The term 1/(b*(rs(j,ity)+a)) is di/dr, hence its appearance
c     wherever a derivative is taken.
c
c     Get coefficients for numerical first derivative based on the 
c     order specified.
      write(7,*) 'Using order',2*mor,'for calculating dV/dr in pseudo' 
      call fornberg(1,mor,coe,ierr)
      if (ierr .ne. 0) then
         write(7,*) ' ERROR: Wrong parameter in call of Fornberg'
         write(7,*) ' STOP in pseudo'
         return
      endif

c     for all atom types...
      do ity = 1, naty
c
c     temporary variables 
         npoint = p_pot%ns(ity)
         a = p_pot%par_a(ity)
         b = p_pot%par_b(ity)
c
c     for all pseudopotential components...
         do i0 = 1, p_pot%nlocp(ity)
c
c     move the local component into the array vion and calculate its
c     derivative
            if(i0 .eq. p_pot%loc(ity))  then
               do  j =  -p_pot%norder,npoint
                  p_pot%vion(j,ity) = vspd(j,ity,p_pot%loc(ity)) 
               enddo
    
               do j = mor, npoint - mor
                  temp0 = zero
                  do kk = -mor,mor,1
                     temp0 = temp0 + coe(kk)*p_pot%vion(j+kk,ity)
                  enddo
                  p_pot%dvion(j,ity) = temp0/(b*(p_pot%rs(j,ity)+a))
               enddo
 
               do j = -p_pot%norder,mor-1
                  p_pot%dvion(j,ity) = zero
               enddo

c     for the last few points, vion is just the coloumb potential
c     (for rs is beyond the core, so the derivative can be taken
c     analytically. The factor of 2 is for conversion from Hartree to
c     Rydberg
               do j = npoint - mor + 1, npoint
                  p_pot%dvion(j,ity) = two*p_pot%zion(ity)/
     1                 (p_pot%rs(j,ity)*p_pot%rs(j,ity))
               enddo

c     set first few points where the derivative is mostly numerical
c     noise anyway to zero
               do j=-p_pot%norder,npoint
                  if(p_pot%rs(j,ity).le.3.d-4) p_pot%dvion(j,ity) = zero
               enddo

            else                !  this is local component
c
c     set the first mor-1 elements of wfspd*vspd/r be zero	   
c     set the derivative of the first mor-1 point to be zero
c
               do j = -p_pot%norder,mor-1
                  p_pot%vw(j,ity,i0) = zero 
                  p_pot%dvw(j,ity,i0) = zero
               enddo

               do j = mor, npoint
                  p_pot%vw(j,ity,i0) = vspd(j,ity,i0)*
     1                 wfspd(j,ity,i0)/p_pot%rs(j,ity)
               enddo

               do j = mor, npoint-mor
                  temp0 = zero
                  do kk = -mor,mor,1
                     temp0 = temp0 + coe(kk)*p_pot%vw(j+kk,ity,i0)
                  enddo
                  p_pot%dvw(j,ity,i0) = temp0/(b*(p_pot%rs(j,ity) +a))
               enddo
c     The last few one may be set to zero, because rs is large
c     enough to be outside the non-local core.
               do j = npoint - mor + 1, npoint
                  p_pot%dvw(j,ity,i0) = zero
               enddo
c
               do j = -p_pot%norder,mor
                  p_pot%vw(j,ity,i0) = p_pot%vw(mor*2,ity,i0)
               enddo
               do j = -p_pot%norder,2*mor
                  p_pot%dvw(j,ity,i0) = p_pot%dvw(mor*2,ity,i0)
               enddo 
            endif               ! this is local component
         enddo                  ! i0 = 1, p_pot%nlocp(ity)
c
c     calculate the radial derivative of the core charge density, if
c     exists.
c
         if (p_pot%icore(ity).gt.0) then
            do j = mor, npoint - mor
               temp0 = zero
               do kk = -mor,mor,1
                  temp0 = temp0 + coe(kk)*p_pot%denc(j+kk,ity)
               enddo
               p_pot%ddenc(j,ity) = temp0/(b*(p_pot%rs(j,ity) + a))
            enddo
            do j = -p_pot%norder,mor-1
               p_pot%ddenc(j,ity) = zero
            enddo
            do j = npoint - mor + 1, npoint
               p_pot%ddenc(j,ity) = zero
            enddo
         endif

      enddo                     ! ity = 1, naty (third loop)
c
c     Calculate the VOLUME atomic charge density and its derivative,
c     for each atom type, on the logarithmic pseudopotential grid.
c
c     go over all atom types
      do ity = 1, naty
         npoint = p_pot%ns(ity)

c     get the total RADIAL charge density for each atom type, by
c     summing over the number of atoms per orbital, eleatm(ity,iorb),
c     times the square of that orbital's pseudo wavefunction,
c     wfspd(i,ity,iorb). Then divide by 4*pi*r^2 to convert the
c     RADIAL density into a VOLUME one.

         do i = 2, npoint
            temp0=0.d0
            do iorb = 1, p_pot%nlocp(ity)
               temp0 = temp0+
     1              p_pot%eleatm(ity,iorb)*wfspd(i,ity,iorb)**2
            enddo
            p_pot%rho_r(i,ity) = temp0/(four*pi*p_pot%rs(i,ity)*
     1           p_pot%rs(i,ity))
         enddo

c     set the "negative index" points (used for derivation near the
c     origin) to zero.

         do i = -p_pot%norder,1
            p_pot%rho_r(i,ity) = p_pot%rho_r(2,ity)
         enddo

c     Calculate the radial derivative of the atomic charge density
c     It is used for calculating forces.

         do i = -p_pot%norder, mor
            p_pot%drhodr(i,ity) = zero
         enddo
         do i = mor, npoint - mor
            temp0 = zero
            do kk = -mor,mor,1
               temp0 = temp0 + coe(kk)*p_pot%rho_r(i+kk,ity)
            enddo
            p_pot%drhodr(i,ity) = temp0/(p_pot%par_b(ity)*
     1           (p_pot%rs(i,ity)+p_pot%par_a(ity)))
         enddo
         do i = npoint - mor + 1, npoint, 1
            p_pot%drhodr(i,ity) = zero
         enddo
      enddo
c
c     Calculate (in ekbi) the Kleinman-Bylander normalization factor 
c     <psi|(V_l-V_locaa)l|psi> (this is the denominator of Eq. (3) in
c     Chelikowsky et al., PRL, 72, 1240 (1994)). As this is needed
c     for the non-local pseudopotential only, the results for the
c     local pseudopotential are junk and are never used.
c
c     Also calculate (in psinorm) the integral of psi* times psi over
c     the radial grid (in psinorm) in order to make sure it is close
c     to one, and write it out to parsec.out. This is for information
c     only - psinorm is not used anywhere else. 
c
      write(7,*)
      write(7,75)
      write(7,*)
 75   format(1x,'Normalization check and Kleinman-Bylander Integral')
      write(7,76) 
 76   format(33x,'s',17x,'p',17x,'d')

      do ity = 1,naty
         npoint = p_pot%ns(ity)
         do j0 = 1, p_pot%nlocp(ity)
            delr = p_pot%rs(2,ity) - p_pot%rs(1,ity)
            fspd = wfspd(1,ity,j0)*wfspd(1,ity,j0)
            psinorm(j0) = fspd*delr
            p_pot%ekbi(j0,ity) = vspd(1,ity,j0)*fspd*delr
            do j = 2,npoint-1
               delr = half*(p_pot%rs(j+1,ity)-p_pot%rs(j-1,ity))
               fspd = wfspd(j,ity,j0)*wfspd(j,ity,j0)
               psinorm(j0) =  psinorm(j0) + fspd*delr
               p_pot%ekbi(j0,ity) = p_pot%ekbi(j0,ity) + 
     1              vspd(j,ity,j0)*fspd*delr
            enddo
            delr = p_pot%rs(npoint,ity) - p_pot%rs(npoint-1,ity)
            fspd = wfspd(npoint,ity,j0)*wfspd(npoint,ity,j0)
            psinorm(j0) =  psinorm(j0) + fspd*delr
            p_pot%ekbi(j0,ity) = p_pot%ekbi(j0,ity) +
     1           vspd(npoint,ity,j0)*fspd*delr
         enddo
         write(7,77) name(ity), (psinorm(j), j=1,p_pot%nlocp(ity))
         write(7,78) name(ity), (p_pot%ekbi(j,ity),j=1,p_pot%nlocp(ity))
         write(7,*)
      enddo

 77   format(2x,a2,2x,' Normalization', 3(3x,f15.8))
 78   format(2x,a2,2x,' K-B Integral ', 3(3x,f15.8))
c
c     Because the Kleinman-Bylander normalization factor is eventually used
c     in a denominator, if it is close to zero its inverse will explode.
c     Therefore, check its value and it is too small for any non-local
c     component of any atom - print out an error message and quit.
c
      do ity  = 1, naty
         do j = 1, p_pot%nlocp(ity)
            if (j .ne. p_pot%loc(ity)) then
               if (abs(p_pot%ekbi(j,ity)).lt.small)  then
                  write(7,*) 'ERROR: 
     1                 The K-B normalization factor is zero!!'
                  write(7,79) j-1,ity,p_pot%ekbi(j,ity)
                  write(7,*) 'STOP in pseudo'
                  ierr = 1
                  return
               endif
            endif
         enddo
      enddo

 79   format(1x,'the K-B factor of L=',1x,i1,1x,'for atom type #',
     1     1x,i2,1x,'is',1x,f15.8)

c     deallocate the arrays
      if (allocated (wfspd)) deallocate (wfspd)
      if (allocated (vspd)) deallocate (vspd)
      if (allocated (fname)) deallocate(fname)
      return

c     print out error message and exit if files do not exist
 50   write(7,92) trim(fname(ity))
 92   format('ERROR: The file',1x,a,1x,' does not seem to exist',/)
      write(7,*) ' STOP in pseudo'
      ierr = 1

      end subroutine pseudo
c     ===============================================================
