c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine determines states occupations, based on an
c     input Fermi temperature, or based (if the input Fermi
c     temperature is negative) on a user provided file - occup.in.
c
c     ---------------------------------------------------------------
      subroutine flevel(elec_st,ierr)

      use constants
      use electronic_struct_module 
      implicit none
c
c     Input/Output variables:
c
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
 
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     useful parameters:
c     numerically negligible difference
      real(dp), parameter :: small=1.0d-12
c     value of beta|E-Ef| for which the occupation is exactly 1.0 or
c     0.0 given the numerical accuracy of the computation. 
      real(dp), parameter :: difmax = 35.0d0
c     temperature for which kT is 1 Rydberg
      real(dp), parameter :: tryd = one/tempkry
c     maximum number of iterations for Fermi level position before
c     giving up
      integer, parameter :: iterlim = 100

c     Fermi energy (in Rydbergs)
      real(dp) :: efermi
c     total number of electrons, per given guess of E_f
      real(dp) :: enum

c     total number of electrons, divided by two or as is, for
c     non-spin-polarized and spin-polarized computation, respectively.
      real(dp) :: xeletemp
c     distance of an eigenvalue from Fermi level guess, normalized by
c     temperature
      real(dp) :: etemp
c     Boltzmann and Fermi levels for an eigenvalue, per given guess
c     of the Fermi level
      real(dp) :: bol, fd
c     counters
      integer i,ii,j,jj,nn

c     (1/kT), in Rydberg
      real(dp) :: beta
c     upper and lower bounds for E_f during the iterative search 
      real(dp) :: efl, efh
c     spin identifier for printing
      character (len=2) :: idsp
c     eigenvalues (electron states) from all representations
      real(dp), dimension(:,:), allocatable :: eigen
c     occupation of each eigenvalue, all representations
      real(dp), dimension(:,:), allocatable :: occup
c     counters for representations
      integer irp,jrep(elec_st%nrep)
c     maximum number of computed eigenstates
      integer nmax
c     maximum number of computed eigenstates for each spin
      integer nstate(elec_st%nspin)

c     ---------------------------------------------------------------
c
c     Start by merging eigenvalues and occupancy factors from all
c     representations. They will be stored in arrays eigen, occup.
c
      do ii = 1, elec_st%nspin
         nstate(ii) = sum(elec_st%eig(:,ii)%nec)
      enddo
      nmax = maxval(nstate)

      allocate(eigen(nmax,elec_st%nspin))
      allocate(occup(nmax,elec_st%nspin))
      eigen = zero
      occup = zero

c     Get the list of lowest eigenvalues and occupancy factors by
c     going through all representations. array elec_st%eig%irep has
c     the ordering of states in ascending order of energy eigenvalue.

      do ii = 1, elec_st%nspin
         jrep = 0
         nstate(ii) = sum(elec_st%eig(:,ii)%nec)
         do jj = 1, nstate(ii)
            irp = elec_st%irep(jj,ii)
            jrep(irp) = jrep(irp) + 1
            nn = jrep(irp)
            eigen(jj,ii) = elec_st%eig(irp,ii)%en(nn)
            occup(jj,ii) = elec_st%eig(irp,ii)%occ(nn)
         enddo
      enddo
c
c     If tfermi is negative, occupations are pre-read from a file -
c     occup.in and stored in array occ_in. Transfer that to occupancy
c     array and skip calculation of Fermi energy
c
      if (elec_st%tfermi .lt. zero) then
         occup(1:elec_st%nstate,:) = 
     1        elec_st%occ_in(1:elec_st%nstate,:)
         goto 26
      endif
c
c     If not spin-polarized, the sum of all weighted occupations
c     should be equal to HALF the total number of electrons (occup=1
c     is two electrons). If spin-polarized, the sum of weighted
c     ocupations from the up and down channels is simply the total
c     number of electrons.
c
      xeletemp = elec_st%xele * real(elec_st%nspin,dp) / two

c     Find largest and smallest eigenvalues. Set them as lower and
c     upper bound for the Fermi energy.
      efl = minval(eigen)
      efh = maxval(eigen)

c     Compute beta = (1/kT), in Rydberg. initial guess is the highest
c     eigenvalue the Fermi energy.

      beta = tryd/elec_st%tfermi
      efermi = efh

c     Iterative search for the Fermi level position starts here. For
c     each guess of the Fermi level, the occupation of each
c     eigenlevel is computed  based on the Fermi-Dirac factor. 
c     The total number of electrons (let's call it n) is then the sum
c     of all occupations. If n is equal (within numerical accuracy)
c     to xeletemp, we're done. If not, our next guess for the Fermi
c     level is 0.5*(efl+efh), and efh or efl (depending on whether n
c     was larger or smaller than the true number of electrons,
c     respectively) is set to the old guess value. 

      do j=1,iterlim
         enum = zero
c     for each eigenvalue...
         do ii=1, elec_st%nspin
            do i=1, nstate(ii)
c     calculate distance from Fermi level guess, normalized by
c     temperature
               etemp = (eigen(i,ii)-efermi)*beta
c     if far below the Fermi level, the occupation is one
               if (etemp .lt. -difmax) then
                  enum = enum + one
c     if not too far above the Fermi level, calculate fractional
c     occupation
               elseif (etemp .lt. difmax) then
                  bol = exp(etemp)
                  fd = one/(bol+one)
                  enum = enum + fd
c     it far above the Fermi level, no contribution to occupation -
c     goto next eigvenvalue
               else
                  exit
               endif
            enddo   
c     If total number of electrons is right - we're done.
         enddo
         if (abs(xeletemp-enum) .lt. small) goto 20
c     update this guess for E_f as lower or upper bound for future
c     guesses, depending if the number of electrons was too low or
c     too high, respectively.
         if (enum .lt. xeletemp) then
            efl = efermi
         else
            efh = efermi
         endif
c     create new guess for E_f, as explained above
         efermi=(efh+efl)*half
c     iterative search for the Fermi level position ends here
      enddo

c     If Fermi level not found even after "iterlim" iterations,
c     complain and quit
      write(7,*)
      write(7,*) 'ERROR: Fermi level not found'
      write(7,62) efermi
      write(7,*) 'STOP in flevel'
      ierr = 1
      return
 20   continue

c     For the Fermi level found, determine occupancy array
      do ii=1, elec_st%nspin
         do i=1, nstate(ii)
            etemp = (eigen(i,ii)-efermi)*beta
            if (etemp .lt. -difmax) then
               occup(i,ii) = one
            elseif (etemp .lt. difmax) then
               occup(i,ii) = one/(exp(etemp)+one)
            else
               occup(i,ii) = zero
            endif

c     Find highest eigenvalue with non-zero occupation
            do j=nstate(ii),1,-1
               if (occup(j,ii) .ne. zero) then
                  elec_st%ifmax(ii) = j
                  exit
               endif
            enddo
         enddo
      enddo

 26   continue

c     finally, update occupancies in structure
      do ii = 1, elec_st%nspin
         jrep = 0
         do irp = 1, elec_st%nrep
            if (associated(elec_st%eig(irp,ii)%occ))
     1          elec_st%eig(irp,ii)%occ(:) = zero
         enddo
         do i = 1, nstate(ii)
            irp = elec_st%irep(i,ii)
            jrep(irp) = jrep(irp) + 1
            elec_st%eig(irp,ii)%occ(jrep(irp)) = occup(i,ii)
         enddo
      enddo

c     If computation not spin-polarized, the total number of
c     electrons is simply xele
      if (elec_st%nspin .eq. 1) then
         elec_st%totel(1)=elec_st%xele
c     If it is spin-polarized, find the number of electrons in each
c     spin channel
      else
         do ii=1, elec_st%nspin
            elec_st%totel(ii) = sum(occup(:,ii))
         enddo
c     report the number of electrons and the magnetic moment
         write(7,*)
         write(7,51) elec_st%totel(1), elec_st%totel(2)
         write(7,52) elec_st%totel(1)-elec_st%totel(2)
         write(7,*)
      endif 
 51   format(1x,'Electrons in up, down spins:',1x,f7.2,1x,f7.2)
 52   format(1x,'Net magnetic moment (in Bohr magnetons):',1x,f7.2)

c     report Fermi level, 
      if (elec_st%tfermi .ge. zero) write(7,61) efermi   
      do ii=1, elec_st%nspin
c     set spin identifier
         jj=ii-1+elec_st%nspin
         if (jj .eq. 1) idsp = '  '
         if (jj .eq. 2) idsp = 'up'
         if (jj .eq. 3) idsp = 'dn'
         write(7,*)
         write(7,64) idsp
         write(7,*)
         do i = 1,nstate(ii)
            write(7,63) i, eigen(i,ii), 
     1           rydberg*eigen(i,ii),occup(i,ii),elec_st%irep(i,ii)
         enddo
      enddo
      call myflush(7)

      deallocate(eigen, occup)

 61   format(1x,'Fermi level at',1x,f10.4,' [Ry]')
 62   format(1x,'Last Fermi level guess:',1x,f10.4,' [Ry]')
 63   format(i5,3x,f18.10,3x,f18.10,1x,f9.4,3x,i6)
 64   format(a2,' State   Eigenvalue [Ry]      Eigenvalue [eV]',    
     1     '    Occup.     Repr.')

      end subroutine flevel
c     ===============================================================
