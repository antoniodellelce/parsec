c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Calculates the local ionic potential in the g-vectors (i.e.
c     interpolates vloc and multiplies by the structure factor).
c     It also calculates the core charge density (dnc) this is going
c     to be used later when calculating the local force on ions.
c
c     Adapted from plane-wave programs written by S. Froyen and
c     J. L. Martins.
c
c     ---------------------------------------------------------------
      subroutine v_first(pbc,natom,vionz)

      use constants
      use pbc_module
      implicit none
c
c     Input/Output variables:
c
c     pbc related data
      type(pbc_data), intent(inout) :: pbc
c     number of atoms of each type
      integer, intent(in) :: natom(pbc%type_num)
c     ionic potential
      complex(dpc), dimension(pbc%ng), intent(out) :: vionz
c
c     Work variables:
c
      complex(dpc) :: strfac
      integer ik,ig,iat,j,n,nt,nql,ns
      real(dp) :: vcell,gmax,glmax,delql,fi
      real(dp) :: qj,xn,q2vn,q2vp,q2vm,vqj,dvqj,dcj

c     ---------------------------------------------------------------

c     scaling
      vcell = pbc%vcell
c     initializations
      ns = pbc%nstar
      vionz = cmplx(zero,zero,dp)

      do nt=1,pbc%type_num
c     check argument limits and compare with gmax
         delql = pbc%delq(nt)
         nql = pbc%nq(nt)
         gmax = sqrt(two*pbc%ek(ns))
         glmax = delql * real(nql-3,dp)
         if (glmax .lt. gmax) write(7,10) glmax
c
c     compute ionic local potential
c     ignore first point (G = 0)
c
         ig = pbc%mstar(1)
         do j=2,ns
            pbc%vql(nt,j) = zero
c
c     quadratic interpolation of q**2 * potential
c
            qj = sqrt(two*pbc%ek(j))
            xn = qj / delql + two
            n  = xn + half
            if (n .lt. nql) then
               if (n .le. 3) n = 4
               xn = xn - real(n,dp)

               q2vn = real((n-2)*(n-2),dp)*pbc%vloc(n,nt)
               q2vp = real((n-1)*(n-1),dp)*pbc%vloc(n+1,nt)
               q2vm = real((n-3)*(n-3),dp)*pbc%vloc(n-1,nt)
               vqj  = q2vn * (one-xn) * (one+xn)
     1              + half * (q2vp*(one+xn) - q2vm*(one-xn)) * xn
               vqj  = delql*delql * vqj / (vcell * two*pbc%ek(j))
               dvqj = -two * q2vn * xn
     1              + q2vp * (half+xn) - q2vm * (half-xn)
               dvqj = (delql*dvqj/(two*vcell*qj) - vqj)/(two*pbc%ek(j))

c     calculate structure factor by doing direct sum over atoms of
c     this type and over vectors in this star
               do ik = 1, pbc%mstar(j)
                  strfac = cmplx(zero,zero,dp)
                  ig = ig + 1
                  do iat = 1, natom(nt)
                     fi = dot_product(real(pbc%kgv(:,ig),dp)
     1                    ,pbc%rat(:,iat,nt))
                     strfac = strfac + cmplx(cos(fi),sin(fi))
                  enddo
                  vionz(ig) = vionz(ig) + vqj*conjg(strfac)
               enddo
               pbc%vql(nt,j) = vqj
            endif
         enddo

c     compute core charge
         do j=1,ns
            pbc%dnc(nt,j) = zero

c     interpolate vda
            qj = sqrt(two*pbc%ek(j))
            xn = qj/delql + two
            n = xn + half
            if (n .lt. nql) then
               xn = xn - real(n,dp)
               dcj = pbc%dcor(n,nt) * (one+xn) * (one-xn)
     1              + half * (pbc%dcor(n+1,nt)*(one+xn)
     2              -pbc%dcor(n-1,nt)*(one-xn)) * xn
               pbc%dnc(nt,j) = dcj
            endif
         enddo
c     end loop over atomic types
      enddo
 10   format('  *** warning  in v_first tape contains information',
     1     ' about local potential up to gmax = ',e10.3)

      end subroutine v_first
c     ===============================================================
