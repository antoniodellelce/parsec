c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c     This is a slightly modified version of EASYPBE, a driver for
c     the PBE subroutines, using simple inputs.
c     author: K. Burke, May 14, 1996.
c     Subroutines upgraded to the fortran 95 standard by M. Tiago,
c     February 19, 2005.
c
c     ---------------------------------------------------------------
      subroutine pbe(rho,agr,delgr,lap,expbe,vxuppbe,vxdnpbe,ecpbe,
     1     vcuppbe,vcdnpbe)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(inout) ::
     1     rho                  ! density
      real(dp), intent(in) ::
     1     agr,                 ! |grad rho|
     2     delgr,               ! (grad rho).(grad |grad rho|)
     3     lap                  ! grad^2 rho=Laplacian of rho

      real(dp), intent(out) ::
     1     expbe,               ! exchange energy density, so that
                                ! Ex=int d^3r rho(r) ex(r)
     2     vxuppbe,             ! up PBE exchange potential
     3     vxdnpbe,             ! down PBE exchange potential
     4     ecpbe,               ! PBE correlation energy density
     5     vcuppbe,             ! up PBE correlation potential
     6     vcdnpbe              ! down PBE correlation potential
c
c     Work variables:
c
      real(dp) :: dn,up,dnlap,uplap,agrdn,agrup,dvcdn,dvcup,
     1     delgrdn,delgrup,ec,exdnpbe,exuppbe,fk,g,h,rho2,rholap,
     2     rs,s,sk,u,uu,t,twoksg,v,vcdn,vcup,vv,ww,zet

      real(dp), parameter :: thrd2=two*third
      real(dp), parameter :: pi32=three*pi*pi
      real(dp), parameter ::
     1     alpha=1.91915829267751300662482032624669d0
c     ---------------------------------------------------------------

c
c     circumvent spin polarization
      up=rho/two
      dn=rho/two
      agrup=agr/two
      agrdn=agr/two
      delgrup=delgr/four
      delgrdn=delgr/four
      uplap=lap/two
      dnlap=lap/two
c
c     PBE exchange
c     use  Ex[up,dn]=0.5*(Ex[2*up]+Ex[2*dn]) (i.e., exact spin
c     -scaling)
c     do up exchange
c     fk=local Fermi wavevector for 2*up=(3 pi^2 (2up))^(1/3)
c     s=dimensionless density gradient=|grad rho|/ (2*fk*rho)_(rho=2*up)
c     u=delgrad/(rho^2*(2*fk)**3)_(rho=2*up)
c     v=Laplacian/(rho*(2*fk)**2)_(rho=2*up)
c
      rho2=two*up
      if(rho2.gt.1d-18)then
         fk=(pi32*rho2)**third
         s=two*agrup/(two*fk*rho2)
         u=four*delgrup/(rho2*rho2*(two*fk)**3)
         v=two*uplap/(rho2*four*fk*fk)
         call exchpbe(rho2,s,u,v,exuppbe,vxuppbe)
      else
         exuppbe=zero
         vxuppbe=zero
      endif
c     repeat for down
      rho2=two*dn
      if(rho2.gt.1d-18)then
         fk=(pi32*rho2)**third
         s=two*agrdn/(two*fk*rho2)
         u=four*delgrdn/(rho2*rho2*(two*fk)**3)
         v=two*dnlap/(rho2*four*fk*fk)
         call exchpbe(rho2,s,u,v,exdnpbe,vxdnpbe)
      else
         exdnpbe=zero
         vxdnpbe=zero
      endif
c     construct total density and contribution to ex
      rho=up+dn
      expbe=(exuppbe*up+exdnpbe*dn)/rho
c
c     Now do correlation
c         zet=(up-dn)/rho
c         g=phi(zeta)
c         rs=(3/(4pi*rho))^(1/3)=local Seitz radius=alpha/fk
c         sk=Ks=Thomas-Fermi screening wavevector=sqrt(4fk/pi)
c         twoksg=2*Ks*phi
c         t=correlation dimensionless gradient=|grad rho|/(2*Ks*phi*rho)
c         uu=delgrad/(rho^2*twoksg^3)
c         rholap=Laplacian
c         vv=Laplacian/(rho*twoksg^2)
c         ww=(|grad up|^2-|grad dn|^2-zet*|grad rho|^2)/(rho*twoksg)^2
c         ec=LSD correlation energy
c         vcup=LSD up correlation potential
c         vcdn=LSD down correlation potential
c         h=gradient correction to correlation energy
c         dvcup=gradient correction to up correlation potential
c         dvcdn=gradient correction to down correlation potential
c
      if(rho.lt.1.d-18)return
      zet=(up-dn)/rho
      g=((one+zet)**thrd2+(one-zet)**thrd2)/two
      fk=(pi32*rho)**third
      rs=alpha/fk
      sk=sqrt(four*fk/pi)
      twoksg=two*sk*g
      t=agr/(twoksg*rho)
      uu=delgr/(rho*rho*twoksg**3)
      rholap=uplap+dnlap
      vv=rholap/(rho*twoksg*twoksg)
      ww=(agrup*agrup-agrdn*agrdn-zet*agr*agr)/(rho*rho*twoksg*twoksg)
      call corpbe(rs,zet,t,uu,vv,ww,ec,vcup,vcdn,h,dvcup,dvcdn)
      ecpbe=ec+h
      vcuppbe=vcup+dvcup
      vcdnpbe=vcdn+dvcdn

      end subroutine pbe
c     ===============================================================
c
c     official pbe correlation code. K. Burke, May 14, 1996.
c
c     References:
c     [a] J.P.~Perdew, K.~Burke, and M.~Ernzerhof , PRL 77, 3865 (1996)
c     [b] J. P. Perdew, K. Burke, and Y. Wang, {\sl Real-space cutoff
c         construction of a generalized gradient approximation:  The PW91
c         density functional}, submitted to Phys. Rev. B, Feb. 1996.
c     [c] J. P. Perdew and Y. Wang, Phys. Rev. B 45, 13244 (1992).
c
c     ---------------------------------------------------------------
      subroutine corpbe(rs,zet,t,uu,vv,ww,ec,vcup,vcdn,
     1     h,dvcup,dvcdn)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     rs,                  ! Seitz radius=(3/4pi rho)^(1/3)
     2     zet,                 ! relative spin polarization = (rhoup-rhodn)/rho
     3     t,                   ! ABS(GRAD rho)/(rho*2.*ks*g)  -- only needed for PBE
     4     uu,                  ! (GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*ks*g)**3)
     5     vv,                  ! (LAPLACIAN rho)/(rho * (2*ks*g)**2)
     6     ww                   ! (GRAD rho)*(GRAD zet)/(rho * (2*ks*g)**2

      real(dp), intent(out) ::
     7     ec,                  ! LSD correlation energy from [a]
     8     vcup,                ! LSD up correlation potential
     9     vcdn,                ! LSD dn correlation potential
     1     h,                   ! nonlocal part of correlation energy per electron
     2     dvcup,               ! nonlocal correction to vcup
     3     dvcdn                ! nonlocal correction to vcdn
c
c     Work variables:
c
      real(dp) :: alfc,alfm,alfrsm,b,b2,bec,bg,comm,ecrs,eczet,
     1     ep,eprs,eu,eurs,f,fac,fact0,fact1,fact2,fact3,fact5,fz,g
     2     ,g3,g4,gz,hb,hbt,hrs,hrst,ht,htt,hz,hzt,pon,pref,q4,q5,q8
     3     ,q9,rs2,rs3,rsthrd,rtrs,t2,t4,t6,z4
c
c     third*=various multiples of 1/3
c     numbers for use in LSD energy spin-interpolation formula, [c](9).
c        gam= 2^(4/3)-2
c        fzz=f''(0)= 8/(9*gam)
c     numbers for construction of PBE
c        gamma=(1-log(2))/pi^2
c        bet=coefficient in gradient expansion for correlation, [a](4).
c        eta=small number to stop d phi/ dzeta from blowing up at 
c            |zeta|=1.
c
      real(dp), parameter :: thrdm=-third
      real(dp), parameter :: thrd2=two*third
      real(dp), parameter :: sixthm=thrdm/two
      real(dp), parameter :: thrd4=four*third
      real(dp), parameter :: 
     1     gam=0.5198420997897463295344212145565d0
      real(dp), parameter :: fzz=8.d0/(9.d0*gam)
      real(dp), parameter :: gamma=0
     2     .03109069086965489503494086371273d0
      real(dp), parameter :: bet=0.06672455060314922d0
      real(dp), parameter :: delt=bet/gamma
      real(dp), parameter :: eta=1.d-12
c     ---------------------------------------------------------------
c
c     find LSD energy contributions, using [c](10) and table i[c].
c         eu=unpolarized LSD correlation energy
c         eurs=deu/drs
c         ep=fully polarized LSD correlation energy
c         eprs=dep/drs
c         alfm=-spin stiffness, [c](3).
c         alfrsm=-dalpha/drs
c         f=spin-scaling factor from [c](9).
c     construct ec, using [c](8)
c
      rtrs=sqrt(rs)
      call gcor2(0.0310907d0,0.21370d0,7.5957d0,3.5876d0,1.6382d0,
     1     0.49294d0,rtrs,eu,eurs)
      call gcor2(0.01554535d0,0.20548d0,14.1189d0,6.1977d0,3.3662d0,
     1     0.62517d0,rtrs,ep,eprs)
      call gcor2(0.0168869d0,0.11125d0,10.357d0,3.6231d0,0.88026d0,
     1     0.49671d0,rtrs,alfm,alfrsm)
      alfc = -alfm
      z4 = zet**4
      f=((one+zet)**thrd4+(one-zet)**thrd4-two)/gam
      ec = eu*(one-f*z4)+ep*f*z4-alfm*f*(one-z4)/fzz
c
c     LSD potential from [c](a1)
c         ecrs = dec/drs [c](a2)
c         eczet=dec/dzeta [c](a3)
c         fz = df/dzeta [c](a4)
c
      ecrs = eurs*(one-f*z4)+eprs*f*z4-alfrsm*f*(one-z4)/fzz
      fz = thrd4*((one+zet)**third-(one-zet)**third)/gam
      eczet = four*(zet**3)*f*(ep-eu+alfm/fzz)+fz*(z4*ep-z4*eu
     1     -(one-z4)*alfm/fzz)
      comm = ec -rs*ecrs/three-zet*eczet
      vcup = comm + eczet
      vcdn = comm - eczet
c
c     PBE correlation energy
c         g=phi(zeta), given after [a](3)
c         delt=bet/gamma
c         b=a of [a](8)
c
      g=((one+zet)**thrd2+(one-zet)**thrd2)/two
      g3 = g**3
      pon=-ec/(g3*gamma)
      b = delt/(exp(pon)-one)
      b2 = b*b
      t2 = t*t
      t4 = t2*t2
      rs2 = rs*rs
      rs3 = rs2*rs
      q4 = one+b*t2
      q5 = one+b*t2+b2*t4
      h = g3*(bet/delt)*log(one+delt*q4*t2/q5)
c
c     energy done. now the potential, using appendix E of [b].
c
      g4 = g3*g
      t6 = t4*t2
      rsthrd = rs/three
      gz=(((one+zet)*(one+zet)+eta)**sixthm-
     1     ((one-zet)*(one-zet)+eta)**sixthm)/three
      fac = delt/b+one
      bg = -three*b2*ec*fac/(bet*g4)
      bec = b2*fac/(bet*g3)
      q8 = q5*q5+delt*q4*q5*t2
      q9 = one+two*b*t2
      hb = -bet*g3*b*t6*(two+b*t2)/q8
      hrs = -rsthrd*hb*bec*ecrs
      fact0 = two*delt-6.d0*b
      fact1 = q5*q9+q4*q9*q9
      hbt = two*bet*g3*t4*((q4*q5*fact0-delt*fact1)/q8)/q8
      hrst = rsthrd*t2*hbt*bec*ecrs
      hz = three*gz*h/g + hb*(bg*gz+bec*eczet)
      ht = two*bet*g3*q9/q8
      hzt = three*gz*ht/g+hbt*(bg*gz+bec*eczet)
      fact2 = q4*q5+b*t2*(q4*q9+q5)
      fact3 = two*b*q5*q9+delt*fact2
      htt = four*bet*g3*t*(two*b/q8-(q9*fact3/q8)/q8)
      comm = h+hrs+hrst+t2*ht/6.d0+7.d0*t2*t*htt/6.d0
      pref = hz-gz*t2*ht/g
      fact5 = gz*(two*ht+t*htt)/g
      comm = comm-pref*zet-uu*htt-vv*ht-ww*(hzt-fact5)
      dvcup = comm + pref
      dvcdn = comm - pref

      end subroutine corpbe
c     ===============================================================
c
c     PBE exchange for a spin-unpolarized electronic system
c     K Burke's modification of PW91 codes, May 14, 1996
c     Modified again by K. Burke, June 29, 1996, with simpler Fx(s)
c
c     References:
c     [a]J.P.~Perdew, K.~Burke, and M.~Ernzerhof, PRL 77, 3865 (1996);
c     [b]J.P. Perdew and Y. Wang, Phys. Rev.  B 33,  8800(1986);
c     40,  3399  (1989) (E).
c
c     Formulas:
c          e_x[unif]=ax*rho^(4/3)  [LDA]
c          ax = -0.75*(3/pi)^(1/3)
c          e_x[PBE]=e_x[unif]*FxPBE(s)
c          FxPBE(s)=1+uk-uk/(1+ul*s*s)                 [a](13)
c     uk, ul defined after [a](13) 
c
c     for u,v, see PW86(24)
c
c     ---------------------------------------------------------------
      subroutine exchpbe(rho,s,u,v,ex,vx)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     rho,                 ! density
     2     s,                   ! ABS(GRAD rho)/(2*KF*rho), where kf=(3 pi^2 rho)^(1/3)
     3     u,                   ! (GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*kf)**3)
     4     v                    ! (LAPLACIAN rho)/(rho*(2*kf)**2)

      real(dp), intent(out) ::
     1     ex,                  ! exchange energy per electron
     2     vx                   ! potential
c
c     Work variables:
c
      real(dp) :: exunif,fs,fss,fxpbe,p0,s2

      real(dp), parameter :: thrd4=four/three
      real(dp), parameter :: ax=-0
     1     .738558766382022405884230032680836d0
      real(dp), parameter :: um=0.2195149727645171d0
      real(dp), parameter :: uk=0.8040d0
      real(dp), parameter :: ul=um/uk
c     ---------------------------------------------------------------
c
c     construct lda exchange energy density
      exunif = ax*rho**third
c
c     construct PBE enhancement factor
      s2 = s*s
      p0=one+ul*s2
      fxpbe = one+uk-uk/p0
      ex = exunif*fxpbe
c
c     energy done. now the potential:
c     find first and second derivatives of fx w.r.t s.
c          fs=(1/s)*d fxpbe/ ds
c          fss=d fs/ds
c
      fs=two*uk*ul/(p0*p0)
      fss=-four*ul*s*fs/p0
c
c     calculate potential from [b](24) 
      vx = exunif*(thrd4*fxpbe-(u-thrd4*s2*s)*fss-v*fs)

      end subroutine exchpbe
c     ===============================================================
c
c     Slimmed down version of gcor used in PW91 routines, to
c     interpolate LSD correlation energy, as given by (10) of
c     J. P. Perdew and Y. Wang, Phys. Rev. B 45, 13244 (1992).
c     K. Burke, May 11, 1996.
c
c     ---------------------------------------------------------------
      subroutine gcor2(a,a1,b1,b2,b3,b4,rtrs,gg,ggrs)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) :: a,a1,b1,b2,b3,b4,rtrs

      real(dp), intent(out) :: gg,ggrs
c
c     Work variables:
c
      real(dp) :: q0,q1,q2,q3
c     ---------------------------------------------------------------
      q0 = -two*a*(one+a1*rtrs*rtrs)
      q1 = two*a*rtrs*(b1+rtrs*(b2+rtrs*(b3+b4*rtrs)))
      q2 = log(one+one/q1)
      gg = q0*q2
      q3 = a*(b1/rtrs+two*b2+rtrs*(three*b3+four*b4*rtrs))
      ggrs = -two*a*a1*q2-q0*q3/(q1*(one+q1))

      end subroutine gcor2
c     ===============================================================
c
c     PW91 spin-unpolarized functional, extracted from spn_pbe
c
c     ---------------------------------------------------------------
      subroutine pw91(rho,agr,delgr,lap,expw91,vxpw91,ecpw91,
     1     vcuppw91,vcdnpw91)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     rho,                 ! density
     2     agr,                 ! |grad rho|
     3     delgr,               ! (grad up).(grad |grad up|)
     4     lap                  ! grad^2 up=Laplacian of up
      real(dp), intent(out) ::
     1     expw91,vxpw91,ecpw91,vcuppw91,vcdnpw91
                                ! PW91 quantities
c
c     Work variables:
c
      real(dp) :: alfc,dvcdn,dvcup,ec,ecrs,eczet,fk,g,h,
     1     rholap,rs,s,sk,twoksg,t,u,uu,vv,v,vcdn,vcup,ww,zet
c
c     constants: pi32=3 pi**2, alpha=(9pi/4)**thrd
c
      real(dp), parameter :: thrd2=two*third
      real(dp), parameter :: pi32=three*pi*pi
      real(dp), parameter ::
     1     alpha=1.91915829267751300662482032624669d0

c     ---------------------------------------------------------------
c
      if(rho.lt.1.d-18) then
        expw91 = zero
        vxpw91 = zero
        ecpw91 = zero
        vcuppw91 = zero
        vcdnpw91 = zero
      else
         fk=(pi32*rho)**third
         s=agr/(two*fk*rho)
         u=two*delgr/(rho*rho*(two*fk)**3)
         v=lap/(rho*(two*fk)**2)
         call exchpw91(rho,s,u,v,expw91,vxpw91)

         zet=zero
         g=one
         rs=alpha/fk
         sk=sqrt(four*fk/pi)
         twoksg=two*sk*g
         t=agr/(twoksg*rho)
         uu=delgr/(rho*rho*twoksg**3)
         rholap=lap
         vv=rholap/(rho*twoksg**2)
         ww=zero
         call corlsd(rs,zet,ec,vcup,vcdn,ecrs,eczet,alfc)
         call corpw91(rs,zet,g,ec,ecrs,eczet,t,uu,vv,ww,h,dvcup,dvcdn)
         ecpw91=ec+h
         vcuppw91=vcup+dvcup
         vcdnpw91=vcdn+dvcdn
      endif

      end subroutine pw91
c     ===============================================================
