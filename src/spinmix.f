c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     author: ?
c
      subroutine spinmix(pot,mixer)

      use constants
      use potential_module
      use mixer_module
      implicit none
c
c     Input/Output variables:
c
      type (potential), intent(in) :: pot
      type (mixer_data), intent(inout) :: mixer
c
c     Work variables:
c
      integer i
c     ---------------------------------------------------------------
      do i = 1,pot%ndim
         mixer%xin(i)=pot%vold(i,1)
         mixer%xin(pot%ndim+i)=pot%vold(i,2)
         mixer%xout(i)=pot%vnew(i,1)
         mixer%xout(pot%ndim+i)=pot%vnew(i,2)
      enddo 

      end subroutine spinmix
c     ===============================================================
