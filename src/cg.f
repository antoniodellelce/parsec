c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c                          S P A R S K I T
c     ---------------------------------------------------------------
c           Basic Iterative Solvers with Reverse Communication
c     ---------------------------------------------------------------
c     This file currently has several basic iterative linear system
c     solvers. They are:
c     CG       -- Conjugate Gradient Method
c     CGNR     -- Conjugate Gradient Method (Normal Residual equation)
c     BCG      -- Bi-Conjugate Gradient Method
c     DBCG     -- BCG with partial pivoting
c     BCGSTAB  -- BCG stabilized
c     TFQMR    -- Transpose-Free Quasi-Minimum Residual method
c     FOM      -- Full Orthogonalization Method
c     GMRES    -- Generalized Minimum RESidual method
c     FGMRES   -- Flexible version of Generalized Minimum
c                 RESidual method
c     DQGMRES  -- Direct versions of Quasi Generalize Minimum
c                 Residual method
c     ---------------------------------------------------------------
c     They all have the following calling sequence:
c        subroutine solver(n, rhs, sol, ipar, fpar, w)
c        integer n, ipar(16)
c        real*8 rhs(n), sol(n), fpar(16), w(*)
c     Where
c     (1) 'n' is the size of the linear system,
c     (2) 'rhs' is the right-hand side of the linear system,
c     (3) 'sol' is the solution to the linear system,
c     (4) 'ipar' is an integer parameter array for the reverse
c     communication protocol,
c     (5) 'fpar' is an floating-point parameter array storing
c     information to and from the iterative solvers.
c     (6) 'w' is the work space (size is specified in ipar)
c
c     They are preconditioned iterative solvers with reverse
c     communication. The preconditioners can be applied from either
c     from left or right or both (specified by ipar(2), see below).
c
c     Author: Kesheng John Wu (kewu@mail.cs.umn.edu) 1993
c
c     NOTES:
c
c     (1) Work space required by each of the iterative solver
c     routines is as follows:
c       CG      == 5 * n
c       CGNR    == 5 * n
c       BCG     == 7 * n
c       DBCG    == 11 * n
c       BCGSTAB == 8 * n
c       TFQMR   == 11 * n
c       FOM     == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
c       GMRES   == (n+3)*(m+2) + (m+1)*m/2 (m = ipar(5), default m=15)
c       FGMRES  == n*(2m+1) + (m+1)*m/2 + 3*m + 2 (m = ipar(5),
c                  default m=15)
c       DQGMRES == n + lb * (2*n+4) (lb=ipar(5)+1, default lb = 16)
c
c     (2) ALL iterative solvers require a user-supplied DOT-product
c     routine named DISTDOT. The prototype of DISTDOT is
c
c     real*8 function distdot(n,x,ix,y,iy)
c     integer n, ix, iy
c     real*8 x(1+(n-1)*ix), y(1+(n-1)*iy)
c
c     This interface of DISTDOT is exactly the same as that of
c     DDOT (or SDOT if real == real*8) from BLAS-1. It should have
c     same functionality as DDOT on a single processor machine. On a
c     parallel/distributed environment, each processor can perform
c     DDOT on the data it has, then perform a summation on all the
c     partial results.
c
c     (3) To use this set of routines under SPMD/MIMD program
c     paradigm, several things are to be noted: (a) 'n' should be the
c     number of vector elements of 'rhs' that is present on the local
c     processor .
c     (b) if RHS(i) is on processor j, it is expected that SOL(i)
c     will be on the same processor, i.e. the vectors are distributed
c     to each processor in the same way. (c) the preconditioning and
c     stopping criteria specifications have to be the same on all
c     processor involved, ipar and fpar have to be the same on each
c     processor. (d) DISTDOT should be replaced by a distributed
c     dot-product function.
c
c     ..............................................................
c     Reverse Communication Protocols
c
c     When a reverse-communication routine returns, it could be
c     either that the routine has terminated or it simply requires
c     the caller to perform one matrix-vector multiplication. The
c     possible matrices that involve in the matrix-vector
c     multiplications are:
c     A       (the matrix of the linear system),
c     A^T     (A transposed),
c     Ml^{-1} (inverse of the left preconditioner),
c     Ml^{-T} (inverse of the left preconditioner transposed),
c     Mr^{-1} (inverse of the right preconditioner),
c     Mr^{-T} (inverse of the right preconditioner transposed).
c     For all the matrix vector multiplication, v = A u. The input
c     and output vectors are supposed to be part of the work space 'w',
c     and the starting positions of them are stored in ipar(8:9), see
c     below.
c
c     The array 'ipar' is used to store the information about the
c     solver. Here is the list of what each element represents:
c
c     ipar(1) -- status of the call/return.
c     A call to the solver with ipar(1) == 0 will initialize the
c     iterative solver. On return from the iterative solver, ipar(1)
c     carries the status flag which indicates the condition of the
c     return. The status information is divided into two categories,
c     (1) a positive value indicates the solver requires a
c     matrix-vector multiplication,
c     (2) a non-positive value indicates termination of the solver.
c     Here is the current definition:
c       1 == request a matvec with A,
c       2 == request a matvec with A^T,
c       3 == request a left preconditioner solve (Ml^{-1}),
c       4 == request a left preconditioner transposed solve (Ml^{-T}),
c       5 == request a right preconditioner solve (Mr^{-1}),
c       6 == request a right preconditioner transposed solve (Mr^{-T}),
c      10 == request the caller to perform stopping test,
c       0 == normal termination of the solver, satisfied the stopping
c            criteria,
c      -1 == termination because iteration number is greater than the
c            preset limit,
c      -2 == return due to insufficient work space,
c      -3 == return due to anticipated break-down / divide by zero,
c            in the case where Arnoldi procedure is used, additional
c            error code can be found in ipar(12), where ipar(12) is
c            the error code of orthogonalization procedure MGSRO:
c               -1: zero input vector
c               -2: input vector contains abnormal numbers
c               -3: input vector is a linear combination of others
c               -4: trianguler system in GMRES/FOM/etc. has rank 0 (zero)
c      -4 == the values of fpar(1) and fpar(2) are both <= 0, the valid
c            ranges are 0 <= fpar(1) < 1, 0 <= fpar(2), and they can
c            not be zero at the same time
c      -9 == while trying to detect a break-down, an abnormal number is
c            detected.
c     -10 == return due to some non-numerical reasons, e.g. invalid
c            floating-point numbers etc.
c
c     ipar(2) -- status of the preconditioning:
c       0 == no preconditioning
c       1 == left preconditioning only
c       2 == right preconditioning only
c       3 == both left and right preconditioning
c
c     ipar(3) -- stopping criteria (details of this will be
c     discussed later).
c
c     ipar(4) -- number of elements in the array 'w'. if this is less
c     than the desired size, it will be over-written with the minimum
c     requirement. In which case the status flag ipar(1) = -2.
c
c     ipar(5) -- size of the Krylov subspace (used by GMRES and its
c     variants), e.g. GMRES(ipar(5)), FGMRES(ipar(5)),
c     DQGMRES(ipar(5)).
c
c     ipar(6) -- maximum number of matrix-vector multiplies, if not a
c     positive number the iterative solver will run till convergence
c     test is satisfied.
c
c     ipar(7) -- current number of matrix-vector multiplies. It is
c     incremented after each matrix-vector multiplication. If there
c     is preconditioning, the counter is incremented after the
c     preconditioning associated with each matrix-vector
c     multiplication.
c
c     ipar(8) -- pointer to the input vector to the requested
c     matrix-vector multiplication.
c
c     ipar(9) -- pointer to the output vector of the requested
c     matrix-vector multiplication.
c
c     To perform v = A * u, it is assumed that u is
c     w(ipar(8):ipar(8)+n-1) and v is stored as w(ipar(9):ipar(9)+n-1).
c
c     ipar(10) -- the return address (used to determine where to go
c     to inside the iterative solvers after the caller has performed
c     the requested services).
c
c     ipar(11) -- the result of the external convergence test
c     On final return from the iterative solvers, this value
c     will be reflected by ipar(1) = 0 (details discussed later)
c
c     ipar(12) -- error code of MGSRO, it is
c                  1 if the input vector to MGSRO is linear combination
c                    of others,
c                  0 if MGSRO was successful,
c                 -1 if the input vector to MGSRO is zero,
c                 -2 if the input vector contains invalid number.
c
c     ipar(13) -- number of initializations. During each initilization
c                 residual norm is computed directly from M_l(b - A x).
c
c     ipar(14) to ipar(16) are NOT defined, they are NOT USED by
c     any iterative solver at this time.
c
c     Information about the error and tolerance are stored in the
c     array FPAR. So are some internal variables that need to be
c     saved from one iteration to the next one. Since the internal
c     variables are not the same for each routine, we only define the
c     common ones.
c
c     The first two are input parameters:
c     fpar(1) -- the relative tolerance,
c     fpar(2) -- the absolute tolerance (details discussed later),
c
c     When the iterative solver terminates,
c     fpar(3) -- initial residual/error norm,
c     fpar(4) -- target residual/error norm,
c     fpar(5) -- current residual norm (if available),
c     fpar(6) -- current residual/error norm,
c     fpar(7) -- convergence rate,
c
c     fpar(8:10) are used by some of the iterative solvers to save
c     some internal information.
c
c     fpar(11) -- number of floating-point operations. The iterative
c     solvers will add the number of FLOPS they used to this variable,
c     but they do NOT initialize it, nor add the number of FLOPS due
c     to matrix-vector multiplications (since matvec is outside of
c     the iterative solvers). To insure the correct FLOPS count, the
c     caller should set fpar(11) = 0 before invoking the iterative
c     solvers and account for the number of FLOPS from matrix-vector
c     multiplications and preconditioners.
c
c     fpar(12:16) are not used in current implementation.
c
c     Whether the content of fpar(3), fpar(4) and fpar(6) are residual
c     norms or error norms depends on ipar(3). If the requested
c     convergence test is based on the residual norm, they will be
c     residual norms. If the caller want to test convergence based the
c     error norms (estimated by the norm of the modifications applied
c     to the approximate solution), they will be error norms.
c     Convergence rate is defined by (Fortran 77 statement)
c     fpar(7) = log10(fpar(3) / fpar(6)) / (ipar(7)-ipar(13))
c     If fpar(7) = 0.5, it means that approximately every 2 (= 1/0.5)
c     steps the residual/error norm decrease by a factor of 10.
c
c     ............................................................
c     Stopping criteria,
c
c     An iterative solver may be terminated due to (1) satisfying
c     convergence test; (2) exceeding iteration limit; (3)
c     insufficient work space; (4) break-down. Checking of the work
c     space is only done in the initialization stage, i.e. when it is
c     called with ipar(1) == 0. A complete convergence test is done
c     after each update of the solutions. Other conditions are
c     monitored continuously.
c
c     With regard to the number of iteration, when ipar(6) is
c     positive, the current iteration number will be checked against
c     it. If current iteration number is greater the ipar(6) than the
c     solver will return with status -1. If ipar(6) is not positive,
c     the iteration will continue until convergence test is satisfied.
c
c     Two things may be used in the convergence tests, one is the
c     residual 2-norm, the other one is 2-norm of the change in the
c     approximate solution. The residual and the change in
c     approximate solution are from the preconditioned system (if
c     preconditioning is applied). The DQGMRES and TFQMR use two
c     estimates for the residual norms. The estimates are not
c     accurate, but they are acceptable in most of the cases.
c     Generally speaking, the error of the TFQMR's estimate is less
c     accurate.
c
c     The convergence test type is indicated by ipar(3). There are
c     four type convergence tests: (1) tests based on the residual
c     norm; (2) tests based on change in approximate solution; (3)
c     caller does not care, the solver choose one from above two on
c     its own; (4) caller will perform the test, the solver should
c     simply continue.
c     Here is the complete definition:
c      -2 == || dx(i) || <= rtol * || rhs || + atol
c      -1 == || dx(i) || <= rtol * || dx(1) || + atol
c       0 == solver will choose test 1 (next)
c       1 == || residual || <= rtol * || initial residual || + atol
c       2 == || residual || <= rtol * || rhs || + atol
c     999 == caller will perform the test
c     where dx(i) denote the change in the solution at the ith update.
c     ||.|| denotes 2-norm. rtol = fpar(1) and atol = fpar(2).
c
c     If the caller is to perform the convergence test, the outcome
c     should be stored in ipar(11).
c     ipar(11) = 0 -- failed the convergence test, iterative solver
c     should continue
c     ipar(11) = 1 -- satisfied convergence test, iterative solver
c     should perform the clean up job and stop.
c
c     Upon return with ipar(1) = 10,
c     ipar(8)  points to the starting position of the change in
c              solution Sx, where the actual solution of the step is
c              x_j = x_0 + M_r^{-1} Sx.
c              Exception: ipar(8) < 0, Sx = 0. It is mostly used by
c              GMRES and variants to indicate (1) Sx was not necessary,
c              (2) intermediate result of Sx is not computed.
c     ipar(9)  points to the starting position of a work vector that
c              can be used by the caller.
c
c     NOTE: the caller should allow the iterative solver to perform
c     clean up job after the external convergence test is satisfied,
c     since some of the iterative solvers do not directly update
c     the 'sol' array. A typical clean-up stage includes performing
c     the final update of the approximate solution and computing
c     the convergence information (e.g. values of fpar(3:7)).
c
c     NOTE: fpar(4) and fpar(6) are not set by the accelerators (the
c     routines implemented here) if ipar(3) = 999.
c
c     ..............................................................
c     Usage:
c
c     To start solving a linear system, the user needs to specify
c     first 6 elements of the ipar, and first 2 elements of fpar.
c     The user may optionally set fpar(11) = 0 if one wants to count
c     the number of floating-point operations. (Note: the iterative
c     solvers will only add the floating-point operations inside
c     themselves, the caller will have to add the FLOPS from the
c     matrix-vector multiplication routines and the preconditioning
c     routines in order to account for all the arithmetic operations.)
c
c     Here is an example:
c     ipar(1) = 0       ! always 0 to start an iterative solver
c     ipar(2) = 2       ! right preconditioning
c     ipar(3) = 1       ! use convergence test scheme 1
c     ipar(4) = 10000   ! the 'w' has 10,000 elements
c     ipar(5) = 10      ! use *GMRES(10) (e.g. FGMRES(10))
c     ipar(6) = 100     ! use at most 100 matvec's
c     fpar(1) = 1.0D-6  ! relative tolerance 1.0D-6
c     fpar(2) = 1.0D-10 ! absolute tolerance 1.0D-10
c     fpar(11) = 0.0    ! clearing the FLOPS counter
c
c     After the above specifications, one can start to call an
c     iterative solver, say BCG. Here is a piece of pseudo-code
c     showing how it can be done,
c
c 10   call bcg(n,rhs,sol,ipar,fpar,w)
c      if (ipar(1).eq.1) then
c         call amux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
c         goto 10
c      else if (ipar(1).eq.2) then
c         call atmux(n,w(ipar(8)),w(ipar(9)),a,ja,ia)
c         goto 10
c      else if (ipar(1).eq.3) then
c         left preconditioner solver
c         goto 10
c      else if (ipar(1).eq.4) then
c         left preconditioner transposed solve
c         goto 10
c      else if (ipar(1).eq.5) then
c         right preconditioner solve
c         goto 10
c      else if (ipar(1).eq.6) then
c         right preconditioner transposed solve
c         goto 10
c      else if (ipar(1).eq.10) then
c         call my own stopping test routine
c         goto 10
c      else if (ipar(1).gt.0) then
c         ipar(1) is an unspecified code
c      else
c         the iterative solver terminated with code = ipar(1)
c      endif
c
c     This segment of pseudo-code assumes the matrix is in CSR
c     format, AMUX and ATMUX are two routines from the SPARSKIT
c     MATVEC module. They perform matrix-vector multiplications for
c     CSR matrices, where w(ipar(8)) is the first element of the
c     input vectors to the two routines, and w(ipar(9)) is the first
c     element of the output vectors from them. For simplicity, we did
c     not show the name of the routine that performs the
c     preconditioning operations or the convergence tests.
c
c     This is a implementation of the Conjugate Gradient (CG) method
c     for solving linear system.
c
c     NOTE: This is not the PCG algorithm. It is a regular CG
c     algorithm. To be consistent with the other solvers, the
c     preconditioners are applied by performing 
c     Ml^{-1} A Mr^{-1} P in place of A P in the CG algorithm. The
c     PCG uses its preconditioners very differently.
c
c     fpar(7) is used here internally to store <r, r>.
c     w(:,1) -- residual vector
c     w(:,2) -- P, the conjugate direction
c     w(:,3) -- A P, matrix multiply the conjugate direction
c     w(:,4) -- temporary storage for results of preconditioning
c     w(:,5) -- change in the solution (sol) is stored here until
c               termination of this solver
c
c     ---------------------------------------------------------------
      subroutine cg(n, kss0, rhs, sol, ipar, fpar, w, nnodes,comm)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) :: n, kss0, nnodes, comm
      integer, intent(inout) :: ipar(16)
      real(dp), intent(in) :: rhs(n)
      real(dp), intent(inout) :: sol(n), fpar(16), w(n,kss0)
c
c     Work variables:
c
      integer i
      real(dp) :: alpha
      logical lp,rp
      save
c
c     External functions:
c
      real(dp), external :: distdot
      logical, external :: stopbis, brkdn
c     ---------------------------------------------------------------
c
c     check the status of the call
c
      if (ipar(1).le.0) ipar(10) = 0
      select case (ipar(10))
      case (1)
         goto 10
      case (2)
         goto 20
      case (3)
         goto 40
      case (4)
         goto 50
      case (5)
         goto 60
      case (6)
         goto 70
      case (7)
         goto 80
      end select
c
c     initialization
c
      call bisinit(ipar,fpar,5*n,1,kss0*n,lp,rp,w)
      if (ipar(1).lt.0) return
c
c     request for matrix vector multiplication A*x in the initialization
c
      ipar(1) = 1
      ipar(8) = n+1
      ipar(9) = ipar(8) + n
      ipar(10) = 1
      do i = 1, n
         w(i,2) = sol(i)
      enddo
      return
 10   ipar(7) = ipar(7) + 1
      ipar(13) = 1
      do i = 1, n
         w(i,2) = rhs(i) - w(i,3)
      enddo
      fpar(11) = fpar(11) + n
c
c     if left preconditioned
c
      if (lp) then
         ipar(1) = 3
         ipar(9) = 1
         ipar(10) = 2
         return
      endif

 20   if (lp) then
         do i = 1, n
            w(i,2) = w(i,1)
         enddo
      else
         do i = 1, n
            w(i,1) = w(i,2)
         enddo
      endif

      fpar(7) = distdot(n,w,1,w,1,nnodes,comm)
      fpar(11) = fpar(11) + 2 * n
      fpar(3) = sqrt(fpar(7))
      fpar(5) = fpar(3)
      if (ipar(3).eq.2.or.ipar(3).eq.-2) then
         fpar(4) = fpar(1) * sqrt(distdot(n,rhs,1,rhs,1,nnodes,comm))
     1        + fpar(2)
         fpar(11) = fpar(11) + 2 * n
      else if (ipar(3).ne.999) then
         fpar(4) = fpar(1) * fpar(3) + fpar(2)
      endif
c
c     before iteration can continue, we need to compute A * p, which
c     includes the preconditioning operations
c
 30   if (rp) then
         ipar(1) = 5
         ipar(8) = n + 1
         if (lp) then
            ipar(9) = ipar(8) + n
         else
            ipar(9) = 3*n + 1
         endif
         ipar(10) = 3
         return
      endif

 40   ipar(1) = 1
      if (rp) then
         ipar(8) = ipar(9)
      else
         ipar(8) = n + 1
      endif
      if (lp) then
         ipar(9) = 3*n+1
      else
         ipar(9) = n+n+1
      endif
      ipar(10) = 4
      return

 50   if (lp) then
         ipar(1) = 3
         ipar(8) = ipar(9)
         ipar(9) = n+n+1
         ipar(10) = 5
         return
      endif
c
c     continuing with the iterations
c
 60   ipar(7) = ipar(7) + 1
      alpha = distdot(n,w(1,2),1,w(1,3),1,nnodes,comm)
      fpar(11) = fpar(11) + 2*n
      if (brkdn(alpha,ipar)) goto 90
      alpha = fpar(7) / alpha
      do i = 1, n
         w(i,5) = w(i,5) + alpha * w(i,2)
         w(i,1) = w(i,1) - alpha * w(i,3)
      enddo
      fpar(11) = fpar(11) + 4*n
c
c     are we ready to terminate ?
c
      if (ipar(3).eq.999) then
         ipar(1) = 10
         ipar(8) = 4*n + 1
         ipar(9) = 3*n + 1
         ipar(10) = 6
         return
      endif
 70   if (ipar(3).eq.999) then
         if (ipar(11).eq.1) goto 90
      else if (stopbis(n,ipar,1,fpar,w,w(1,2),alpha,nnodes,comm)) then
         goto 90
      endif
c
c     continue the iterations
c
      alpha = fpar(5)*fpar(5) / fpar(7)
      fpar(7) = fpar(5)*fpar(5)
      do i = 1, n
         w(i,2) = w(i,1) + alpha * w(i,2)
      enddo
      fpar(11) = fpar(11) + 2*n
      goto 30
c
c     clean up -- necessary to accommodate the right-preconditioning
c
 90   if (rp) then
         if (ipar(1).lt.0) ipar(12) = ipar(1)
         ipar(1) = 5
         ipar(8) = 4*n + 1
         ipar(9) = ipar(8) - n
         ipar(10) = 7
         return
      endif
 80   if (rp) then
         call tidycg(n,ipar,fpar,sol,w(1,4))
      else
         call tidycg(n,ipar,fpar,sol,w(1,5))
      endif

      end subroutine cg
c     ===============================================================
c
c     Inititialize parameters and work arrays
c
c     ---------------------------------------------------------------
      subroutine bisinit(ipar,fpar,wksize,dsc,lwk,lp,rp,wk)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) :: wksize, dsc, lwk
      integer, intent(inout) :: ipar(16)
      real(dp), intent(inout) :: fpar(16)
      logical, intent(out) :: lp,rp
      real(dp), intent(inout) :: wk(lwk)
c
c     Work variables:
c
      integer i
c     ---------------------------------------------------------------
c
c     ipar(1) = -2 inidcate that there are not enough space in the work
c     array
c
      if (ipar(4).lt.wksize) then
         ipar(1) = -2
         ipar(4) = wksize
         return
      endif

      if (ipar(2).gt.2) then
         lp = .true.
         rp = .true.
      else if (ipar(2).eq.2) then
         lp = .false.
         rp = .true.
      else if (ipar(2).eq.1) then
         lp = .true.
         rp = .false.
      else
         lp = .false.
         rp = .false.
      endif
      if (ipar(3).eq.0) ipar(3) = dsc
c     .. clear the ipar elements used
      ipar(7) = 0
      ipar(8) = 0
      ipar(9) = 0
      ipar(10) = 0
      ipar(11) = 0
      ipar(12) = 0
      ipar(13) = 0
c
c     fpar(1) must be between (0, 1), fpar(2) must be positive,
c     fpar(1) and fpar(2) can NOT both be zero
c     Normally return ipar(1) = -4 to indicate any of above error
c
      if (fpar(1).lt.zero .or. fpar(1).ge.one .or. fpar(2).lt.zero .or.
     1     (fpar(1).eq.zero .and. fpar(2).eq.zero)) then
         if (ipar(1).eq.0) then
            ipar(1) = -4
            return
         else
            fpar(1) = 1.0D-6
            fpar(2) = 1.0D-16
         endif
      endif
c     .. clear the fpar elements
      do i = 3, 10
         fpar(i) = zero
      enddo
      if (fpar(11).lt.zero) fpar(11) = zero
c     .. clear the used portion of the work array to zero
      do i = 1, wksize
         wk(i) = zero
      enddo

      end subroutine bisinit
c     ===============================================================
c
c     Test whether alpha is zero or an abnormal number, if yes,
c     this routine will return .true.
c
c     If alpha == 0, ipar(1) = -3,
c     if alpha is an abnormal number, ipar(1) = -9.
c
c     ---------------------------------------------------------------
      logical function brkdn(alpha, ipar)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(inout) :: ipar(16)
      real(dp), intent(in) :: alpha
c
c     Work variables:
c
      real(dp) :: beta
c     ---------------------------------------------------------------
      brkdn = .false.
      if (alpha.gt.zero) then
         beta = one / alpha
         if (.not. beta.gt.zero) then
            brkdn = .true.
            ipar(1) = -9
         endif
      else if (alpha.lt.zero) then
         beta = one / alpha
         if (.not. beta.lt.zero) then
            brkdn = .true.
            ipar(1) = -9
         endif
      else if (alpha.eq.zero) then
         brkdn = .true.
         ipar(1) = -3
      else
         brkdn = .true.
         ipar(1) = -9
      endif

      end function brkdn
c     ===============================================================
c
c     Function for determining the stopping criteria. return value of
c     true if the stopbis criteria is satisfied.
c
c     ---------------------------------------------------------------
      logical function stopbis(n,ipar,mvpi,fpar,r,delx,sx,nnodes,comm)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) :: n, mvpi, nnodes, comm
      integer, intent(inout) :: ipar(16)
      real(dp), intent(inout) :: fpar(16)
      real(dp), intent(in) :: r(n), delx(n), sx
c
c     External functions:
c
      real(dp), external :: distdot
c     ---------------------------------------------------------------
      if (ipar(11) .eq. 1) then
         stopbis = .true.
      else
         stopbis = .false.
      endif
      if (ipar(6).gt.0 .and. ipar(7).ge.ipar(6)) then
         ipar(1) = -1
         stopbis = .true.
      endif
      if (stopbis) return
c
c     computes errors
c
      fpar(5) = sqrt(distdot(n,r,1,r,1,nnodes, comm))
      fpar(11) = fpar(11) + 2 * n
      if (ipar(3).lt.0) then
c
c     compute the change in the solution vector
c
         fpar(6) = sx * sqrt(distdot(n,delx,1,delx,1,nnodes,comm))
         fpar(11) = fpar(11) + 2 * n
         if (ipar(7).lt.mvpi+mvpi+1) then
c
c     if this is the end of the first iteration, set fpar(3:4)
c
            fpar(3) = fpar(6)
            if (ipar(3).eq.-1) then
               fpar(4) = fpar(1) * fpar(3) + fpar(2)
            endif
         endif
      else
         fpar(6) = fpar(5)
      endif
c
c     .. the test is struct this way so that when the value in
c     fpar(6) is not a valid number, STOPBIS is set to .true.
c
      if (fpar(6).gt.fpar(4)) then
         stopbis = .false.
         ipar(11) = 0
      else
         stopbis = .true.
         ipar(11) = 1
      endif

      end function stopbis
c     ===============================================================
c
c     Some common operations required before terminating the CG
c     routines.
c
c     ---------------------------------------------------------------
      subroutine tidycg(n,ipar,fpar,sol,delx)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) :: n
      integer, intent(inout) :: ipar(16)
      real(dp), intent(inout) :: fpar(16)
      real(dp), intent(in) :: delx(n)
      real(dp), intent(inout) :: sol(n)
c
c     Work variables:
c
      integer i
c     ---------------------------------------------------------------
      if (ipar(12).ne.0) then
         ipar(1) = -3
      else if (ipar(1).gt.0) then
         if ((ipar(3).eq.999 .and. ipar(11).eq.1) .or.
     1        fpar(6).le.fpar(4)) then
            ipar(1) = 0
         else if (ipar(7).ge.ipar(6) .and. ipar(6).gt.0) then
            ipar(1) = -1
         else
            ipar(1) = -10
         endif
      endif
      if (fpar(3).gt.zero .and. fpar(6).gt.zero .and.
     1     ipar(7).gt.ipar(13)) then
         fpar(7) = log10(fpar(3) / fpar(6)) /
     1        real(ipar(7)-ipar(13),dp)
      else
         fpar(7) = zero
      endif
      do i = 1, n
         sol(i) = sol(i) + delx(i)
      enddo

      end subroutine tidycg
c     ===============================================================
