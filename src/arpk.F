c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     author: M. Alemany, E. Lorin
c
c     Driver routine for the ARPACK eigensolver
c
c     ---------------------------------------------------------------
      subroutine arpk_diag(nloc_p_pot,solver,parallel,nstate,nec,kss
     1     ,maxmvs,eval,evec,vec,tol,resid_restart,ipr,info)

      use constants
      use non_local_psp_module
      use eigen_solver_module
      use parallel_data_module

      implicit none
#ifdef MPI
      include 'mpif.h'
#endif
      include 'debug.h'
c
c     Input/Output variables:
c
c     non local pseudopotential related data
      type (nonloc_pseudo_potential), intent(in) :: nloc_p_pot 
c     solver related data
      type (eigen_solver), intent(in) :: solver
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

      integer, intent(in) :: ipr, nstate, kss
      integer, intent(inout) :: nec, maxmvs

      integer, intent(inout) :: info

      real(dp), intent(in) :: tol
      real(dp), intent(out) :: eval(nstate)
      real(dp), intent(inout) :: evec(parallel%ldn,nstate)

      real(dp), intent(inout) :: vec(parallel%nwedge+1)

      real(dp), intent(inout) :: resid_restart(parallel%ldn)
c
c     Work variables:
c
      integer i

c     arpack scalars
      character (len=1) :: bmat
      character (len=2) :: which
      integer ido, nev, ncv, lworkl, j,jj
      integer nx, nconv, maxitr, mode, ishfts,k

      logical rvec
      real(dp) :: sigma

c     arpack variables

c     change (adi):
      real(dp), allocatable :: vtmp(:,:)
      real(dp), dimension(:), allocatable :: workl,resid,workd

      real(dp) :: d(2*nstate+5,2)

      logical select(2*nstate+5)
      integer iparam(11), ipntr(11)
c     allocation check
      integer alcstat

c     ---------------------------------------------------------------

      vec = zero

      allocate(vtmp(parallel%mydim, 2*nstate+5),stat=alcstat)
      call alccheck('arpk_vtmp',parallel%mydim*(2*nstate+5),alcstat)
      allocate(workl((2*nstate+5)*(2*nstate+5+8)),stat=alcstat)
      call alccheck('arpk_workl',(2*nstate+5)*(2*nstate+5+8),alcstat)
      allocate(workd(3*parallel%mydim),stat=alcstat)
      call alccheck('arpk_workd',3*parallel%mydim,alcstat)
      allocate(resid(parallel%mydim),stat=alcstat)
      call alccheck('arpk_resid',parallel%mydim,alcstat)

      vtmp = zero; workl = zero; workd = zero; d = zero; resid = zero
      iparam = 0; ipntr = 0
c
c     debug variables
c
      ndigit = -3
      logfil = 6
      msgets = 0
      msaitr = 0
      msapps = 0
      msaupd = 0
      msaup2 = 0
      mseigt = 0
      mseupd = 1
c
c     Initialize the arpack variables
c
      nev = nstate
      ncv = 2*nstate+5

      bmat = 'I'
      which = 'SA'

      lworkl = ncv*(ncv+8)
      call dcopy(parallel%mydim,resid_restart,1,resid,1)

      iparam(4) = 1
 
      ido = 0
      ishfts = 1
      maxitr = nev*50
      mode   = 1

      iparam(1) = ishfts
      iparam(3) = maxitr
      iparam(7) = mode

c     Main loop (Reverse Communication)

 10   continue

#ifdef MPI
      call pdsaupd (parallel%comm,ido,bmat,parallel%mydim,which,nev
     1     ,tol,resid,ncv, vtmp, parallel%mydim, iparam, ipntr, workd
     2     , workl, lworkl, info)
#else
      call dsaupd (ido, bmat, parallel%nwedge, which, nev, tol, resid
     1     ,ncv, vtmp, parallel%nwedge, iparam, ipntr, workd, workl,
     2     lworkl, info)
#endif
      if (ido .eq. -1 .or. ido .eq. 1) then
         call matvec (nloc_p_pot,solver,parallel,workd(ipntr(1))
     1        ,workd(ipntr(2)),1,vec)
         if (iparam(9) .lt. maxmvs) go to 10
      end if

      if ( info .eq. 1) then
         write(6,'(/,a,/)') ' Maximum number of iterations reached.'
      else if ( info .eq. 3) then
         write(6,'(/,a,a,/)') ' No shifts could be applied during',
     1        ' implicit Arnoldi update, try increasing NCV.'
      end if      

      if ( info .lt. 0 ) then
         write(6,'(/,a,i5)') ' Error with _saupd, info = ', info
         write(6,'(a,/)') ' Check documentation in _saupd '
      else 

c     postprocess with dseupd

         rvec = .true.
#ifdef MPI
         call pdseupd (parallel%comm,rvec,'A',select,d,vtmp
     1        ,parallel%mydim,sigma,bmat, parallel%mydim, which, nev,
     2        tol, resid, ncv, vtmp, parallel%mydim,iparam, ipntr,
     3        workd, workl, lworkl,info)
#else
         call dseupd (rvec, 'A',select, d, vtmp, parallel%nwedge,
     1        sigma,bmat, parallel%nwedge, which, nev, tol, resid, ncv,
     2        vtmp, parallel%nwedge,iparam, ipntr, workd, workl, lworkl
     3        , info)
#endif
         resid_restart = zero

         do j = 1,parallel%mydim
            do jj= 1,nev
               resid_restart(j) = resid_restart(j)+vtmp(j,jj)
            enddo
         enddo
         resid_restart = resid_restart / real(nev,dp)

         if ( info .ne. 0) then
            write(6,*)
            write(6,*) ' Error with _seupd, info = ', info
            write(6,*) ' Check the documentation of _seupd. '
            write(6,*)
         end if

c     Print additional convergence information

         if (ipr .ge. 2) then

            write(6,*) ' '
            write(6,*) ' _SDRV1 '
            write(6,*) ' ====== '
            write(6,*) ' '
            write(6,*) ' Size of the matrix is ', parallel%mydim
            write(6,*) ' The number of Ritz values requested is ', nev
            write(6,*) ' The number of Arnoldi vectors generated',
     1           ' (NCV) is ', ncv
            write(6,*) ' What portion of the spectrum: ', which
            write(6,*) ' The number of converged Ritz values is ', 
     1           iparam(5)
            write(6,*) ' The number of Implicit Arnoldi update',
     1           ' iterations taken is ', iparam(3)
            write(6,*) ' The number of OP*x is ', iparam(9)
            write(6,*) ' The convergence criterion is ', tol
            write(6,*) ' '

         end if

      end if

c     evec <- workl
       
      eval = zero
      evec = zero
      do j=1,nev
         eval(j) = d(j,1)
         do  i=1,parallel%mydim
            evec(i,j) = vtmp(i,j)
         enddo
      enddo

      nec = iparam(5)
      maxmvs = iparam(9)
c
      if (allocated(vtmp)) deallocate(vtmp)
      if (allocated(workl)) deallocate(workl)
      if (allocated(workd)) deallocate(workd)
      if (allocated(resid)) deallocate(resid)

      end subroutine arpk_diag
c     ===============================================================
