c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Define the distribution of grid points among processors so
c     that each one receives the same number of points (or the
c     difference in number of points held by all processors is at
c     most one).
c
c     ---------------------------------------------------------------
      subroutine grid_partition(grid,pbc,rsymm,parallel,ierr)

      use constants
      use grid_module
      use pbc_module
      use symmetry_module
      use parallel_data_module

      implicit none
c
c     Input/Output variables:
c
c     grid related data
      type (grid_data), intent(inout) :: grid
c     periodic boundary conditions data
      type (pbc_data), intent(inout) :: pbc
c     symmetry operations in reduced (Abelian) group
      type (symmetry), intent(in) :: rsymm
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     cutoff radius used in the determination of grid points
      real(dp) :: rcut
c     number of neighbors used for derivation (on one side)
      integer nord2
c     actual size of hamiltonian, dimension of grid arrays
      integer ndim,ldn
c     bounds of grid along each direction
      integer nxmin,nxmax,nymin,nymax,nzmin,nzmax
c     variable for distance between points, maximum distance
      real(dp) :: dist,dmax
c     temporary 1-d index of each point
      integer nef
c     wrapping index
      integer iwrap
c     counters
      integer ii,i,j,k,j00,k00,ny1,ny2,nz1,nz2,iold
c     variables for the irreducible wedge
      integer itrans, ir(3), itest, nwedge
c     Cartesian coordinates of a grid point and its equivalent one
      real(dp) :: rr(3), rw(3)
c     arrays for irreducible wedge
      integer, allocatable :: ninv(:),rinv(:,:)

c     number of procs available
      integer nnodes
c     processor rank
      integer inode
c     Indexing of distributed blocks
      integer Irows(0:parallel%procs_num)
c     maximum number of FFT grid points (to be stored in pbc
c     structure)
      integer maxdfft
c     allocation check
      integer alcstat
c
c     External function:
c
      integer, external :: Part

c     ---------------------------------------------------------------

      nnodes = parallel%procs_num
      nord2 = grid%norder
c
c     PART1: Setup the GRID
c
c     determine minimal number of mesh points (per each axis, per
c     each side of origin) that would encompass a sphere of size rmax
c     (confined system), or a box of side rmax (periodic system)
c     inside which is a grid with a spacing of h. Note that for a
c     periodic system rmax is the WHOLE side of the box, not half of
c     it, so it's really comparable to the diameter of the sphere!

      nxmin = -grid%n1/2
      nxmax = grid%n1 + nxmin -1
      nymin = -grid%n2/2
      nymax = grid%n2 + nymin -1
      nzmin = -grid%n3/2
      nzmax = grid%n3 + nzmin -1

      nxmax = nxmax + nord2
      nxmin = nxmin - nord2
      nymax = nymax + nord2
      nymin = nymin - nord2
      nzmax = nzmax + nord2
      nzmin = nzmin - nord2

      call grid_set_index (nxmin,nxmax,nymin,nymax,nzmin,nzmax,grid)

      if (pbc%is_on) then  
         maxdfft = (nxmax-nxmin+1)*(nymax-nymin+1)*(nzmax-nzmin+1)
         call pbc_set_maxdfft (maxdfft, pbc)
      endif

      nxmin = -grid%n1/2
      nxmax = grid%n1 + nxmin -1
      nymin = -grid%n2/2
      nymax = grid%n2 + nymin -1
      nzmin = -grid%n3/2
      nzmax = grid%n3 + nzmin -1
c
c     Assign spatial coordinates (x,y,z) to each array point (i,j,k).
c     To each point, assign a 1d index (the counter is
c     nef). Build the 3d->1d conversion arrays, kx, ky, kz. The
c     actual position (atomic units, cartesian coordinates, relative
c     to the origin) of each grid point is: xx=h*(kx+shift),
c     yy=h*(ky+shift), zz=h*(kz+shift).
c
c     start by initializing the 3d -> 1d conversion index
      grid%indexg = 0
c
c     FOR CONFINED SYSTEM ONLY: must drop grid points outside a
c     sphere of radius grid%rmax. For periodic system, use a cut-off
c     radius (rcut) that certainly encompasses the periodic cell.
c
      if (pbc%is_on) then
         rcut = 1.d5 * maxval(grid%step) * max(nxmax,nymax,nzmax)
      else
         rcut = grid%rmax
      endif
      nef = 0
      do i = nxmax, nxmin, -1
         rr(1) = (grid%shift(1) + i)*grid%step(1)
         do j = nymax, nymin, -1
            rr(2) = (grid%shift(2) + j)*grid%step(2)
            do k = nzmax, nzmin, -1
               rr(3) = (grid%shift(3) + k)*grid%step(3)
               dist = sqrt(dot_product(rr,rr))
               if (dist .gt. rcut) cycle
               nef = nef + 1
               grid%indexg(i,j,k) = nef
            enddo
         enddo
      enddo
c
c     The total number of points inside the sphere is the size of the
c     Hamiltonian of the system, ndim. Allocate arrays accordingly.
c
      ndim = nef
      call grid_set_ndim (ndim, grid)
c
c     Define the set of grid points in the irredubible wedge, IW.
c     The IW contains all grid points that can reconstruct the
c     original grid by the application of symmetry operations, and it
c     is such that no two points in the IW are equivalent to each
c     other by the application of any symmetry operation.
c
c     Information about the wedge is contained in arrays indexw,
c     rindex, rtrans:
c     the n-th point in the IW has coordinates i,j,k so that
c     indexw(i,j,k)=n. If indexw(i,j,k)=0, this point is not in the IW
c
c     if rindex(n)=m and rtrans(n)=i, the n-th point in original grid
c     is equivalent to the m-th point in IW upon symmetry operation i.
c
c     dmax stores the maximum distance between a grid point and its
c     equivalent in the wedge after a symmetry operation. Ideally,
c     these two point should coincide, but numerical roundoff or the
c     fractional translation symm%tnp (see subroutine symop) may interfere.
      dmax = zero
      nwedge = 0
      do i = nxmax, nxmin, -1
         rr(1) = (grid%shift(1) + i)*grid%step(1)
         do j = nymax, nymin, -1
            rr(2) = (grid%shift(2) + j)*grid%step(2)
            do k = nzmax, nzmin, -1
               rr(3) = (grid%shift(3) + k)*grid%step(3)
               if (grid%indexg(i,j,k) .eq. 0) cycle
               do itrans = 1, rsymm%ntrans
                  call symop(rsymm,itrans,rr,rw)
                  do ii = 1, 3
                     ir(ii)=nint(rw(ii)/grid%step(ii)-grid%shift(ii))
                     rw(ii)=rw(ii) - 
     1                    (ir(ii) + grid%shift(ii))*grid%step(ii)
                  enddo
                  dist = sqrt(dot_product(rw,rw))
                  dmax = max(dist,dmax)
                  if (dist .gt. 1.d-4) then
                     write(7,*) ' ERROR: grid point displaced from '
     1                    ,'image. Irreducible wedge may be wrong!'
                     write(7,*) ' Must ignore symmetry operations!'
                     write(7,*) rr
                     write(7,*) rw
                     write(7,*) ir
                     write(7,*) itrans
                     ierr = 1
                     return
                  endif
                  itest = grid%indexw(ir(1),ir(2),ir(3))
                  if ( itest .ne. 0) exit
               enddo
               if (itest .eq. 0) then
                  nwedge = nwedge + 1
                  grid%indexw(i,j,k) = nwedge
                  itest = nwedge
                  itrans = 1
               endif
               grid%rindex(grid%indexg(i,j,k)) = itest
               grid%rtrans(grid%indexg(i,j,k)) = itrans
            enddo
         enddo
      enddo
      grid%nwedge = nwedge

      call grid_set_wedge (nord2, grid)

      ldn = nwedge/nnodes + nnodes
      parallel%ldn = ldn
      parallel%ndim = ndim
      parallel%nwedge = nwedge

      write(7,*)
      write(7,*) 'Setup messages:'
      write(7,*) '---------------'
      write(7,*) 'The Hamiltonian matrix size is', ndim
      write(7,*) ' reduced size is ',nwedge
      write(7,*) ' maximum distance between grid points'
     1     ,' and their images is'
      write(7,'(2x,g10.4,a)') dmax,'  [bohr]'

      write(7,*)
      write(7,15) 6*nord2 
 15   format(1x,'There are',1x,i2,1x,
     1     'laplacian-related non-diagonal elements per row') 
c
c     PART2: Partitioning The Domain
c
c     At this point we are ready to do any kind of partitioning
c     geometrical or based on the nwedge-array.
c     Part(i,j,k,..) = processor number that the (i,j,k) resides on
c
      Irows = 0
c
c     Find how many rows each processor has and how many equivalent
c     points each grid point in IW has.
c     Note: since grid%indexw already has information about which
c     points to keep, we can run over (-nx,nx),(-ny,ny),(-nz,nz) for
c     periodic and non-periodic boundary conditions. For equivalent
c     points, search over all grid points where indexg in non-zero,
c     since those ones define the full grid.
c
      allocate(grid%ist(0:parallel%procs_num))
      allocate (ninv(grid%nwedge),stat=alcstat)
      call alccheck('ninv',grid%ndim,alcstat)
      ninv = 0
      allocate (rinv(rsymm%ntrans,grid%nwedge),stat=alcstat)
      call alccheck('rinv',rsymm%ntrans*grid%nwedge,alcstat)
      do i = nxmax, nxmin, -1
         do j = nymax, nymin, -1
            do k = nzmax, nzmin, -1
               if (grid%indexg(i,j,k).ne.0) then
                  ii = grid%rindex(grid%indexg(i,j,k))
                  ninv(ii) = ninv(ii) + 1
                  rinv(ninv(ii),ii) = grid%indexg(i,j,k)
                  grid%fx(grid%indexg(i,j,k)) = i
                  grid%fy(grid%indexg(i,j,k)) = j
                  grid%fz(grid%indexg(i,j,k)) = k
               endif
               if (grid%indexw(i,j,k).ne.0) then
                  Irows(Part(grid,parallel%procs_num,i,j,k)) = 
     1                 Irows(Part(grid,parallel%procs_num,i,j,k)) + 1
               endif
            enddo
         enddo
      enddo
      if (maxval(ninv) .gt. rsymm%ntrans .or. minval(ninv) .lt.
     1     rsymm%ntrans) then
         write(7,*) 'ERROR: grid points in irreducible wedge '
     1        ,'have weights'
         write(7,*) 'different from the order of the reduced group'
         write(7,*) ' minimum and maximum values of weight ='
     1        ,minval(ninv),maxval(ninv)
         write(7,*) ' order of the reduced group = ',rsymm%ntrans
         ierr = 1
         return
      endif
c
c     Transform Irows to show where the first row of each node
c     begins. I.e., 3 nodes: first has 10 rows, second has 5 rows,
c     third 6 rows Irows(0) = 1, Irows(1) = 11, Irows(2) = 16,
c     Irows(3) = 22.
c     Used for permuting the rows later.
c
      Irows(nnodes) = nwedge + 1
      do i = nnodes-1, 0, -1
         Irows(i) = Irows(i+1) - Irows(i)
      enddo
c
c     Permutation so that rows on the same processors appear in
c     adjacent positions in the indexw numbering. Perform a
c     "stiching" movement through space, in order to ensure that
c     grid points with close (i,j,k) indices are also physically
c     close.
c
      j00 = -1
      k00 = -1
      do i = nxmin,nxmax
         j00 = -j00
         if (j00.eq.1) then
            ny1 = nymin
            ny2 = nymax
         else
            ny1 = nymax
            ny2 = nymin
         endif
         do j = ny1, ny2, j00
            k00 = -k00
            if (k00.eq.1) then
               nz1 = nzmin
               nz2 = nzmax
            else
               nz1 = nzmax
               nz2 = nzmin
            endif
            do k = nz1, nz2, k00
               if (grid%indexw(i,j,k).ne.0) then
                  iold = grid%indexw(i,j,k)
                  inode = Part(grid,parallel%procs_num,i,j,k)
                  nef           = Irows( inode )
                  grid%indexw(i,j,k) = nef
                  do ii = 1, rsymm%ntrans
                     grid%rindex(rinv(ii,iold)) = nef
                  enddo
                  Irows( inode )= Irows( inode ) + 1
                  grid%kx(nef)  = i
                  grid%ky(nef)  = j
                  grid%kz(nef)  = k
               endif
            enddo
         enddo
      enddo
      deallocate(ninv, rinv)
c
c     Restoring Irows so that they point at the first row of each
c     processor
c
      Irows(nnodes) = grid%nwedge + 1
      do i = nnodes -1, 1,-1
         Irows(i) = Irows(i-1)
      enddo
      Irows(0) = 1
c
c     Transfer Irows to parallel structure
c
      parallel%Irows = Irows
c
c     set the indexes for grid points outside the domain to be nwedge+1
c
      if (.not. pbc%is_on) then
c
c     if in a sphere geometry (confined system) then:
c     set the 3d->1d index, indexw, explicitly to nwedge+1 (it was zero
c     before) for all points outside the boundary sphere
c
         do k = nzmin - nord2,nzmax + nord2
            do j = nymin - nord2,nymax + nord2
               do i = nxmin - nord2,nxmax + nord2
                  if (grid%indexw(i,j,k) .eq. 0) 
     1                 grid%indexw(i,j,k) = grid%nwedge + 1
                  if (grid%indexg(i,j,k) .eq. 0) 
     1                 grid%indexg(i,j,k) = grid%ndim + 1
               enddo
            enddo
         enddo

         grid%rindex(grid%ndim+1) = grid%nwedge + 1
         grid%rtrans(grid%ndim+1) = 1
c
c     if in a periodic system, points must be "wrapped around" the
c     boundary so that points on opposite sides of the box are
c     actually recognized as neighbors. For that, we go over
c     -nx-nord2 to -nx-1 and nx to nx+nord2-1 (and so on in the y
c     and z directions), thus finding all the neighbors of points
c     that have neighbors that are "wrapped" on the other side of the
c     box. For each point, we go over 6 combinations in all,
c     coresponding to the 6 faces of the cube. Every time i00, j00
c     and k00 are flipped in sign accordingly so that all the 6
c     combinations are covered. In any one of these combinations, the
c     elements are mapped to the corresponding element on the
c     other side of the boundary (i.e., shifted by 2*nx and so on).
c
      else
c     ---- -x direction
         iwrap = grid%n1
         do i = nxmin-nord2,nxmin-1
            do j = nymin,nymax
               do k = nzmin,nzmax
                  grid%indexg(i,j,k) = grid%indexg(i+iwrap,j,k)
               enddo
            enddo
         enddo
c     ---- +x direction
         do i = nxmax+1,nxmax+nord2
            do j = nymin,nymax
               do k = nzmin,nzmax
                  grid%indexg(i,j,k) = grid%indexg(i-iwrap,j,k)
               enddo
            enddo
         enddo
c     ---- -y direction
         iwrap = grid%n2
         do i = nxmin,nxmax
            do j = nymin-nord2,nymin-1
               do k = nzmin,nzmax
                  grid%indexg(i,j,k) = grid%indexg(i,j+iwrap,k)
               enddo
            enddo
         enddo
c     ---- +y direction
         do i = nxmin,nxmax
            do j = nymax+1,nymax+nord2
               do k = nzmin,nzmax
                  grid%indexg(i,j,k) = grid%indexg(i,j-iwrap,k)
               enddo
            enddo
         enddo
c     ---- -z direction
         iwrap = grid%n3
         do i = nxmin,nxmax
            do j = nymin,nymax
               do k = nzmin-nord2,nzmin-1
                  grid%indexg(i,j,k) = grid%indexg(i,j,k+iwrap)
               enddo
            enddo
         enddo
c     ---- +z direction
         do i = nxmin,nxmax
            do j = nymin,nymax
               do k = nzmax+1,nzmax+nord2
                  grid%indexg(i,j,k) = grid%indexg(i,j,k-iwrap)
               enddo
            enddo
         enddo

c     finally, define grid shift in units of lattice vector
         pbc%mx = minval(grid%fx)
         pbc%my = minval(grid%fy)
         pbc%mz = minval(grid%fz)
         pbc%shift(1) = (grid%shift(1) + real(pbc%mx,dp)) *
     1        grid%step(1) * twopi / pbc%box_size(1)
         pbc%shift(2) = (grid%shift(2) + real(pbc%my,dp)) *
     1        grid%step(2) * twopi / pbc%box_size(2)
         pbc%shift(3) = (grid%shift(3) + real(pbc%mz,dp)) *
     1        grid%step(3) * twopi / pbc%box_size(3)
         write(7,*) 'pbc shift indices = ',pbc%mx,pbc%my,pbc%mz
         write(7,'(a,3f10.4)') 
     1        ' pbc shift [latt. vect. units] = ',pbc%shift/twopi

      endif                     !   pbc%is_on

      end subroutine grid_partition
c     ===============================================================
c
c     Simple partitioning routine.
c     Split the ndim-long vector into nnodes equal parts.
c
c     ---------------------------------------------------------------
      integer function Part(grid,nnodes,i,j,k)

      use constants
      use grid_module
      implicit none
c
c     Input/Output variables:
c
      type (grid_data), intent(in) :: grid
      integer, intent(in) :: nnodes
      integer, intent(in) :: i,j,k

      integer ndim,nef
      integer ii
      integer isize,remainder
      logical, save :: first = .true.
c     ---------------------------------------------------------------
      ndim = grid%nwedge

      if (first) then
c     the first time create the partitioning before choosing proc for
c     (i,j,k)
         first = .false.
         isize = ndim / nnodes
         remainder = ndim - isize*nnodes
c     distribute (ndim) points so that the first PEs receive
c     (isize+1) points and the last ones receive (isize)
         grid%ist(0) = 1
         do ii = 1, remainder
            grid%ist(ii) = grid%ist(ii-1) + isize + 1
         enddo
         do ii = remainder+1,nnodes-1
            grid%ist(ii) = grid%ist(ii-1) + isize
         enddo
         grid%ist(nnodes) = ndim + 1
      endif
c
c     Give the processor number for the current i,j,k
c     For this routine partition according to rows:
c     (ijk) -> Indexw(ijk)
c
      nef = grid%indexw(i,j,k)
      do ii = 0, nnodes-1
         if (nef.ge.grid%ist(ii) .and. nef.lt.grid%ist(ii+1)) then
            Part = ii
            return
         endif
      enddo

      write(7,*) 'The row nef:',nef,'is outside bounds ndim:',ndim

      end function Part
c     ===============================================================
