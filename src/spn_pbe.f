c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c     This is a slightly modified version of EASYPBE, a driver for
c     the PBE subroutines, using simple inputs.
c     author: K. Burke, May 14, 1996.
c     Subroutines upgraded to the fortran 95 standard by M. Tiago,
c     February 19, 2005.
c
c     ---------------------------------------------------------------
      subroutine spn_pbe(up,agrup,delgrup,uplap,dn,agrdn,delgrdn,dnlap, 
     1     agr,delgr,lcor,lpot,
     2     exlsd,vxuplsd,vxdnlsd,eclsd,vcuplsd,vcdnlsd,
     3     expw91,vxuppw91,vxdnpw91,ecpw91,vcuppw91,vcdnpw91,
     4     expbe,vxuppbe,vxdnpbe,ecpbe,vcuppbe,vcdnpbe)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     up,                  ! up density
     2     agrup,               ! |grad up|
     3     delgrup,             ! (grad up).(grad |grad up|)
     4     uplap,               ! grad^2 up=Laplacian of up
     5     dn,agrdn,delgrdn,dnlap,
                                ! corresponding for down density
     6     agr,                 ! |grad rho|
     7     delgr                ! (grad rho).(grad |grad rho|)
      integer, intent(in) :: 
     1     lcor,                ! flag to do correlation(=0=>don't) 
     2     lpot                 ! flag to do potential(=0=>don't)
      real(dp), intent(out) ::
     1     exlsd,               ! LSD exchange energy density, so that
                                ! ExLSD=int d^3r rho(r) exlsd(r)
     2     vxuplsd,             ! up LSD exchange potential
     3     vxdnlsd,             ! down LSD exchange potential
     4     eclsd,               ! LSD correlation energy density
     5     vcuplsd,             ! up LSD correlation potential
     6     vcdnlsd,             ! down LSD correlation potential
     7     expw91,vxuppw91,vxdnpw91,ecpw91,vcuppw91,vcdnpw91,
                                ! corresponding PW91 quantities
     8     expbe,vxuppbe,vxdnpbe,ecpbe,vcuppbe,vcdnpbe
                                ! corresponding PBE quantities
c
c     Work variables:
c
      real(dp) :: alfc,dvcdn,dvcup,ec,ecrs,eczet,exdnlsd,exdnpbe
     1     ,exdnpw91,exuppbe,exuplsd,exuppw91,fk,g,h,rho,rholap,rho2
     2     ,rs,s,sk,twoksg,t,u,uu,vv,v,vcdn,vcup,ww,zet
c
c     constants: pi32=3 pi**2, alpha=(9pi/4)**thrd
c
      real(dp), parameter :: thrd2=two*third
      real(dp), parameter :: pi32=three*pi*pi
      real(dp), parameter ::
     1     alpha=1.91915829267751300662482032624669d0

c     ---------------------------------------------------------------
c
c     PBE exchange
c     use  Ex[up,dn]=0.5*(Ex[2*up]+Ex[2*dn]) (i.e., exact spin
c     -scaling)
c     do up exchange
c     fk=local Fermi wavevector for 2*up=(3 pi^2 (2up))^(1/3)
c     s=dimensionless density gradient=|grad rho|/ (2*fk*rho)_(rho=2*up)
c     u=delgrad/(rho^2*(2*fk)**3)_(rho=2*up) 
c     v=Laplacian/(rho*(2*fk)**2)_(rho=2*up) 
c
      rho2=two*up
      if(rho2.gt.1d-18)then
         fk=(pi32*rho2)**third
         s=two*agrup/(two*fk*rho2)
         u=four*delgrup/(rho2*rho2*(two*fk)**3)
         v=two*uplap/(rho2*(two*fk)**2)

         call spn_exchpbe(rho2,s,u,v,0,lpot,exuplsd,vxuplsd)
         call exchpw91(rho2,s,u,v,exuppw91,vxuppw91)
         call spn_exchpbe(rho2,s,u,v,1,lpot,exuppbe,vxuppbe)
      else
         exuplsd=zero
         vxuplsd=zero
         exuppw91=zero
         vxuppw91=zero
         exuppbe=zero
         vxuppbe=zero
      endif
c     repeat for down 
      rho2=two*dn
      if(rho2.gt.1d-18)then
         fk=(pi32*rho2)**third
         s=two*agrdn/(two*fk*rho2)
         u=four*delgrdn/(rho2*rho2*(two*fk)**3)
         v=two*dnlap/(rho2*(two*fk)**2)

c     note: 2 following lines are missing in pbe.f (adi)
         call spn_exchpbe(rho2,s,u,v,0,lpot,exdnlsd,vxdnlsd)
         call exchpw91(rho2,s,u,v,exdnpw91,vxdnpw91)
         call spn_exchpbe(rho2,s,u,v,1,lpot,exdnpbe,vxdnpbe)
      else

c     note: 4 following lines are missing in pbe.f (adi)
         exdnlsd=zero
         vxdnlsd=zero
         exdnpw91=zero
         vxdnpw91=zero
         exdnpbe=zero
         vxdnpbe=zero
      endif
c     construct total density and contribution to ex 
c     note: different in pbe.f (adi)
      if(up.gt.1d-18.and.dn.gt.1d-18)then
         rho=up+dn
         exlsd=(exuplsd*up+exdnlsd*dn)/rho
         expw91=(exuppw91*up+exdnpw91*dn)/rho
         expbe=(exuppbe*up+exdnpbe*dn)/rho
      else
         exlsd=zero
         expw91=zero
         expbe=zero
      endif
      if(lcor.eq.0)return
c
c     Now do correlation 
c     zet=(up-dn)/rho 
c     g=phi(zeta) 
c     rs=(3/(4pi*rho))^(1/3)=local Seitz radius=alpha/fk 
c     sk=Ks=Thomas-Fermi screening wavevector=sqrt(4fk/pi) 
c     twoksg=2*Ks*phi 
c     t=correlation dimensionless gradient=|grad rho|/(2*Ks*phi*rho) 
c     uu=delgrad/(rho^2*twoksg^3) 
c     rholap=Laplacian 
c     vv=Laplacian/(rho*twoksg^2) 
c     ww=(|grad up|^2-|grad dn|^2-zet*|grad rho|^2)/(rho*twoksg)^2 
c     ec=LSD correlation energy 
c     vcup=LSD up correlation potential 
c     vcdn=LSD down correlation potential 
c     h=gradient correction to correlation energy 
c     dvcup=gradient correction to up correlation potential 
c     dvcdn=gradient correction to down correlation potential 
c
      if (rho.lt.1.d-18) return
      zet=(up-dn)/rho
      g=((one+zet)**thrd2+(one-zet)**thrd2)/two
      fk=(pi32*rho)**third
      rs=alpha/fk
      sk=sqrt(four*fk/pi)
      twoksg=two*sk*g
      t=agr/(twoksg*rho)
      uu=delgr/(rho*rho*twoksg**3)
      rholap=uplap+dnlap
      vv=rholap/(rho*twoksg**2)
      ww=(agrup**2-agrdn**2-zet*agr**2)/(rho*rho*twoksg**2)
      call spn_corpbe(rs,zet,t,uu,vv,ww,1,lpot,ec,vcup,vcdn,
     1     h,dvcup,dvcdn)
      eclsd=ec
      ecpbe=ec+h
      vcuplsd=vcup
      vcdnlsd=vcdn
c     note: 7 following lines are missing in pbe.f
      vcuppbe=vcup+dvcup
      vcdnpbe=vcdn+dvcdn
      call corlsd(rs,zet,ec,vcup,vcdn,ecrs,eczet,alfc)
      call corpw91(rs,zet,g,ec,ecrs,eczet,t,uu,vv,ww,h,dvcup,dvcdn)
      ecpw91=ec+h
      vcuppw91=vcup+dvcup
      vcdnpw91=vcdn+dvcdn

      end subroutine spn_pbe
c     ===============================================================
c
c     PBE EXCHANGE FOR A SPIN-UNPOLARIZED ELECTRONIC SYSTEM 
c     K Burke's modification of PW91 codes, May 14, 1996 
c     Modified again by K. Burke, June 29, 1996, with simpler Fx(s) 
c
c     (for u,v, see PW86(24)) 
c
c     References: 
c     [a]J.P.~Perdew, K.~Burke, and M.~Ernzerhof, PRL 77, 3865 (1996)
c     [b]J.P. Perdew and Y. Wang, Phys. Rev.  B 33,  8800 (1986);
c     40,  3399  (1989) (E). 
c
c     Formulas: 
c         e_x[unif]=ax*rho^(4/3)  [LDA] 
c         ax = -0.75*(3/pi)^(1/3) 
c         e_x[PBE]=e_x[unif]*FxPBE(s) 
c         FxPBE(s)=1+uk-uk/(1+ul*s*s)                 [a](13) 
c     uk, ul defined after [a](13) 
c
c     ---------------------------------------------------------------
      subroutine spn_exchpbe(rho,s,u,v,lgga,lpot,ex,vx) 

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     rho,                 ! density
     2     s,                   ! ABS(GRAD rho)/(2*KF*rho), where kf=(3 pi^2 rho)^(1/3)
     3     u,                   ! (GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*KF)**3)
     4     v                    ! (LAPLACIAN rho)/(rho*(2*KF)**2)
      integer, intent(in) :: 
     1     lgga,                ! (=0=>don't put in gradient corrections, just LDA)
     2     lpot                 ! (=0=>don't get potential and don't need U and V)
      real(dp), intent(out) ::
     1     ex,                  ! exchange energy per electron
     2     vx                   ! potential
c
c     Work variables:
c
      real(dp) :: uk,p0,s2,fxpbe,fss,fs,exunif,ul

      real(dp), parameter :: thrd4 = four/three
      real(dp), parameter ::
     1     ax = -0.738558766382022405884230032680836d0
      real(dp), parameter :: um=0.2195149727645171d0

c     ---------------------------------------------------------------

c     note: in pbe.f uk is defined as parameter: uk=0.8040d0
      uk = 0.8040d0
      ul=um/uk
c
c     construct LDA exchange energy density 
      exunif = AX*rho**third

c     note: this part is missing in pbe.f (adi)
      if(lgga.eq.0)then
         ex=exunif
         vx=ex*thrd4
         return
      endif
c
c     construct PBE enhancement factor 
      S2 = S*S
      P0=one+ul*S2
      FxPBE = 1d0+uk-uk/P0
      EX = exunif*FxPBE
c     note: the following line is missing in pbe.f (adi)
      if(lpot.eq.0)return
c
c     energy done. now the potential: 
c     find first and second derivatives of Fx w.r.t s. 
c     Fs=(1/s)*d FxPBE/ ds 
c     Fss=d Fs/ds 
      Fs=two*uk*ul/(P0*P0)
      Fss=-four*ul*S*Fs/P0
c
c     calculate potential from [b](24) 
      vx = exunif*(thrd4*fxpbe-(u-thrd4*s2*s)*fss-v*fs)

      end subroutine spn_exchpbe
c     ===============================================================
c
c     Official PBE correlation code. K. Burke, May 14, 1996. 
c
c     References: 
c       [a] J.P.~Perdew, K.~Burke, and M.~Ernzerhof, PRL 77, 3865 (1996)
c       [b] J. P. Perdew, K. Burke, and Y. Wang, {\sl Real-space cutoff 
c           construction of a generalized gradient approximation:  The PW91 
c           density functional}, submitted to Phys. Rev. B, Feb. 1996. 
c       [c] J. P. Perdew and Y. Wang, Phys. Rev. B {45, 13244 (1992). 
c
c     ---------------------------------------------------------------
      subroutine spn_corpbe(rs,zet,t,uu,vv,ww,lgga,lpot,ec,vcup,vcdn
     1     ,h,dvcup,dvcdn)
      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     rs,                  ! Seitz radius=(3/4pi rho)^(1/3)
     2     zet,                 ! relative spin polarization = (rhoup-rhodn)/rho
     3     t,                   ! ABS(GRAD rho)/(rho*2.*KS*G)  -- only needed for PBE 
     4     uu,                  ! (GRAD rho)*GRAD(ABS(GRAD rho))/(rho**2 * (2*KS*G)**3)
     5     vv,                  ! (LAPLACIAN rho)/(rho * (2*KS*G)**2)
     6     ww                   ! (GRAD rho)*(GRAD ZET)/(rho * (2*KS*G)**2 
                                ! uu,vv,ww, only needed for PBE potential
      integer, intent(in) ::
     1     lgga,                ! flag to do gga (0=>LSD only)
     2     lpot                 ! flag to do potential (0=>energy only)
      real(dp), intent(out) ::
     1     ec,                  ! LSD correlation energy from [a]
     2     vcup,                ! LSD up correlation potential
     3     vcdn,                ! LSD dn correlation potential
     4     h,                   ! nonlocal part of correlation energy per electron
     5     dvcup,               ! nonlocal correction to vcup
     6     dvcdn                ! nonlocal correction to vcdn
c
c     Work variables:
c
c     thrd*=various multiples of 1/3 
c     numbers for use in LSD energy spin-interpolation formula, [c](9). 
c            GAM= 2^(4/3)-2 
c            FZZ=f''(0)= 8/(9*GAM) 
c     numbers for construction of PBE 
c            gamma=(1-log(2))/pi^2 
c            bet=coefficient in gradient expansion for correlation, [a](4). 
c            eta=small number to stop d phi/ dzeta from blowing up at 
c                |zeta|=1. 
c
      real(dp) :: alfc,alfm,alfrsm,b,b2,bec,bg,comm,ecrs,eczet,
     1     ep,eprs,eu,eurs,f,fac,fact0,fact1,fact2,fact3,fact4,fact5,
     2     fz,g,g3,g4,q5,gz,hb,hbt,hrs,hrst,ht,htt,hz,hzt,pon,pref,
     3     q4,q8,q9,rsthrd,rtrs,t2,t4,t6,rs2,rs3,z4

      real(dp), parameter :: thrdm=-third
      real(dp), parameter :: thrd2=two*third
      real(dp), parameter :: sixthm=thrdm/two
      real(dp), parameter :: thrd4=four*third
      real(dp), parameter :: gam=0
     1     .5198420997897463295344212145565d0
      real(dp), parameter :: fzz=8.d0/(9.d0*GAM)
      real(dp), parameter :: gamma=0
     1     .03109069086965489503494086371273d0
      real(dp), parameter :: bet=0.06672455060314922d0
      real(dp), parameter :: delt=bet/gamma
      real(dp), parameter :: eta=1.d-12
c     ---------------------------------------------------------------
c
c     find LSD energy contributions, using [c](10) and Table I[c]. 
c       eu=unpolarized LSD correlation energy 
c       eurs=deu/drs 
c       ep=fully polarized LSD correlation energy 
c       eprs=dep/drs 
c       alfm=-spin stiffness, [c](3). 
c       alfrsm=-dalpha/drs 
c       f=spin-scaling factor from [c](9). 
c     construct ec, using [c](8) 
c
      rtrs=sqrt(rs)
      call spn_gcor2(0.0310907d0,0.21370d0,7.5957d0,3.5876d0,1.6382d0
     1     ,0.49294d0,rtrs,eu,eurs)
      call spn_gcor2(0.01554535d0,0.20548d0,14.1189d0,6.1977d0,3
     1     .3662d0,0.62517d0,rtrs,ep,eprs)
      call spn_gcor2(0.0168869d0,0.11125d0,10.357d0,3.6231d0,0
     1     .88026d0,0.49671d0,rtrs,alfm,alfrsm)
      alfc = -alfm
      z4 = zet**4
      f=((one+zet)**thrd4+(one-zet)**thrd4-two)/gam
      ec = eu*(one-f*z4)+ep*f*z4-alfm*f*(one-z4)/fzz
c
c     LSD potential from [c](a1) 
c          ecrs = dec/drs [c](a2) 
c          eczet=dec/dzeta [c](a3) 
c          fz = df/dzeta [c](a4) 
c
      ecrs = eurs*(one-f*z4)+eprs*f*z4-alfrsm*f*(one-z4)/fzz
      fz = thrd4*((one+zet)**third-(one-zet)**third)/gam
      eczet = four*(zet**3)*f*(ep-eu+alfm/fzz)+fz*(z4*ep-z4*eu
     1     -(one-z4)*alfm/fzz)
      comm = ec -rs*ecrs/three-zet*eczet
      vcup = comm + eczet
      vcdn = comm - eczet

c     note: in pbe.f the following line is missing (adi)
      if(lgga.eq.0)return
c
c     PBE correlation energy 
c         g=phi(zeta), given after [a](3) 
c         delt=bet/gamma 
c         b=a of [a](8)
c
      g=((one+zet)**thrd2+(one-zet)**thrd2)/two
      g3 = g**3
      pon=-ec/(g3*gamma)
      b = delt/(exp(pon)-one)
      b2 = b*b
      t2 = t*t
      t4 = t2*t2
      rs2 = rs*rs
      rs3 = rs2*rs
      q4 = one+b*t2
      q5 = one+b*t2+b2*t4
      h = g3*(bet/delt)*log(one+delt*q4*t2/q5)
c     note: in pbe.f the following line is missing (adi)
      if(lpot.eq.0)return
c
c     energy done. now the potential, using appendix E of [b]. 
      g4 = g3*g
      t6 = t4*t2
      rsthrd = rs/three
      gz=(((one+zet)**2+eta)**sixthm-
     1     ((one-zet)**2+eta)**sixthm)/three
      fac = delt/b+one
      bg = -three*b2*ec*fac/(bet*g4)
      bec = b2*fac/(bet*g3)
      q8 = q5*q5+delt*q4*q5*t2
      q9 = one+two*b*t2
      hb = -bet*g3*b*t6*(two+b*t2)/q8
      hrs = -rsthrd*hb*bec*ecrs
      fact0 = two*delt-6.d0*b
      fact1 = q5*q9+q4*q9*q9
      hbt = two*bet*g3*t4*((q4*q5*fact0-delt*fact1)/q8)/q8
      hrst = rsthrd*t2*hbt*bec*ecrs
      hz = three*gz*h/g + hb*(bg*gz+bec*eczet)
      ht = two*bet*g3*q9/q8
      hzt = three*gz*ht/g+hbt*(bg*gz+bec*eczet)
      fact2 = q4*q5+b*t2*(q4*q9+q5)
      fact3 = two*b*q5*q9+delt*fact2
      htt = four*bet*g3*t*(two*b/q8-(q9*fact3/q8)/q8)
      comm = h+hrs+hrst+t2*ht/6.d0+7.d0*t2*t*htt/6.d0
      pref = hz-gz*t2*ht/g
      fact5 = gz*(two*ht+t*htt)/g
      comm = comm-pref*zet-uu*htt-vv*ht-ww*(hzt-fact5)
      dvcup = comm + pref
      dvcdn = comm - pref

      end subroutine spn_corpbe
c     ===============================================================
c
c     Slimmed down version of gcor used in PW91 routines, to
c     interpolate LSD correlation energy, as given by (10) of 
c     J. P. Perdew and Y. Wang, Phys. Rev. B 45, 13244 (1992). 
c     K. Burke, May 11, 1996. 
c
      subroutine spn_gcor2(a,a1,b1,b2,b3,b4,rtrs,gg,ggrs) 

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) :: a,a1,b1,b2,b3,b4,rtrs
      real(dp), intent(out) :: gg,ggrs
c
c     Work variables:
c
      real(dp) :: q0,q1,q2,q3

      q0 = -two*a*(one+a1*rtrs*rtrs)
      q1 = two*a*rtrs*(b1+rtrs*(b2+rtrs*(b3+b4*rtrs)))
      q2 = log(one+one/q1)
      gg = q0*q2
      q3 = a*(b1/rtrs+two*b2+rtrs*(three*b3+four*b4*rtrs))
      ggrs = -two*a*a1*q2-q0*q3/(q1*(one+q1))

      end subroutine spn_gcor2
c     ===============================================================
c
c     gga91 exchange for a spin-unpolarized electronic system 
c
c     ---------------------------------------------------------------
      subroutine exchpw91(d,s,u,v,ex,vx) 

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     d,                   ! density
     2     s,                   ! ABS(GRAD d)/(2*kf*d)
     3     u,                   ! (GRAD d)*GRAD(ABS(GRAD d))/(d**2 * (2*kf)**3)
     4     v                    ! (LAPLACIAN d)/(d*(2*kf)**2)

      real(dp), intent(out) ::
     1     ex,                  ! exchange energy per electron
     2     vx                   ! potential
c
c     Work variables:
c
      real(dp) :: f,fac,fs,fss,
     1     p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,s2,s3,s4

      real(dp), parameter :: a1=0.19645d0
      real(dp), parameter :: a2=0.27430d0
      real(dp), parameter :: a3=0.15084d0
      real(dp), parameter :: a4=100.d0
      real(dp), parameter :: ax=-0.7385588d0
      real(dp), parameter :: a=7.7956d0
      real(dp), parameter :: b1=0.004d0
      real(dp), parameter :: thrd4 = four/three
c     ---------------------------------------------------------------
c     for Becke exchange, set a3=b1=0
      fac = ax*d**third
      s2 = s*s
      s3 = s2*s
      s4 = s2*s2
      p0 = one/sqrt(one+a*a*s2)
      p1 = log(a*s+one/p0)
      p2 = exp(-a4*s2)
      p3 = one/(one+a1*s*p1+b1*s4)
      p4 = one+a1*s*p1+(a2-a3*p2)*s2
      f = p3*p4
      ex = fac*f
c     local exchange option 
c     ex = fac 
c     energy done. now the potential: 
      p5 = b1*s2-(a2-a3*p2)
      p6 = a1*s*(p1+a*s*p0)
      p7 = two*(a2-a3*p2)+two*a3*a4*s2*p2-four*b1*s2*f
      fs = p3*(p3*p5*p6+p7)
      p8 = two*s*(b1-a3*a4*p2)
      p9 = a1*p1+a*a1*s*p0*(three-a*a*s2*p0*p0)
      p10 = four*a3*a4*s*p2*(two-a4*s2)-8.d0*b1*s*f-four*b1*s3*fs
      p11 = -p3*p3*(a1*p1+a*a1*s*p0+four*b1*s3)
      fss = p3*p3*(p5*p9+p6*p8)+two*p3*p5*p6*p11+p3*p10+p7*p11
      vx = fac*(thrd4*f-(u-thrd4*s3)*fss-v*fs)
c     local exchange option: 
c     vx = fac*thrd4 

      end subroutine exchpw91
c     ===============================================================
c
c     uniform-gas correlation of Perdew and Wang 1991 
c
c     ---------------------------------------------------------------
      subroutine corlsd(rs,zet,ec,vcup,vcdn,ecrs,eczet,alfc) 

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     zet,                 ! relative spin polarization
     2     rs                   ! Seitz radius
      real(dp), intent(out) ::
     1     ec,                  ! correlation energy per electron
     2     vcup,vcdn,           ! up- and down-spin potentials
     4     ecrs,                ! derivative of ec w.r.t. rs
     5     eczet,               ! derivative of ec w.r.t. zet
     6     alfc                 ! correlation contribution to the spin stiffness
c
c     Work variables:
c
      real(dp) :: alfm,alfrsm,comm,ep,eprs,eu,eurs,f,z4,fz

      real(dp), parameter :: gam=0.5198421d0
      real(dp), parameter :: fzz=1.709921d0
      real(dp), parameter :: thrd4=four/three
c     ---------------------------------------------------------------
      f = ((one+zet)**thrd4+(one-zet)**thrd4-two)/gam
      call spn_gcor(0.0310907d0,0.21370d0,7.5957d0,3.5876d0,1.6382d0,
     1     0.49294d0,1.00d0,rs,eu,eurs)
      call spn_gcor(0.01554535d0,0.20548d0,14.1189d0,6.1977d0,3
     1     .3662d0,0.62517d0,1.00d0,rs,ep,eprs)
      call spn_gcor(0.0168869d0,0.11125d0,10.357d0,3.6231d0,0.88026d0
     1     ,0.49671d0,1.00d0,rs,alfm,alfrsm)
c     alfm is minus the spin stiffness alfc 
      alfc = -alfm
      z4 = zet**4
      ec = eu*(one-f*z4)+ep*f*z4-alfm*f*(one-z4)/fzz
c     energy done. now the potential: 
      ecrs = eurs*(one-f*z4)+eprs*f*z4-alfrsm*f*(one-z4)/fzz
      fz = thrd4*((one+zet)**third-(one-zet)**third)/gam
      eczet = four*(zet**3)*f*(ep-eu+alfm/fzz)+fz*(z4*ep-z4*eu
     1     -(one-z4)*alfm/fzz)
      comm = ec -rs*ecrs/three-zet*eczet
      vcup = comm + eczet
      vcdn = comm - eczet

      end subroutine corlsd
c     ===============================================================
c
c     This subroutine computes the local correlation energy and
c     potential for the Perdew-Wang exchange-correlation scheme.
c
c     ---------------------------------------------------------------
      subroutine spn_gcor(a,a1,b1,b2,b3,b4,p,rs,gg,ggrs) 

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) :: a,a1,b1,b2,b3,b4,p,rs
      real(dp), intent(out) :: gg,ggrs
c
c     Work variables:
c
      real(dp) :: p1,q0,rsp,q1,q2,q3,rs12,rs32
c     ---------------------------------------------------------------
      p1 = p + 1.d0
      q0 = -two*a*(one+a1*rs)
      rs12 = sqrt(rs)
      rs32 = rs12**3
      rsp = rs**p
      q1 = two*a*(b1*rs12+b2*rs+b3*rs32+b4*rs*rsp)
      q2 = log(one+one/q1)
      gg = q0*q2
      q3 = a*(b1/rs12+two*b2+three*b3*rs12+two*b4*p1*rsp)
      ggrs = -two*a*a1*q2-q0*q3/(q1**2+q1)

      end subroutine spn_gcor
c     ===============================================================
c
c     PW91 correlation, modified by K. Burke to put all arguments 
c     as variables in calling statement, rather than in common block 
c     May, 1996. 
c
c     ---------------------------------------------------------------
      subroutine corpw91(rs,zet,g,ec,ecrs,eczet,t,uu,vv,ww,h, 
     1     dvcup,dvcdn)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) ::
     1     rs,                  ! Seitz radius
     2     zet,                 ! relative spin polarization
     3     t,                   ! ABS(GRAD d)/(d*2.*ks*g)
     4     uu,                  ! (GRAD d)*GRAD(ABS(GRAD d))/(d**2 * (2*ks*g)**3)
     5     vv,                  ! (LAPLACIAN d)/(d * (2*ks*g)**2)
     6     ww,                  ! (GRAD d)*(GRAD zet)/(d * (2*ks*g)**2
     7     g,ec,ecrs,eczet

      real(dp), intent(out) ::
     1     h,                   ! nonlocal part of correlation energy per electron
     2     dvcup,dvcdn          ! nonlocal parts of correlation potentials
c
c     Work variables:
c
      real(dp) :: b,b2,bec,bet,bg,cc,ccrs,coeff,comm,delt,
     1     fac,fact0,fact1,fact2,fact3,fact4,fact5,g3,g4,gz,h0b,h0bt,
     2     h0rs,h0rst,h0t,h0tt,h0z,h0zt,h1,h1rs,h1rst,h1t,h1tt,h1z,h1zt,
     3     hrs,hrst,ht,htt,hz,hzt,pon,pref,rs2,rs3,q4,q5,q6,q7,q8,q9,
     4     r0,r1,h0,r2,r3,r4,rsthrd,t2,t4,t6

      real(dp), parameter :: xnu=15.75592d0
      real(dp), parameter :: cc0=0.004235d0
      real(dp), parameter :: cx=-0.001667212d0
      real(dp), parameter :: alf=0.09d0
      real(dp), parameter :: c1=0.002568d0
      real(dp), parameter :: c2=0.023266d0
      real(dp), parameter :: c3=7.389d-6
      real(dp), parameter :: c4=8.723d0
      real(dp), parameter :: c5=0.472d0
      real(dp), parameter :: c6=7.389d-2
      real(dp), parameter :: a4=100.d0
      real(dp), parameter :: thrdm=-third
      real(dp), parameter :: thrd2=two/three
c     ---------------------------------------------------------------
      bet = xnu*cc0
      delt = two*alf/bet
      g3 = g**3
      g4 = g3*g
      pon = -delt*ec/(g3*bet)
      b = delt/(exp(pon)-one)
      b2 = b*b
      t2 = t*t
      t4 = t2*t2
      t6 = t4*t2
      rs2 = rs*rs
      rs3 = rs2*rs
      q4 = one+b*t2
      q5 = one+b*t2+b2*t4
      q6 = c1+c2*rs+c3*rs2
      q7 = one+c4*rs+c5*rs2+c6*rs3
      cc = -cx + q6/q7
      r0 = 0.663436444d0*rs
      r1 = a4*r0*g4
      coeff = cc-cc0-three*cx/7.d0
      r2 = xnu*coeff*g3
      r3 = exp(-r1*t2)
      h0 = g3*(bet/delt)*log(one+delt*q4*t2/q5)
      h1 = r3*r2*t2
      h = h0+h1
c     local correlation option: 
c     h = 0.0d0 
c     energy done. now the potential: 
      ccrs = (c2+2.*c3*rs)/q7 - q6*(c4+2.*c5*rs+3.*c6*rs2)/q7**2
      rsthrd = rs/three
      r4 = rsthrd*ccrs/coeff
      gz = ((one+zet)**thrdm - (one-zet)**thrdm)/three
      fac = delt/b+one
      bg = -three*b2*ec*fac/(bet*g4)
      bec = b2*fac/(bet*g3)
      q8 = q5*q5+delt*q4*q5*t2
      q9 = one+two*b*t2
      h0b = -bet*g3*b*t6*(two+b*t2)/q8
      h0rs = -rsthrd*h0b*bec*ecrs
      fact0 = two*delt-6.d0*b
      fact1 = q5*q9+q4*q9*q9
      h0bt = two*bet*g3*t4*((q4*q5*fact0-delt*fact1)/q8)/q8
      h0rst = rsthrd*t2*h0bt*bec*ecrs
      h0z = three*gz*h0/g + h0b*(bg*gz+bec*eczet)
      h0t = two*bet*g3*q9/q8
      h0zt = three*gz*h0t/g+h0bt*(bg*gz+bec*eczet)
      fact2 = q4*q5+b*t2*(q4*q9+q5)
      fact3 = two*b*q5*q9+delt*fact2
      h0tt = four*bet*g3*t*(two*b/q8-(q9*fact3/q8)/q8)
      h1rs = r3*r2*t2*(-r4+r1*t2/three)
      fact4 = two-r1*t2
      h1rst = r3*r2*t2*(two*r4*(one-r1*t2)-thrd2*r1*t2*fact4)
      h1z = gz*r3*r2*t2*(three-four*r1*t2)/g
      h1t = two*r3*r2*(one-r1*t2)
      h1zt = two*gz*r3*r2*(three-11.d0*r1*t2+four*r1*r1*t4)/g
      h1tt = two*r3*r2*r1*t*(-two+r1*t2)
      hrs = h0rs+h1rs
      hrst = h0rst+h1rst
      ht = h0t+h1t
      htt = h0tt+h1tt
      hz = h0z+h1z
      hzt = h0zt+h1zt
      comm = h+hrs+hrst+t2*ht/6.d0+7.d0*t2*t*htt/6.d0
      pref = hz-gz*t2*ht/g
      fact5 = gz*(two*ht+t*htt)/g
      comm = comm-pref*zet-uu*htt-vv*ht-ww*(hzt-fact5)
      dvcup = comm + pref
      dvcdn = comm - pref
c     local correlation option: 
c     dvcup = 0.0d0 
c     dvcdn = 0.0d0 

      end subroutine corpw91
c     ===============================================================
