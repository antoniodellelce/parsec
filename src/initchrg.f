c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine sets up the superposition of the atomic charge
c     densities on the grid, to be used as an initial guess for the
c     self -consistent loop, in case of a fresh calculation.
c
c     This subroutine handles both boundary conditions: periodic and
c     confined.
c
c     WARNING: this subroutine is not fully parallelized!
c
c     ---------------------------------------------------------------
      subroutine initchrg(clust,elec_st,grid,p_pot,parallel,rho
     1     ,is_pbc,alatt)

      use constants
      use cluster_module
      use electronic_struct_module
      use grid_module
      use pseudo_potential_module
      use parallel_data_module
      implicit none 
c
c     Input variables:
c
c     the cluster
      type(cluster), intent(in) :: clust
c     electronic structure
      type (electronic_struct), intent(in) :: elec_st
c     grid related data
      type (grid_data), intent(in) :: grid
c     pseudo_potential related data
      type (pseudo_potential), intent(in) :: p_pot
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel
c     superposition of atomic charges, given on the 3-d grid points
      real(dp), intent(out) :: rho(parallel%mydim,2*elec_st%nspin-1) 
c     PBC flag
      logical, intent(in) :: is_pbc
c     size of side of box, for periodic systems
      real(dp), intent(in) :: alatt(3)
c
c     Work variables:
c
c     total number of electrons calculated from integrating 
c     over the charge density
      real(dp) :: tele
c     work array for master processor
      real(dp), allocatable :: rho_t(:,:)
c     temporary storage variables
      real(dp) :: rindex,delr,acha,dxx,dyy,dzz,coef,xa,ya,za
      integer jok
c     allocation check
      integer alcstat
c     counters
      integer ja,itype,i,ii,iat,icellx,icelly,icellz,iw
c     change:
c     number of replicas of the periodic cell to be used in the
c     construction of non-local spheres around each atom
c     nreplica = 0 if .not. is_pbc
      integer nreplica
c     distance from a grid point to atoms
      real(dp) :: dist_tot,dist_x,dist_y,dist_z,dist

c     ---------------------------------------------------------------

      if (parallel%iammaster) then
c
c     initialize the total core-correction charge to zero
c
         allocate(rho_t(grid%nwedge,2*elec_st%nspin-1),stat=alcstat)
         call alccheck('rho_t',grid%nwedge*(2*elec_st%nspin-1)
     1        ,alcstat)
         rho_t = zero

c     Define parameters for periodic boundary conditions.
         if (is_pbc) then
            nreplica = 1
         else
            nreplica = 0
         endif
c     initialize charge density
         rho = zero

c     initialize ja - counter of all atoms in the system
         ja = 0
c     go over all atom types
         do itype = 1, clust%type_num
c     go over all atoms superimpose the contribution of atomic charge
c     and ionic potential from each atom over each point of the grid
            do iat = 1, clust%natmi(itype)
               ja = ja + 1
               xa = clust%xatm(ja)
               ya = clust%yatm(ja)
               za = clust%zatm(ja)
               do iw = 1, grid%nwedge
c     For an improved guess for the initial charge density, add in
c     contributions from the nearest REPLICAS of the atoms in the
c     original supercell, if periodic boundary conditions are used.
                  do icellx = -nreplica,nreplica
                     do icelly = -nreplica,nreplica
                        do icellz = -nreplica,nreplica
                           dxx = (grid%shift(1) + grid%kx(iw))
     1                          *grid%step(1) - xa
     2                          + real(icellx,dp)*alatt(1)
                           dyy = (grid%shift(2) + grid%ky(iw))
     1                          *grid%step(2) - ya
     2                          + real(icelly,dp)*alatt(2)
                           dzz = (grid%shift(3) + grid%kz(iw))
     1                          *grid%step(3) - za
     2                          + real(icellz,dp)*alatt(3)
                           dist = sqrt(dxx*dxx+dyy*dyy+dzz*dzz)

c     If dist is larger than the largest value of r given in the
c     pseudopotential file, rs(i,itype), set the atomic charge
c     density, acha, to zero. 

                           if (dist .ge. p_pot%rs(p_pot%ns(itype)-1
     1                          ,itype)) then 
                              acha = zero
                           else
c     If dist is smaller than the largest value of r, find the index
c     of dist in the pseudopotential by inverting the logarithmic
c     pseudopotential grid, then interpolate the atomic charge
c     density to determine the charge density on the real-space grid.
c
c     rindex = 1/B(itype)*log(1+1/A(itype)*dist)
c     A = p_pot%par_a
c     B = p_pot%par_b    
                              rindex=one/p_pot%par_b(itype)*
     1                             log(one+one/p_pot%par_a(itype)
     2                             *dist)
                              jok  = int(rindex) + 1
                              delr=(dist-p_pot%rs(jok,itype))/
     1                             (p_pot%rs(jok+1,itype)
     2                             -p_pot%rs(jok,itype))
                              acha = p_pot%rho_r(jok,itype) + delr*
     1                             (p_pot%rho_r(jok+1,itype) - 
     2                             p_pot%rho_r(jok,itype))
                           endif
                           rho_t(iw,1) =  rho_t(iw,1) + acha 
                        enddo
                     enddo
                  enddo
               enddo            ! iw = 1, grid%nwedge
            enddo               ! iat = 1, clust%natmi(itype)
         enddo                  ! itype = 1, clust%type_num
c
c     renormalize the total electron count to the correct number:
c     tele = total electrons = integral of rho over all space
c          = h**3 * sum(pho)
c
         tele = sum(rho_t(:,1))*real(elec_st%nrep,dp)
         tele = tele*grid%hcub
         rho_t(:,1) = elec_st%xele/tele*rho_t(:,1)
         write(7,10) tele
 10      format(' total electrons from atomic density is ',1x,f10.5)
c
c     If spin-polarized, initialize the spin-up and spin-down charge
c     densities to close to half the total initial charge density,
c     but with some small asymmetry to allow spin-polarization to
c     develop. Charge asymmetry has Gaussian weight around the
c     position of atoms. Empirical observation shows that this is
c     usually more efficient than uniform change asymmetry.
         if (elec_st%nspin .eq. 2) then
            do iw = 1,grid%nwedge
               ja = 0
               dist_tot = zero
               do itype = 1, clust%type_num 
                  do iat  = 1, clust%natmi(itype) 
                     ja = ja + 1
                     dist_x=(grid%shift(1) + grid%kx(iw))*
     1                    grid%step(1) -clust%xatm(ja)     
                     dist_y=(grid%shift(2) + grid%ky(iw))*
     1                    grid%step(2) -clust%yatm(ja)
                     dist_z=(grid%shift(3) + grid%kz(iw))*
     1                    grid%step(3) -clust%zatm(ja)
                     dist=dist_x*dist_x+dist_y*dist_y+dist_z*dist_z
                     if(dist .gt. 8.d0) then
                        dist=0.0d0
                     else
                        dist=exp(-dist/two)
                     endif
                     dist_tot=dist_tot + dist/5.25d0
                  enddo         ! iat   = 1, clust%natmi(itype)
               enddo            ! itype = 1, clust%type_num
               if(dist_tot .gt. 0.08d0) dist_tot = 0.08d0
               if(dist_tot .lt. -0.08d0) dist_tot = -0.08d0 
               do ii=2,3
                  coef = half + real(5-2*ii,dp)*dist_tot*5.d0
                  rho_t(iw,ii) = coef*rho_t(iw,1)
                  if (rho_t(iw,ii) .lt. zero) then
                     write(6,*) ' WARNING in initchrg : negative '
     1                    ,'electron density ',rho_t(iw,ii),' at '
                     write(6,*) grid%kx(iw),grid%ky(iw) ,grid%kz(iw)
     1                    ,' ispin = ',ii
                  endif
               enddo
            enddo               ! iw = 1,grid%nwedge
         endif
      endif

c     master PE distributes the ion potential
      do ii = 1, 2*elec_st%nspin - 1
         parallel%ftmp = rho_t(:,ii)
         call export_function(parallel,rho(1,ii))
      enddo
      if (parallel%iammaster) deallocate(rho_t)

      end subroutine initchrg
c     ===============================================================
