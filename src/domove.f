c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine minimizes total energy using either one of 2
c     schemes:
c     (1) steepest descent, moving the atoms in the direction of the
c     force acting on them; or 
c     (2) BFGS (Broyden-Fletcher-Goldfarb-Shanno) algorithm,
c     implemented with the Limited Memory BFGS routines by 
c     J. Nocedal.
c     If instead the user desires to manually give a sequence of
c     coordinates to be used, those coordinates are read from 
c     manual.dat. This is useful for checking energy and force minima
c     along pre-defined paths of atom motion.
c
c     ---------------------------------------------------------------
      subroutine domove(clust,move,etot,bindev,is_pbc,alatt,ipr,ierr)

      use constants
      use cluster_module
      use movement_module

      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     movement data
      type (movement), intent(inout) :: move
c     total energy in Rydberg, total energy per atom in eV
      real(dp), intent(in) :: etot,bindev
c     PBC flag
      logical, intent(in) :: is_pbc
c     size of supercell (used only if is_pbc = .true.)
      real(dp), intent(in) :: alatt(3)
c     printout flag (for BFGS)
      integer, intent(in) :: ipr
c     error flag (for BFGS)
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     counters
      integer i,j,k,ity
c     largest force component found, magnitude of forces
      real(dp) :: fmax, flen
c     number of movable atoms
      integer nmovable
c     negative of atomic forces of the movable atoms
      real(dp), allocatable :: mforce(:,:)
c
c     steepest descent variables:
c     increments of positions
      real(dp) :: disp(3)
c     scaling coefficient in formula of position increments
      real(dp) :: coeff
c     max step(au), min step (au), absolute value of minimum force
      real(dp) :: stepmax, stepmin, abs_fmin
c     wraping of supercell if PBC are used
      real(dp) :: xmax,ymax,zmax,xmin,ymin,zmin,xmov,ymov,zmov,
     1     alattinv(3)
c
c     BFGS variables:
      integer, dimension(:), allocatable :: nbound
      integer nparam,iprint
      real(dp) :: xtol

c     ---------------------------------------------------------------

      nmovable = sum(clust%mvat)
      nparam = 3*nmovable
      xtol = 1.d-15
      if (move%name .eq. BFGS) then
         if (ipr .ge. 3) then
            iprint = 100
         elseif (ipr .ge. 4) then
            iprint = 101
         else
            iprint = 1
         endif
         allocate(nbound(nparam))
         if (move%stepmax .gt. zero) then
            nbound = 2
         else
            nbound = 0
         endif
      endif
c
c     Select the forces on movable atoms, and compute the maximum
c     component (note the negative sign in front of clust%force)
c
      allocate(mforce(3,nmovable))

      j = 0
      fmax = 0.0d0
      do i = 1, clust%atom_num
         if (clust%mvat(i) .eq. 1) then
            j = j + 1
            mforce(:,j) = -clust%force(:,i)
            flen = sqrt(dot_product(mforce(:,j),mforce(:,j)))
            if (flen .gt. fmax) fmax = flen
         endif
      enddo
c
c     report total energy and max force belonging to the input
c     coordinates
c
      write(66,30) bindev, fmax
 30   format('Total Energy = ',f12.5,1x,'[eV/at],'
     1     ,1x,'Max force =',f9.5,1x,'[Ry/au]')
      write(66,*) 
c
c     If maximal force is below the user-supplied required minimal
c     force then set 'minimum found' flag, report to files, and
c     return.
c
      if(fmax.lt.move%fmin) then
         move%min_frc_found = .true.
         write(7,*) ' Local minimum of total energy was found '
         write(66,*) 'Local minimum of total energy was found '
         deallocate(mforce)
         if ( allocated(nbound) ) deallocate(nbound)
         return
      endif
c
c     In first iteration, initialize the coordinates of movable atoms
c     and other parameters.
c
      if (move%num .eq. 0) then
         j = 0
         allocate(move%rcur(3,nmovable))
         do i = 1, clust%atom_num
            if (clust%mvat(i) .eq. 1) then
               j = j + 1
               move%rcur(1,j) = clust%xatm(i)
               move%rcur(2,j) = clust%yatm(i)
               move%rcur(3,j) = clust%zatm(i)
            endif
         enddo
c     for BFGS
         if (move%name .eq. BFGS) then
            allocate(move%lower(3*nmovable))
            allocate(move%upper(3*nmovable))
            allocate(move%ibfgs(44+9*nmovable))
            allocate(move%wbfgs((2*move%mmax + 4)*3*nmovable + 12
     1           *move%mmax*move%mmax + 12*move%mmax))
            allocate(move%lbfgs(4))
            allocate(move%dbfgs(29))

            if (move%is_restart) then
               write(7,*) ' RESTART LBFGS FROM PREVIOUS RUN'
               open(45,file='relax_restart.dat',form='formatted'
     1              ,status='old')
               do j=1,clust%atom_num + 4
                  read(45,*)
               enddo
               read(45,*) move%mmax
               read(45,'(60a)') move%cbfgs(1:60)
               read(45,'(60a)') move%cbfgs(61:120)
               do j=1,4
                  read(45,*) move%lbfgs(j)
               enddo
               do j=1,3*nmovable
                  read(45,*) move%lower(j)
                  read(45,*) move%upper(j)
               enddo
               do j=1,44+9*nmovable
                  read(45,*) move%ibfgs(j)
               enddo
               do j=1,size(move%wbfgs)
                  read(45,*) move%wbfgs(j)
               enddo
               do j=1,29
                  read(45,*) move%dbfgs(j)
               enddo
               close(45)
            else
               if (move%stepmax .gt. zero) then
                  do i =1, nmovable
                     do j = 1,3
                        move%lower(3*(i-1) + j) = 
     1                       move%rcur(j,i) - move%stepmax
                        move%upper(3*(i-1) + j) = 
     2                       move%rcur(j,i) + move%stepmax
                     enddo
                  enddo
               else
                  move%lower = zero
                  move%upper = zero
               endif
c
c     Initialize internal BFGS parameters
c
               move%cbfgs = 'START'
               call setulb(nparam,move%mmax,
     1              move%rcur,move%lower,move%upper,nbound,etot,mforce
     2              ,xtol,xtol,move%wbfgs,move%ibfgs(45)
     3              ,move%cbfgs(1:60),iprint,move%cbfgs(61:120)
     4              ,move%lbfgs,move%ibfgs(1),move%dbfgs)
            endif
         endif
      endif
c
c     Move the atoms according to the desired scheme:
c
      select case (move%name)
c     perform simple movement of atoms (steepest descent)
      case (SIMPLE)

c     the movement is related to the force component by  dx=stepmax
c     *fx/(const+fx), and the same for dy, dz. For small f, dx->f
c     /const. The choice of the constant made here guarantees that
c     for f=fmin the movement is stepmin - the smallest movement
c     allowed by the user. For large f, dx->stepmax, and hence
c     stepmax - the largest movement allowed by the user, even if the
c     force is huge. stepmax controls how fast the motion  proceeds.
c     If it's too small, many steps will be needed. It it's too large,
c     the coordinates will oscillate around the true minimum
c     energy positions.  The best values for stepmin and stepmax
c     strongly depend on how "hard" or "soft" the system under study
c     is.
         stepmax = move%stepmax
         stepmin = move%stepmin
         abs_fmin = abs(move%fmin)
c
c     steepest descent with "fixed" movement
c
c         coeff = move%fmin*(stepmax/stepmin-one)
c         coeff = 0.1d0
c         do i = 1, nmovable
c            do j=1,3
c               disp(j) =  -stepmax*mforce(j,i)/(coeff +
c     1              abs(mforce(j,i)))
c            enddo
c            move%rcur(:,i) = move%rcur(:,i) + disp(:)
c         enddo
c
c     steepest descent with "adjustable" movement
c
         coeff =log((stepmax-stepmin)/(stepmax-50*stepmin))/
     1        (1000*move%fmin)
         do i = 1, nmovable
            do j=1,3
               if (mforce(j,i) .eq. zero) then
                  disp(j) = zero
               else
                  disp(j) =  -(mforce(j,i)/abs(mforce(j,i)))*
     1                 (stepmin+(stepmax-stepmin)*
     2                 (1-exp(-coeff*(abs(mforce(j,i))-abs_fmin)
     3                 )))
               endif
            enddo
            move%rcur(:,i) = move%rcur(:,i) + disp(:)
         enddo

c     perform BFGS minimization of energy
      case (BFGS)
         do
            call setulb(nparam,move%mmax,move%rcur,move%lower
     1           ,move%upper,nbound,etot,mforce,xtol,xtol,move%wbfgs
     2           ,move%ibfgs(45),move%cbfgs(1:60),iprint
     3           ,move%cbfgs(61:120),move%lbfgs,move%ibfgs(1)
     4           ,move%dbfgs)
c     BFGS routines need forces/energy at current coordinates
            if (move%cbfgs(1:2) .eq. 'FG') then
               exit
            endif
            if (move%cbfgs(1:4) .eq. 'CONV') then
               write(7,*) 'LBFGS minimization converged '
               write(7,*) move%cbfgs(1:60)
               write(66,*) 'Local minimum of total energy was found'
               move%done = .true.
               write(7,*) 'LBFGS is terminating'
               write(66,*) 'LBFGS is terminating'
               return
            endif
            if (move%cbfgs(1:4) .eq. 'WARN') then
              write(7,*) 'LBFGS minimization warning'
              write(7,*) move%cbfgs(1:60)
              move%done = .true.
              write(7,*) 'LBFGS is terminating'
              write(66,*) 'LBFGS is terminating'
              return
            endif
c     Error exits
            if (move%cbfgs(1:5) .eq. 'ERROR') then
               write(7,*) 'ERROR in LBFGS routines : '
               write(7,*) move%cbfgs(1:60)
               move%done = .true.
               write(7,*) 'LBFGS is terminating'
               write(66,*) 'LBFGS is terminating'
               ierr = 1
               return
            endif
            if (move%cbfgs(1:4) .eq. 'ABNO') then
               write(7,*) 'Abnormal exit from LBFGS routines : '
               write(7,*) move%cbfgs(1:60)
               move%done = .true.
               write(7,*) 'LBFGS is terminating'
               write(66,*) 'LBFGS is terminating'
               ierr = 1
               return
            endif
         enddo
         deallocate(nbound)

c     no miminization, move atoms according to user-supplied
c     coordinates
      case (MANUAL)
c     Unless the results reported were for the final step, 
c     read coordinates for next position from 'manual.dat'
         if (move%num .lt. move%mxmove) then
            do i=1,nmovable
               read(67,*) move%rcur(:,i)
            enddo 
c     unless reading the last set of coordinates, skip empty line 
c     between coordinate sets
            if (move%num .lt. move%mxmove-1) read(67,*)
         endif
      end select
c
c     Update atom coordinates in clust structure (notice that only
c     the coordinates of movable atoms are modified).
c
      j = 0
      do i = 1, clust%atom_num
         if (clust%mvat(i) .eq. 1) then
            j = j + 1
            clust%xatm(i) = move%rcur(1,j)
            clust%yatm(i) = move%rcur(2,j)
            clust%zatm(i) = move%rcur(3,j)
         endif
      enddo

      if (is_pbc) then
c
c     IF THIS IS A PERIODIC SYSTEM
c     It is possible that some of the atoms have "moved outside of
c     the cell" because of the minimization. For the electronic
c     structure problem, we need to "wrap" them back into the
c     supercell.
c
         alattinv = one / alatt
         do j=1,clust%atom_num
            clust%xatm(j) = clust%xatm(j) -
     1           alatt(1)*nint(clust%xatm(j)*alattinv(1))
            clust%yatm(j) = clust%yatm(j) -
     1           alatt(2)*nint(clust%yatm(j)*alattinv(2))
            clust%zatm(j) = clust%zatm(j) -
     1           alatt(3)*nint(clust%zatm(j)*alattinv(3))
         enddo
      endif
c
c     Save temporary information for a relaxation restart.
c
      open(45,file='relax_restart.dat',form='formatted')
      write(45,*) ' Atomic coordinates follow (all atoms)'
      do j=1,clust%atom_num
         write(45,*) clust%xatm(j),clust%yatm(j),clust%zatm(j)
      enddo
      write(45,*) 'Iteration number, number of movable atoms'
      write(45,*) move%num,nmovable
      if (move%name .eq. BFGS) then
c     if BFGS is done, write out info for a BFGS restart.
         write(45,*) ' LBFGS work variables follow'
         write(45,*) move%mmax,move%stepmax
         write(45,'(60a)') move%cbfgs(1:60)
         write(45,'(60a)') move%cbfgs(61:120)
         do j=1,4
            write(45,*) move%lbfgs(j)
         enddo
         do j=1,3*nmovable
            write(45,*) move%lower(j)
            write(45,*) move%upper(j)
         enddo
         do j=1,44+9*nmovable
            write(45,*) move%ibfgs(j)
         enddo
         do j=1,size(move%wbfgs)
            write(45,*) move%wbfgs(j)
         enddo
         do j=1,29
            write(45,*) move%dbfgs(j)
         enddo
         close(45)
         call myflush(46)
      endif
c
c     Report new coordinates to file, unless this was the last step.
c
      if (move%num .lt. move%mxmove) then
         write(66,32) move%num+1
         j=0
         do ity=1,clust%type_num
            write(66,*) clust%name(ity),'    ',clust%natmi(ity)
            do i=1,clust%natmi(ity)
               j = j + 1
               write(66,34) clust%xatm(j), clust%yatm(j), clust%zatm(j)
            enddo
         enddo
      endif
      call myflush(66)
 32   format('Step #',1x,i3)
 34   format(3(3x,f10.6))

      deallocate(mforce)

      end subroutine domove
c     ===============================================================
