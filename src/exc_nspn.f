c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This suborutine computes the exchange correlation potential,
c     Vxc, and the exchange correlation energy, Exc, both in Rydbergs,
c     based on an input charge density given in e/bohr^3, for the
c     case of a spin-unpolarized calculation.
c
c     NOTE: Extra factor of two in formulas is due to conversion from
c     Hartree to Rydberg!
c
c     The following exchange-correlation functionals are implemented:
c
c     Local density functionals:
c     1. Slater's x-alpha - J. C. Slater, Phys. Rev. 81, 385 (1951).
c     2. Wigner's interpolation - E. Wigner, Phys. Rev. 46, 1002
c               (1934).
c     3. Hedin-Lundqvist -  L. Hedin and B. I. Lundqvist, J. Phys. C,
c               4, 2064 (1971).
c     4. Ceperley-Alder - D. M. Ceperley and B. J. Alder, Phys. Rev.
c               Lett., 45, 566 (1980).
c
c     Gradient-corrected functional:
c     PBE - Perdew, Burke, Ernzerhof - J. P. Perdew, K. Burke, and 
c               M. Ernzerhof, Phys. Rev. Lett., 77, 3865 (1996).
c
c     Asymptotically-corrected functionals:
c     1. Leeuwen-Baerends correction - Leeuwen and Baerends, 
c               Phys. Rev. 49, 2421 (1994).
c     2. Casida-Salahub correction - Casida and Salahub, J. Chem.
c               Phys., 113, 8918 (2000).
c
c     ---------------------------------------------------------------
      subroutine exc_nspn(grid,parallel,nrep,icorr
     1     ,dioniz,vxc,wvec,ipr,exc)

      use constants
      use grid_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     grid related data
      type (grid_data), intent(in) :: grid
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

c     correlation type
      character (len=2), intent(in) :: icorr
c     ionization energy difference
c     (used only if the correlation is of type cc)
      real(dp), intent(in) :: dioniz
c     print flag
      integer, intent(in) :: ipr
c     order of the reduced group
      integer, intent(in) :: nrep
c     output exchange-correlation potential
      real(dp), intent(out) :: vxc(parallel%mydim)
c     work array
      real(dp), intent(out) :: wvec(parallel%nwedge + 1)
c     total exchange-correlation energy
      real(dp), intent(out) :: exc
c
c     Work variables:
c
c     multiplicity of grid points in irreducible wedge (equal to
c     order of the reduced group)
      real(dp) :: weight

c     GGA variables:
c     quantities fed to pbe.f:
c     gradient - grad(n), where n is the charge density
c     absgrad  - |grad(n)|
c     gradgrad - components of grad(|grad(n)|
c     laplac - laplacian(n)
c     prod  -  grad(n) dot grad(|grad(n)|
      real(dp), dimension(:), allocatable :: absgrad,laplac
      real(dp), dimension(:,:), allocatable :: gradient,gradgrad
      real(dp) :: prod 

c     quantitites read from pbe.f:
c     expbe,vxuppbe,vxdnpbe - exchange enregy and (up and down)
c     potentials
c     ecpbe,vcuppbe,vcdnpbe - correlation enregy and (up and down)
c     potentials
      real(dp) :: expbe,vxuppbe,vxdnpbe,vcuppbe,vcdnpbe,ecpbe
c     equivalent quantities for pw91
      real(dp) :: expw91,vxpw91,ecpw91,vcuppw91,vcdnpw91

c     LDA variables:
c     various recurring parameters in the exchange-correlation
c     formulas: 
c     quantities common to all local density expressions:
c     rs is the local value of the Wigner-Seitz radius
c     rho temporarily stores the charge at a given grid point
      real(dp) :: a0, twovpia0, p75vpi, alpha, rs, rho
c     Wigner parameters:
      real(dp), parameter :: aw = -0.875529d0 , bw = 7.8d0
c     Hedin-Lundqvist parameters:
      real(dp), parameter :: ah = 1.d0/21.d0, ch = 0.045d0
      real(dp) :: x 
c     Ceperley-Alder parameters
      real(dp), parameter :: g =-0.2846d0, b1 = 1.0529d0
      real(dp), parameter :: b2 = 0.3334d0, c1 = 0.0622d0
      real(dp), parameter :: c2 = 0.096d0, c3 = 0.004d0
      real(dp), parameter :: c4 = 0.0232d0,c5 = 0.0192d0
      real(dp) :: ec,sqrs
c     Asymptotic corrections parameters and temporary variables
      real(dp), parameter :: betalb = 0.05d0
      real(dp) :: ac1, ac2 
      real(dp) :: abstemp, corrlb, asinhx

c     actual number of grid points
      integer ndim
c     allocation check
      integer alcstat
c     temporary variables
      real(dp), dimension (3) :: temp1,temp2
c     counters
      integer i, k, kk

c     ---------------------------------------------------------------

c     initialize arrays:

      ndim = parallel%mydim
      weight = real(nrep,dp)

      allocate(gradient(3,ndim),stat=alcstat)
      call alccheck('gradient',3*ndim,alcstat)
      allocate(absgrad(ndim),stat=alcstat)
      call alccheck('absgrad',ndim,alcstat)
      allocate(gradgrad(3,ndim),stat=alcstat)
      call alccheck('gradgrad',3*ndim,alcstat)
      allocate(laplac(ndim),stat=alcstat)
      call alccheck('laplac',ndim,alcstat)

c     and some more constants:
      a0 = (four/(nine*pi))**third
      twovpia0 = two/(pi*a0)
      p75vpi = 0.75d0/pi
c
c     initialize the total exchange-correlation energy to zero
c
      exc = zero
c
      if (icorr .eq. 'pw' .or. icorr .eq. 'pb') then

c     calculate the gradient and the Laplacian of the charge density
c     at each grid point

         call lapmvs(parallel,vxc,laplac,grid%coe2,grid%norder,wvec)
         laplac = -laplac
         call gradmvs(parallel,vxc,gradient,grid%coe1,grid%norder,wvec)
         do i = 1,ndim
            temp1 = gradient(:,i)
            absgrad(i) = sqrt(dot_product(temp1,temp1))
         enddo

c     calculate other input quantities and then calculate the 
c     exchange-correlation

c     calculate grad(rho)_dot_grad|grad(rho)| and
c     calculate the exchange-correlation value using pbe
         exc = zero
         call gradmvs(parallel,absgrad,gradgrad,grid%coe1,grid%norder
     1        ,wvec)

         if (icorr .eq. 'pb') then
c     using PBE - a gradient-corrected functional
            do i = 1,ndim
               temp1 = gradient(:,i)
               temp2 = gradgrad(:,i)
               prod = dot_product(temp1,temp2)
               call pbe(vxc(i),absgrad(i),prod,laplac(i),
     1              expbe,vxuppbe,vxdnpbe,ecpbe,vcuppbe,vcdnpbe)
               exc=exc+vxc(i)*two*(expbe+ecpbe)*weight
               vxc(i)=vxuppbe+vxdnpbe+vcuppbe+vcdnpbe
            enddo

         elseif (icorr .eq. 'pw') then
c     using PW91 - a gradient-corrected functional
            do i = 1,ndim
               temp1 = gradient(:,i)
               temp2 = gradgrad(:,i)
               prod = dot_product(temp1,temp2)
               call pw91(vxc(i),absgrad(i),prod,laplac(i),
     1              expw91,vxpw91,ecpw91,vcuppw91,vcdnpw91)
               exc=exc+vxc(i)*two*(expw91+ecpw91)*weight
               vxc(i)=two*vxpw91+vcuppw91+vcdnpw91
            enddo
         endif

      else if (icorr .eq. 'xa') then
c     x-alpha correlation
   
         alpha = one
c     Note: alpha = 2/3 is pure exchange
         write(7,10) alpha
 10      format(' performed x-alpha correlation with alpha = ',f6.3)

         do i=1,ndim
            rho = vxc(i)
            vxc(i) = zero
            if (rho .gt. zero) then
               rs = (p75vpi/rho)**third
               vxc(i) = -1.5d0*twovpia0*alpha/rs
               exc = exc + 0.75d0*rho*vxc(i)*weight
            endif
         enddo

      else if (icorr .eq. 'wi') then
c     Wigner correlation

         do i=1,ndim
            rho = vxc(i)
            vxc(i) = zero
            if (rho .gt. zero) then
               rs = (p75vpi/rho)**third
               vxc(i) = -twovpia0/rs
               exc = exc + 0.75d0*rho*vxc(i)*weight
     1              + rho*aw/(rs + bw)
               vxc(i) = vxc(i) + aw*(four*rs*third+bw)/((rs+bw)*(rs+bw))
            endif
         enddo

      else if (icorr .eq. 'hl') then
c     Hedin-lundqvist exchange-correlation

         do i=1,ndim
            rho = vxc(i)
            vxc(i) = zero
            if (rho .gt. zero) then
               rs = (p75vpi/rho)**third
               vxc(i) = -twovpia0/rs
               exc = exc + 0.75d0*rho*vxc(i)*weight
               x = rs*ah
               alpha = log(one+one/x)
               vxc(i) = vxc(i) - ch*alpha
               exc = exc - rho*ch*((one+x*x*x)*alpha+x*half-x*x-third)
     1              *weight
            endif
         enddo

      else if (icorr .eq. 'ca') then
c     Ceperly-Alder exchange correlation

         do i=1,ndim
            rho = vxc(i)
            vxc(i) = zero
            if (rho .gt. zero) then
               rs = (p75vpi/rho)**third
               vxc(i) = -twovpia0/rs
               exc = exc + 0.75d0*rho*vxc(i)*weight
               if (rs .ge. one) then
                  sqrs = sqrt(rs)
                  ec = g/(one + b1*sqrs + b2*rs)
                  vxc(i) = vxc(i) + ec*ec*
     2                 (one+3.5d0*b1*sqrs*third+four*b2*rs*third)/g
               else
                  alpha = log(rs)
                  ec = c1*alpha - c2 + (c3*alpha - c4)*rs
                  vxc(i) = vxc(i) +
     1                 ec - (c1 + (c3*alpha - c5)*rs)*third
               endif
               exc = exc + rho*ec*weight
            endif
         enddo

      else if (icorr .eq. 'lb' .or. icorr .eq. 'cs') then
c     Asymptotically corrected Ceperly-Alder exchange correlation

c     define parameters
         ac1=two**third
         ac2=four/three

c     calculate 2^(1/3) * |grad(rho)|/rho^(4/3)
         call gradmvs(parallel,vxc,gradient,grid%coe1,grid%norder,wvec)
         do i = 1,ndim
            temp1 = gradient(:,i)
            abstemp = sqrt(dot_product(temp1,temp1))
            if (vxc(i) .gt. zero) then
               absgrad(i)=ac1*abstemp/(vxc(i)**ac2)
            endif
         enddo

c     compute exchange-correlation potential and energy
         do i=1,ndim
            rho = vxc(i)
            vxc(i) = zero
            if (rho .gt. zero) then

c     compute Ceperely-Alder term
               rs = (p75vpi/rho)**third
               vxc(i) = -twovpia0/rs
               exc = exc + 0.75d0*rho*vxc(i)*weight
               if (rs .ge. one) then
                  sqrs = sqrt(rs)
                  ec = g/(one + b1*sqrs + b2*rs)
                  vxc(i) = vxc(i) +
     1                 ec*ec*(one+3.5d0*b1*sqrs*third + 
     2                 four*b2*rs*third)/g
               else
                  alpha = log(rs)
                  ec = c1*alpha - c2 + (c3*alpha - c4)*rs
                  vxc(i) = vxc(i) +
     1                 ec - (c1 + (c3*alpha - c5)*rs)*third
               endif
               exc = exc + rho*ec*weight

c     Compute Leeuwen-Baerends correction to the potential
               abstemp = absgrad(i)
               asinhx = log(abstemp+sqrt(abstemp*abstemp+one))
               corrlb = two*betalb*(rho**third)/ac1*abstemp*abstemp/
     1              (one + three*betalb*abstemp*asinhx)

c     If correcting for the ionization energy difference, shift the
c     correlation energy 
               if (icorr .eq. 'cs') then
                  if (corrlb .gt. dioniz) corrlb = dioniz
               endif
               vxc(i) = vxc(i) - corrlb
            endif
         enddo
      endif
c
c     scale the total energy integral by h^3 (factor necessary due to the
c     expression of an integral as a summation over grid points) and
c     sum it up over processors
c
      call psum(exc,1,parallel%procs_num,parallel%comm)
      exc  = exc * (grid%hcub)

      deallocate(laplac)
      deallocate(absgrad)
      deallocate(gradient)
      deallocate(gradgrad)

      end subroutine exc_nspn
c     ===============================================================
