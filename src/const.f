c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     NUMERICAL CONSTANTS
c     This file should NEVER be touched, barring new
c     and exciting developments in the value of, say, pi.
c     ---------------------------------------------------------------
      module constants

      implicit none
c
c     Definitions of double precision real/complex types:
c
      integer, parameter :: dp = kind(1.0d0)
      integer, parameter :: dpc = kind((1.0d0,1.0d0))
c
c     Numerical constants:
c
      real(dp), parameter :: zero = 0.d0, one = 1.d0,
     1     two = 2.d0, three=3.d0, four=4.d0, five = 5.d0,
     2     six = 6.d0, eight = 8.d0, nine = 9.d0, mone = -1.d0,
     3     half = 0.5d0, third = 1.d0/3.d0, mhalf = -0.5d0,
     4     root3=1.73205080756887729352744634151d0,
     5     pi = 3.1415926535897932384626433832795d0,
     6     twopi = two * pi
c
c     Physical constants:
c
c     convert energy units from rydbergs to eV
      real(dp), parameter :: rydberg = 13.6058d0
c     convert length units from atomic units (bohrs) to angstroms
      real(dp), parameter :: angs = 0.529177d0, cubangs = angs**3
c     convert dipole units from au to debyes
      real(dp), parameter :: debye = 2.54d0
c     convert temperature from kelvins to rydbergs
      real(dp), parameter :: tempkry = 6.33327186d-06
c     convert time from a.u. to ps
      real(dp), parameter :: timeaups = 2.4188843d-05
c
c     Parsec constants:
c
c     constants for minimization scheme
      integer, parameter :: NONE = 0, SIMPLE = 1, BFGS = 2, MANUAL = 3
c     constants for mixing scheme
      integer, parameter :: Anderson = 0, Broyden = 1
c     constants for diagonalization scheme
      integer, parameter :: ARPACK = 0, DIAGLA = 1

      end module constants
c     ===============================================================
