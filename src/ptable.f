c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     =======  Periodic Table =======
c     Give the core charge and atomic mass for each element 
c
c     note: if the pseudopotential file already contains core
c     charge, that will override the output from this subroutine.
c
c     ---------------------------------------------------------------
      subroutine ptable(element,zion,am,ierr)

      use constants
      implicit none
c
c     Input/Output variables:
c
c     name of the element
      character (len=2), intent(in) ::  element
c     charge, mass
      real(dp), intent(out) :: zion, am
c     error flag
      integer, intent(out) :: ierr
c     ---------------------------------------------------------------
      if (element.eq.'H ') then
         zion=1.d0
         am = 1.01d0
      else if (element.eq.'H0') then
         zion=1.05d0
         am = 1.01d0
      else if (element.eq.'H1') then
         zion=1.10d0
         am = 1.01d0
      else if (element.eq.'H2') then
         zion=1.15d0
         am = 1.01d0
      else if (element.eq.'H3') then
         zion=1.20d0
         am = 1.01d0
      else if (element.eq.'H4') then
         zion=1.25d0
         am = 1.01d0
      else if (element.eq.'H5') then
         zion=1.30d0
         am = 1.01d0
      else if (element.eq.'H6') then
         zion=1.35d0
         am = 1.01d0
      else if (element.eq.'H7') then
         zion=1.40d0
         am = 1.01d0
      else if (element.eq.'H8') then
         zion=1.45d0
         am = 1.01d0
      else if (element.eq.'H9') then
         zion=1.50d0
         am = 1.01d0
      else if (element.eq.'F0') then
         zion=1.55d0
         am = 1.01d0
      else if (element.eq.'F1') then
         zion=1.60d0
         am = 1.01d0
      else if (element.eq.'F2') then
         zion=1.65d0
         am = 1.01d0
      else if (element.eq.'F3') then
         zion=1.70d0
         am = 1.01d0
      else if (element.eq.'F4') then
         zion=1.75d0
         am = 1.01d0
      else if (element.eq.'F5') then
         zion=1.80d0
         am = 1.01d0
      else if (element.eq.'F6') then
         zion=1.85d0
         am = 1.01d0
      else if (element.eq.'F7') then
         zion=1.90d0
         am = 1.01d0
      else if (element.eq.'F8') then
         zion=1.95d0
         am = 1.01d0
      else if (element.eq.'HI') then
         zion=0.95d0
         am = 1.01d0
      else if (element.eq.'HJ') then
         zion=0.90d0
         am = 1.01d0
      else if (element.eq.'HK') then
         zion=0.85d0
         am = 1.01d0
      else if (element.eq.'HL') then
         zion=0.80d0
         am = 1.01d0
      else if (element.eq.'HM') then
         zion=0.75d0
         am = 1.01d0
      else if (element.eq.'HN') then
         zion=0.70d0
         am = 1.01d0
      else if (element.eq.'HO') then
         zion=0.65d0
         am = 1.01d0
      else if (element.eq.'HP') then
         zion=0.60d0
         am = 1.01d0
      else if (element.eq.'HQ') then
         zion=0.55d0
         am = 1.01d0
      else if (element.eq.'HR') then
         zion=0.50d0
         am = 1.01d0
      else if (element.eq.'FI') then
         zion=0.45d0
         am = 1.01d0
      else if (element.eq.'FJ') then
         zion=0.40d0
         am = 1.01d0
      else if (element.eq.'FK') then
         zion=0.35d0
         am = 1.01d0
      else if (element.eq.'FL') then
         zion=0.30d0
         am = 1.01d0
      else if (element.eq.'FM') then
         zion=0.25d0
         am = 1.01d0
      else if (element.eq.'FN') then
         zion=0.20d0
         am = 1.01d0
      else if (element.eq.'FO') then
         zion=0.15d0
         am = 1.01d0
      else if (element.eq.'FP') then
         zion=0.10d0
         am = 1.01d0
      else if (element.eq.'FQ') then
         zion=0.05d0
         am = 1.01d0
      else if (element.eq.'He ') then
         zion=2.d0
         am = 4.0026d0
      else if(element.eq.'Li') then 
         zion=1.d0
         am = 6.941d0 
      else if(element.eq.'Na') then 
         zion=1.d0
         am = 22.9898d0
      else if(element.eq.'K ') then 
         zion=1.d0
         am = 39.09d0 
      else if(element.eq.'Rb') then 
         zion=1.d0
         am = 85.47d0  
      else if(element.eq.'Ag') then 
         zion=11.d0
         am = 107.87d0 
      else if(element.eq.'Au') then 
         zion=11.d0
         am = 196.97d0 
      else if(element.eq.'Be') then 
         zion=2.d0
         am = 9.0122d0 
      else if(element.eq.'Mg') then 
         zion=2.d0
         am = 24.305d0 
      else if(element.eq.'Ca') then 
         zion=2.d0
         am = 40.08d0
      else if(element.eq.'Sr') then 
         zion=2.d0
         am = 87.62d0 
      else if(element.eq.'Ba') then 
         zion=2.d0
         am = 137.34d0 
      else if(element.eq.'V ') then 
         zion=5.d0
         am = 50.94d0
      else if(element.eq.'Cu') then 
         zion=11.d0 
         am = 63.55d0 
      else if(element.eq.'Zn') then 
         zion=2.d0
         am = 65.39d0
      else if(element.eq.'Cd') then 
         zion=2.d0
         am = 112.41d0
      else if(element.eq.'B ') then 
         zion=3.d0
         am = 10.81d0 
      else if(element.eq.'Al') then 
         zion=3.d0
         am = 26.982d0 
      else if(element.eq.'Ga') then 
         zion=3.d0
         am = 69.72d0 
      else if(element.eq.'In') then 
         zion=3.d0
         am = 114.82d0 
      else if(element.eq.'Tl') then 
         zion=3.d0
         am = 204.37d0
      else if(element.eq.'C ') then 
         zion=4.d0
         am = 12.01d0 
      else if(element.eq.'Si') then 
         zion=4.d0
         am = 28.096d0 
      else if(element.eq.'Ge') then 
         zion=4.d0
         am = 72.59d0 
      else if(element.eq.'Sn') then 
         zion=4.d0
         am = 118.69d0 
      else if(element.eq.'N ') then 
         zion=5.d0
         am = 14.007d0 
      else if(element.eq.'P ') then 
         zion=5.d0
         am = 30.974d0 
      else if(element.eq.'As') then 
         zion=5.d0
         am = 74.922d0 
      else if(element.eq.'Sb') then 
         zion=5.d0
         am = 121.75d0 
      else if(element.eq.'Bi') then 
         zion=5.d0
         am = 208.98d0 
      else if(element.eq.'O ') then 
         zion=6.d0
         am = 15.999d0 
      else if(element.eq.'S ') then 
         zion=6.d0
         am = 32.064d0
      else if(element.eq.'Se') then 
         zion=6.d0
         am = 78.96d0 
      else if(element.eq.'Te') then
         zion=6.d0
         am = 127.60d0  
      else if(element.eq.'Po') then
         zion=6.d0
         am = 210.00d0 
      else if(element.eq.'Ar') then 
         zion=8.d0
         am = 39.95d0
      else if(element.eq.'Ce') then 
         zion=4.d0
         am = 140.12d0
      else if(element.eq.'Hf') then 
         zion=18.d0
         am = 178.49d0
      else if(element.eq.'Fe') then
         zion=8.d0
         am = 55.845d0
      else if(element.eq.'Mn') then
         zion=7.d0
         am = 54.93d0
      else if(element.eq.'Cl') then
         zion=7.d0
         am = 19.0d0
      else
         write(7,*) 'ERROR: unknown element: ',element
         write(7,*) 'go to ptable.f and add element'
         write(7,*) 'STOP in ptable'
         ierr = 1
         return
      endif

      end subroutine ptable
c     ===============================================================
