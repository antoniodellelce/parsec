c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Calculates g-vectors (kgv) for lattice points inside a G-grid.
c     G-vectors are ordered by stars depending on symmetry and pairs of
c     (+G,-G) vectors are put together. Notice that
c     the G-grid is defined as (-kmax,kmax), and the number of grid points
c     along each direction is odd by construction. But the FFT box is
c     defined as (m,m + N - 1), where N is *even*. In the parent
c     routine, kmax is defined so that 2*kmax + 1 >= N.
c
c     Adapted from plane-wave programs written by S. Froyen and
c     J. L. Martins.
c
c     ---------------------------------------------------------------
      subroutine g_space(pbc,symm,kmax,ipr)

      use constants
      use pbc_module
      use symmetry_module

      implicit none
c
c     Input/Output variables:
c
c     periodic boundary conditions data
      type (pbc_data), intent(inout) :: pbc
c     symmetry operations
      type (symmetry), intent(in) :: symm
c     half-size of reciprocal space
      integer, intent(inout) :: kmax(3)
c     print flag
      integer, intent(in) :: ipr
c
c     Work variables:
c
      integer ns,ng,nx
      integer i,j,k,l,istar,istop,ir,itest,iprot,iavail,ncount
      integer kk(3)
      integer, dimension (:), allocatable :: izstar
c     number of g-vectors per star (transfered to pbc%mstar)
      integer, dimension(:), allocatable :: tmstar
c     kinetic energy for each star (transfered to pbc%ek)
      real(dp), dimension(:), allocatable :: tek

      integer ktran(3,symm%ntrans)

      real(dp) :: dk1,dk2,dk3,gmod,rgm,gmr

      real(dp) :: fi,diff,bdot(3)
      complex(dpc) :: phtmp
      complex(dpc) :: expt(symm%ntrans)
      real(dp), dimension (:), allocatable :: gm

      real(dp), parameter :: delta = 1.0d-8

c     ---------------------------------------------------------------

      bdot = pbc%bdot

c     g = k1*b1+k2*b2+k3*b3  
      ng = 0
      do i=-kmax(1),kmax(1)
         dk1 = real(i,dp)
         do j=-kmax(2),kmax(2)
            dk2 = real(j,dp)
            do k=-kmax(3),kmax(3)
               dk3 = real(k,dp)
               gmod = dk1* bdot(1) *dk1 + 
     1              dk2* bdot(2) *dk2 + dk3 * bdot(3) *dk3
               ng = ng + 1
            enddo
         enddo
      enddo
      if(ng .le. 1) then
         write(7,56) ng
         stop
      endif

      if (.not. allocated (gm)) allocate (gm(ng))
      allocate(tek(ng))
      allocate(tmstar(ng))
      call pbc_set_ng (ng,pbc)

      ng = 0
      do i=-kmax(1),kmax(1)
         dk1 = real(i,dp)
         do j=-kmax(2),kmax(2)
            dk2 = real(j,dp)
            do k=-kmax(3),kmax(3)
               dk3 = real(k,dp)
               gmod = dk1*bdot(1)*dk1 + dk2*bdot(2)*dk2 + 
     1              dk3*bdot(3)*dk3 
               ng = ng + 1
               gm(ng) = gmod
               pbc%kgv(1,ng) = i
               pbc%kgv(2,ng) = j
               pbc%kgv(3,ng) = k
            enddo
         enddo
      enddo

c     sort the g-vectors according to length and collect
c     heapsort algorithm from numerical recipes
      l=ng/2+1
      ir=ng
 20   continue

      if(l.gt.1)then
         l=l-1
         rgm=gm(l)
         kk(:)=pbc%kgv(:,l)
      else
         rgm=gm(ir)
         kk(:)=pbc%kgv(:,ir)
         gm(ir)=gm(1)
         pbc%kgv(:,ir)=pbc%kgv(:,1)
         ir=ir-1
         if(ir.eq.1)then
            gm(1)=rgm
            pbc%kgv(:,1)=kk(:)
            goto 25
     
         endif
      endif
      i=l
      j=l+l

 21   continue

      if(j.le.ir)then
         if(j.lt.ir)then
            if(gm(j).lt.gm(j+1))j=j+1
         endif
         if(rgm.lt.gm(j))then
            gm(i)=gm(j)
            pbc%kgv(:,i)=pbc%kgv(:,j)
            i=j
            j=j+j
         else
            j=ir+1
         endif

         go to 21

      endif
      gm(i)=rgm
      pbc%kgv(:,i)=kk(:)

      go to 20

 25   continue
c
c     sort g vectors of same length by the kgv(1,i), then kgv(2,i),
c     then the kgv(3,i) value. this makes the kgv sort machine
c     independent.
c
      istar = 2

      do i=2,ng
         if( i .eq. ng) then
            diff = 1.0
         else
            diff = gm(i+1)-gm(istar)
         endif
         if(diff .gt. delta) then
            istop = i
            do j=istar,istop-1
               do k=j+1,istop
                  if (pbc%kgv(1,k) .lt. pbc%kgv(1,j)) then
                     rgm = gm(j)
                     kk(:) = pbc%kgv(:,j)
                     gm(j) = gm(k)
                     pbc%kgv(:,j) = pbc%kgv(:,k)
                     gm(k) = rgm
                     pbc%kgv(:,k)=kk(:)
                  elseif (pbc%kgv(1,k) .eq. pbc%kgv(1,j)) then
                     if (pbc%kgv(2,k) .lt. pbc%kgv(2,j)) then
                        rgm = gm(j)
                        kk(:) = pbc%kgv(:,j)
                        gm(j) = gm(k)
                        pbc%kgv(:,j) = pbc%kgv(:,k)
                        gm(k) = rgm
                        pbc%kgv(:,k)=kk(:)
                     elseif (pbc%kgv(2,k) .eq. pbc%kgv(2,j)) then
                        if (pbc%kgv(3,k) .lt. pbc%kgv(3,j)) then
                           rgm = gm(j)
                           kk(:) = pbc%kgv(:,j)
                           gm(j) = gm(k)
                           pbc%kgv(:,j) = pbc%kgv(:,k)
                           gm(k) = rgm
                           pbc%kgv(:,k)=kk(:)
                        endif
                     endif
                  endif
               enddo
            enddo
            istar = istop + 1
         endif
      enddo
c
c     collects g-vectors into stars by symmetry
c     first star is (0 0 0)
c
      ns = 1
      tek(1) = zero
      pbc%phase(1) = cmplx(one,zero,dp)
      pbc%conj(1) = one
      pbc%inds(1) = 1
      tmstar(1) = 1

      iprot = 1
      iavail = 2
      itest = 2

 30   continue

      if(itest .gt. ng) then
         diff = one
      else
         diff = abs(gm(itest)-gm(iprot))
      endif
      if(diff .gt. delta) then
c     new star
         iprot = iavail
         ns = ns + 1
         tek(ns) = gm(iprot) / two
         phtmp = cmplx(zero,zero,dp)
         ncount = 0
         do i=1,symm%ntrans
            ktran(:,i) = matmul(symm%gmtrx(:,:,i),pbc%kgv(:,iprot))
            fi = dot_product(symm%tnp(:,i),ktran(:,i)*twopi)
            expt(i) = cmplx(cos(fi),sin(fi),dp)
            if ((ktran(1,i)-pbc%kgv(1,iprot) .eq. 0) .and.
     1          (ktran(2,i)-pbc%kgv(2,iprot) .eq. 0) .and.
     2          (ktran(3,i)-pbc%kgv(3,iprot) .eq. 0)) then
               phtmp = phtmp + expt(i)
               ncount = ncount + 1
            endif
         enddo
         pbc%phase(iprot) = phtmp/real(ncount,dp)
         pbc%conj(iprot) = one
         tmstar(ns) = 1
         pbc%inds(iprot) = ns

c     searches for the inverse of star member
         do i=iprot+1,ng
            if ((pbc%kgv(1,i)+pbc%kgv(1,iprot) .eq. 0) .and.
     1          (pbc%kgv(2,i)+pbc%kgv(2,iprot) .eq. 0) .and.
     2          (pbc%kgv(3,i)+pbc%kgv(3,iprot) .eq. 0)) then
               if(i .ne. iprot+1) then
                  kk(:) = pbc%kgv(:,iprot+1)
                  pbc%kgv(:,iprot+1) = pbc%kgv(:,i)
                  pbc%kgv(:,i) = kk(:)
                  gmr = gm(iprot+1)
                  gm(iprot+1) = gm(i)
                  gm(i) = gmr
               endif
c
c     verifies if the inverse is related by symmetry
c
               ncount = 0
               phtmp = cmplx(zero,zero,dp)
               do j=1,symm%ntrans
                  if ((ktran(1,j)-pbc%kgv(1,iprot+1) .eq. 0) .and.
     1                (ktran(2,j)-pbc%kgv(2,iprot+1) .eq. 0) .and.
     2                (ktran(3,j)-pbc%kgv(3,iprot+1) .eq. 0)) then
                     phtmp = phtmp + expt(j)
                     ncount = ncount + 1
                  endif
               enddo

               if(ncount .ne. 0) then
                  pbc%phase(iprot+1) = phtmp/real(ncount,dp)
                  pbc%conj(iprot+1) = one
               else
                  pbc%phase(iprot+1) = pbc%phase(iprot)
                  pbc%conj(iprot+1) = -one
               endif

               tmstar(ns) = tmstar(ns) + 1
               pbc%inds(iprot+1) = ns

               goto 31

            endif
         enddo

 31      continue

         iavail = iprot + 2
         itest = iprot + 2

      else
c
c     checks if it is related by symmetry
c
         ncount = 0
         phtmp = cmplx(zero,zero,dp)
         do i=1,symm%ntrans
            if ((ktran(1,i)-pbc%kgv(1,itest) .eq. 0) .and.
     1          (ktran(2,i)-pbc%kgv(2,itest) .eq. 0) .and.
     2          (ktran(3,i)-pbc%kgv(3,itest) .eq. 0)) then
               phtmp = phtmp + expt(i)
               ncount = ncount + 1
            endif
         enddo

         if(ncount .ne. 0) then
c
c     it is related by symmetry
c
            if(itest .ne. iavail) then
               kk(:) = pbc%kgv(:,iavail)
               pbc%kgv(:,iavail) = pbc%kgv(:,itest)
               pbc%kgv(:,itest) = kk(:)
               gmr = gm(iavail)
               gm(iavail) = gm(itest)
               gm(itest) = gmr
            endif
            pbc%phase(iavail) = phtmp/real(ncount,dp)
            pbc%conj(iavail) = one
            tmstar(ns) = tmstar(ns) + 1
            pbc%inds(iavail) = ns
c
c     searches for the inverse of star member
c
            do i=iavail+1,ng
               if ((pbc%kgv(1,i)+pbc%kgv(1,iavail) .eq. 0) .and.
     1             (pbc%kgv(2,i)+pbc%kgv(2,iavail) .eq. 0) .and.
     2             (pbc%kgv(3,i)+pbc%kgv(3,iavail) .eq. 0)) then
                  if(i .ne. iavail+1) then
                     kk(:) = pbc%kgv(:,iavail+1)
                     pbc%kgv(:,iavail+1) = pbc%kgv(:,i)
                     pbc%kgv(:,i) = kk(:)
                     gmr = gm(iavail+1)
                     gm(iavail+1) = gm(i)
                     gm(i) = gmr
                  endif
c
c     verifies if the inverse is related by symmetry
c
                  ncount = 0
                  phtmp = cmplx(zero,zero,dp)
                  do j=1,symm%ntrans
                     if ((ktran(1,j)-pbc%kgv(1,iavail+1) .eq. 0) .and.
     1                   (ktran(2,j)-pbc%kgv(2,iavail+1) .eq. 0) .and.
     2                   (ktran(3,j)-pbc%kgv(3,iavail+1) .eq. 0)) then
                        phtmp = phtmp + expt(j)
                        ncount = ncount + 1
                     endif
                  enddo

                  if(ncount .ne. 0) then
                     pbc%phase(iavail+1) = phtmp/real(ncount,dp)
                     pbc%conj(iavail+1) = one
                  else
                     pbc%phase(iavail+1) = pbc%phase(iavail)
                     pbc%conj(iavail+1) = -one
                  endif
                  tmstar(ns) = tmstar(ns) + 1
                  pbc%inds(iavail+1) = ns

                  goto 32

               endif
            enddo

 32         continue

            if(itest .eq. iavail) then
               itest = itest +2
            else
               itest = itest +1
            endif
            iavail = iavail +2

         else
c
c     increases test counter
c
            itest = itest + 1
         endif

      endif

      if(iavail .le. ng) goto 30
c
c     reset kmax 
c
      kmax = maxval(pbc%kgv,dim=2)
c
c     checks stars with zero phase factors
c
      if (.not. allocated (izstar)) allocate (izstar (1:ns))
      ncount = 1
      izstar(1) = 1
      do i=2,ns
         if(abs(pbc%phase(ncount+1)-one) .gt. delta) then
            do j=ncount+1,ncount+tmstar(i)
               pbc%phase(j) = cmplx(zero,zero,dp)
            enddo
            izstar(i) = 0
         else
            izstar(i) = 1
         endif
         ncount = ncount + tmstar(i)
      enddo
c
c     Save information of stars
c
      call pbc_set_nstar (pbc,ns)
      call icopy(pbc%nstar,tmstar,1,pbc%mstar,1)
      call dcopy(pbc%nstar,tek,1,pbc%ek,1)

      write(7,50) ng,ns,(kmax(i),i=1,3)
      if (ipr .ge. 5) then
c
c     print g-vectors
c
         istop = 0
         do i=1,pbc%nstar
            write(7,52) i,pbc%mstar(i),pbc%ek(i),izstar(i)
            istar = istop+1
            istop = istar+pbc%mstar(i)-1
            do j=istar,istop
               write(7,54) j,pbc%inds(j),(pbc%kgv(k,j),k=1,3),
     1              real(pbc%phase(j)),aimag(pbc%phase(j)),pbc%conj(j)
            enddo
         enddo
      endif

      deallocate (gm)
      deallocate (izstar)
      deallocate(tmstar)
      deallocate(tek)

 50   format(/,1x,i8,' g-vectors are set up in ',i7,' stars',
     1     ' - kmax = ',3i4)
 52   format(/,' star no',i4,'  with',i3,' members - kin energy =',
     1     f12.8,4x,i3,//,
     2     4x,'i',3x,'inds',5x,'kx',3x,'ky',3x,'kz',9x,'phase')
 54   format(i5,2x,i5,2x,3i5,f10.5,3x,f10.5,5x,f5.2)
 56   format('  *** stopped in gspace   no. of g-vec.= ',i6)

      end subroutine g_space
c     ===============================================================
