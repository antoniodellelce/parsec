c ==============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c ==============================================================
c                               FLAGS
c ==============================================================
c The following flags are provided by the user (in cluster.in):

c restart flag
        integer istart
c boundary condition (system type) flag
        integer ibcflg

c correlation type
c 'xa' -- x-alpha method
c 'wi' -- Wigner interpolation formula
c 'hl' -- hedin-lundquist
c 'ca' -- Ceperly-Alder
c 'pb' -- Perdew-Burke-Erznerhof (gga)
        character (len=2) :: icorr
c polarizability flag
        integer npolflg
c spin flag
        logical ispin
c flag for printout of setup time
        logical is_setup
c pseudopotential format flag
        logical pspflag
c true if writing out wfn.dat ; false if writing out old.dat
        logical olddat
c extrpolation upon restart flag
        integer ixtrpflg
c printing flag
        integer ipr
c error flag
        integer ierr
c     Define what kind of pseudopotential files are available:
c     mod(ipsp,2) = 0  :  *_POTR.DAT in new format (with cutoff radii
c                          and other information included)
c     mod(ipsp,2) = 1  :  *_POTR.DAT in old format
      integer ipsp
c     output flag for writing wfn.dat file:
c     mod(outflag,2) = 1 : write all calculated wave functions 
c     mod(outflag,2) = 0 : write only the ones for occupied states
c     mod(outflag/2,2) = 1 : write wfn.dat only after SCF is finished
c     mod(outflag/2,2) = 0 : write wfn.dat between steps of SCF loop
c
      integer outflag
c additional output flag - outEvFlag
c     output flag for writing eigen.dat file:
c     mod(OutEvFlag,2) = 0 : write all calculated wave functions
c     mod(OutEvFlag,2) = 1 : write only the ones for occupied states
c     mod((OutEvFlag-1)/2,2) = 0 : write eigen.dat only after SCF is finished
c     mod(OutEvFlag/2,2) = 1 : write eigen.dat between steps of SCF loop

      integer OutEvFlag

c     allocation check
      integer alcstat
c     interpolation flag
      logical OldInpFormat
c     output info from MPI communication
      integer mpinfo

c ==============================================================
c                   NUMERICAL ACCURACY PARAMETERS
c ==============================================================
c maximal # of iterations before declaring failure to converge
        integer mxiter
c convergence criterion
        double precision vconv

c ==============================================================
c                        ELECTRONIC STRUCTURE
c ==============================================================
c total energy in Rydberg, total energy per atom in eV
       double precision bdev
c exchange-correlation energy
       double precision exc
c ionization energy difference (used with cc correlation flag only)
       double precision dioniz
c total nuclear energy
      double precision enuc

c ==============================================================
c                             ATOM DATA
c ==============================================================
c elval - the # of valence electrons for a neutral cluster
        double precision elval

c ==============================================================
c                              DIPOLE 
c ==============================================================
c total cluster charge (as computed from the charge density)
        double precision charge

c ==============================================================
c                          POLARIZABILITY 
c ==============================================================
c polarizability field (in a.u.)
        double precision field
c polarizability run counter (indicator of field direction)
        integer ifield

c ==============================================================
c                           COUNTERS 
c ==============================================================
c counters
      integer i,ii,j,idx,k,iter
c temporary variables
      double precision tdummy

c ==============================================================
c                       TIMING VARIABLES
c ==============================================================
c total timing
      double precision tstrt,tfinish,wstrt,wfinish
c Hartree potential timers
      double precision thart0, thart1
c Diagonalization timers
      double precision tdiag0, tdiag1
c Movement and self-consistent-loop timers
      double precision tmove0, tmove1, tscf

c ==============================================================
c                    PERIODIC BOUNDARY CONDITIONS
c ==============================================================
       double precision vhartav

c ==============================================================
c                    LITERAL VARIABLES
c ==============================================================
c     date label
      character (len=26) :: datelabel
c string with mixing history file name
      character(len=6) fnam
c string for storing the extension of the present node; this is
c concatenated to the end of stdout files to indicate messages
c from the nodes separately
      character(len=3) :: idstring
c ==============================================================
