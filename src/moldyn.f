c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     modified by M. Alemany (2003)
c
c     This subroutine computes new positions, velocities, and
c     accelerations for the atoms given an equation of motion. 
c
c     For canonical ensemble : it couples the system to a Langevin
c     heat bath.
c     It solves the equation: d^2(r)/dt^2=F/m-beta*v+G/m, where, for
c     each atom, r is the position vector, F is the previously
c     computed quantum force, beta is a viscosity parameter, v is the
c     velocity, m is the nass, and G is a "white "noise" (Gaussian)
c     force with an autocorrelation of sqrt(2*beta*mass*kT*delta(t-t')),
c     delta() being the Dirac delta function.
c     For a detailed discussion of the Langevin equation see:
c     Reif, Statistical and Thermal Physics, section 15.5, pp. 560
c     -567 (mcGraw-Hill, New York, 1965).
c
c     The subroutine integrates the Langevin equation at finite time
c     steps, using the method suggested by Biswas and Hamann [Phys.
c     Rev. B, 34 895 (1986)] but using the integration method
c     suggested by Beeman  [J. Comput. Phys., 20, 130 (1976)] eqns
c     10A and 10C in that paper. The previous estimate of velocity 
c     is used for the friction force.
c
c     For micro-canonical ensemble: it integrates the newtonian eqns
c     of motion.
c     It solves the equation: d^2(r)/dt^2=F/m, where, per each atom,
c     r is the position vector, F is the previously computed quantum
c     force and m is the mass.
c
c     The subroutine integrates the above equation at finite time
c     steps, using a modification of the Verlet algorithm suggested
c     by Beeman [J. Comput. Phys., 20, 130 (1976)] This is same as
c     verlet algorithm except the estimate of velocity is improved.
c
c     IMPORTANT : To decide whether it is microcanonical or canonical
c     equation of motion this routine just tests the value of beta.
c     If beta < 10^-6 then it is microcanonical else it is canonical.
c
c     NOTE: This subroutine uses Hartree as the units. The reason for
c     this choice rather than Rydbergs is that unless one puts in
c     factors of 2 and sqrt(2) in in force and velocity, the are
c     directly not in the same units and so you cannot just add them
c     without a problem. If you consistently use hartrees you 
c     are always fine (they are atomic units and hence compatible
c     with everything else).
c
c     ---------------------------------------------------------------

      subroutine moldyn(clust,mol_dynamic,imove,is_pbc,bdev,alatt)

      use constants
      use cluster_module
      use molecular_dynamic_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type(cluster), intent(inout) :: clust
c     molecular dynamic related data
      type (molecular_dynamic), intent(inout) :: mol_dynamic
c     # of time steps
      integer, intent(inout) :: imove
c     boundary condition flag
      logical, intent(in) :: is_pbc
c     total energy per atom in eV
      real(dp), intent(in) :: bdev
c     size of sides of box (need ONLY for periodic boundary conditions)
      real(dp), intent(in) :: alatt(3)
c
c     Work variables:
c
c     atomic mass of each ion type
      real(dp) :: amass(clust%atom_num)

c     atomic coordinates
      real(dp), dimension(clust%atom_num) ::  xatm, yatm, zatm

c     Langevin trajectory stuff
c     current positions, old positions (from previous step)
      real(dp), dimension(mol_dynamic%atom_num) :: 
     1     xcur, ycur, zcur, xold, yold, zold

c     velocities (from previous step and the current ones); they are
c     stored in the same variable (although it is called old)
      real(dp), dimension(mol_dynamic%atom_num) :: vxold, vyold, vzold

c     old accelerations (from previous step and current ones); again,
c     they are stored in the same variable (although it is called old)
      real(dp), dimension(mol_dynamic%atom_num) :: 
     1     accxold, accyold, acczold

c     total actual number of atoms
      integer natom
c     number of different atom types
      integer naty
c     Temperature in hartree
      real(dp) :: tempeh
c     accumulators for total mass and center of mass velocity
      real(dp) :: vxav, vyav, vzav, tmass
c     thermal velocity along each axis
      real(dp) :: vel
c     the standard deviation of the random force
      real(dp) :: sigm
c     square of time step
      real(dp) :: dtsq
c     temporary storage of older position components
      real(dp) :: xoldr, yoldr, zoldr
c     temporary storage of older velocity components
      real(dp) :: vxoldr,vyoldr,vzoldr
c     temporary storage of older acceleration components
      real(dp) :: accxoldr, accyoldr, acczoldr
c     minimum and maximum of coordinates along each axis
      real(dp) :: xmin, xmax, ymin, ymax, zmin, zmax
c     amount by which to shift xcur,ycur,zcur to center coordinates
c     about the origin
      real(dp) :: xmov, ymov, zmov
c     total kinetic energy [Ry] and total kinetic energy per atom
c     [eV].
      real(dp) :: tke, tkeev
c     effective actual temperature 
      real(dp) :: tcalc
c     factor with which to rescale the velocities
c     (ratio of t_input /tcalc)
      real(dp) :: rescale
c     inverse of alatt
      real(dp) :: alattinv(3)

c     current and final temperatures (in kelvin)
      real(dp) :: tempi, tempf
c     time step, friction coefficient
      real(dp) :: deltat,beta

c     Counter of atom movement cycles in molecular dynamics. It is
c     different from imove only when using stairstep cooling, where
c     imove is reset to zero for each "stair" and iframe is the total
c     number of movements.
      integer iframe

c     size of vrandom
      integer, parameter :: nrandom = 10000
c     set of random numbers; this set is reconstructed every time all
c     numbers are used 
      real(dp) :: vrandom(nrandom)
c     position counter
      integer, save :: iset=0
c     counters
      integer i,j,ii
c
c     External functions:
c
c     a random Gaussian variable with a mean of zero and a standard 
c     deviation of one
      real(dp), external :: gdev

c     ---------------------------------------------------------------

      natom = clust%atom_num
      naty = clust%type_num
      xatm = clust%xatm
      yatm = clust%yatm
      zatm = clust%zatm
      amass = clust%amass

      tempi = mol_dynamic%tempi
      tempf = mol_dynamic%tempi
      beta = mol_dynamic%friction_coef
      deltat =  mol_dynamic%time_step
      iframe = mol_dynamic%iframe

      xcur = mol_dynamic%xcur
      ycur = mol_dynamic%ycur
      zcur = mol_dynamic%zcur

      xold = mol_dynamic%xold
      yold = mol_dynamic%yold
      zold = mol_dynamic%zold

      vxold = mol_dynamic%vxold
      vyold = mol_dynamic%vyold
      vzold = mol_dynamic%vzold

      accxold = mol_dynamic%accxold
      accyold = mol_dynamic%accyold
      acczold = mol_dynamic%acczold

c
c     change temperature from kelvin (input) to atomic unit (Hartree)
      tempeh = tempi * tempkry * half
 
c
c     if initial run, there are no previous steps to use a random guess 
c     for the velocity
c
      if ((imove .eq. 0) .and. (.not. mol_dynamic%is_restart)) then
c     set accumulators for total mass and center of mass velocity
         do  j=1, natom
c     find thermal velocity in each direction, x, y, z
c                                  (0.5kT=0 .5m(vel)^2)
            vel = sqrt(tempeh/amass(j))
c     assign velocity based on a maxwell distribution:
c          p(v) ~ exp (-0 .5*v^2/vel^2)
c     gdev returns a random number x with a propbability
c          p(x) ~ exp ( -0.5*x^2)
c     and therefore the needed v is simply vel*x
            vxold(j) = vel*gdev(nrandom,iset,vrandom)
            vyold(j) = vel*gdev(nrandom,iset,vrandom)
            vzold(j) = vel*gdev(nrandom,iset,vrandom)
c     accumulate velocities for center of mass velocity calculation
            vxav = dot_product (amass,vxold)
            vyav = dot_product (amass,vyold)
            vzav = dot_product (amass,vzold) 
c     accumulate total mass
         enddo
         tmass = sum (amass)

c
c     find center of mass velocity
         vxav = vxav/tmass
         vyav = vyav/tmass
         vzav = vzav/tmass
c     Correct random velocities so that the center of mass does not
c     move and also find the kinetic energy of the atoms for
c     calculation of temperature. Note that the kinetic energy is in
c     hartrees.
         tke = zero

         vxold = vxold-vxav; vyold = vyold-vyav; vzold = vzold-vzav 
         do j = 1, natom
            tke = tke+half*amass(j)*(vxold(j)*vxold(j)+vyold(j)*
     1           vyold(j)+vzold(j)*vzold(j))
         enddo
c     calculate the actual temperature and rescale the velocities so
c     that you get the input temperature
         if (natom .gt. 1) then
            if (beta .lt. 10d-6) then
               rescale = two*tke/real(3*natom,dp)
            else
               rescale = two*tke/real(3*natom - 3,dp)
            endif
            rescale = sqrt(tempeh/rescale)
         else
            rescale = one
         endif
c     rescale the velocities
         vxold=vxold*rescale; vyold=vyold*rescale; vzold=vzold*rescale

c     rescale the kinetic energy as well in hartree
         tke = tke * rescale * rescale
c 
c     If beta is zero we are doing a micro canonical ensemble (and
c     the system is not coupled to a heat bath at all we use the
c     Beeman algorithm to integrate the equations of motions). 
c     If however beta is non zero ie we are in a canonical ensemble
c     and will have some random force terms.
c
c     save the current coordinates into {x,y,z}old
         xold = xcur; yold = ycur; zold = zcur

         if (beta .lt. 1.0d-6) then
            do j = 1, natom
c     calculate the accelarations (in hartree/mass-au)
               accxold(j) = half*clust%force(1,j)/amass(j)
               accyold(j) = half*clust%force(2,j)/amass(j)
               acczold(j) = half*clust%force(3,j)/amass(j)
c     calculate the new positions based on the velocity and
c     acceleration as x1=x0+v*t+1/2*a*t^2
               xcur(j) = xold(j) + deltat*(vxold(j) +
     1              half*accxold(j)*deltat)
               ycur(j) = yold(j) + deltat*(vyold(j) +
     1              half*accyold(j)*deltat)
               zcur(j) = zold(j) + deltat*(vzold(j) +
     1              half*acczold(j)*deltat)
c     also update the velocity based on v1=v0+a*t
               vxold(j) = vxold(j) + accxold(j)*deltat
               vyold(j) = vyold(j) + accyold(j)*deltat
               vzold(j) = vzold(j) + acczold(j)*deltat
            enddo
         else
            do j = 1, natom
c     Calculate the standard deviation (i.e., the autocorrelation)
c     sigm = sqrt(two*beta*amass(j)*tempeh/deltat) is the standard
c     deviation of the force. The devision by deltat comes from
c     approximating delta(t-t') of continuous time as the square
c     pulse function: 1/(t-t')[u(t) -u(t')], u() being the Heavyside
c     square function.
c     Because we are interested in accelerations and not forces, we
c     will further divide by amass(j), moving it from numerator to
c     denominator.
               sigm = sqrt(two*beta*tempeh/(deltat*amass(j)))
c     For the random acceleration, again gdev is used to get a
c     standard deviation of one, and then mutiplying by sigm corrects
c     the standard deviation.
c     Basically a = F/2*m - beta*v + G/m Again the division by 2 in
c     the first term F/2m is to take care of Ry to hartree conversion
               accxold(j)=half*clust%force(1,j)/amass(j) -
     1              beta*vxold(j) + sigm*gdev(nrandom,iset,vrandom)
               accyold(j)=half*clust%force(2,j)/amass(j) -
     1              beta*vyold(j) + sigm*gdev(nrandom,iset,vrandom)
               acczold(j)=half*clust%force(3,j)/amass(j) -
     1              beta*vzold(j) + sigm*gdev(nrandom,iset,vrandom)
c     calculate the first new position : x1=x0+v0*t+1/2*a*t^2
               xcur(j) = xold(j) + deltat*(vxold(j) + 
     1              half*deltat*accxold(j))
               ycur(j) = yold(j) + deltat*(vyold(j) + 
     1              half*deltat*accyold(j))
               zcur(j) = zold(j) + deltat*(vzold(j) + 
     1              half*deltat*acczold(j))
c     now update the velocities v1=v0+at
               vxold(j) = vxold(j) + deltat*accxold(j)
               vyold(j) = vyold(j) + deltat*accyold(j)
               vzold(j) = vzold(j) + deltat*acczold(j)
            enddo
         endif
c
c     print initial coordinates to atom.cor
         write(66,*) 'Coordinates for step #', iframe
         ii = 0
         do i = 1, naty
            write(66,*) clust%name(i)
            do j = 1, clust%natmi(i)
               ii = ii + 1
               write(66,82) xold(ii),yold(ii),zold(ii)
            enddo
         enddo
c     also, initialize random number generator
         call random_seed
         i = 1
         call random_seed(SIZE=i)
c
c     if restarted MD, or MD has already been used at least once
c     before, then use the two previous sets of positions,
c     velocities, accelerations to create new atomic position
c
      else
c     again there are two cases here: microcanonical and canonical
         tke = zero
         if (beta .lt. 1.0d-6) then
c     in this case, we also have to make sure that we subtarct the
c     center of mass velocity. Tha just complicates things a little 
c     bit and some careful handling of old and new coordinates and 
c     accelerations is required. This will not be that big a problem
c     because the newer accelerations are just F/m.
c     To begin with we will not move anything around. Will do that 
c     as and when needed.

c     set accumulators for total mass and center of mass velocity
            do  j= 1, natom
c     first predict the velocities
c     while in the actual formula it should be 
c     v=(x1-x0)/dt +  dt*(2*F/m+accold)/6 
c     but since the force is in Ry and has to 
c     be converted to hatree the factor of 2 is missing
               vxold(j) = (xcur(j) - xold(j))/deltat +
     1              deltat*(clust%force(1,j)/amass(j)+accxold(j))/six
               vyold(j) = (ycur(j) - yold(j))/deltat +
     1              deltat*(clust%force(2,j)/amass(j)+accyold(j))/six
               vzold(j) = (zcur(j) - zold(j))/deltat +
     1              deltat*(clust%force(3,j)/amass(j)+acczold(j))/six
            enddo
            vxav = dot_product(amass,vxold)
            vyav = dot_product(amass,vyold)
            vzav = dot_product(amass,vzold)
            tmass = sum (amass)
c
c     find center of mass velocity
            vxav = vxav/tmass
            vyav = vyav/tmass
            vzav = vzav/tmass
c     correct velocities so that the center of mass does not move and
            vxold = vxold-vxav
            vyold = vyold-vyav
            vzold = vzold-vzav
c     move the positions to the old
            xold = xcur; yold = ycur; zold = zcur
            do j = 1, natom

c     calculate the updated positions
c     again the actual expression should have been
c     x1 = x0 + dt*(v0 + dt*(4*F/m-accold)/6) but again
c     since the units are hartree we replace 4*F/m by 2*F/m
               xcur(j) = xold(j) + deltat*(vxold(j) + deltat*
     1              (two*clust%force(1,j)/amass(j)-accxold(j))/six)
               ycur(j) = yold(j) + deltat*(vyold(j) + deltat*
     1              (two*clust%force(2,j)/amass(j)-accyold(j))/six)
               zcur(j) = zold(j) + deltat*(vzold(j) + deltat*
     1              (two*clust%force(3,j)/amass(j)-acczold(j))/six)
c     find the kinetic energy of the atoms for calculation of
c     temperature.
               tke = tke + half*amass(j)*(vxold(j)*vxold(j)+vyold(j)*
     1              vyold(j)+vzold(j)*vzold(j))
c     finally correct the velocity again the force term has the
c     factor half to take care of Ry to Hartree
               vxold(j) = vxold(j) + 
     1              deltat*(three*half*clust%force(1,j)/
     2              amass(j)-accxold(j))*half
               vyold(j) = vyold(j) + 
     1              deltat*(three*half*clust%force(2,j)/
     2              amass(j)-accyold(j))*half
               vzold(j) = vzold(j) + 
     1              deltat*(three*half*clust%force(3,j)/
     2              amass(j)-acczold(j))*half
c     And now that we finally dont need acc{x,y,z}old anymore replace
c     them with F/m again F/2m because of Ry to hartree
               accxold(j) = half*clust%force(1,j)/amass(j)
               accyold(j) = half*clust%force(2,j)/amass(j)
               acczold(j) = half*clust%force(3,j)/amass(j)
            enddo
         else
c
c     for all atoms...
            do j = 1, natom
c     Calculate the standard deviation (i.e., the autocorrelation)
c     sigm = sqrt(two*beta*amass(j)*temper/deltat) is the standard
c     deviation of the force. The devision by deltat comes from
c     approximating delta(t-t') of continuous time as the square
c     pulse function: 1/(t-t')[u(t) -u(t')],  u() being the Heavyside
c     square function.
c     Because we are interested in accelerations and not forces, we
c     will further divide by amass(j), moving it from numerator to
c     denominator.
               sigm = sqrt(two*beta*tempeh/(deltat*amass(j)))
c     Put the old position coordinates into temporary position
c     coordinates.
               xoldr = xold(j)
               yoldr = yold(j)
               zoldr = zold(j)
c     Put the current position coordinates into old ones (We have not
c     updated the position yet).
               xold(j) = xcur(j)
               yold(j) = ycur(j)
               zold(j) = zcur(j)
c     Put the older accelerations in the temporary acceleration
c     holders.
               accxoldr = accxold(j)
               accyoldr = accyold(j)
               acczoldr = acczold(j)
c     Use old velocity guess in Langevin equation to get new
c     accelerations -  the half  factor is there because of Ry to
c     hartree.
               accxold(j)=half*clust%force(1,j)/amass(j)-
     1              beta*vxold(j)+sigm*gdev(nrandom,iset,vrandom)
               accyold(j)=half*clust%force(2,j)/amass(j)-
     1              beta*vyold(j)+sigm*gdev(nrandom,iset,vrandom)
               acczold(j)=half*clust%force(3,j)/amass(j)-
     1              beta*vzold(j)+sigm*gdev(nrandom,iset,vrandom)
c     predict the velocity
               vxoldr=(xold(j)-xoldr)/deltat +
     1              deltat*(two*accxold(j)+ accxoldr)/six
               vyoldr=(yold(j)-yoldr)/deltat +
     1              deltat*(two*accyold(j)+ accyoldr)/six
               vzoldr=(zold(j)-zoldr)/deltat +
     1              deltat*(two*acczold(j)+ acczoldr)/six
c     calculate the new position coordinates 
c     and put them in xcur
               xcur(j)=xold(j)+deltat*(vxoldr + deltat* 
     1              (four*accxold(j)-accxoldr)/six)
               ycur(j)=yold(j)+deltat*(vyoldr + deltat*
     1              (four*accyold(j)-accyoldr)/six)
               zcur(j)=zold(j)+deltat*(vzoldr + deltat*
     1              (four*acczold(j)-acczoldr)/six)
c     calculate the kinetic energy
               tke = tke+half*amass(j)*(vxoldr*vxoldr+vyoldr*
     1              vyoldr+vzoldr*vzoldr)
c     finally correct the velocity
               vxold(j) = vxoldr + deltat*
     1              (three*accxold(j)-accxoldr)*half
               vyold(j) = vyoldr + deltat*
     1              (three*accyold(j)-accyoldr)*half
               vzold(j) = vzoldr + deltat*
     1              (three*acczold(j)-acczoldr)*half
            enddo
         endif
      endif
      if (.not. is_pbc) then
c
c     if this is a confined system:
c     If the coordinates are not symmetrized about the origin, shift
c     them so that they are, in order to allow the results to
c     converge already for a smaller Rmax.
 
c     find minimum and maximum coordinate along each axis
         xmin = minval(xcur); ymin = minval(ycur); zmin = minval(zcur)
         xmax = maxval(xcur); ymax = maxval(ycur); zmax = maxval(zcur)

c     move the coordinates so that along each axis the farthest atom
c     in the positive direction is as far as the farthest atom in
c     the negative direction
         xmov = half*(xmin+xmax)
         ymov = half*(ymin+ymax)
         zmov = half*(zmin+zmax)
 
         do j=1,natom
            xatm(j) = xcur(j)-xmov
            yatm(j) = ycur(j)-ymov
            zatm(j) = zcur(j)-zmov
         enddo

      else                      ! .not. is_pbc
c
c     if this is a periodic system:
c     It is possible that some of the atoms have "moved outside of
c     the cell" because of the Langevin dynamics. For the electronic
c     structure problem, we need to "wrap" them back into the
c     supercell. The stored trajectories (those printed to
c     md_mech .dat) DO NOT have this wrapping so that the integration
c     of the positions remains correct.
         alattinv=one/alatt
         do j=1,natom
            xatm(j) = xcur(j)-alatt(1)*nint(xcur(j)*alattinv(1))
            yatm(j) = ycur(j)-alatt(2)*nint(ycur(j)*alattinv(2))
            zatm(j) = zcur(j)-alatt(3)*nint(zcur(j)*alattinv(3))
         enddo
      endif
c
c     print coordinates, velocities, accelerations to md_mech.dat
      write(79,24) iframe+1
      do j=1,natom
         write(79,22) xcur(j),ycur(j),zcur(j),xold(j),yold(j),zold(j)
     1        ,vxold(j),vyold(j),vzold(j)
     2        ,accxold(j),accyold(j),acczold(j)
      enddo
 22   format(12(1x,e15.8))
 24   format('Step #',i4)
c
c     just for the purpose of reporting, calculate the center of mass
c     velocity in the end as well...
c     set accumulators for total mass and center of mass velocity
      vxav = dot_product(amass,vxold)
      vyav = dot_product(amass,vyold)
      vzav = dot_product(amass,vzold)
      tmass = sum(amass)
c
c     find center of mass velocity
c
      vxav = vxav/tmass
      vyav = vyav/tmass
      vzav = vzav/tmass
   
c     calculate total kinetic energy per atom (tke was calculated
c     earlier) since it is in hatree we convert it to Ry/atom and eV
      tke = tke*two/real(natom,dp)
      tkeev = tke*rydberg

c     Calculate the effective temperature (as opposed to the input
c     one).
c     Note here the natom/(natom-1) has to do with the correct
c     degrees of freedom! for canonical ensemble. For micro-canonical 
c     there is no such correction needed.
      if (beta .lt. 1.0d-6) then
         tcalc = two/three*tke/tempkry
      else
         tcalc = two/three*tke/tempkry*real(natom,dp)/real(natom-1,dp)
      endif

c     report the kinetic, potential, and total energies per atom, in
c     eV, input and actual temperature  to md_nrg.dat
      write(78,12) iframe,real(iframe,dp)*deltat*timeaups,tkeev,bdev,
     1     tkeev+bdev,tempi,tcalc
 12   format(i4,3x,f7.3,2x,f11.5,2(2x,f11.5),2x,f7.2,3x,f7.2)
c
c     unless this was already the last step...
      if ((imove .ne. mol_dynamic%step_num) .or. 
     1     (abs(tempi-tempf).gt.1.d-5)) then
c
c     report coordinates to atom.cor
         write(66,*) 'Coordinates for step #', iframe+1
         ii = 0
         do i = 1, naty
            write(66,*) clust%name(i)
            do j = 1, clust%natmi(i)
               ii = ii + 1
               write(66,82) xatm(ii),yatm(ii),zatm(ii)
            enddo
         enddo

c     report xyz formatted data to movie.xyz
         write(92,84) natom
         write(92,*) 'Step # ', iframe+1
         ii = 0
         do i = 1, naty
            do j = 1, clust%natmi(i)
               ii = ii + 1
               write(92,88) clust%name(i), xatm(ii)*angs,
     1              yatm(ii)*angs, zatm(ii)*angs
            enddo
         enddo
c     If this was the last step, report coordinates for next run to
c     atom.cor.
      else
         write(66,*)
         write(66,*) ' Coordinates for next Run '
         do j = 1,natom
            write(66,80) xatm(j),yatm(j),zatm(j)
         enddo
      endif
 80   format(3(2x,f11.6))
 82   format(3(2x,f11.6))
 84   format(i3)
 88   format(a2, 3(3x, f10.6))
c
c     update the temperature (in K) for the next run, depending on
c     the MD scheme

      select case (mol_dynamic%cool_type)
c     "stairstep" annealing
      case(1)
c     if at last step per the given "stair"
         if (imove .eq. mol_dynamic%step_num) then  
c     calculate the temperature for the next stair - 
c     move according to fixed step, unless close to 
c     final temperature, in which case clamp the temperature 
c     to the final one
            if (abs(tempi-tempf) .gt. 1.d-5) then
               tempi = tempi + mol_dynamic%tscale
               imove = 0
               if ( abs(tempi-tempf) .le. 
     1              abs(mol_dynamic%tscale/two) ) tempi = tempf
            endif
         endif
c     linear annealing
      case (2)
         tempi = tempi + mol_dynamic%tscale
c     logrithmic annealing
      case (3)
         tempi = tempi * mol_dynamic%tscale
      end select
c
c     update frame and step number for next movement
      iframe = iframe + 1
      imove = imove + 1

      clust%xatm = xatm
      clust%yatm = yatm
      clust%zatm = zatm

      mol_dynamic%tempi = tempi
      mol_dynamic%tempf = tempf
      mol_dynamic%iframe = iframe

      mol_dynamic%xcur = xcur
      mol_dynamic%ycur = ycur
      mol_dynamic%zcur = zcur

      mol_dynamic%xold = xold
      mol_dynamic%yold = yold
      mol_dynamic%zold = zold

      mol_dynamic%vxold = vxold
      mol_dynamic%vyold = vyold
      mol_dynamic%vzold = vzold

      mol_dynamic%accxold = accxold
      mol_dynamic%accyold = accyold
      mol_dynamic%acczold = acczold

c     flush buffer of md output files
      call myflush(66)
      call myflush(78)
      call myflush(79)
      call myflush(92)
 
      end subroutine moldyn
c     ===============================================================
c
c     This function selects one random number between 0 and 1 from a
c     given set and generates a random Gaussian variable with a mean
c     of zero and a standard deviation of one.
c     On output, iset is incremented by one.
c     When random set is exhausted (iset = nrandom), update seed and
c     create a new set.
c
c     ---------------------------------------------------------------
      function gdev(nrandom,iset,vrandom)

      use constants
      implicit none
c
c     Input/Output variables:
c
c     size of random set
      integer, intent(in) :: nrandom
c     position of last used random number in set
      integer, intent(inout) :: iset
c     set of random numbers
      real(dp) :: vrandom(nrandom)
c     selected random number
      real(dp) :: gdev
c
c     Work variables:
c
c     two random uniform variables between -1 and +1, and the sum of
c     their squares
      real(dp) :: v1, v2, r
c     Two random Guassian variable with 0 mean and unity standard
c     deviation
      real(dp) :: gset
c     temporary variable
      real(dp) :: fac
c
c     External function:
c
      real(dp), external :: getrandom
c     ---------------------------------------------------------------

c     Create two random variables distributed uniformly between -1
c     and +1, from two calls of getrandom, which returns a uniform
c     variable between 0 and 1.
 1    v1 = two * getrandom(nrandom,iset,vrandom) - one
      v2 = two * getrandom(nrandom,iset,vrandom) - one
c     in the (v1,v2) plane, is the results in the unit circle?
c     If not, try again.
      r= v1 * v1 + v2 * v2
      if (r .ge. 1.d0) goto 1
c     Use the Box-muller transformation to create two independent
c     Gaussian variables - gset and gdev - from the two uniform ones.
c     gdev will be returned.
      fac = sqrt( -two * log(r) / r )
      gset = v1 * fac
      gdev = v2 * fac

      end function gdev
c     ===============================================================
c
c     Picks element of order "iset" from an array of random numbers,
c     "vrandom".
c     On output, iset is incremented by one.
c     When random set is exhausted (iset = nrandom), update seed and
c     create a  new set. In order to avoid the unlikely possibility
c     of two successive calls to system clock in the same milisecond
c     creating the same seed, accumulate system clock to seed.
c
c     ---------------------------------------------------------------
      function getrandom(nrandom,iset,vrandom)

      use constants
      implicit none
c
c     Input/Output variables:
c
c     size of random set
      integer, intent(in) :: nrandom
c     position of last used random number in set
      integer, intent(inout) :: iset
c     set of random numbers
      real(dp), intent(inout) :: vrandom(nrandom)
c     selected random number
      real(dp) :: getrandom
c
c     Work variables:
c
c     seed (updated if vrandom is recreated)
      integer :: iseed,oldseed,nr,ii
c     ---------------------------------------------------------------
      if (iset .eq. 0) then
         call system_clock(COUNT=iseed)
         call random_seed(SIZE=nr)
c         oldseed = 0
c         call random_seed(GET=oldseed)
         iseed = iseed + oldseed

ccm
c     fixed seed (use only for debugging purposes!!!!)
c         iseed = 123456
ccm

         call random_seed(PUT=(/ (iseed,ii=1,nr) /))
         call random_number(vrandom)
         iset = 1
      endif
      getrandom = vrandom(iset)
      iset = iset + 1
      if (iset .gt. nrandom) iset = 0

      end function getrandom
c     ===============================================================
