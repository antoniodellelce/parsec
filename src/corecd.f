c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine sets up the superposition of core-correction
c     charge densities, if any, associated with all atoms, on the
c     grid. This subroutine handles both the case of a periodic
c     system and a confined system.
c
c     Core correction is described in: S. G. Louie, S. Froyen, and M.
c     L. Cohen, Phys. Rev. B26, 1738 (1982).
c
c     NOTE: The core charge density is NOT divided by 4*pi*r^2 to
c     convert to a volume density. This means that we expect the
c     input from the pseudopotential code to be given as a VOLUME
c     density even when still given on the radial pseudopotential
c     grid (compare with the computation of rho_r in initchrg.f)
c
c     WARNING: this subroutine is not fully parallelized!
c
c     ---------------------------------------------------------------
      subroutine corecd(clust,grid,p_pot,parallel,rhoc,is_pbc,alatt
     1     ,ierr)

      use constants
      use cluster_module
      use grid_module
      use pseudo_potential_module
      use parallel_data_module
      implicit none 
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(in) :: clust
c     grid related data
      type (grid_data), intent(in) :: grid
c     pseudo_potential related data
      type (pseudo_potential), intent(in) :: p_pot
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel
c     PBC flag
      logical, intent(in) :: is_pbc
c     lattice vectors, used only if is_pbc
      real(dp), intent(in) :: alatt(3)
c     superposition of the core-correction charge density,
c     given on the 3-d grid points
      real(dp), intent(out) :: rhoc(parallel%mydim)
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     temporary storage variables
      real(dp) :: dist,rindex,delr,corch,dmax,dmin,dxx,dyy,dzz,xa,ya,za
c     work array for master processor
      real(dp), allocatable :: rho_t(:)
c     coordinates of grid point with lowest charge density
      real(dp) :: xmin,ymin,zmin
      integer jok,jmin
c     number of replicas of the periodic cell to be used in the
c     construction of non-local spheres around each atom
c     nreplica = 0 if .not. is_pbc
      integer nreplica
c     allocation check
      integer alcstat
c     counters
      integer ja,itype,i,iat,iw,icellx,icelly,icellz
c     minimal numerical tolerance for zero in this subroutine
      real(dp), parameter :: eps = 1.d-1

c     ---------------------------------------------------------------

      if (parallel%iammaster) then
c
c     if there is not core-correction charge, skip all this
c
         if (maxval(p_pot%icore) .eq. 0) then
            parallel%ftmp = zero
            goto 20
         endif
c
c     initialize the total core-correction charge to zero
c
         allocate(rho_t(grid%nwedge),stat=alcstat)
         call alccheck('rho_t',grid%nwedge,alcstat)
         rho_t = zero

c     Define parameters for periodic boundary conditions.
         if (is_pbc) then
            nreplica = 1
         else
            nreplica = 0
         endif
c     initialize ja - counter of all atoms in the system
         ja = 0
c     go over all atom types
         do itype = 1, clust%type_num
c     If core-correction flag is on:
            if (p_pot%icore(itype) .eq. 1) then
c     go over all atoms and superimpose the contribution 
c     from each atom over each point of the grid
               do iat = 1, clust%natmi(itype)
                  ja = ja + 1
                  xa = clust%xatm(ja)
                  ya = clust%yatm(ja)
                  za = clust%zatm(ja)
                  do iw = 1, grid%nwedge
                     do icellx = -nreplica,nreplica
                        do icelly = -nreplica,nreplica
                           do icellz = -nreplica,nreplica
                              dxx = (grid%shift(1) + grid%kx(iw))
     1                             *grid%step(1) - xa
     2                             + real(icellx,dp)*alatt(1)
                              dyy = (grid%shift(2) + grid%ky(iw))
     1                             *grid%step(2) - ya
     2                             + real(icelly,dp)*alatt(2)
                              dzz = (grid%shift(3) + grid%kz(iw))
     1                             *grid%step(3) - za
     2                             + real(icellz,dp)*alatt(3)

                              dist = sqrt(dxx*dxx+dyy*dyy+dzz*dzz)
c     if dist is larger than the largest value of r given in the 
c     pseudopotential file, rs(i,itype), there is no core charge
c     correction

                              if (dist .ge.
     1                             p_pot%rs(p_pot%ns(itype)-1,itype))
     2                             then
                                 corch = zero
                              else
c
c     if dist is smaller than the largest value of r, find the 
c     index of dist in the pseudopotential by inverting the
c     logarithmic pseudopotential grid, then interpolate the radial
c     core-correction charge, denc, to determine the charge
c     contribution

c     rindex = 1/B(itype)*log(1+1/A(itype)*dist)
c     A = p_pot%par_a
c     B = p_pot%par_b
                                 rindex = one/p_pot%par_b(itype)*
     1                                log(one+one/p_pot%par_a(itype)
     2                                *dist)
                                 jok  = idint(rindex) + 1
                                 delr=(dist-p_pot%rs(jok,itype))/
     1                                (p_pot%rs(jok+1,itype)-
     2                                p_pot%rs(jok,itype))
                                 corch = p_pot%denc(jok,itype) + delr
     1                                *(p_pot%denc(jok+1,itype)
     2                                -p_pot%denc(jok,itype))
                              endif
                              rho_t(iw) = rho_t(iw) + corch
                           enddo
                        enddo
                     enddo
                  enddo         ! iw = 1, grid%nwedge
               enddo            ! iat = 1, clust%natmi(itype)
c     if no core correction for this type just update the atom
c     counter
            else
               ja = ja + clust%natmi(itype)
            endif
         enddo                  ! itype = 1, clust%type_num
c
c     find minimum and maximum values of core-correction charge
c     density, and the grid index where these values occur
c
         dmax = maxval (rhoc)
         dmin = minval (rhoc)
         jmin = maxval (minloc (rhoc))
         xmin = (grid%shift(1) + grid%kx(jmin))*grid%step(1)
         ymin = (grid%shift(2) + grid%ky(jmin))*grid%step(2)
         zmin = (grid%shift(3) + grid%kz(jmin))*grid%step(3)

         write(7,*) 'Setup of inital charge and Hartree potential:'
         write(7,*) '---------------------------------------------'
c
c     report smallest and largest values of core-correction charge
c     density
c
         write(7,*)
     1        'extremal values of core charge density [e/bohr^3]'
         write(7,10) dmax,dmin
 10      format(e12.5,3x,e12.5)
 11      format(1x,'x,y,z =',1x,f6.3,1x,f6.3,1x,f6.3)
c
c     Look for negative densities. If the negative density is within
c     numerical accuracy of zero, set it to zero. If not, declare an
c     error and quit.
c
         if (dmin .lt. zero) then
            if (abs(dmin) .lt. eps) then
               do i = 1, grid%nwedge
                  if (rho_t(i) .lt. zero) rho_t(i) = zero
               enddo
            else
               write(7,*)
               write(7,*)
     1              'ERROR: negative core-correction charge density'
               write(7,*) 'Charge is most negative at:'
               write(7,11) xmin,ymin,zmin
               write(7,*) 'STOP in corecd'
               ierr = 1
               return
            endif
         endif
         parallel%ftmp = rho_t
         deallocate(rho_t)
 20      continue
      endif                     ! parallel%iammaster

c     master PE distributes the ion potential
      call export_function(parallel,rhoc)

      end subroutine corecd
c     ===============================================================
