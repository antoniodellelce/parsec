c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine calculate ionic force contributions and the
c     total nuclear  energy using CLASSICAL formulas, based on the
c     ionic charges and the distance between the atoms.
c      
c     ---------------------------------------------------------------
      subroutine forceion(clust,zion,ipr,enuc)

      use constants
      use cluster_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     positive charge of pseudo ion for each ion type
      real(dp), intent(in) :: zion(clust%type_num)
c     print flag
      integer, intent(in) :: ipr
c     total nuclear energy
      real(dp), intent(out) :: enuc
c
c     Work variables:
c
c     distance between two atoms and their cube,
c     multiplication of their charges
      real(dp) :: rij, rij2, qiqj, dxx, dyy, dzz,qdevr
c     counters
      integer ia,ja,ity,jty,i,j

c     ---------------------------------------------------------------

c     initialize nuclear energy
      enuc = zero

c     initialize force on each atom
      clust%force = zero

c     initialize first atom counter ("i atoms")
      ia = 0
c     for all "i" atoms (atom types and atom within type)
      do ity = 1,clust%type_num
         do i = 1, clust%natmi(ity)
            ia = ia + 1
c     initialize second atom counter ("j atoms")
            ja = 0
c     for all "j" atoms (atom types and atom within type)
            do jty = 1, clust%type_num
               do j = 1, clust%natmi(jty)
                  ja = ja + 1
c     for each i, j pair (disregard ia=ja because this is unphysical 
c     self-interaction):
c     calculate distance between atoms and its cube, and the
c     multiplication of their ionic charges
                  if (ia .ne. ja) then
                     dxx = clust%xatm(ia)-clust%xatm(ja)
                     dyy = clust%yatm(ia)-clust%yatm(ja)
                     dzz = clust%zatm(ia)-clust%zatm(ja)
                     rij2 = (dxx*dxx+dyy*dyy+dzz*dzz)
                     rij = sqrt(rij2)
                     qiqj = zion(ity)*zion(jty)
                     qdevr = qiqj/(rij2*rij)
c     update forces: F=qi*qj*r/|r|^3
c     the factor of two is because we want the force in Rydberg/bohr
c     and not Hartree/bohr !
                     clust%force(1,ia) = clust%force(1,ia) + 
     1                    two*(clust%xatm(ia)-clust%xatm(ja))*qdevr
                     clust%force(2,ia) = clust%force(2,ia) + 
     1                    two*(clust%yatm(ia)-clust%yatm(ja))*qdevr
                     clust%force(3,ia) = clust%force(3,ia) + 
     1                    two*(clust%zatm(ia)-clust%zatm(ja))*qdevr

c     compute total nuclear energy
c     Here there is no unit problem because the two of
c     Hartree->Rydberg cancels with the half resulting from summing
c     over all atom pairs, so each pair is counted twice.
                     enuc = enuc + qiqj/rij
                  endif
               enddo
            enddo
         enddo
      enddo

c     if print flag on, print the ion-ion forces
      if(ipr .ge. 1) then
         write(7,*)
         write(7,*) 'Forces from ion-ion interaction:'
         write(7,*) '================================'
         write(7,22)
         write(7,*)
         do ia = 1, clust%atom_num
            write(7,20) ia,clust%xatm(ia),clust%yatm(ia)
     1           ,clust%zatm(ia),clust%force(:,ia)
         enddo
         write(7,*)
      endif

 20   format(i4,2x,3(f11.6,1x),2x,3(f11.6,1x))
 22   format('-atom-  ----x----   ----y----   ----z-----'
     1     ,'   ----Fx----  ----Fy----   ----Fz---',/
     2     '                       [bohr]           '
     3     ,'                  [Ry/bohr]            ')

      end subroutine forceion
c     ===============================================================
