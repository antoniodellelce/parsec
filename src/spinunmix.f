c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     author: ?
c
      subroutine spinunmix(pot,mixer)

      use constants
      use potential_module
      use mixer_module
      implicit none
c
c     Input/Output variables:
c
      type (potential), intent(inout) :: pot
      type (mixer_data), intent(in) :: mixer
c
c     Work variables:
c
      integer i
c     ---------------------------------------------------------------
      do i = 1,pot%ndim
         pot%vnew(i,1)=mixer%xout(i)
         pot%vnew(i,2)=mixer%xout(pot%ndim+i)
      enddo 
      
      end subroutine spinunmix
c     ===============================================================
