c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine checks whether there are insulated atoms -
c     i.e., atoms whose position prevents any bonding with any other
c     atoms and may therefore lead to non-physical results.

c     It must be placed INSIDE the atom movement loop, so that 
c     the atom positions  will be rechecked after each molecular 
c     dynamics or energy minimization step.

c     WARNING!
c     At present the only allowed grid setup involves one sphere 
c     encompassing the entire cluster. Therefore the only check
c     needed is that no atom is outside this boundary sphere. Should
c     other grid constructions (e.g., an individual sphere around
c     each atom) be implemented, the test(s) should be updated
c     accordingly.
c
c     ---------------------------------------------------------------
      subroutine isolat(clust,rmax,ierr)

      use constants
      use cluster_module 
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(in) :: clust
c     boundary sphere size 
      real(dp), intent(in) :: rmax
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     index which is false if all is well, true if any isolated atom
c     found
      logical isolated_found 
c     distance of coordinate from origin
      real(dp) :: dist 
c     counter
      integer i
c     temp r-vactor 
      real(dp) :: r_vec (3)
c     ---------------------------------------------------------------
c
c     For each atom, find its distance from the origin, if this
c     distance is larger than rmax, flag a problem for the specific
c     atom and for the whole cluster.
c
      isolated_found = .false.
      do i=1, clust%atom_num
         r_vec = (/clust%xatm(i),clust%yatm(i),clust%zatm(i)/)
         dist = sqrt (dot_product (r_vec,r_vec))
         if (dist .gt. rmax) then 
            if (.not. isolated_found) then
               isolated_found = .true.
               write(7,*)
               write(7,*) 'ERROR: isolated atoms found:'
            endif
            write(7,10) i
         endif
      enddo

      if (isolated_found) then
c     if problem found - report it and terminate
         write(7,*) 'STOP in isolat'
         ierr = 1
         return
      endif

 10   format(1x,'Atom #',i4,2x,'is outside the boundary sphere!')

      end subroutine isolat
c     ===============================================================
