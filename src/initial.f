c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Initializing various variables, arrays, and files.
c
c     ---------------------------------------------------------------
      subroutine initial(clust,elec_st,pbc,mol_dynamic,move,
     1     elval,npolflg,field,ifield,istart,OutEvFlag,ierr)

      use constants
      use cluster_module
      use electronic_struct_module
      use pbc_module
      use molecular_dynamic_module
      use movement_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
      type (pbc_data), intent(in) :: pbc
c     molecular dynamic related data
      type (molecular_dynamic), intent(inout) :: mol_dynamic
c     movement data
      type (movement), intent(inout) :: move
c     number of valence electrons in system if neutral
      real(dp), intent(in) :: elval
c     polarizability flag
      integer, intent(in) :: npolflg
c     polarizability field (in a.u.)
      real(dp), intent(in) :: field
c     polarizability field index (direction of field)
      integer, intent(out) :: ifield
c     restart parameter 
      integer, intent(in) :: istart
c     output flag
      integer, intent(in) :: OutEvFlag
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     size of temperature step in K, calculated according to the
c     cooling scheme
      real(dp) :: tscale
c     atomic coordinates (changed only if cluster is not centered
c     about the origin)
      real(dp), dimension(clust%atom_num) :: xatm, yatm, zatm
c     number of atoms for each atom type
      integer natmi(clust%type_num)
c     LMD parameters:
c     initial and final temperatures (in kelvin), 
c     step temperature for stair cooling
      real(dp) :: tempi,tempf,tstep
c     total actual number of atoms (from all types combined)
      integer natom
c     counters
      integer ity, i, j, l, m, ii
c     number of electrons calculated from pre-fixed occupation file,
c     if used
      real(dp) :: elsum
c     temporary storage for minimum, maximum of coordinates
      real(dp) :: xmin,ymin,zmin,xmax,ymax,zmax
c     amount by which to shift the cluster in order to center it
      real(dp) :: xmov,ymov,zmov
c     temporary storage for coordinates
      real(dp) :: x1, y1, z1
c     inverse of the sides of the supercell
      real(dp) :: alinv(3)
c     actual number of spins used in computation
      integer nspin
c     user-provided occupation of each eigenvalue
      real(dp) ::  occup(elec_st%nstate,elec_st%nspin)
c     size of side of box (if periodic system)
      real(dp) :: alatt(3)
c     atom names
      character(len=2) name(1:clust%type_num)
c     numerically negligible difference
      real(dp), parameter :: small = 1.0d-5

c     ---------------------------------------------------------------

      alatt = pbc%box_size

      natom = clust%atom_num
      xatm = clust%xatm
      yatm = clust%yatm
      zatm = clust%zatm
      natmi = clust%natmi
      name = clust%name

      nspin = elec_st%nspin
      tempi = mol_dynamic%tempi
      tempf = mol_dynamic%tempf
      tstep = mol_dynamic%step_tmp
c
c     If restart of molecular dynamics then we need to read this file
c     first. We get the coordinates for our run from this file.
c     read in restart data. We also read the last time step of the 
c     previous run (iframe).
c
      if ((mol_dynamic%is_on) .and. (mol_dynamic%is_restart)) then
         open(76,file='mdinit.dat',status='old',form='formatted')
         read(76,20) mol_dynamic%iframe
         do j = 1,natom
            read(76,22) mol_dynamic%xcur(j),mol_dynamic%ycur(j),
     1           mol_dynamic%zcur(j),mol_dynamic%xold(j),
     2           mol_dynamic%yold(j),mol_dynamic%zold(j),
     3           mol_dynamic%vxold(j),mol_dynamic%vyold(j),
     4           mol_dynamic%vzold(j),mol_dynamic%accxold(j),
     5           mol_dynamic%accyold(j),mol_dynamic%acczold(j)
         enddo
         close(76)
c     Assign xatm, yatm and zatm to be xcur, ycur and zcur 
c     They are moved to their rightful places right after this.
         do j = 1, natom
            xatm(j) = mol_dynamic%xcur(j)  
            yatm(j) = mol_dynamic%ycur(j)  
            zatm(j) = mol_dynamic%zcur(j)  
         enddo
      endif
 20   format('Step #',i4)

      if ((.not. pbc%is_on) .and. (istart .eq. 0)) then
c     If confined system:
c     if the coordinates are not symmetrized about the origin, shift
c     them so that they are, in order to allow the results to
c     converge already for a smaller Rmax.
c
c     find minimum and maximum coordinate along each axis
         xmin=xatm(1)
         xmax=xatm(1)
         ymin=yatm(1)
         ymax=yatm(1)
         zmin=zatm(1)
         zmax=zatm(1)
         
         do j = 2, natom
            if (xmin .gt. xatm(j)) xmin = xatm(j)
            if (xmax .lt. xatm(j)) xmax = xatm(j)
            if (ymin .gt. yatm(j)) ymin = yatm(j)
            if (ymax .lt. yatm(j)) ymax = yatm(j)
            if (zmin .gt. zatm(j)) zmin = zatm(j)
            if (zmax .lt. zatm(j)) zmax = zatm(j)
         enddo
 
c     Move the coordinates so that along each axis the farthest atom
c     in the positive direction is as far as the farthest atom in
c     the negative direction.
         xmov = half*(xmin+xmax)
         ymov = half*(ymin+ymax)
         zmov = half*(zmin+zmax)

         if (sqrt(xmov*xmov+ymov*ymov+zmov*zmov) .gt. 0.1) then
            write(7,*)
            write(7,*) 'WARNING: atom coordinates were shifted so'
            write(7,*) 'that the cluster is centered at the origin'
            write(7,*) 'The new atom coordinates are:'

            do j=1,natom
               xatm(j) = xatm(j)-xmov
               yatm(j) = yatm(j)-ymov
               zatm(j) = zatm(j)-zmov
               write(7,18) xatm(j),yatm(j),zatm(j)
            enddo
 18         format(3(f15.9,3x))
         endif
      elseif (pbc%is_on) then
c     If periodic system
c     If atoms are "outside the box", move them back in by
c     replication.
         alinv = one/alatt

         do j=1,natom
            x1 = xatm(j)-alatt(1)*nint(xatm(j)*alinv(1))
            y1 = yatm(j)-alatt(2)*nint(yatm(j)*alinv(2))
            z1 = zatm(j)-alatt(3)*nint(zatm(j)*alinv(3))
            if ((abs(x1-xatm(j)) .ge. alatt(1)) .or.
     1          (abs(y1-yatm(j)) .ge. alatt(2)) .or.
     2          (abs(z1-zatm(j)) .ge. alatt(3))) then
               xatm(j) = x1
               yatm(j) = y1
               zatm(j) = z1
            endif
         enddo
      endif

c     compute actual number of electrons from # of valence electrons
c     and cluster charge.
      elec_st%xele = elval - elec_st%ncharge
c
c     If polarizability flag is on -
c     set ifield, field direction index, to 1 (initial direction)
c     open polar.dat file (this is an ascii file!), into which 
c     polarizability results are written. Write in some headers for
c     the information will be fed later on in the calculation.
c
      if (npolflg .eq. 1) then
         ifield = 1
         open(91,file='polar.dat', status='unknown',form='formatted')
         write(91,*) 'Total number of atoms is: ', natom
         write(91,*) 'Number of different types of atoms is: ',
     1        clust%type_num 
         l = 0

         do i = 1, clust%type_num
            write(91,*)
            write(91,15) natmi(i),name(i)
            do j = 1, natmi(i)
               l = l + 1
               write(91,92) xatm(l), yatm(l), zatm(l)
            enddo
         enddo
 92      format(3x, f10.6, 3x, f10.6, 3x, f10.6)
 15      format(i3,1x,a2,1x,'atoms with coordinates:')
         write(91,*)
         write(91,85) field
 85      format(1x,'Applied Electric field  = ',f8.5,1x,'[Ry/au]')
         write(91,*)
         write(91,86) 
         write(91,87)
 86      format(3x,'Field',11x,'Total',14x,'Dipole  Moment [Deb.]')
 87      format(1x,'Component',6x,'Energy [Ry]',10x,'X',10x,'Y',10x,'Z')
      end if
c
c     If atoms move, open atom coordinate file and write out initial
c     coord. Initialize 'is minimum force satisfied' flag to zero.
c
      if (move%is_on) then
         open(66,file='atom.cor',status='unknown',form='formatted')
         write(66,12) move%num
         j=0

         do ity = 1, clust%type_num
            write(66,*) name(ity),'    ',natmi(ity)
            do i=1,natmi(ity)
               j = j + 1
               write(66,14) xatm(j), yatm(j), zatm(j)
            enddo
         enddo
         call myflush(66)
c
c     If minimization is being done restarting from previous run,
c     we should keep the user-provided geometry otherwise that will
c     not be consistent with information from previous runs.
c
         if (move%is_restart) then
            open(45,file='relax_restart.dat',
     1           form='formatted',status='old',iostat=ii)
            if (ii .ne. 0) then
               write(7,*) 'WARNING! relax_restart.dat not found'
               write(7,*) 'start structural relaxation from scratch'
               move%is_restart = .false.
            else
               write(7,*) 'WARNING: atom coordinates in input were'
               write(7,*) 'replaced by coordinates in relax_restart.dat'
               write(7,*) 'The new atom coordinates are:'
               read(45,*)
               do j=1,natom
                  read(45,*) xatm(j),yatm(j),zatm(j)
                  write(7,18) xatm(j),yatm(j),zatm(j)
               enddo
               write(7,*)
               close(45)
            endif
         endif
c     if manual mode open the file from which coordinates are read
         if (move%name .eq. MANUAL) then
            open(67,file='manual.dat',status='unknown',form
     1           ='formatted')
         endif
c     if BFGS minimization, open file for output of BFGS routines
         if (move%name .eq. BFGS) then
            open(46,file='bfgs.dat',status='unknown',form='formatted')
         endif
      endif
 12   format('Step #',1x,i3)
 14   format(3(3x,f10.6))
c
c     If molecular dynamics - open files.
c     Also write initial statements into these files,initialize
c     counters, and initialize temperature step according to cooling
c     scheme.
c
      if (mol_dynamic%is_on) then
c
c     initialize frame counter and open atom coordinate file
c     if we are restarting the molecular dynamics iframe is read
c     in from the restart file (mdinit.dat)
c
         if (.not. mol_dynamic%is_restart) mol_dynamic%iframe = 0 
         open(66,file='atom.cor',status='unknown',form='formatted')
c     open file containing energy data from MD run
         open(78,file='md_nrg.dat',status='unknown',form='formatted')
         write(78,88)
c     open file containing mechanical data from MD run
         open(79,file='md_mech.dat',status='unknown',form='formatted')
         write(79,81)
         write(79,82)
         write(79,83)
 81      format('Each line contains: six coordinates, three',
     1        ' velocities, and three accelerations',/,'for each',
     2        ' molecular dynamics step')
 82      format('The data are: next position, current position',
     1        'current velocity',/,'current acceleration')
 83      format('The last step of this file should be copied to',
     1        ' mdinit.dat for a restart')
c     Open file containing coordinates for each step. "movie.xyz"
c     has coordinates for each step, in angstrom and in .xyz format
c     (compatible with xmol and other molecular visualization
c     codes).
         open(92,file='movie.xyz',status='unknown',form='formatted')
         write(92,90) natom
         write(92,*) 'Step # ', mol_dynamic%iframe
         ii = 0

         do i = 1, clust%type_num
            do j = 1, natmi(i)
               ii = ii + 1
               write(92,93) name(i), xatm(ii)*angs,
     1              yatm(ii)*angs, zatm(ii)*angs
            enddo
         enddo

         select case (mol_dynamic%cool_type)
c     "Stairstep" cooling
         case (1)
            tscale = -tstep
c     error checks: moving in right direction?
            if (((tempi .gt. tempf) .and. (tscale .ge. 0.d0)) .or. 
     1           ((tempi .lt. tempf) .and. (tscale .le. 0.d0))) then
               write(7,*) 'ERROR: temperature step of wrong sign!'
               write(7,*) 'STOP in initial'
               ierr = 1
               return
            endif
c     unless it's an isothermal run (in which case the temperature
c     step is irrelevant), check if the temperature step is not
c     too large. "Too large" means that it is more than twice the
c     temperature gap (because then we won't be able to clamp it to
c     the final temperature - see the temperature clamping in the 
c     molecular dynamics subroutine).
            if ((abs(tempi-tempf) .gt. 1.d-5) .and.
     1           (abs(tscale/2.d0) .ge. abs(tempi-tempf))) then
               write(7,*) 'ERROR: temperature step too large!'
               write(7,*) 'it goes far beyond the final temperature!'
               write(7,*) 'STOP in initial'
               ierr = 1
               return
            endif
c     linear cooling - substract a fixed temperature step
         case (2)
            tscale = (tempf-tempi)/real(mol_dynamic%step_num,dp)
c     logarithm cooling - multiply the temperature by a fixed 
c     ratio at each step
         case(3)
            tscale = (tempf/tempi)**(one/real(mol_dynamic%step_num,dp))
         end select
         mol_dynamic%tscale = tscale

c     if retsart of molecular dynamics
         if (mol_dynamic%is_restart) then

c     restart data has already been read. Just do some reporting.
c     report initial coordinates to atom.cor
            write(66,*) 'Coordinates for step #', mol_dynamic%iframe
            ii = 0
            do i = 1, clust%type_num
               write(66,*) name(i)
               do j = 1, natmi(i)
                  ii = ii + 1
                  write(66,89) xatm(ii),yatm(ii),zatm(ii)
               enddo
            enddo

c     report initial position,velicity, acceleration to md_mech.dat
            write(79,*) 'Step #', mol_dynamic%iframe
            do j=1,natom
c     change:
               write(79,22) mol_dynamic%xcur(j),mol_dynamic%ycur(j),
     1              mol_dynamic%zcur(j),mol_dynamic%xold(j),
     2              mol_dynamic%yold(j),mol_dynamic%zold(j),
     3              mol_dynamic%vxold(j),mol_dynamic%vyold(j),
     3              mol_dynamic%vzold(j),mol_dynamic%accxold(j),
     4              mol_dynamic%accyold(j),mol_dynamic%acczold(j)
            enddo
         else
c     if we are not restarting then we need to make xcur etc equal
c     the xatm etc.
            do j = 1, natom
               mol_dynamic%xcur(j) = xatm(j)
               mol_dynamic%ycur(j) = yatm(j)
               mol_dynamic%zcur(j) = zatm(j)
            enddo
         endif
      end if

 22   format(12(1x,e15.8))
 88   format('Step',2x,'Time[ps]',2x,'Ekin[eV/at]',2x,'Epot[eV/at]',
     1     2x,'Etot[eV/at]',5x,'T[K]',2x,'Tcalc[K]')
 89   format(3(2x,f11.6))
 90   format(i3)
 93   format(a2, 3(3x, f10.6))
c
c     In the case of artificial occupations, denoted by a negative
c     Fermi temperature, read occupations from occup.in.
c
      if (elec_st%tfermi .lt. zero) then
         open(90,file='occup.in',status='old',form='formatted')
         do ii=1, nspin
c     skip title line
            read(90,*) 
c     read state up to which the states are full
            read(90,*) elec_st%ifmax(ii)
c     occupy states
            do i=1,elec_st%ifmax(ii)
               occup(i,ii)=one
            enddo 
            do i=elec_st%ifmax(ii)+1,elec_st%nstate
               occup(i,ii)=zero
            enddo
c     read in manual over-ride for occupation of specific states
            read(90,*) m
            do i=1,m
               read(90,*) j, occup(j,ii)
            enddo
         enddo
         close(90)

c     check if occupation consistent with number of electrons
         elsum = zero
         do ii=1, nspin
            do i=1,elec_st%nstate
               elsum = elsum +occup(i,ii)
            enddo
         enddo
c
c     3-nspin is 2 if nspin=1 and 1 if nspin=2. This is because if
c     there's only one spin channel, our occupation count is defined
c     as 1.0 for TWO electrons filling the state, whereas if there
c     are two spin channels our occupation count is defined as 1.0
c     for ONE electron filling the state.
c
         if (abs(real(3-nspin,dp) * elsum-elec_st%xele) .gt.
     1        small) then
            write(7,*)
            write(7,*) 'ERROR: fixed occupation inconsistent'
            write(7,*) '       with stated number of electrons!' 
            write(7,*) 'STOP in initial'
            ierr = 1
            return
         endif

c     Find highest eigenvalue with non-zero occupation
         do ii=1, nspin
            do i=elec_st%nstate,1,-1
               if (occup(i,ii) .ne. zero) then
                  elec_st%ifmax(ii) = i
                  goto 25
               endif
            enddo
 25         continue
         enddo

c     end fixed occupation read-in
      endif

      clust%xatm = xatm
      clust%yatm = yatm
      clust%zatm = zatm

      elec_st%occ_in = occup
c     output flag
      if (OutEvFlag .gt. 0) then
         open(61,file='eigen.dat',status='unknown',form='formatted')
         write(61,*) ' State     Eigenvalue [Ry]    Eigenvalue [eV]',
     1        '    Occupation   Repr.   Movement   spin'

      endif
c     end  output flag read-in

      end subroutine initial
c     ===============================================================
c
c     Subroutine to set the various clm coefficients. Separated so as
c     to not clutter up the above code. calculates clm = (l-m)!/(l+m)! 
c     for l = 0,lpole, m=0,l. 
c     Evaluating these to 20 decimal places is certainly as accurate 
c     as doing the calculations in the code but less work.
c
c     ---------------------------------------------------------------
      subroutine setclm(lpole, clm)
      use constants
      implicit none
c
c     Input/Output variables:
c
c     order of mutipole expansion
      integer, intent(in) :: lpole
c     clm coefficients
      real(dp), intent(out) :: clm(0:lpole, 0:lpole)
c     ---------------------------------------------------------------
      clm(0,0) = 1.00000000000000000000e+00
      if (lpole .ge. 1) then
         clm(1,0) = 1.00000000000000000000e+00
         clm(1,1) = 5.00000000000000000000e-01
      endif
      if (lpole .ge. 2) then
         clm(2,0) = 1.00000000000000000000e+00
         clm(2,1) = 1.66666666666666666670e-01
         clm(2,2) = 4.16666666666666666670e-02
      endif
      if (lpole .ge. 3) then
         clm(3,0) = 1.00000000000000000000e+00
         clm(3,1) = 8.33333333333333333330e-02
         clm(3,2) = 8.33333333333333333330e-03
         clm(3,3) = 1.38888888888888888890e-03
      endif
      if (lpole .ge. 4) then
         clm(4,0) = 1.00000000000000000000e+00
         clm(4,1) = 5.00000000000000000000e-02
         clm(4,2) = 2.77777777777777777780e-03
         clm(4,3) = 1.98412698412698412700e-04
         clm(4,4) = 2.48015873015873015870e-05
      endif
      if (lpole .ge. 5) then
         clm(5,0) = 1.00000000000000000000e+00
         clm(5,1) = 3.33333333333333333330e-02
         clm(5,2) = 1.19047619047619047620e-03
         clm(5,3) = 4.96031746031746031750e-05
         clm(5,4) = 2.75573192239858906530e-06
         clm(5,5) = 2.75573192239858906530e-07
      endif
      if (lpole .ge. 6) then
         clm(6,0) = 1.00000000000000000000e+00
         clm(6,1) = 2.38095238095238095240e-02
         clm(6,2) = 5.95238095238095238100e-04
         clm(6,3) = 1.65343915343915343920e-05
         clm(6,4) = 5.51146384479717813050e-07
         clm(6,5) = 2.50521083854417187750e-08
         clm(6,6) = 2.08767569878680989790e-09
      endif
      if (lpole .ge. 7) then
         clm(7,0) = 1.00000000000000000000e+00
         clm(7,1) = 1.78571428571428571430e-02
         clm(7,2) = 3.30687830687830687830e-04
         clm(7,3) = 6.61375661375661375660e-06
         clm(7,4) = 1.50312650312650312650e-07
         clm(7,5) = 4.17535139757361979580e-09
         clm(7,6) = 1.60590438368216145990e-10
         clm(7,7) = 1.14707455977297247140e-11
      endif
      if (lpole .ge. 8) then
         clm(8,0) = 1.00000000000000000000e+00
         clm(8,1) = 1.38888888888888888890e-02
         clm(8,2) = 1.98412698412698412700e-04
         clm(8,3) = 3.00625300625300625300e-06
         clm(8,4) = 5.01042167708834375500e-08
         clm(8,5) = 9.63542630209296875960e-10
         clm(8,6) = 2.29414911954594494280e-11
         clm(8,7) = 7.64716373181981647590e-13
         clm(8,8) = 4.77947733238738529740e-14
      endif
      if (lpole .ge. 9) then
         clm(9,0) = 1.00000000000000000000e+00
         clm(9,1) = 1.11111111111111111110e-02
         clm(9,2) = 1.26262626262626262630e-04
         clm(9,3) = 1.50312650312650312650e-06
         clm(9,4) = 1.92708526041859375190e-08
         clm(9,5) = 2.75297894345513393130e-10
         clm(9,6) = 4.58829823909188988550e-12
         clm(9,7) = 9.55895466477477059490e-14
         clm(9,8) = 2.81145725434552076320e-15
         clm(9,9) = 1.56192069685862264620e-16
      endif

      end subroutine setclm
c     ===============================================================
