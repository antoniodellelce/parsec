c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Symmetrize the set of atomic forces according to symmetry
c     operations.
c
c     Author: Murilo Tiago (2005)
c
c     ---------------------------------------------------------------
      subroutine symm_forces(clust,symm,ierr)

      use constants
      use cluster_module
      use symmetry_module

      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     symmetry operations
      type (symmetry), intent(in) :: symm
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     symmetrized forces
      real(dp) :: fsym(3,clust%atom_num)
c     input forces on ions
      real(dp), allocatable :: cforce(:,:,:)
c     temporary variables
      integer :: ii,jj,kk,iat,ity,itrans,mxdatm,ishift
      real(dp) :: cro(3),tmpvec(3),dist
c     atomic coordinates
      real(dp), allocatable :: ratom(:,:,:)
c     tolerance in the position of one atom and its equivalent by
c     symmetry
      real(dp), parameter :: tol = 1.d-6

c     ---------------------------------------------------------------

      fsym = zero
      mxdatm = maxval(clust%natmi)
      allocate(ratom(clust%type_num,mxdatm,3))
      allocate(cforce(clust%type_num,mxdatm,3))
      ratom = zero
      iat = 0
      do ity = 1, clust%type_num
         do jj = 1, clust%natmi(ity)
            iat = iat + 1
            ratom(ity,jj,1) = clust%xatm(iat)
            ratom(ity,jj,2) = clust%yatm(iat)
            ratom(ity,jj,3) = clust%zatm(iat)
            cforce(ity,jj,:) = clust%force(:,iat)
         enddo
      enddo

      iat = 0
      do ity = 1, clust%type_num
         do jj = 1, clust%natmi(ity)
            iat = iat + 1
            do itrans = 1, symm%ntrans
c
c                    -1
c     find cro = mtrx    * (ratom - tnp)
c
               tmpvec = ratom(ity,jj,:)
               call symop(symm,itrans,tmpvec,cro)
               do ii = 1, clust%natmi(ity)
c     make sure the rotated atom is inside the periodic cell
                  tmpvec = cro - ratom(ity,ii,:)
                  tmpvec = matmul(tmpvec,symm%invlat)
                  do kk = 1, 3
                     ishift = nint(tmpvec(kk))
                     tmpvec(kk) = tmpvec(kk) - one*ishift
                  enddo
                  dist = sqrt( dot_product(tmpvec,tmpvec) )
                  if (dist .gt. tol) cycle
                  goto 10
               enddo
               write(7,*) ' ERROR: unable to find equivalent of atom'
               write(7,*) ' at position ',ratom(ity,jj,:)
               ierr = 1
               return
 10            continue
c
c     rotate force and add
c
               fsym(:,iat) = fsym(:,iat) + matmul(
     1              cforce(ity,ii,:),symm%trans(:,:,itrans))
            enddo
         enddo
      enddo
c
c     update forces
c
      clust%force = fsym/(one*symm%ntrans)

      deallocate(ratom, cforce)

      end subroutine symm_forces
c     ===============================================================
