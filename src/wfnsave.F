c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine saves grid, potential, wave function, energy
c     levels, etc, in wfn.dat file for restarting purposes.
c
c     If olddat = .true., write output in old style (no
c     information about symmetry operations) to file old.dat.
c
c     ---------------------------------------------------------------
      subroutine wfnsave(elec_st,grid,pbc,pot,rsymm,parallel
     1     ,vnew,rho,olddat,outflag)

      use constants
      use electronic_struct_module
      use grid_module
      use pbc_module
      use potential_module
      use symmetry_module
      use parallel_data_module

      implicit none 
#ifdef MPI
c     include mpi definitions
      include 'mpif.h'
#endif
c
c     Input/Output variables:
c
c     electronic structure
      type (electronic_struct), intent(in) :: elec_st
c     grid related data
      type (grid_data), intent(in) :: grid
c     periodic boundary conditions data
      type (pbc_data), intent(in) :: pbc
c     potential related data
      type (potential), intent(in) :: pot
c     symmetry operations:
      type (symmetry), intent(in) :: rsymm
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel
c     distributed self-consistent potential and electron density
c     (passed outside the structure to overcome a bug with the IBM compiler)
      real(dp), intent(in) :: vnew(parallel%mydim,elec_st%nspin)
      real(dp), intent(in) :: rho(parallel%mydim,2*elec_st%nspin-1)
c     flag for old-style output, old.dat
      logical, intent(in) :: olddat
c     output of all calculated wave functions or only occupied ones
      integer, intent(in) :: outflag
c
c     Work variables:
c
c     communicator
      integer comm
c     exit code for mpi calls
      integer mpinfo
c     allocation check
      integer :: alcstat
c     counters
      integer isp, jj, kk
      real(dp) :: tmpd

      integer lstart, node, msgtype, ldim, nstate
c     counters for representations
      integer irp
c     temporary arrays for eigenvalues/occupations, before reshaping
c     according to symmetry operations
      real(dp), dimension(:), allocatable :: en_tmp, occ_tmp
c     jrep keeps track of how many eigenstates are already in each
c     representation
      integer :: jrep(elec_st%nrep)
c     work array
      real(dp) :: wftmp(parallel%mydim)
c     date/time tag
      character (len=26) :: datelabel

#ifdef MPI
      integer status(MPI_STATUS_SIZE)
#endif

c     ---------------------------------------------------------------

      comm = parallel%comm

      if (parallel%iammaster) then
         rewind(4)
         call date_time(datelabel,tmpd)

         if (olddat) then
            if (pbc%is_on) then
               write(4) (grid%step(kk),kk=1,3),(pbc%box_size(kk),kk=1
     1              ,3),grid%ndim,elec_st%nspin,datelabel
            else
               write(4) grid%step(1), grid%rmax
     1              ,grid%ndim,elec_st%nspin,datelabel
            endif
         else
            write(4) datelabel
            if (pbc%is_on) then
               write(4) (grid%step(kk),kk=1,3),(pbc%box_size(kk),kk=1
     1              ,3),grid%ndim,grid%nwedge,rsymm%ntrans
     2              ,elec_st%nspin
            else
               write(4) grid%step(1), grid%rmax
     1              ,grid%ndim,grid%nwedge,rsymm%ntrans,elec_st%nspin
            endif
            write(4) grid%shift
            write(4) rsymm%rmtrx
            write(4) rsymm%trans
            write(4) rsymm%tnp
            write(4) rsymm%alatt, rsymm%invlat
            write(4) ((rsymm%chi(kk,jj),kk=1,rsymm%ntrans),jj=1
     1           ,rsymm%ntrans)
         endif
         write(4) (grid%kx(kk),grid%ky(kk),grid%kz(kk),
     1        kk=1,grid%nwedge)
      endif
      do isp=1,elec_st%nspin
         jj = isp-1+elec_st%nspin
         if (mod(outflag,2).eq.1) then
            nstate = elec_st%nstate
         else
            nstate = elec_st%ifmax(isp)
         endif
         if (parallel%iammaster) then
            allocate(en_tmp(nstate))
            allocate(occ_tmp(nstate))
            jrep = 0
            do kk = 1, nstate
               irp = elec_st%irep(kk,isp)
               jrep(irp) = jrep(irp) + 1
               en_tmp(kk) = elec_st%eig(irp,isp)%en(jrep(irp))
               occ_tmp(kk) = elec_st%eig(irp,isp)%occ(jrep(irp))
            enddo
            write(4) nstate
            if (.not. olddat) 
     1           write(4) (elec_st%irep(kk,isp),kk=1,nstate)
            write(4) (en_tmp(kk),kk=1,nstate)
            write(4) (occ_tmp(kk),kk=1,nstate)
            deallocate(en_tmp, occ_tmp)
         endif
         call collect_function(parallel,vnew(1,isp))
         if (parallel%iammaster) write(4)
     1        (parallel%ftmp(kk),kk=1,grid%nwedge)
         call collect_function(parallel,rho(1,jj))
         if (parallel%iammaster) write(4)
     1        (parallel%ftmp(kk),kk=1,grid%nwedge)

         msgtype = 1
         jrep = 0
         do kk=1,nstate
            irp = elec_st%irep(kk,isp)
            jrep(irp) = jrep(irp) + 1
            call dcopy(parallel%mydim,
     1            elec_st%eig(irp,isp)%wf(1,jrep(irp)),1,wftmp,1)
            call collect_function(parallel,wftmp)
            if (parallel%iammaster) write(4)
     1           (parallel%ftmp(jj),jj=1,grid%nwedge)
         enddo
      enddo
#ifdef MPI
      call MPI_Barrier(comm,mpinfo)
#endif

      if (parallel%iammaster) call myflush(4)

      end subroutine wfnsave
c     ===============================================================
c
c     Gets the date (day-month-year) and time, with time zone. It
c     also returns the wall-clock time in absolute seconds.
c
c     ---------------------------------------------------------------
      subroutine date_time(datelabel,wallclock)

      use constants
      implicit none
c
c     Input/Output variables:
c
      character (len=26), intent(out) :: datelabel
      real(dp), intent(out) :: wallclock
c
c     Work variables:
c
      integer values(8)
      integer lmonth
      integer idate (8)
      character (len=11) :: bdate
      character (len=14) :: btime
      character (len=10) :: atime
      character (len=8) :: adate
      character (len=5) :: azone
      character (len=4) :: year
      character (len=2) :: day
      character (len=2) :: hour,min,sec
      character (len=3) :: month(12)
      character (len=1) ::  dash
      data dash/'-'/
      data month/'JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG'
     1     ,'SEP','OCT','NOV','DEC'/
c     ---------------------------------------------------------------
      call date_and_time(adate,atime,azone,idate)
      read(adate,12) year,lmonth,day
      write(bdate,14) day,dash,month(lmonth),dash,year
      read(atime,16) hour,min,sec
      write(btime,18) hour,min,sec,azone
      write(datelabel,'(a,1x,a)') bdate,btime

      call date_and_time(values=values)
      wallclock = ((values(3)*24.0d0+values(5))*60.0d0+values(6))
     1     *60.0d0+values(7)+values(8)*0.001d0
 12   format(a4,i2,a2)
 14   format(a2,a1,a3,a1,a4)
 16   format(a2,a2,a2,a4)
 18   format(a2,':',a2,':',a2,' ',a5)

      end subroutine date_time
c     ===============================================================
