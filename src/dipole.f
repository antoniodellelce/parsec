c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine calculates the total charge and the total 
c     dipole of the system (i.e., the zeroth and first multipole
c     terms), based on the charge density.
c
c     ---------------------------------------------------------------
      subroutine dipole(clust,rsymm,elec_st,grid,parallel,zion)

      use constants
      use cluster_module
      use symmetry_module
      use electronic_struct_module
      use grid_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(in) :: clust
c     symmetry operations in reduced group:
      type (symmetry), intent(in) :: rsymm
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
c     grid related data
      type (grid_data), intent(in) :: grid
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

c     positive charge of pseudo ion for each atom type
      real(dp), intent(in) :: zion(1:clust%type_num)
c     
c     Work variables:
c
c     charge of cluster
      real(dp) :: charge
c     dipole along the three axes
      real(dp) :: dipr(3)
c     counters
      integer i,j,ity,igrid,itrans,ioffset
      real(dp) :: rhoi, rw(3), rrw(3)

c     ---------------------------------------------------------------
c
c     sum over all grid points to get electron contribution to the
c     charge and dipole. Note that the charge density, rho, is
c     calculated elsewhere in the code as a positive quantity, even
c     though it is of course negative - hence the minus signs in rhoi.
c
      charge = zero
      dipr = zero

      ioffset = parallel%Irows(parallel%iam) - 1
      do j = parallel%Irows(parallel%iam)
     1     ,parallel%Irows(parallel%iam+1)-1
         rhoi = -elec_st%rho(j-ioffset,1)
         charge = charge + rhoi
         rw(1) = (grid%shift(1) + parallel%kx(j)) * grid%step(1)
         rw(2) = (grid%shift(2) + parallel%ky(j)) * grid%step(2)
         rw(3) = (grid%shift(3) + parallel%kz(j)) * grid%step(3)
         do itrans = 1, rsymm%ntrans
            call symop(rsymm,itrans,rw,rrw)
            dipr = dipr + rrw * rhoi
         enddo
      enddo
      call psum(charge,1,parallel%procs_num,parallel%comm)
      call psum(dipr,3,parallel%procs_num,parallel%comm)

c     multiply by h^3 to approximate volume integration by the
c     summation; charge has the extra ntrans factor because it was
c     summed over the wedge above.

      charge = charge*grid%hcub*real(rsymm%ntrans,dp)
      dipr = dipr*grid%hcub

      if (parallel%iammaster) then
c     sum over all atoms to get the nuclei contribution to the 
c     charge and dipole
         j = 0
         do ity = 1, clust%type_num
            do i  = 1, clust%natmi(ity)
               j = j + 1
               charge = charge + zion(ity)
               dipr(1) = dipr(1) + zion(ity)*clust%xatm(j)
               dipr(2) = dipr(2) + zion(ity)*clust%yatm(j)
               dipr(3) = dipr(3) + zion(ity)*clust%zatm(j)
            enddo
         enddo

c     convert dipole units from atomic ones to Debye
         dipr = dipr*debye 

         elec_st%dipx = dipr(1)
         elec_st%dipy = dipr(2)
         elec_st%dipz = dipr(3)
         elec_st%charge = charge

c     report results
         write(7,50) elec_st%ncharge
         write(7,51) charge 
         write(7,*) 'Dipole components [Debye]:'
         write(7,52) elec_st%dipx
         write(7,53) elec_st%dipy 
         write(7,54) elec_st%dipz 
         call myflush(7)

 50      format('Total charge (user input) [e] = ',f12.8)
 51      format('Total charge (computed) [e] = ',f12.8)
 52      format('x-component = ',f12.8)
 53      format('y-component = ',f12.8)
 54      format('z-component = ',f12.8)
      endif

      end subroutine dipole
c     ===============================================================
