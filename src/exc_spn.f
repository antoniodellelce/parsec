c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine computes the exchange correlation potential,
c     Vxc, and the exchange correlation energy, Exc, both in Rydbergs,
c     based on an input charge density given in e/(bohr^3).
c
c     NOTE: Extra factor of two in formulas is due to conversion from
c     Hartree to Rydberg!
c
c     The following exchange-correlation functionals are implemented:
c
c     Local density functionals:
c     Ceperley-Alder - D. M. Ceperley and B. J. Alder, Phys. Rev.
c     Lett., 45, 566 (1980).
c
c     Gradient-corrected functionals:
c     PBE - Perdew, Burke, Ernzerhof - J. P. Perdew, K. Burke, 
c     and M. Ernzerhof, Phys. Rev. Lett., 77, 3865 (1996).
c
c     PW(91) - J. P. Perdew and Y. Wang, Phys. Rev. B 45, 13244 (1992). 
c
c     ---------------------------------------------------------------
      subroutine exc_spn(grid,parallel,nrep,icorr,vxc,wvec,ipr,exc)

      use constants
      use grid_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     grid related data
      type (grid_data), intent(in) :: grid
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

c     correlation type
      character (len=2), intent(in) :: icorr
c     print flag
      integer, intent(in) :: ipr
c     order of the reduced group
      integer, intent(in) :: nrep
c     Input: charge density. Output: exchange-correlation potential
      real(dp), intent(inout) :: vxc(parallel%mydim,2)
c     work array
      real(dp), intent(out) :: wvec(parallel%nwedge + 1)
c     total exchange-correlation energy
      real(dp), intent(out) :: exc
c
c     Work variables:
c
c     multiplicity of grid points in irreducible wedge (equal to
c     order of the reduced group)
      real(dp) :: weight
c     average exchange-correlation potential
      real(dp) :: vxcav

c     should be changed to parameters
      integer, parameter :: UP = 1,DOWN = 2

c     GGA variables:
c     quantities fed to pbe.f:
c     gradient - grad(n), where n is the charge density
c     absgrad  - |grad(n)|
c     grdgrd - components of grad(|grad(n)|
c     laplac - laplacian(n)
c     prod  -  grad(n) dot grad(|grad(n)|
c     abs_total_grad - |grad(total rho)| 

      real(dp), dimension (:,:), allocatable ::
     1     gradUP, gradDN, gradgradUP, gradgradDN, total_gradgrad
      real(dp),dimension (:,:), allocatable :: absgrad,laplac

      real(dp), allocatable :: abs_total_grad(:)
      real(dp),dimension (UP:DOWN):: prod
      real(dp) :: total_prod

      integer spin

      integer ::
     1     lcor,                ! flag to do correlation(=0=>don't)
     2     lpot                 ! flag to do potential(=0=>don't)


      real(dp) ::
     1     exlsd,               ! LSD exchange energy density, so that
                                !     ExLSD=int d^3r rho(r) exlsd(r)
     2     vxuplsd,             ! up LSD exchange potential
     3     vxdnlsd,             ! down LSD exchange potential
     4     eclsd,               ! LSD correlation energy density
     5     vcuplsd,             ! up LSD correlation potential
     6     vcdnlsd              ! down LSD correlation potential

      real(dp) ::
     1     expw91,vxuppw91,     !
     2     vxdnpw91,ecpw91,     ! corresponding PW91 quantities
     3     vcuppw91,vcdnpw91    !

      real(dp) ::
     1     expbe,vxuppbe,       !
     2     vxdnpbe,vcuppbe,     ! corresponding PBE quantities
     3     vcdnpbe,ecpbe        !

c     LDA variables:
c     rs is the local value of the Wigner-Seitz radius
c     rho,vx,vc temporarily store the charge,exchange and correlation 
c     potentials at a given grid point
      real(dp) :: rs, rh(2),vx(2),vc(2)

c     Other parameters
      real(dp), parameter :: ac2 = four/three
      real(dp), parameter :: p75vpi = 0.75d0/pi
      real(dp), parameter :: ax = -0.7385588d0
      real(dp), parameter :: gamma = 0.5198421d0
      real(dp), parameter :: fzz = 1.709921d0
c     other temporary holders
      real(dp) :: ex(2),ec,eu,f,zet,z4,ecrs,eurs,eczet,comm,fz,d
      real(dp) :: ep,eprs,alfrsm,alfm

      real(dp) :: temp_val
c     counters
      integer i, ii, k, kk
c     allocation check
      integer alcstat

      real(dp), dimension (3) :: temp1,temp2,temp3,temp4,
     1     temp5,temp6

c     local number of grid points
      integer ndim

c     ---------------------------------------------------------------

      ndim = parallel%mydim
      weight = real(nrep,dp)
c
c     initialize the total exchange-correlation energy to zero
c
      exc = zero

      if (icorr .eq. 'pb' .or. icorr .eq. 'pw') then

c     using PBE - a gradient-corrected functional

c     quantities fed to pbe.f:
c     gradient - grad(n), where n is the charge density

c     absgrad  - |grad(n)|
c     gradgrad - components of grad(|grad(n)|
c     laplac - laplacian(n)
c     prod  -  grad(n) dot grad(|grad(n)|

c     abs_total_grad - |grad(total rho)|

c     initialize working arrays:

         allocate(abs_total_grad(ndim),stat=alcstat)
         call alccheck('abs_total_grad',ndim,alcstat)
         allocate(absgrad(ndim,UP:DOWN),stat=alcstat)
         call alccheck('absgrad',ndim*(DOWN-UP+1),alcstat)
         allocate(laplac(ndim,UP:DOWN),stat=alcstat)
         call alccheck('laplac',ndim*(DOWN-UP+1),alcstat)
         allocate(gradUP(3,ndim),stat=alcstat)
         call alccheck('gradUP',3*ndim,alcstat)
         allocate(gradDN(3,ndim),stat=alcstat)
         call alccheck('gradDN',3*ndim,alcstat)
         allocate(gradgradUP(3,ndim),stat=alcstat)
         call alccheck('gradgradUP',3*ndim,alcstat)
         allocate(gradgradDN(3,ndim),stat=alcstat)
         call alccheck('grdgrdDN',3*ndim,alcstat)
         allocate(total_gradgrad(3,ndim),stat=alcstat)
         call alccheck('total_gradgrad',3*ndim,alcstat)

c     prepare vectors for invoking pbe
         
c     calculate the gradient of the charge density (rho==exc) at each
c     grid point: gradUP, gradDN
         call gradmvs(parallel,vxc(1,UP),gradUP,grid%coe1
     1        ,grid%norder,wvec)
         call gradmvs(parallel,vxc(1,DOWN),gradDN,grid%coe1
     1        ,grid%norder,wvec)

c     calculate abs_total_grad
c     (|grad(total rho)| = |grad(up)+grad(down)|)
         do i = 1,ndim
            temp1 = gradUP(:,i) + gradDN(:,i)
            abs_total_grad(i) = sqrt(dot_product(temp1,temp1))
         enddo

c     calculate total_gradgrad(i) = grad|grad(rho(i))| 
         call gradmvs(parallel,abs_total_grad,total_gradgrad,grid%coe1
     1        ,grid%norder,wvec)

c     calculate absgrad(i,spin) -  the absolute value of the gradient
c     in UP/DN: |grad(rho(i,spin)|
         do i = 1,ndim
            temp1 = gradUP(:,i)
            absgrad(i,UP)= sqrt(dot_product(temp1,temp1))
            temp2 = gradDN(:,i)
            absgrad(i,DOWN)= sqrt(dot_product(temp2,temp2))
         enddo

c     calculate gradgrad_spin = grad|grad(rho(i,spin))| 
         call gradmvs(parallel,absgrad(1,UP),gradgradUP,grid%coe1
     1        ,grid%norder,wvec)
         call gradmvs(parallel,absgrad(1,DOWN),gradgradDN,grid%coe1
     1        ,grid%norder,wvec)

c     Calculate laplac(i,spin) -  the Laplac of rho(spin) at each
c     grid point
         do spin = UP,DOWN
            call lapmvs(parallel,vxc(1,spin),laplac(1,spin),grid%coe2
     1           ,grid%norder,wvec)
         enddo
         laplac = -laplac

c     do correlation
         lcor = 1
c     do potential
         lpot = 1

         do i = 1,ndim

c     prod(spin) = grad(rho(spin))_dot_grad|grad(rho(spin))| 
c     grad(rho(spin)) = gradUP , gradDN
c     grad|grad(rho(spin))| = gradgradUP , gradgradDN
c     UP
            temp1 = gradUP(:,i)
            temp2 = gradgradUP(:,i)
            prod(UP) = dot_product (temp1,temp2)
c     DOWN
            temp3 = gradDN(:,i)
            temp4 = gradgradDN(:,i)
            prod(DOWN) = dot_product (temp3,temp4)

c     Total Prod
c     total_prod = delgr=(grad rho).(grad |grad rho|) 
c     (grad rho) = grad(rho up) + grad (rho dn)
c     (grad |grad rho|) = total_gradgrad
            temp5 = gradUP(:,i) + gradDN(:,i)
            temp6 = total_gradgrad(:,i)
            total_prod = dot_product(temp5,temp6)

            call spn_pbe(vxc(i,UP),absgrad(i,UP),prod(UP),laplac(i,UP),
     1           vxc(i,DOWN),absgrad(i,DOWN),prod(DOWN),laplac(i,DOWN),
     2           abs_total_grad(i),total_prod,lcor,lpot,
     3           exlsd,vxuplsd,vxdnlsd,eclsd,vcuplsd,vcdnlsd,
     4           expw91,vxuppw91,vxdnpw91,ecpw91,vcuppw91,vcdnpw91,
     5           expbe,vxuppbe,vxdnpbe,ecpbe,vcuppbe,vcdnpbe)

            if (icorr .eq. 'pb') then
c     using PBE - a gradient-corrected functional
               exc=exc+( (vxc(i,UP)+vxc(i,DOWN))*two*(expbe+ecpbe) )
     1              *weight
               vxc(i,UP)=two*(vxuppbe+vcuppbe)
               vxc(i,DOWN)=two*(vxdnpbe+vcdnpbe)
c     using non-local functional of rho, PW91
            else
c     using GGA functional of rho, PW91
               exc=exc+( (vxc(i,UP)+vxc(i,DOWN))*two*(expw91+ecpw91) )
     1              *weight
               vxc(i,UP)=two*(vxuppw91+vcuppw91)
               vxc(i,DOWN)=two*(vxdnpw91+vcdnpw91)
            endif
         enddo

c     free up memory
         deallocate(abs_total_grad)
         deallocate(absgrad)
         deallocate(laplac)
         deallocate(gradUP)
         deallocate(gradDN)
         deallocate(gradgradUP)
         deallocate(gradgradDN)
         deallocate(total_gradgrad)
      else if (icorr .eq. 'ca') then
c     Ceperly-Alder exchange correlation
         temp_val = ax*two**third
         do i=1,ndim
            rh(1)=vxc(i,1)
            rh(2)=vxc(i,2)
            if (minval(rh) .gt. zero) then
c     exchange
               d=two*rh(1)
               ex(1)= ax*d**third
               vx(1)=ex(1)*ac2
               d=two*rh(2)
               ex(2)= ax*d**third
               vx(2)=ex(2)*ac2
c     local correlation
               d=sum(rh)
               rs=(p75vpi/d)**third
               call spn_gcor(0.0310907d0,0.21370d0,7.5957d0,3.5876d0,
     1              1.6382d0,0.49294d0,1.00d0,rs,eu,eurs)
               call spn_gcor(0.01554535d0,0.20548d0,14.1189d0,6.1977d0,
     1              3.3662d0,0.62517d0,1.00D0,rs,ep,eprs)
               call spn_gcor(0.0168869d0,0.11125d0,10.357d0,3.6231d0,
     1              0.88026d0,0.49671d0,1.00d0,rs,alfm,alfrsm)
               zet=(rh(1)-rh(2))/d
               f = ((one+zet)**ac2+(one-zet)**ac2-two)/gamma
               z4 = zet**4
               ec = eu*(one-f*z4)+ep*f*z4-alfm*f*(one-z4)/fzz
               ecrs = eurs*(one-f*z4)+eprs*f*z4-alfrsm*f*(one-z4)/fzz
               fz = ac2*((one+zet)**third-(one-zet)**third)/gamma
               eczet = four*(zet**3)*f*(ep-eu+alfm/fzz)+fz*(z4*ep-z4*eu
     1              -(one-z4)*alfm/fzz)
               comm = ec -rs*ecrs/three-zet*eczet
               vc(1) = comm + eczet
               vc(2) = comm - eczet

               vxc(i,1)=two*(vx(1)+vc(1))
               vxc(i,2)=two*(vx(2)+vc(2))
               ec=two*(rh(1)*ex(1)+rh(2)*ex(2)+ec*d)
               exc = exc + ec*weight
            endif
         enddo
      endif
c
c     scale the total energy integral by h^3 (factor necessary due to
c     the expression of an integral as a summation over grid points) and
c     sum it up over processors
c
      call psum(exc,1,parallel%procs_num,parallel%comm)
      exc  = exc  * (grid%hcub)

      end subroutine exc_spn
c     ===============================================================
