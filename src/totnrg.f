c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine determines the total energy of the system,
c     based on Eq. (17) of Chelikowsky and Louie, Phys. Rev. B 29,
c     3470 (1984). 
c
c     ---------------------------------------------------------------
      subroutine totnrg(elec_st,pot,pbc,parallel,totexc,enuc,hcub
     1     ,natom,bdev)

      use constants
      use electronic_struct_module
      use potential_module
      use pbc_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
c     potential related data
      type (potential), intent(in) :: pot
c     periodic boundary condition data
      type (pbc_data), intent(in) :: pbc
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel
c     new total exchange-correlation energy
      real(dp), intent(in) :: totexc
c     total nuclear energy
      real(dp), intent(in) :: enuc
c     hcub = h**3
      real(dp), intent(in) :: hcub
c     total actual number of atoms (from all types combined)
      integer, intent(in) :: natom

c     total energy per atom in eV
      real(dp), intent(out) :: bdev
c
c     Work variables:
c
c     actual number of grid points
      integer ndim
c     energy terms due to eigenvalue sum, new Hartree potential, new
c     exchange- correlation potential, old Hartree-Exchange
c     -correlation potential, ionic potential
      real(dp) :: eeig,ehnew,excnew,ehxcold,eion
c     electronic part of total energy: this iteration, previous
c     iteration, and difference from previous iteration
      real(dp) :: eele,eold,dele
c     counters
      integer i,j,ii,jj
c     total energy in Rydberg
      real(dp) :: etot

c     save old electronic energy (computed below) for next iteration
      save eold

c     ---------------------------------------------------------------

      ndim = parallel%mydim

c     sum over eigenvalues, weighted by the occupation:
      eeig = zero
      do ii=1, elec_st%nspin
         do i = 1, elec_st%nrep
            if (elec_st%eig(i,ii)%nn .eq. 0) cycle
            eeig = eeig +  dot_product(
     1           elec_st%eig(i,ii)%occ,elec_st%eig(i,ii)%en)
         enddo
      enddo

c     If not spin-polarized, multiply by two to account for two
c     electrons in each eigenvalue
      if (elec_st%nspin .eq. 1) eeig = two*eeig
 
c     energy terms corresponding to new Hartree, new exchange
c     -correlation, and old Hartree+exchange-correlation. Each one is
c     an integral over the grid of the relevant potential weighted by
c     the charge density. 
      ehnew = dot_product(pot%vhart,elec_st%rho(1:ndim,1))
      call psum(ehnew,1,parallel%procs_num,parallel%comm)

      excnew = zero
      ehxcold = zero
      do ii=1, elec_st%nspin
         jj=ii-1+elec_st%nspin
         ehxcold = ehxcold + dot_product (
     1        pot%vhxcold(1:ndim,ii),elec_st%rho(1:ndim,jj))
         excnew = excnew + dot_product (
     1        pot%vxc(1:ndim,ii), elec_st%rho(1:ndim,jj))
      enddo
      call psum(ehxcold,1,parallel%procs_num,parallel%comm)
      call psum(excnew,1,parallel%procs_num,parallel%comm)

      ehnew = ehnew*hcub * real(elec_st%nrep)
      excnew = excnew*hcub* real(elec_st%nrep)
      ehxcold = ehxcold*hcub * real(elec_st%nrep)
 
c     energy term due to ionic potential (NOT used explicitly for the
c     total energy, computed for completeness of energies reported in
c     parsec.out)
      eion = dot_product (pot%vion,elec_st%rho(1:ndim,1))
      call psum(eion,1,parallel%procs_num,parallel%comm)
      eion = eion*hcub * real(elec_st%nrep)

c     compute total energy:
c     The total electronic energy is - (sum of eigenvalues)-(old hxc
c     energy) +1/2*(new Hartree term) + (total new xc energy)
      eele = eeig - ehxcold + ehnew/two + totexc  
c     compute difference from previous iteration (only the electronic
c     part changes), per atom
      dele = (eele - eold)*rydberg/real(natom,dp)
c     save the electronic energy as the old one for the next run
      eold = eele
c     add nuclear energy to get total energy, in Rydberg
      etot = eele + enuc
      if (pbc%is_on) etot = etot + pbc%ealpha
c     total energy per atom/ in eV
      bdev = rydberg*etot/real(natom,dp)

c     report results
      if (parallel%iammaster) then
         write(7,*)
         write(7,40) eeig
         write(7,42) ehnew/two
         write(7,44) excnew
         write(7,46) totexc
         write(7,47) eion 
         write(7,49) enuc
         if (pbc%is_on) then
            write(7,55) pbc%ealpha
         endif

         write(7,50) dele 
         write(7,*)
         write(7,52) etot 
         write(7,54) bdev 
         write(7,*)
 40      format(2x,' Eigenvalue Energy             = ',f20.8,' [Ry]')
 42      format(2x,' Hartree Energy                = ',f20.8,' [Ry]')
 44      format(2x,' Integral_{Vxc*rho}            = ',f20.8,' [Ry]')
 46      format(2x,' Exc = Integral{eps_xc*rho}    = ',f20.8,' [Ry]')
 47      format(2x,' Electron-Ion energy           = ',f20.8,' [Ry]')
 49      format(2x,' Ion-Ion Energy                = ',f20.8,' [Ry]')
 50      format(2x,' (E(new)-E(old))/atom  = ',2x,f20.8,' [eV]')
 55      format(2x,' Alpha Energy                  = ',f20.8,' [Ry]')
 52      format(2x,' Total Energy = ',2x,f20.8,' [Ry]')
 54      format(2x,' Energy/atom  = ',2x,f20.8,' [eV]')
         elec_st%etot = etot
      endif

      end subroutine totnrg
c     ===============================================================
