c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine shifts a potential so that its average is zero.
c     Needed only with periodic boundary conditions.
c
c     author: ?
c
c     ---------------------------------------------------------------
      subroutine potshift(hcub,alatt,nrep,ndim,procs_num,comm,pot,avg)

      use constants
      implicit none
c
c     Input/Output variables:
c
c     hcub = (grid spacing)^3
      real(dp), intent(in) :: hcub
c     size of side of box, for periodic systems
      real(dp), intent(in) :: alatt(3)
c     number of symmetry representations (used as ratio between
c     volumes of full cell and of irreducible wedge)
      integer, intent(in) :: nrep
c     size of input potential
      integer, intent(in) :: ndim
c     communication parameters
      integer, intent(in) :: procs_num,comm
c     input: potential, output: shifted potential
      real(dp), intent(inout) :: pot(ndim)  
c     average of potential prior to shifting
      real(dp), intent(out) :: avg
c
c     Work variables:
c
c     counter
      integer i
c     ---------------------------------------------------------------
      avg = sum(pot) * real(nrep,dp)
      call psum(avg,1,procs_num,comm)

c     rescale average
      avg = avg*hcub / (alatt(1)*alatt(2)*alatt(3))

      pot = pot - avg

      end subroutine potshift
c     ===============================================================
