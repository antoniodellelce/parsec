c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Read in user parameters from parsec.in.
c
c     ---------------------------------------------------------------
      subroutine usrinput(clust,elec_st,grid,pot,p_pot,mol_dynamic
     1     ,pbc,mixer,solver,move,istart,mxiter,vconv
     2     ,npolflg,field,ipsp,outflag,ipr
     3     ,OldInpFormat,OutEvFlag,olddat,ierr)

      use constants
      use cluster_module
      use electronic_struct_module
      use grid_module
      use potential_module
      use pseudo_potential_module
      use molecular_dynamic_module
      use pbc_module
      use mixer_module
      use eigen_solver_module
      use movement_module
      use esdf

      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type(cluster), intent(out) :: clust
c     electronic structure
      type(electronic_struct), intent(out) :: elec_st
c     grid related data
      type(grid_data), intent(out) :: grid
c     potential related data
      type (potential), intent(out) :: pot
c     pseudopotential related data
      type(pseudo_potential), intent (out) :: p_pot
c     molecular dynamic related data
      type(molecular_dynamic), intent (out) :: mol_dynamic
c     periodic boundary conditions data
      type(pbc_data), intent (out) :: pbc
c     mixer related data
      type(mixer_data), intent (out) :: mixer
c     solver related data
      type(eigen_solver), intent (out) :: solver
c     movement data
      type(movement), intent (out) :: move

c     restart flag
      integer, intent(out) :: istart
c     maximal # of iterations
      integer, intent(out) :: mxiter
c     convergence criterion
      real(dp), intent(out) :: vconv
c     polarizability flag
      integer, intent(out) :: npolflg
c     polarizability field
      real(dp), intent(out) :: field
c     pseudopotential flag
      integer, intent(out) :: ipsp
c
c     output flag for writing wfn.dat file:
c     mod(outflag,2) = 1 : write all calculated wave functions 
c     mod(outflag,2) = 0 : write only the ones for occupied states
c     mod(outflag/2,2) = 1 : write wfn.dat only after SCF is finished
c     mod(outflag/2,2) = 0 : write wfn.dat between steps of SCF loop
c
      integer, intent(out) :: outflag
c
c     output flag for additional info in parsec.out
c     default is ipr = 0 (only basic output); higher values will
c     cause additional information to be printed out
c     ipr .ge. 1 : print out local/non-local components of force and
c                  initial eigenvalues
c     ipr .ge. 2 : print out all symmetry data and additional arpack output
c     ipr .ge. 3 : print out more BFGS output
c     ipr .ge. 4 : print out all BFGS output
c     ipr .ge. 5 : print out all g-stars (warning: _lots_ of data)
c     ipr .ge. 6 : print out local potentials (warning: _lots_ of data)
c
      integer, intent(out) :: ipr

      logical, intent(out) :: OldInpFormat
c
c     output flag for writing eigen.dat file:
c     mod(OutEvFlag,2) = 0 : write all calculated eigenvalues in eigen.dat
c     mod(OutEvFlag,2) = 1 : write only the ones for occupied states
c     mod((OutEvFlag-1)/2,2) = 0 : write eigen.dat only after SCF is finished
c     mod(OutEvFlag/2,2) = 1 : write eigen.dat between steps of SCF loop
c
      integer, intent(out) :: OutEvFlag
c     true if wavefunctions to be writen to old.dat
      logical, intent(out) :: olddat

c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     counters
      integer ity, i, lor, itmp
      real(dp) :: dtmp

c     spin number
      integer nspin
c     correlation type
      character (len=2) :: icorr
c     ionization energy difference, used with cc correlation only
      real(dp) :: dioniz
c     electron temperature (in Kelvin), tfermi
c     [fixed occupations flag if negative]
      real(dp) :: tfermi
c     string flag
      character (len=60) ::  strflag
c     string with local components of pseudopotential
      character (len=1) :: locstr
c     flag for selecting which atoms are allows to move
c     flag is used for constructing mvat only.
c     flag is NOT passed to the main code
      integer nslctflg
c     default values for the different types
      integer integer_default
      real(dp) ::  double_default 
      character string_default
      logical boolean_default

c     boundary sphere size (atomic units) - for confined systems
c     (zero boundary condition)
      real(dp) :: rmax
c     size of side of box (atomic units), for periodic systems
      real(dp) :: alatt(3)
c     scale used for atomic coordinates
      real(dp) :: cscale(3,3),vtmp(3)
c     grid spacing (atomic units)
      real(dp) :: h
c     net charge in the system (in units of electron charge)
      real(dp) ncharge
c     number of different atom types
      integer naty
c     total actual number of atoms (from all types combined)
c     used as counter during atom coordinate read-in 
      integer natom

c     number of lines in block data
      integer nlines
c     number of states
      integer nstate
c     order of finite difference expansion
      integer norder
      integer, parameter :: mxorder = 20

      logical istart_tmp
      logical npolflg_tmp

      integer l_max
      real(dp) :: stpmax

      logical is_move,lflag, OutputAllStates

c     temporary arrays for atom information
      integer, parameter :: maxatm = 10000
      integer, allocatable :: tmove(:)
      real(dp), allocatable :: acoord(:,:)

c     ---------------------------------------------------------------

c     initializing default values
      integer_default = 0
      double_default = zero
      string_default = ' '
      boolean_default = .false.

      allocate(tmove(maxatm))
      allocate(acoord(maxatm,3))

c     initialize error flag
      ierr = 0

c     User parameter input from file 'parsec.in'	
c     Progress output written into 'parsec.out'
      call esdf_init ('parsec.in')

c     read in restart flag: 
c     .false. for initial run, .true. for restaring from wfn.dat
      istart_tmp = esdf_boolean ('Restart_run', boolean_default)
      if (istart_tmp) then
         istart = 1
         write(7,*) ' Restarted Run - from wfn.dat'
      else
         istart = 0
         write(7,*) 'Initial Run - starting from atomic potentials'
      endif
      write(7,*)
c     read in boundary condition type 
      write(7,*) 'Grid data:'
      write(7,*) '----------'
      pbc%is_on =  esdf_boolean ('Periodic_System', boolean_default)

c     read in (boundary sphere size / side of box), and grid spacing
      if (pbc%is_on) then
         if (esdf_block('Cell_Shape',nlines)) then
            if ( nlines .ne. 1) then
               write(7,*) 'ERROR: there should be only one line '
               write(7,*) 'in the "Cell_Shape" block'
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            endif
            read (block_data(nlines),*,iostat=ierr) 
     1           alatt(1),alatt(2),alatt(3)
            if (ierr .ne. 0) then
               write(7,*) 'ERROR: bad specification of "Cell_Shape" '
               write(7,*) ' block for periodic system! '
               write(7,*)block_data(nlines)
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            endif
         else
            write(7,*) 'ERROR: specification of "Cell_Shape" block'
            write(7,*) ' for periodic system not found! '
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
         dtmp = esdf_physical ('Lattice_Vector_Scale',one,'bohr')
         alatt = alatt * dtmp
      else
         rmax = esdf_physical ('Boundary_Sphere_Radius',
     1        double_default,'bohr')
         if (rmax .eq. double_default) then
            write(7,*) 'ERROR: specification of boundary radius for'
            write(7,*) ' finite system not found! '
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
      endif
      h = esdf_physical ('Grid_Spacing',double_default,'bohr')
      if (h .eq. double_default) then
         write(7,*) 'ERROR: specification of grid spacing '
     1        ,' not found! '
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif
      call create_grid (rmax,h,grid)

      if (.not. pbc%is_on) then 
         write(7,*) 'Confined system with zero boundary condition!'
         write(7,30) rmax
 30      format(' Boundary sphere size is', 1x, f10.6, 1x, 'bohrs')
         write(7,32) h
 32      format(' Grid spacing is', 1x, f9.6, 1x, 'bohrs')
         write(7,*)
      else
         pbc%box_size = alatt
         write(7,*) 'Periodic boundary conditions!' 
         write(7,*) 'Dimensions of the box [bohr] : '
         write(7,34) alatt(1),alatt(2),alatt(3)
         write(7,*)
 34      format(3(f15.10, 1x))
         write(7,36) h
 36      format(' User-provided grid spacing is',1x,f9.6,1x,'bohrs')
         write(7,*) 'WARNING: grid spacing may be rescaled below!!'
         write(7,*)
      endif

      olddat = esdf_boolean ('Olddat_style', .false.)
      if (olddat) then
         write(7,*) ' WARNING: writing output to file old.dat'
         write(7,*) ' symmetry operations will be ignored!'
         grid%shift = zero
      else
         write(7,*) 'Grid points are shifted from origin! '
         write(7,'(a,3f8.4,a)') ' shift vector = ',grid%shift,
     1        '   [units of grid spacing]'
      endif

c     read in order of finite difference expansion
      itmp = 12
      norder = esdf_integer ('Expansion_Order',itmp)

      write(7,38) norder
 38   format(' Finite-difference expansion of order', 1x, i2)
      write(7,*)
      if ((norder .lt. 1) .or. (norder .gt. mxorder)) then
         write(7,*)
         write(7,*) 'ERROR: Expansion_Order is the number of'
         write(7,*) 'neighbors used on two sides in numerical'
         write(7,*) 'derivative. It should be an integer' 
         write(7,*) 'between 2 to', mxorder
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif

      if (mod(norder,2) .ne. 0) then
         write(7,*)
         write(7,*) 'ERROR: Expansion_Order should be an even number'
         write(7,*) 'between 2 to', mxorder
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif

      grid%norder = norder/2

c     read in the number of eigenstates to be calculated, nstate;
c     net cluster charge, ncharge
      nstate = esdf_integer ('States_Num',integer_default)
      if (nstate .eq. integer_default) then
         write(7,*) 'ERROR: unknown number of states. '
         write(7,*) ' Input line "States_Num" not found!'
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif
      ncharge = esdf_physical ('Net_Charges',double_default,'e')
      write(7,*) 'Eigenvalue data: '
      write(7,*) '---------------- '
      write(7,*) 'Number of states: ', nstate
      write(7,'(a,f8.3)') 'Net cluster charge: ', ncharge

c     read in maximal # of iterations before giving up on self-consistency
c     read in electron temperature (in Kelvin)
      dtmp = 80.0
      tfermi = esdf_physical ('Fermi_Temp',dtmp,'k')
      itmp = 50
      mxiter = esdf_integer ('Max_Iter',itmp)
      write(7,*)
      if (tfermi .ge. zero) then
         write(7,80) tfermi
      else 
         write(7,*) 'WARNING: reading occupations from occup.in file'
c     occupations read from file in initial.f
      endif
 80   format(1x,'Fermi temperature =',1x,f6.1,1x,'[K]')

      write(7,*)
      write(7,*) 'Self-consistency data: '
      write(7,*) '----------------------'
      write(7,*) 'Max number of iterations is ', mxiter
c     read in maximal # of iterations before giving up on self-consistency
c     read in electron temperature (in Kelvin)

c     read in eigensolver
      strflag = esdf_reduce(esdf_string ('Eigensolver', 'arpack'))

      select case (trim(strflag))
      case ('arpack')
         write(7,*) 'Using ARPACK (implicitly restarted Arnoldi '
     1        ,'eigensolver)'
         write(7,*)
         solver%name = ARPACK
      case ('diagla')
         write(7,*) 'Using DIAGLA (block preconditioned '
     1        ,'Lanczos eigen-problem solver)'
         write(7,*)
         solver%name = DIAGLA
      end select

      call create_eigen_solver (solver)

c     read in value for convergence criterion and diagonalization tolerance
      dtmp = 2.d-4
      vconv = esdf_physical ('Convergence_Criterion', dtmp,'ry')
      dtmp = 1.d-4
      solver%toler = esdf_double('Diag_Tolerance', dtmp)
      dtmp = 1.d2
      solver%toler_in = esdf_double('Initial_diag_tolerance',dtmp)
      itmp = 1
      solver%nadd = esdf_integer('Subspace_buffer_size',itmp)
      write(7,40) vconv
 40   format(' Self-consistency convergence criterion is',1x,f8.5,' Ry')
      write(7,42) solver%toler
 42   format(' Diagonalization tolerance is',1x,e11.5)
      write(7,'(a,1x,f7.3)') ' Tolerance in initial diagonalization '
     1     ,solver%toler_in
      write(7,'(a,1x,i5)') ' Buffer size in subspace is ',solver%nadd
      write(7,*)

      write(7,*) 'Mixer data:'
      write(7,*) '-----------'
c     read in mixer type
      strflag = esdf_reduce(esdf_string ('Mixing_Method', 'anderson'))

      select case (trim(strflag))
      case ('anderson')
         mixer%name = ANDERSON
      case ('broyden')
         mixer%name = BROYDEN
      end select

      itmp = 4
      mixer%memory =  esdf_integer ('Memory_Param', itmp)

      dtmp = 0.30d0
      mixer%param = esdf_double ('Mixing_Param', dtmp)

c     Maximum memory parameter, mixer%block > mixer%memory+1
      mixer%block = 20

      if (mixer%name .eq. ANDERSON) then
c     read mixing parameter (Anderson mixer)
         write(7,*) 'Anderson mixer'

      else if (mixer%name .eq. BROYDEN) then
         write(7,*) ' Generalized Broyden mixer'
         if (mixer%memory+1 .ge. mixer%block) then
            write(7,*)
            write(7,*) 'ERROR: insufficient memory for mixing.'
            write(7,*) 'Increase mixer%block in structures.F to 
     1           at least', mixer%memory+1
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
      else
         write(7,*)
         write(7,*) 'ERROR: Unknown mixer type'
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif
      write(7,48) mixer%param, mixer%memory
 48   format(' Initial Jacobian:',1x,f6.3,2x,'Mixing memory is',1x,i2)
      write(7,*)

      write(7,*) 'Atom data:'
      write(7,*) '----------'
c     read in and write number of atom types
      naty = esdf_integer('Atom_Types_Num',integer_default)
      if (naty .eq. integer_default) then
         write(7,*) 'ERROR: unknown number of atom types. '
         write(7,*) ' Input line "Atom_Types_Num" not found!'
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif
      write(7,'(/,a,i5,/)') 'Total # of atom types is ', naty

      strflag = esdf_reduce(esdf_string ('Coordinate_Unit',
     1     'cartesian_bohr'))
c     cscale : for periodic systems, hold unit lattice vectors in
c     column-wise form
      cscale = zero

      select case (trim(strflag))
      case ('cartesian_bohr')
         write(7,*) 'Atom coordinates input in Bohr radii'
         do i = 1, 3
            cscale(i,i) = one
         enddo
      case ('cartesian_ang')
         write(7,*) 'Atom coordinates input in angstroms'
         do i = 1, 3
            cscale(i,i) = one/angs
         enddo
      case ('lattice_vectors')
         write(7,*) 'Atom coordinates input in lattice vector units'
         if (.not. pbc%is_on) then
            write(7,*) ' ERROR: this is not a periodic system'
            write(7,*) ' Lattice vectors not defined. STOP.'
            ierr = 1
            return
         endif
         do i = 1, 3
            cscale(i,i) = alatt(i)
         enddo
      end select
      write(7,*)

      clust%type_num = naty
      allocate (clust%natmi (1:naty))
      allocate (clust%name (naty))

c     creating the pseudo_potential module
      call create_pseudo_potential (naty,norder/2,p_pot)
      p_pot%nlocp = 0
      p_pot%eleatm = zero

      ipsp = 0
      lflag = esdf_boolean ('Old_Pseudopotential_Format',
     1     boolean_default)
      if (lflag) ipsp = ipsp + 1

c     read in minimization data
      strflag = esdf_reduce(esdf_string ('Minimization', 'none'))

      select case (trim(strflag))
      case ('none')
         move%name = NONE
      case ('simple')
         move%name = SIMPLE
      case ('bfgs')
         move%name = BFGS
      case ('manual')
         move%name = MANUAL
      end select

c     For each atom type:
      natom = 0
      do ity = 1,naty
c     read name and core cutoff
         clust%name(ity) = esdf_string ('Atom_Type', string_default)

c     local component
         locstr = esdf_reduce(esdf_string('Local_Component',
     1        string_default))

         if(locstr .eq. 's') then
            p_pot%loc(ity) = 1
         else if (locstr .eq. 'p') then
            p_pot%loc(ity) = 2
         else if (locstr .eq. 'd') then
            p_pot%loc(ity) = 3
         else if (locstr .eq. 'f') then
            p_pot%loc(ity) = 4
         else
            write(7,*)
            write(7,*) 'ERROR: problem with atom type', ity
            write(7,*) 'Impossible local pseudopotential'
            write(7,*) 'Must be s, p, d, or f'
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
c     if pseudopotential files have no header info, get that from input
         if (mod(ipsp,2).eq.1) then
            p_pot%rcore(ity) = esdf_physical ('Core_Cutoff_Radius'
     1           ,double_default,'bohr')
         endif
c
c     read other pseudopotential parameters, if available
c     WARNING: if pseudopotential files have header info, the number
c     of angular momentum channels must be consistent with what is
c     inside the psp files.
c
         p_pot%nlocp(ity) =  esdf_integer ('Potential_Num'
     1        ,integer_default)

         if (esdf_block('Electron_Per_Orbital',nlines)) then
            if ( nlines .ne. 1) then
               write(7,*) 'ERROR: there should be only one line '
               write(7,*) 'in the "Atom_Per_Orbital" block'
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            endif
            read (block_data(nlines),*) 
     1           p_pot%eleatm(ity,1:p_pot%nlocp(ity))
         else
            if (mod(ipsp,2).eq.1) write (6,*)
     1           'Cannot find #electrons/orbital block'
         endif

c     if psp files have no header, check if input parameters are sane
         if (mod(ipsp,2).eq.1) then
            if ((p_pot%nlocp(ity) .le. 0) .or. 
     1           (p_pot%nlocp(ity) .gt. 4)) then
               write(7,*)
               write(7,*) 'ERROR: problem with atom type', ity
               write(7,*) 'impossible number of potentials!'
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            endif
            if (p_pot%loc(ity) .gt. p_pot%nlocp(ity)) then
               write(7,*)
               write(7,*) 'ERROR: problem with atom type', ity
               write(7,*) 'local pseudopot. beyond # of components'
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            endif
         endif

c     read parameters for Fourier filtering of pseudopotentials and
c     core charge (if it exists)
         dtmp = zero
         p_pot%alpha(ity) = esdf_double ('Alpha_filter', dtmp)
         dtmp = zero
         p_pot%beta1(ity) = esdf_double ('Beta1_filter', dtmp)
         dtmp = p_pot%alpha(ity)
         p_pot%acore(ity) = esdf_double ('Core_filter', dtmp)

c     read number of atoms of this type
         nslctflg  = esdf_integer ('Move_Flag', integer_default)

c     read their coordinates
         if (esdf_block ('Atom_Coord',clust%natmi(ity) )) then   
            if(nslctflg.gt.clust%natmi(ity)) then
               write(7,*)
               write(7,*) 'ERROR: invalid atom selection flag'
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            end if 
            write(7,12) clust%natmi(ity), clust%name(ity)
            write(7,*) 'and their initial coordinates are:'
            write(7,*)
            write(7,16)

            do i=1,clust%natmi(ity)
               natom = natom + 1
               if (natom .gt. maxatm) then
                  write(7,*) ' ERROR: too many atoms in input! ',
     1                 natom,maxatm,' Increase maxatm in usrinput. '
                  ierr = 1
                  return
               endif
               if (nslctflg .eq. 0) then
                  read(block_data(i),*) acoord(natom,:)
                  tmove(natom) = 1
               else if (nslctflg .gt. 0) then
                  read(block_data(i),*) acoord(natom,:)
                  if (i .le. nslctflg) then 
                     tmove(natom)=1
                  else
                     tmove(natom)=0
                  endif
               else
                  if (move%name .eq. NONE) then
                     read (block_data(i),*) acoord(natom,:)
                     tmove(natom)=1
                  else
                     read (block_data(i),*) acoord(natom,:)
     1                    ,tmove(natom)
                  endif
                  if((tmove(natom).ne.0).and.
     1                 (tmove(natom).ne.1))then
                     write(7,*)
                     write(7,*)'ERROR: invalid atom movement index'
                     write(7,*) 'STOP in usrinput'
                     ierr = 1
                     return
                  endif
               endif
               vtmp = matmul(cscale,acoord(natom,:))
               acoord(natom,:) = vtmp
               write(7,18) acoord(natom,:),tmove(natom)
            enddo
         else
            write (7,*) 'ERROR: no atom coordinates were found'
            write (7,*) 'for atom type ', clust%name(ity)
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
      enddo                     !do ity = 1,naty
 12   format(1x,'There are', i4, 1x, a2, 1x, 'atoms')
 16   format(4x,'x [bohr]',10x,'y [bohr]',10x,'z [bohr]',5x,'movable?')
 18   format(3(f15.9,3x),3x,i1)

c     creating the 'empty' cluster
      call create_cluster (naty,natom,clust)
      clust%mvat(1:natom) = tmove(1:natom)
      clust%xatm(1:natom) = acoord(1:natom,1)
      clust%yatm(1:natom) = acoord(1:natom,2)
      clust%zatm(1:natom) = acoord(1:natom,3)
      deallocate(tmove)
      deallocate(acoord)

      write(7,*)
      write(7,10) clust%atom_num
      write(7,*)

      mol_dynamic%atom_num = natom

 10   format(1x,'Total number of atoms = ', i4)

c     read in correlation type
      write(7,*) 'Correlation data:'
      write(7,*) '-----------------'
      strflag = esdf_reduce(esdf_string ('Correlation_Type', 'ca'))

      select case (trim(strflag))
      case ('xa')
         icorr = 'xa'
         write(7,20) icorr,'LDA, Slater x-alpha'
      case ('wi')
         icorr = 'wi'
         write(7,20) icorr,'LDA, Wigner interpolation'
      case ('hl')
         icorr = 'hl'
         write(7,20) icorr,'LDA, Hedin-Lundqvist'
      case ('ca','pz','lda')
         icorr = 'ca'
         write(7,20) icorr,
     1        'LDA, Ceperley-Alder, Perdew-Zunger parametrization'
      case ('pw','pw91')
         icorr = 'pw'
         write(7,20) icorr,'GGA, Perdew-Wang parametrization'
      case ('pb','pbe')
         icorr = 'pb'
         write(7,20) icorr
     1        ,'GGA, Perdew-Burke-Ernzerhof parametrization'
      case ('lb')
         icorr = 'lb'
         write(7,20) icorr
     1        ,'ACLDA, Leeuwen-Baerends correction'
      case ('cs')
         icorr = 'cs'
         write(7,20) icorr
     1        ,'ACLDA, Casida-Salahub correction'
c     read-in the ionization energy difference (actually used 
c     only with 'cs' corr.)
         dtmp = 0.d0
         dioniz = esdf_physical('Ion_Energy_Diff', dtmp,'ry')
         write(7,54) dioniz
 54      format(1x,'Ionization energy difference is:',1x,f7.4,' Ry')
      case default
         write(7,*)
         write(7,*) 'ERROR: Wrong Correlation Type.'
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      end select
 20   format(1x,'Exchange-Correlation functional is',1x,a2,/,1x,a)
      write(7,*) 

      write(7,*) 'Other input data:'
      write(7,*) '-----------------'

c     read in output flag: 
c     .true. for writing out all calculated wave functions, .false. otherwise
      outflag = 0
      OutputAllStates = 
     1     esdf_boolean ('Output_All_States', boolean_default)
      if (OutputAllStates)  then
         write(7,*) 'Writing all calculated '
     1        ,'wave functions to wfn.dat file'
         write(7,*)
         outflag = outflag + 1
      endif
      lflag = esdf_boolean ('Save_Intermediate_Charge_Density',
     1     boolean_default)
      if (lflag)  then
         write(7,*) 'Writing charge density to '
     1        ,'wfn.dat file after each step of SCF'
         write(7,*)
         outflag = outflag + 2
      endif

c     read in spin polarization flag
      lflag = esdf_boolean ('Spin_Polarization', boolean_default)
      if (lflag) then
         nspin = 2
         write(7,'(a,/)') 'Spin-polarized computation!'
      else
         nspin = 1
         write(7,'(a,/)') 'No spin effects!'
      endif

      OldInpFormat = esdf_boolean ('Old_Interpolation_Format',
     1     boolean_default)
      write(7,*)
      if (OldInpFormat) then
        write(7,*) 'Using old (direct) interpolation of pseudopot. !'
      else
        write(7,*) 'Using new (r*V_ps) interpolation of pseudopot. !'
      endif

      ipr = esdf_integer('Output_Level',integer_default)
      write(7,*)
c     read in  eigenstate to separated file output  flag:
c     .false. / .true.
      OutEvFlag=0
      lflag = esdf_boolean ('Output_Eigenvalues', .false.)
      if (lflag) then
         OutEvFlag = OutEvFlag + 1
         write(7,*) 'Generating Eigenvalue Output File!' 
         lflag = esdf_boolean ('Output_All_States', boolean_default)
         if (OutputAllStates)  then
            write(7,*) 'Writing all calculated '
     1           ,'eigenvalues to eigen.dat'
            write(7,*)
            OutEvFlag = OutEvFlag + 1
         endif
         lflag = esdf_boolean ('Save_Intermediate_Eigenvalues',
     1        boolean_default)
         if (lflag)  then
            write(7,*) 'Writing eigenvalues to '
     1           ,'eigen.dat after each step of SCF'
            write(7,*)
            OutEvFlag = OutEvFlag + 2
         endif
      else
         write(7,*) 'No eigenvalue output File!'
      endif
      write(7,*) 'OutEvFlag = ', OutEvFlag
      write(7,*)

      if (move%name .eq. NONE) then
         is_move = .false.
         write(7,*) 'No minimization!'
      else
         is_move = .true.
      endif

      if (is_move) then
         itmp = 500
         move%mxmove = esdf_integer ('Movement_Num', itmp)
         dtmp = 1.5d-3
         move%fmin = esdf_physical ('Force_Min', dtmp,'ry/bohr')
         stpmax = 0.05d0
         move%stepmax = esdf_physical ('Max_Step', stpmax,'bohr')
         dtmp = 1.d-4
         move%stepmin = esdf_physical ('Min_Step', dtmp,'bohr')
         move%is_restart = esdf_boolean ('relax_restart'
     1        ,boolean_default)
         write(7,*) 'Minimization data:'
         write(7,*) '------------------'
         if (move%fmin .lt. 1.d-5) then
            write(7,*) 'ERROR: minimum force below numerical accuracy'
            write(7,*) 'increase it to at least 1E-5'
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
         if (move%name .eq. SIMPLE) then
            write(7,*) 'Simple minimization!'
            write(7,*) 'This scheme is not robust!'
            write(7,*) 'make sure the construction of displacements',
     1           ' in domove.f fits your purpose.'
            if (move%stepmax .lt. 1.d-5) then
               write(7,*) 'ERROR: maximum step size too small!'
               write(7,*) 'increase it to at least 1E-5'
               write(7,*) 'STOP in usrinput'
               ierr = 1
               return
            endif
         else if (move%name .eq. BFGS) then
            write(7,*) 'Limited-memory BFGS minimization!'
            dtmp = 0.d0
            itmp = 7
            move%mmax = esdf_integer ('BFGS_number_corr', itmp)
            if (move%stepmax .eq. stpmax) then
c     unrestricted BFGS, no bounds on atom coordinates
               move%stepmax  = -1.d0
            else
c     restricted BFGS, move%stepmax defines the maximum displacement
               write(7,*) 'Restricted BFGS'
               write(7,*) 'Maximum displacement of atoms from ',
     1              'initial position is ',move%stepmax
               write(7,*) ' bohrs along each cartesian direction.'
            endif
         else if (move%name .eq. MANUAL) then
            write(7,*) 'atoms move according to info in manual.dat'
c     make all atoms movable
            clust%mvat = 1
c     set force limit to zero (this avoids undesirable outputs from
c     subroutine domove)
            move%fmin = zero
         end if
         write(7,90) move%mxmove
         if (move%name .eq. SIMPLE) then
            write(7,91) move%stepmax, move%stepmin
         endif
         write(7,92) move%fmin
         if (move%is_restart) then
            write(7,*)
     1           'Restart relaxation, information on previous runs read'
            write(7,*) 'from relax_restart.dat file.'
         endif
         if (move%name .eq. BFGS) write(7,94) move%mmax
      end if
      call create_movement (is_move, clust%atom_num, move)

 90   format(1x,'Max number of movements  =',1x,i4)
 91   format(1x,'Max step size  =',1x,f8.5,1x,'[au]',/,
     1     ' Min step size  =',1x,f8.5,1x,'[au]')
 92   format(1x,'Minimum force  =',1x,f8.5,1x,'[Ry/au]')
 94   format(1x,'Number of corrections used in LBFGS update = ',i4)
      write(7,*)

c     read in Langevin molecular dynamics flag and its associated
c     restart flag
      mol_dynamic%is_on = esdf_boolean ('Molecular_Dynamics', 
     1     boolean_default)

      mol_dynamic%is_restart = 
     1     esdf_boolean('Restart_mode',boolean_default)

c     read in LMD parameters
c     initial and final temperatures (in kelvin), 
c     step temperature for stair cooling
c     # of step, cooling scheme (1 - log, 2- linear, 3 - stair)
c     time step, friction coefficient, # of time steps
      strflag = esdf_reduce(esdf_string ('Cooling_Method','stair'))

      select case (trim(strflag))
      case ('stair')
         mol_dynamic%cool_type = 1
      case ('linear')
         mol_dynamic%cool_type = 2
      case ('log')
         mol_dynamic%cool_type = 3
      end select

      dtmp = 500.0
      mol_dynamic%tempi = esdf_physical ('Tinit', dtmp,'k')
      dtmp = 500.0
      mol_dynamic%tempf = esdf_physical ('T_Final', dtmp,'k')
      dtmp = 150.0
      mol_dynamic%step_tmp = esdf_physical ('T_Step', dtmp,'k')

      dtmp = 150.0
      mol_dynamic%time_step =  esdf_physical ('Time_Step', dtmp,'ps')
      mol_dynamic%friction_coef = esdf_double ('Friction_Coefficient'
     1     ,double_default)
      itmp = 250
      mol_dynamic%step_num = esdf_integer ('Step_num', itmp)

      if (.not.  mol_dynamic%is_on) then
         write(7,*) 'No molecular dynamics!'
      else
         call molecular_dynamic_turn_on (mol_dynamic)
         write(7,*) 'Molecular dynamics data:'
         write(7,*) '-------------------------'
         if (mol_dynamic%is_restart) then
            write(7,*) 'WARNING: restarted molecular dynamics run'
            write(7,*) 'restart data assumed present in mdinit.dat'
         endif

         select case (mol_dynamic%cool_type)
         case (1)
            write(7,*) '"Stairstep" cooling scheme '
         case (2)
            write(7,*) 'Linear cooling scheme '
         case (3)
            write(7,*) 'Logarithmic cooling scheme '
         case default
            write(7,*)  
            write(7,*)'ERROR: unkown molecular dynamics cooling scheme'
            write(7,*)'STOP in usrinput'
            ierr = 1
            return
         end select

         write(7,52) mol_dynamic%tempi, mol_dynamic%tempf
         if (mol_dynamic%cool_type.eq.1)
     1        write(7,81)mol_dynamic%step_tmp 
         write(7,82) mol_dynamic%time_step,
     1        mol_dynamic%time_step*timeaups
         write(7,83) mol_dynamic%friction_coef
         write(7,84) mol_dynamic%step_num
      endif
 52   format(1x,'Initial Temperature =',1x,f6.1,1x,'[K]',/,
     1     ' Final Temperature =',1x,f6.1,1x,'[K]')
 81   format(1x,'Temperature step =',1x,f6.1,1x,'[K]')
 82   format(1x,'Time step =',1x,f8.2,1x,'[au] = ',
     1     1x,f6.3,1x,'[ps]')
 83   format(1x,'Friction Coefficient =',f8.5,1x,'[au]')
 84   format(1x,'# of steps =',1x,i5)
      write(7,*)

c     read in polarizability flag and electric field
      npolflg_tmp = esdf_boolean ('Polarizability', boolean_default)
      if (npolflg_tmp) then
         npolflg = 1
      else
         npolflg = 0
      endif
      dtmp = 0.001
      field = esdf_physical ('Electric_Field', dtmp,'ry/bohr/e')
      if (.not. npolflg_tmp) then
         npolflg = 0
         write(7,*) 'No polarizability!'
      else
         npolflg = 1
         write(7,*) 'Polarizability data:'
         write(7,*) '--------------------'
         write(7,*) 'Using seven-fields polarizability approach'
         write(7,85) field
      endif
 85   format(1x,'Electric field  = ',f8.5,1x,'[Ry/au]')
      write(7,*)

c     flag compatibility tests
      if((mol_dynamic%is_on) .or. (move%name.ne.NONE)) then
         if (npolflg_tmp) then
            write(7,*)  
            write(7,*) 'ERROR: Cannot do polarizability and move atoms'
            write(7,*) 'set MD and Minimization Flags = 0'
            write(7,*) 'STOP in usrinput'
            ierr = 1
            return
         endif
      endif

      if (pbc%is_on .and. npolflg_tmp) then
         write(7,*)
         write(7,*) 'ERROR: Cannot do polarizability with periodic'
         write(7,*) 'boundary conditions!'
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif

      if((mol_dynamic%is_on) .and. (move%name.ne.NONE)) then
         write(7,*)  
         write(7,*) 'ERROR: Cannot do minimization and MD'
         write(7,*) 'set either MD or minimization flag to zero'
         write(7,*) 'STOP in usrinput'
         ierr = 1
         return
      endif

      call esdf_close

c     creating the 'empty' electronic_structure
      call create_electronic_struct (nspin,nstate,elec_st)
      elec_st%ncharge = ncharge 
      elec_st%icorr = icorr
      elec_st%dioniz = dioniz
      elec_st%tfermi = tfermi

      pot%nspin = nspin

      if (pbc%is_on) then
         call pbc_turn_on (natom,naty,pbc)
      endif
 
      end subroutine usrinput
c     ===============================================================
