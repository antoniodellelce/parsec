c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This is a driver routine for the symmetry operation routine
c     symgen. It determines teh set of existing symmetry operations
c     based on crystallographic systems. For non-periodic systems
c     (pbc%is_on = .false.), we need a periodic cell in order to find
c     the crystallographic system. Since a regular grid is always
c     going to impose cubic symmetry, even for highly symmetric
c     systems like an isolated atom, choose a cubic supercell with
c     side four times the boudary radius.
c
c     Unit lattice vectors:
c     A = 3x3 matrix that stores unit lattice vectors in column-wise
c     format
c
c     transformation of coordinates from cartesian to lattice
c     coordinates is done in the usual way:
c        ( r )_cart =     A  * ( r )_latt
c        ( r )_latt = inv(A) * ( r )_cart
c
c     A generic symmetry operation T is defined this way (see Jones,
c     'The Theory of Brillouin Zones and Electronic States in
c     Crystals'):
c
c       T ( r ) -> r' = M * ( r - t)
c
c     where:
c       r is original position (vector written in units of lattice vectors)
c       r' is a point related to r by operation T (same units)
c       M is a 3x3 matrix that executes rotation
c       t is a translation vector
c       operation T itself is defined by (M,t)
c
c     Notice that M is not necessarily orthogonal 
c     ( M * transpose(M) is not I ), but the matrix 
c     U = A * M * inv(A) IS orthogonal!
c
c     In structure symm:
c       M == rmtrx
c       inv(transpose(M)) = gmtrx (needed for rotations in Fourier space)
c       t == tnp
c       U == trans (rotation matrix in cartesian components)
c       A == alatt
c       transpose(inv(A)) == invlat
c
c     Author: Murilo Tiago (2005)
c
c     ---------------------------------------------------------------
      subroutine symmetries(clust,grid,pbc,symm,rsymm,ipr,ierr)

      use constants
      use cluster_module 
      use grid_module
      use pbc_module
      use symmetry_module

      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(in) :: clust
c     grid related data
      type (grid_data), intent(in) :: grid
c     periodic boundary conditions data
      type (pbc_data), intent(in) :: pbc
c     symmetry operations
      type (symmetry), intent(inout) :: symm,rsymm
c     output flag
      integer, intent(in) :: ipr
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     flags passed to symgen
      integer invers_no, nops
c     variables to be saved to symm structure at the end
c     (notice the reshape of arrays!)
      integer ntrans
      integer gmtrx(48, 3, 3)
      real(dp) :: tnp(48, 3), trans(3, 3, 48)
      integer rmtrx(48, 3, 3)
c     multiplication matrix for the group of symmetry operations
      integer mult(48,48)
c     number of elements in maximal Abelian subgroup
      integer nab
c     index for the maximal Abelian subgroup and character table
      integer :: indx(8), chi(8,8)
c     name of Abelian subgroup
      character(len=5) :: name
c     counters, temporary variables
      integer mxdatm, ii, jj, ity
      real(dp) :: tmpvec(3)
      real(dp), allocatable :: coorat(:,:,:)

c     ---------------------------------------------------------------
c
c     alatt: matrix A. If no PBC are used, construct an
c     artificial cubic supercell that encloses the cluster and
c     with unit vectors along cartesian axes x,y,z
c
      symm%alatt = zero
      if (pbc%is_on) then
         symm%alatt(1,1) = pbc%box_size(1)
         symm%alatt(2,2) = pbc%box_size(2)
         symm%alatt(3,3) = pbc%box_size(3)
      else
         symm%alatt(1,1) = four*grid%rmax
         symm%alatt(2,2) = four*grid%rmax
         symm%alatt(3,3) = four*grid%rmax
      endif
ccm
c  input hexagonal lattice vectors by hand
c
c      symm%alatt(1,1) = four*grid%rmax/2.d0
c      symm%alatt(2,1) = four*grid%rmax*sqrt(3.0)/2.d0
c      symm%alatt(1,2) = four*grid%rmax/2.d0
c      symm%alatt(2,2) = -symm%alatt(2,1)
ccm

      symm%invlat = transpose(symm%alatt)
      call mtrxin(symm%invlat,tmpvec(1),tmpvec(2))

      mxdatm = maxval(clust%natmi)
c
c     coorat: coordinates of all atoms with respect to unit lattice
c     vectors, in a layout proper for symgen
c
      allocate(coorat(clust%type_num,mxdatm,3))
      coorat = zero
      ii = 0
      do ity = 1, clust%type_num
         do jj = 1,clust%natmi(ity)
            ii = ii + 1
            tmpvec(1) = clust%xatm(ii)
            tmpvec(2) = clust%yatm(ii)
            tmpvec(3) = clust%zatm(ii)
            coorat(ity,jj,:) = matmul(tmpvec,symm%invlat)
         enddo
      enddo

      write(7,'(/,a)') ' Symmetry Properties:'
      write(7,'(a)') ' --------------------'

      if (symm%use_symm) then
c
c     If symmetry operations are to be used, search for symmetry
c     operations.
c
c         nops = -1  : include fractional translations
c         nops = -2  : do not include fractional translations
         if (pbc%is_on) then
            nops = -1
         else
            nops = -2
         endif

         call symgen(ipr, symm%alatt, coorat, clust%natmi,
     1        clust%type_num, invers_no, nops, ntrans, gmtrx, tnp,
     2        mxdatm, rmtrx, trans, nab, indx, chi)
      else
c
c     Otherwise, create the structure but allow only Identity as
c     symmetry operation.
c
         ntrans = 1
         gmtrx = 0
         rmtrx = 0
         trans = 0
         tnp = zero
         nab = 1
         indx(1) = 1
         chi(1,1) = 1
         do jj=1,3
            gmtrx(1,jj,jj) = 1
            rmtrx(1,jj,jj) = 1
            trans(jj,jj,1) = 1
         enddo
         write(7,'(/,a)') 'skipping non-trivial symmetry operations'
      endif
c
c     save information about symmetry operations:
c
c     full point group stored in structure symm
      call create_symmetry(ntrans,symm)
      do jj = 1, ntrans
         symm%gmtrx(:,:,jj) = gmtrx(jj,:,:)
         symm%rmtrx(:,:,jj) = rmtrx(jj,:,:)
         symm%trans(:,:,jj) = trans(:,:,jj)
         symm%tnp(:,jj) = tnp(jj,:)
      enddo
c
c     Sanity check
c
      if (ntrans .eq. 0) then
         ierr = 1
         return
      endif
c
c     Abelian subgroup (reduced group) stored in rsymm
c
      call create_symmetry(nab,rsymm)
      rsymm%use_symm = symm%use_symm
      rsymm%alatt = symm%alatt
      rsymm%invlat = symm%invlat
      do ii = 1, nab
         jj = indx(ii)
         rsymm%gmtrx(:,:,ii) = gmtrx(jj,:,:)
         rsymm%rmtrx(:,:,ii) = rmtrx(jj,:,:)
         rsymm%trans(:,:,ii) = trans(:,:,jj)
         rsymm%tnp(:,ii) = tnp(jj,:)
         rsymm%chi(1:nab,ii) = chi(1:nab,ii)
      enddo
      deallocate(coorat)

      end subroutine symmetries
c     ===============================================================
c
c     For a given vector r in cartesian coordinates, apply it-th
c     symmetry operation, T, according to the usual rule:
c                  T ( r ) -> tr = M * ( r - t)
c     Output is vector tr in cartesian coordinates.
c
c     ---------------------------------------------------------------
      subroutine symop(symm,it,r,tr)

      use constants
      use symmetry_module
      implicit none
c
c     Input/Output variables:
c
c     symmetry operations in reduced group:
      type (symmetry), intent(in) :: symm
c     index of this operation
      integer, intent(in) :: it
c     input vector
      real(dp), intent(in) :: r(3)
c     transformed vector, tr = T ( r )
      real(dp), intent(out) :: tr(3)
c
c     Work variables:
c
c     translation vectors in cartesian units (needed because the symm%tnp
c     are defined in units of supercell unit vectors)
      real(dp) :: tnplat(3)
c     ---------------------------------------------------------------
      tnplat(:) = matmul(symm%alatt,symm%tnp(:,it))
      tr = r - tnplat
      tr = matmul(symm%trans(:,:,it),tr)

      end subroutine symop
c     ===============================================================
