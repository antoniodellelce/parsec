c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Save DFT eigenvalues to unit # funit
c
c     Author: O. Guliamov, L. Kronik (2004)
c
c     ---------------------------------------------------------------
      subroutine eigensave(elec_st,imove,funit,OutEvFlag)

      use constants
      use electronic_struct_module

      implicit none
c
c     Input/Output variables:
c
c     electronic structure
      type (electronic_struct), intent(in) :: elec_st

c     current itteration number
      integer, intent(in) :: imove
c     unit of output file
      integer, intent(in) :: funit
c     output flag
      integer, intent(in) :: OutEvFlag 
c
c     Work variables:
c
c     counters
      integer i,ii,j,jj
c     number of eigenvalues to be written
      integer nstate
c     representation index
      integer irp
c     jrep keeps track of how many eigenstates are already in each
c     representation
      integer :: jrep(elec_st%nrep)
c     spin identifier for printing
      character (len=2) :: idsp
c     ---------------------------------------------------------------
      do ii=1,elec_st%nspin
         jj=ii-1+elec_st%nspin
         if (jj .eq. 1) idsp = '  '
         if (jj .eq. 2) idsp = 'up'
         if (jj .eq. 3) idsp = 'dn'
         if (mod(OutEvFlag,2).eq.0) then
            nstate = elec_st%nstate
         else
            nstate = elec_st%ifmax(ii)
         endif
         jrep = 0
         do i = 1,nstate
            irp = elec_st%irep(i,ii)
            jrep(irp) = jrep(irp) + 1
            write(funit,11) i, 
     1           elec_st%eig(irp,ii)%en(jrep(irp)),
     2           elec_st%eig(irp,ii)%en(jrep(irp))*rydberg, 
     3           elec_st%eig(irp,ii)%occ(jrep(irp)),
     4           irp,imove, idsp 
         enddo
      enddo
      call myflush(funit)
 11   format(i5,3x,f18.10,3x,f12.4,3x,f12.4,3x,i2,3x,i5,3x,a2)

      end subroutine eigensave
c     ===============================================================
