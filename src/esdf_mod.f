c     ===============================================================
c
c e l e c t r o n i c   s t r u c t u r e   d a t a   f o r m a t
c ---------------------------------------------------------------
c
c                            e s d f
c                            =======
c
c Author: chris j. pickard (c)
c email : cp@min.uni-kiel.de
c place : kiel, germany
c date  : 5/6th august 1999
c
c Summary
c -------
c
c This module is designed to simplify and enhance the input of data into
c electronic structure codes (for example, castep). it works from a
c fashion, and input is independent of the ordering of the input
c file. an important feature is the requirement that most inputs require
c default settings to be supplied within the main program calling
c esdf. this means that rarely used variables will not clutter everyday
c input files, and, even more usefully, "intelligence" may be built into
c the main code as the defaults may be dependent of other set
c variables. block data may also be read in. another important feature
c is the ability to define "physical" values. this means that the input
c files need not depend on the internal physical units used by the main
c program.
c
c History
c -------
c
c Esdf has been written from scratch in f90, but is heavily based
c (especially for the concept) on the fdf package developed by alberto
c garcia and jose soler. it is not as "flexible" as fdf - there is no
c provision for branching to further input files. this simplifies the
c code, and i hope that it is still useful without this feature. also,
c the input and defaults are not dumped to a output file currently. i've
c not found this a hindrance as of now.
c
c Future
c ------
c
c My intention is to make this release available to alberto garcia and
c jose soler for their comments. it might be a good idea to use this as
c a base for fully converting the fdf package to f90. or it may remain
c as a cut down version of fdf. i certainly hope that a package of the
c fdf sort becomes widely used in the electronic structure community. my
c experience has been very positive.
c
c Usage
c -----
c
c First, "use esdf" wherever you wish to make use of its features. in
c the main program call the initialisation routine: call
c esdf_init('input.esdf'). "input.esdf" is the name of the input file -
c it could be anything. this routine opens the input file, and reads
c into a dynamically allocated storage array. the comments and blank
c lines are stripped out. you are now ready to use the
c esdf_functions. for example, if you want to read in the number of
c atoms in your calculation, you would use: natom =
c esdf_integer('numberofatoms',1), where 'numberofatoms' is the label to
c search for in the input file, and '1' is the default. call esdf_close to
c deallocate the data arrays. you may then open another input file using
c esdf_init. it is not currently possible to open more that on input
c file at one time.
c
c Syntax
c ------
c
c The input file can contain comments. these are defined as anything to
c the right of, and including, '#', ';', or '!'. it is straightforward
c to modify the routine to accept further characters. blank lines are
c ignored -- use comments and blank lines to make you input file
c readable.
c
c The "labels" are case insensitive (e.g. unitcell is equivalent to
c unitcell) and punctuation insensitive (unit.cell is equivalent to
c unit_cell is equivalent to unitcell). punctuation characters are '.'
c and '-' at the moment. again - use this feature to improve
c readability.
c
c The following are equivalent ways of defining a physical quantity:
c
c "Ageofuniverse = 24.d0 s" or "ageofuniverse : 24.d0 s" or
c "ageofuniverse 24.d0 s"
c
c It would be read in by the main program in the following way:
c
c Aou = esdf_physical('ageofuniverse',77.d0,'ns')
c
c "Aou" is the double precision variable, 77.d0 is the default number of
c "ns" or nanoseconds. 24s will be converted automatically to its
c equivalent number of nanoseconds.
c
c Block data should be placed in the input file as follows:
c
c Begin cellvectors
c 1.0 1.0 0.0
c 0.0 1.0 1.0
c 1.0 0.0 1.0
c end cellvectors
c
c And it may be read:
c
c   If(esdf_block('cellvectors',nlines))
c     if(nlines.ne.3) then (... break out here if the incorrect number
c of lines)
c     do i=1,nlines
c       read(block_data(i),*) x,y,z
c     end do
c   endif
c
c List of functions
c -----------------
c
c Self explanatory:
c
c Esdf_string(label,default)
c esdf_integer(label,default)
c esdf_single(label,default)
c esdf_double(label,default)
c esdf_physical(label,default,unit)
c
c a little more explanation:
c
c Esdf_defined(label) is true if "label" found, false otherwise
c
c Esdf_boolean(label,default) is true if "label yes/true/t (case/punct.insens)
c                             is false if"label no/false/f (case/punct.insens)
c
c The help feature
c ----------------
c
c The routine "esdf_help(helpword,searchword)" can be used to access the
c information contained within the "esdf_key_mod" module.
c
c If "helpword" is "search" (case insensitive), then all variables whose
c description contains "searchword" will be output.
c
c If "helpword" is "basic", "inter", "expert" or "dummy" the varibles of
c that type will be displayed.
c
c If "helpword" is one of the valid labels, then a description of this
c label will be output.
c
c Finishing off
c -------------
c
c Two routines, "esdf_warnout" and "esdf_close", can be used to finish
c the use of esdf. "esdf_warnout" outputs esdf warnings to screen, and
c "esdf_close" deallocates the allocated esdf arrays.
c
c Contact the author
c ------------------
c
c This code is under development, and the author would be very happy to
c receive comments by email. any use in a commercial software package is
c forbidden without prior arrangement with the author (chris j. pickard).

      MODULE esdf

         USE esdf_key

         IMPLICIT NONE

c Kind parameters

         INTEGER, PRIVATE, PARAMETER :: i4b = KIND (1),
     .                                  dp  = KIND (1.0d0),
     .                                  sp  = KIND (1.0)

c Set the length of the lines

         INTEGER (i4b), PUBLIC, PARAMETER  :: llength = 80
         INTEGER (i4b), PRIVATE, PARAMETER :: nphys = 54

         INTEGER (i4b), PRIVATE :: nrecords, nwarns

         CHARACTER (len=llength), PRIVATE, ALLOCATABLE ::
     .                         llist(:),warns(:), tlist(:,:)

c The public block data array

         CHARACTER (len=llength), PUBLIC, ALLOCATABLE :: 
     .                         block_data(:)

c Set the physical units database

         CHARACTER (len=10), PRIVATE ::
     .        phy_d(nphys), phy_n(nphys) ! D - dimension n - name
         REAL (dp), PRIVATE :: phy_u(nphys) ! U - unit

c     we allow case variations in the units. this could be dangerous
c     (mev --> mev !!) in real life, but not in this restricted field.

c m - mass l - length t - time e - energy f - force p - pressure c - charge
c d - dipole mom - mom inert ef - efield

         DATA phy_d(1)       / 'm' /
         DATA phy_n(1)       / 'kg' /
         DATA phy_u(1)       / 1.0_dp /
         DATA phy_d(2)       / 'm' /
         DATA phy_n(2)       / 'g' /
         DATA phy_u(2)       / 1.0e-3_dp /
         DATA phy_d(3)       / 'm' /
         DATA phy_n(3)       / 'amu' /
         DATA phy_u(3)       / 1.66054e-27_dp /
         DATA phy_d(4)       / 'l' /
         DATA phy_n(4)       / 'm' /
         DATA phy_u(4)       / 1.0_dp /
         DATA phy_d(5)       / 'l' /
         DATA phy_n(5)       / 'nm' /
         DATA phy_u(5)       / 1.0e-9_dp /
         DATA phy_d(6)       / 'l' /
         DATA phy_n(6)       / 'ang' /
         DATA phy_u(6)       / 1.0e-10_dp /
         DATA phy_d(7)       / 'l' /
         DATA phy_n(7)       / 'bohr' /
         DATA phy_u(7)       / 0.529177e-10_dp /
         DATA phy_d(8)       / 't' /
         DATA phy_n(8)       / 's' /
         DATA phy_u(8)       / 1.0_dp /
         DATA phy_d(9)       / 't' /
         DATA phy_n(9)       / 'ns' /
         DATA phy_u(9)       / 1.0e-9_dp /
         DATA phy_d(10)      / 't' /
         DATA phy_n(10)      / 'ps' /
         DATA phy_u(10)      / 1.0e-12_dp /
         DATA phy_d(11)      / 't' /
         DATA phy_n(11)      / 'fs' /
         DATA phy_u(11)      / 1.0e-15_dp /
         DATA phy_d(12)      / 'e' /
         DATA phy_n(12)      / 'j' /
         DATA phy_u(12)      / 1.0_dp /
         DATA phy_d(13)      / 'e' /
         DATA phy_n(13)      / 'erg' /
         DATA phy_u(13)      / 1.0e-7_dp /
         DATA phy_d(14)      / 'e' /
         DATA phy_n(14)      / 'ev' /
         DATA phy_u(14)      / 1.60219e-19_dp /
         DATA phy_d(15)      / 'e' /
         DATA phy_n(15)      / 'mev' /
         DATA phy_u(15)      / 1.60219e-22_dp /
         DATA phy_d(16)      / 'e' /
         DATA phy_n(16)      / 'ry' /
         DATA phy_u(16)      / 2.17991e-18_dp /
         DATA phy_d(17)      / 'e' /
         DATA phy_n(17)      / 'mry' /
         DATA phy_u(17)      / 2.17991e-21_dp /
         DATA phy_d(18)      / 'e' /
         DATA phy_n(18)      / 'hartree' /
         DATA phy_u(18)      / 4.35982e-18_dp /
         DATA phy_d(19)      / 'e' /
         DATA phy_n(19)      / 'kcal/mol' /
         DATA phy_u(19)      / 6.94780e-21_dp /
         DATA phy_d(20)      / 'e' /
         DATA phy_n(20)      / 'mhartree' /
         DATA phy_u(20)      / 4.35982e-21_dp /
         DATA phy_d(21)      / 'e' /
         DATA phy_n(21)      / 'kj/mol' /
         DATA phy_u(21)      / 1.6606e-21_dp /
         DATA phy_d(22)      / 'e' /
         DATA phy_n(22)      / 'hz' /
         DATA phy_u(22)      / 6.6262e-34_dp /
         DATA phy_d(23)      / 'e' /
         DATA phy_n(23)      / 'thz' /
         DATA phy_u(23)      / 6.6262e-22_dp /
         DATA phy_d(24)      / 'e' /
         DATA phy_n(24)      / 'cm-1' /
         DATA phy_u(24)      / 1.986e-23_dp /
         DATA phy_d(25)      / 'e' /
         DATA phy_n(25)      / 'cm^-1' /
         DATA phy_u(25)      / 1.986e-23_dp /
         DATA phy_d(26)      / 'e' /
         DATA phy_n(26)      / 'cm**-1' /
         DATA phy_u(26)      / 1.986e-23_dp /
         DATA phy_d(27)      / 'f' /
         DATA phy_n(27)      / 'N' /
         DATA phy_u(27)      / 1.0_dp /
         DATA phy_d(28)      / 'f' /
         DATA phy_n(28)      / 'ev/ang' /
         DATA phy_u(28)      / 1.60219e-9_dp /
         DATA phy_d(29)      / 'f' /
         DATA phy_n(29)      / 'ry/bohr' /
         DATA phy_u(29)      / 4.11943e-8_dp /
         DATA phy_d(30)      / 'l' /
         DATA phy_n(30)      / 'cm' /
         DATA phy_u(30)      / 1.0e-2_dp /
         DATA phy_d(31)      / 'p' /
         DATA phy_n(31)      / 'pa' /
         DATA phy_u(31)      / 1.0_dp /
         DATA phy_d(32)      / 'p' /
         DATA phy_n(32)      / 'mpa' /
         DATA phy_u(32)      / 1.0e6_dp /
         DATA phy_d(33)      / 'p' /
         DATA phy_n(33)      / 'gpa' /
         DATA phy_u(33)      / 1.0e9_dp /
         DATA phy_d(34)      / 'p' /
         DATA phy_n(34)      / 'atm' /
         DATA phy_u(34)      / 1.01325e5_dp /
         DATA phy_d(35)      / 'p' /
         DATA phy_n(35)      / 'bar' /
         DATA phy_u(35)      / 1.0e5_dp /
         DATA phy_d(36)      / 'p' /
         DATA phy_n(36)      / 'mbar' /
         DATA phy_u(36)      / 1.0e11_dp /
         DATA phy_d(37)      / 'p' /
         DATA phy_n(37)      / 'ry/bohr**3' /
         DATA phy_u(37)      / 1.47108e13_dp /
         DATA phy_d(38)      / 'p' /
         DATA phy_n(38)      / 'ev/ang**3' /
         DATA phy_u(38)      / 1.60219e11_dp /
         DATA phy_d(39)      / 'c' /
         DATA phy_n(39)      / 'c' /
         DATA phy_u(39)      / 1.0_dp /
         DATA phy_d(40)      / 'c' /
         DATA phy_n(40)      / 'e' /
         DATA phy_u(40)      / 1.602177e-19_dp /
         DATA phy_d(41)      / 'd' /
         DATA phy_n(41)      / 'C*m' /
         DATA phy_u(41)      / 1.0_dp /
         DATA phy_d(42)      / 'd' /
         DATA phy_n(42)      / 'D' /
         DATA phy_u(42)      / 3.33564e-30_dp /
         DATA phy_d(43)      / 'd' /
         DATA phy_n(43)      / 'debye' /
         DATA phy_u(43)      / 3.33564e-30_dp /
         DATA phy_d(44)      / 'd' /
         DATA phy_n(44)      / 'e*bohr' /
         DATA phy_u(44)      / 8.47835e-30_dp /
         DATA phy_d(45)      / 'd' /
         DATA phy_n(45)      / 'e*ang' /
         DATA phy_u(45)      / 1.602177e-29_dp /
         DATA phy_d(46)      / 'mom' /
         DATA phy_n(46)      / 'kg*m**2' /
         DATA phy_u(46)      / 1.0_dp /
         DATA phy_d(47)      / 'mom' /
         DATA phy_n(47)      / 'ry*fs**2' /
         DATA phy_u(47)      / 2.1799e-48_dp /
         DATA phy_d(48)      / 'ef' /
         DATA phy_n(48)      / 'v/m' /
         DATA phy_u(48)      / 1.0_dp /
         DATA phy_d(49)      / 'ef' /
         DATA phy_n(49)      / 'v/nm' /
         DATA phy_u(49)      / 1.0e9_dp /
         DATA phy_d(50)      / 'ef' /
         DATA phy_n(50)      / 'v/ang' /
         DATA phy_u(50)      / 1.0e10_dp /
         DATA phy_d(51)      / 'ef' /
         DATA phy_n(51)      / 'v/bohr' /
         DATA phy_u(51)      / 1.8897268e10_dp /
         DATA phy_d(52)      / 'ef' /
         DATA phy_n(52)      / 'ry/bohr/e' /
         DATA phy_u(52)      / 2.5711273e11_dp /
         DATA phy_d(53)      / 'ef' /
         DATA phy_n(53)      / 'har/bohr/e' /
         DATA phy_u(53)      / 5.1422546e11_dp /
         DATA phy_d(54)      / 'e' /
         DATA phy_n(54)      / 'k' /
         DATA phy_u(54)      / 1.38066e-23_dp /

      CONTAINS

c   --------------  esdf_init  ---------------------- 

         SUBROUTINE esdf_init (filename)

         CHARACTER (len=*), INTENT (in) :: filename

c Local

         INTEGER (i4b), PARAMETER :: ncomm = 3, ndiv  = 3

         INTEGER (i4b)       :: unit, ierr, i, j, ic, nt, ndef, nread,
     .                          itemp, itemp2
         CHARACTER (len=llength) :: cjunk, ctemp
         CHARACTER (len=1)   :: comment(ncomm), divide(ndiv)
         LOGICAL             ::
     .        inblock ! Integer :: temp1!, index !  djr:  had to change this to get
                      ! it to compile under linux.

c Define comment characters

         DATA comment        / '#', ';', '!' /
         DATA divide         / ' ', '=', ':' /


c "Reduce" the keyword list for comparison

         DO i = 1, numkw
            ctemp       = kw_label(i)
            kw_label(i) = esdf_reduce(ctemp)
         END DO


c     added by me (?)
c     initializing the array kw_index
         kw_index = 1
         

c Open the esdf file

         CALL esdf_file (unit, filename, ierr)
         cjunk = 'Unable to open main input file "'//TRIM (filename)//
     .           '"'

         IF (ierr .EQ. 1) THEN
            WRITE (*,*) 'ESDF WARNING: '//TRIM (cjunk)//
     .             ' - using defaults'
            nread = 0
         ELSE
            nread = HUGE (1)
         END IF

c Count the number of records (excluding blank lines and commented lines)

         nrecords = 0

         DO i = 1, nread
            READ (unit, '(a)', END = 10) cjunk
            DO j = 1, ncomm
               ic = INDEX (cjunk, comment(j))
               IF (ic .GT. 0) cjunk(ic:) = ' '
            END DO
            IF (LEN_TRIM (cjunk) .GT. 0) nrecords = nrecords + 1
         END DO
 10     CONTINUE
         REWIND (unit)

c Allocate the array to hold the records and tokens

         ALLOCATE (llist(nrecords), block_data(nrecords),
     .        tlist(llength,nrecords), warns(nrecords))

c Set the number of warnings to zero

         nwarns = 0
         warns  = ' '

c Read in the records

         nrecords = 0
         DO i = 1, nread
            READ (unit, '(a)', END = 11) cjunk
            DO j = 1, ncomm
               ic = INDEX (cjunk, comment(j))
               IF (ic .GT. 0) cjunk(ic:) = ' '
            END DO
            IF (LEN_TRIM (cjunk) .GT. 0) THEN
               nrecords = nrecords + 1
               llist(nrecords) = ADJUSTL (cjunk)
            END IF
         END DO
 11     CONTINUE
         CLOSE (unit)

c Now read in the tokens from llist

         tlist = ' '

         DO i = 1, nrecords
            ctemp = llist(i)
            nt    = 0
            DO WHILE (LEN_TRIM (ctemp) .GT. 0)

c Apparently this a hack to make it compile under linux
c        temp1=index(ctemp,divide(1))
c        ic = minval(temp1)
c        ic = minval(index(ctemp,divide),mask=index(ctemp,divide)>0)

               ic = LEN_TRIM (ctemp) + 1
               DO itemp = 1, SIZE (divide)
                  itemp2 = INDEX (ctemp, divide(itemp))
                  IF (itemp2 .EQ. 0) itemp2 = LEN_TRIM (ctemp) + 1
                  IF (itemp2 .LT. ic) ic = itemp2
               END DO
               IF (ic .GT. 1) THEN
                  nt = nt + 1
                  tlist(nt,i) = ADJUSTL (ctemp(:ic-1))
               END IF
               ctemp = ADJUSTL (ctemp(ic+1:))
            END DO
         END DO

c Check if any of the "labels" in the input file are unrecognised

         inblock = .FALSE.
         DO i = 1, nrecords

c Check if we are in a block

            IF (esdf_reduce(tlist(1,i)) .EQ. 'begin') THEN
               inblock = .TRUE.

c Check if block label is recognised

               IF ((COUNT (esdf_reduce(tlist(2,i)) .EQ. kw_label) .EQ.
     .             0)) THEN
                  ctemp = 'Label "'//TRIM (esdf_reduce(tlist(2,i)))//
     .                    '" not in keyword list'
                  IF (COUNT (ctemp .EQ. warns) .EQ. 0)
     .               CALL esdf_warn (ctemp)
               END IF

c Check if "label" is multiply defined in the input file

               ndef = 0
               DO j = 1, nrecords
                  IF (esdf_reduce(tlist(2,i)) .EQ.
     .                esdf_reduce(tlist(2,j)))
     .               ndef = ndef + 1
               END DO
               ctemp = 'Label "'//TRIM (esdf_reduce(tlist(2,i)))//
     .                 '" is multiply defined in the input file. '
               IF ((ndef .GT. 2) .AND.
     .             (COUNT (ctemp .EQ. warns) .EQ. 0))
     .            CALL esdf_warn (ctemp)
            END IF

c Check it is in the list of keywords

            IF ((COUNT (esdf_reduce(tlist(1,i)) .EQ. kw_label) .EQ.
     .          0) .AND. (.NOT. inblock)) THEN
               ctemp = 'Label "'//TRIM (esdf_reduce(tlist(1,i)))//
     .                 '" not in keyword list'
               IF (COUNT (ctemp .EQ. warns) .EQ. 0)
     .            CALL esdf_warn (ctemp)
            END IF
            IF (.NOT. inblock) THEN

c Check if "label" is multiply defined in the input file

               ndef = 0
               DO j = 1, nrecords
                  IF (esdf_reduce(tlist(1,i)) .EQ.
     .                esdf_reduce(tlist(1,j)))
     .               ndef = ndef + 1
               END DO
               ctemp = 'Label "'//TRIM (esdf_reduce(tlist(1,i)))//
     .                 '" is multiply defined in the input file. '
               IF ((ndef .GT. 1) .AND.
     .             (COUNT (ctemp .EQ. warns) .EQ. 0))
     .            CALL esdf_warn (ctemp)
            END IF

c Check if we have left a block

            IF (esdf_reduce(tlist(1,i)) .EQ. 'end') inblock = .FALSE.

         END DO

         RETURN
         END SUBROUTINE esdf_init

c   --------------  esdf_string  ---------------------- 

c Return the string attached to the "label"

         FUNCTION esdf_string (label, default)

         CHARACTER (len=*), INTENT (in) :: label, default
         CHARACTER (len=llength) :: esdf_string
c Local
         INTEGER (i4b)      :: i
         INTEGER (i4b)      :: kw_number

c Check "label" is defined

         ! CALL esdf_lblchk (label, 'T')
         CALL esdf_lablchk (label, 'T',kw_number)

c Set to default

         esdf_string = default

         DO i = kw_index(kw_number), nrecords
c Search in the first token for "label"
c the first instance is returned
            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               esdf_string = llist(i)(INDEX(llist(i),TRIM(tlist(2,i))):)
               kw_index(kw_number) = i+1
               EXIT
            END IF

         END DO

         RETURN
         END FUNCTION esdf_string

c   --------------  esdf_integer  ---------------------- 

c Return the integer attached to the "label"

         FUNCTION esdf_integer (label, default)

         INTEGER (i4b), INTENT (in)     :: default
         CHARACTER (len=*), INTENT (in) :: label
         INTEGER (i4b)      :: esdf_integer
c Local
         INTEGER (i4b)       :: i
         CHARACTER (len=llength) :: ctemp
         INTEGER (i4b)      :: kw_number

c Check "label" is defined
         CALL esdf_lablchk (label, 'I',kw_number)
        
c Set to default
         esdf_integer = default

         DO i = kw_index(kw_number) , nrecords
c Search in the first token for "label"
c the first instance is returned
            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               READ (tlist(2,i), *, ERR = 10) esdf_integer
               kw_index(kw_number) = i+1
               EXIT
            END IF
         END DO
         RETURN

 10     CONTINUE
         ctemp = 'Unable to parse "'//TRIM (esdf_reduce(label))//
     .           '" in esdf_integer'
         CALL esdf_die (ctemp)

         RETURN
         END FUNCTION esdf_integer

c   --------------  esdf_single  ---------------------- 

c Return the single precisioned value attached to the "label"

         FUNCTION esdf_single (label, default)

         REAL (sp), INTENT (in) :: default
         CHARACTER (len=*), INTENT (in) :: label
         REAL (sp)          :: esdf_single
c Local
         INTEGER (i4b)       :: i
         CHARACTER (len=llength) :: ctemp
         INTEGER (i4b)      :: kw_number

c Check "label" is defined

         ! CALL esdf_lblchk (label, 'S')
         CALL esdf_lablchk (label, 'S',kw_number)

c Set to default

         esdf_single = default

         DO i = kw_index(kw_number), nrecords
c Search in the first token for "label"
c the first instance is returned
            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               READ (tlist(2,i), *, ERR = 10) esdf_single
               kw_index(kw_number) = i+1
               EXIT
            END IF
         END DO
         RETURN

 10     CONTINUE
         ctemp = 'Unable to parse "'//TRIM (esdf_reduce(label))//
     .           '" in esdf_single'
         CALL esdf_die (ctemp)

         RETURN
         END FUNCTION esdf_single

c   --------------  esdf_double  ---------------------- 

c Return the double precisioned value attached to the "label"

         FUNCTION esdf_double (label, default)

         REAL (dp), INTENT (in) :: default
         CHARACTER (len=*), INTENT (in) :: label
         REAL (dp)          :: esdf_double
c Local
         INTEGER (i4b)       :: i
         CHARACTER (len=llength) :: ctemp
         INTEGER (i4b)      :: kw_number

c Check "label" is defined

         ! CALL esdf_lblchk (label, 'D')
         CALL esdf_lablchk (label, 'D',kw_number)

c Set to default

         esdf_double = default

         DO i = kw_index(kw_number), nrecords
c Search in the first token for "label"
c the first instance is returned
            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               READ (tlist(2,i), *, ERR = 10) esdf_double
               kw_index(kw_number) = i+1
               EXIT
              END IF
         END DO

         RETURN

 10     CONTINUE
         esdf_double = default
         ctemp       = 'Unable to parse "'//TRIM (esdf_reduce(label))//
     .                 '" in esdf_double'
         CALL esdf_die (ctemp)

         RETURN
         END FUNCTION esdf_double

c   --------------  esdf_physical  ---------------------- 

c Return the double precisioned physical value attached to the "label"
c units converted to "dunit"

         FUNCTION esdf_physical (label, default, dunit)

         REAL (dp), INTENT (in) :: default
         CHARACTER (len=*), INTENT (in) :: label, dunit
         REAL (dp)          :: esdf_physical
c Local
         INTEGER (i4b)       :: i
         CHARACTER (len=llength) :: ctemp, iunit
         INTEGER (i4b)      :: kw_number

c Check "label" is defined

         ! CALL esdf_lblchk (label, 'P')
         CALL esdf_lablchk (label, 'P',kw_number)

c Set to default

         esdf_physical = default

         DO i = kw_index(kw_number), nrecords

c Search in the first token for "label"
c the first instance is returned

            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               READ (tlist(2,i), *, ERR = 10) esdf_physical
               iunit = dunit//REPEAT (' ', llength - LEN (dunit))
               READ (tlist(3,i), *, ERR = 10, END = 13) iunit
               esdf_physical = esdf_convfac(iunit,dunit) * esdf_physical
               kw_index(kw_number) = i+1
               EXIT
            END IF
         END DO
         RETURN
 13     CONTINUE
         iunit = dunit//REPEAT (' ', llength - LEN (dunit))
         esdf_physical = esdf_convfac(iunit,dunit) * esdf_physical
         kw_index(kw_number) = i+1

         RETURN

 10     CONTINUE
         esdf_physical = default
         ctemp = 'Unable to parse "'//TRIM (esdf_reduce(label))//
     .           '" in esdf_physical'
         CALL esdf_die (ctemp)

         RETURN
         END FUNCTION esdf_physical

c   --------------  esdf_defined  ---------------------- 

c Is the "label" defined in the input file

         FUNCTION esdf_defined (label)

         CHARACTER (len=*), INTENT (in) :: label
         LOGICAL            :: esdf_defined
c Local
         INTEGER (i4b)      :: i
         INTEGER (i4b)      :: kw_number


c Check "label" is defined
         ! CALL esdf_lblchk (label, 'E')
         CALL esdf_lablchk (label, 'E',kw_number)
         

c Set to default
         esdf_defined = .FALSE.

         DO i = kw_index(kw_number), nrecords
c Search in the first token for "label"
c the first instance is returned
            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               esdf_defined = .TRUE.
               kw_index(kw_number) = i+1
               EXIT
            END IF

         END DO

         RETURN
         END FUNCTION esdf_defined

c   --------------  esdf_boolean  ---------------------- 

c Is the "label" defined in the input file

         FUNCTION esdf_boolean (label, default)

         CHARACTER (len=*), INTENT (in) :: label
         LOGICAL, INTENT (in) :: default
         LOGICAL            :: esdf_boolean
c Local
         INTEGER (i4b)       :: i
         CHARACTER (len=llength) :: positive(3), negative(3)
         DATA positive       / 'yes', 'true', 't' /
         DATA negative       / 'no', 'false', 'f' /
         INTEGER (i4b)      :: kw_number


c Check "label" is defined

         ! CALL esdf_lblchk (label, 'L')
         CALL esdf_lablchk (label, 'L',kw_number)

c Set to default
         esdf_boolean = default

         DO i = kw_index(kw_number), nrecords
c Search in the first token for "label"
c the first instance is returned
            IF (esdf_reduce(tlist(1,i)) .EQ. esdf_reduce(label)) THEN
               kw_index(kw_number) = i+1
               IF (LEN_TRIM (tlist(2,i)) .EQ. 0) THEN
                  esdf_boolean = .TRUE.
                  EXIT
               END IF
               IF (ANY (INDEX (positive, esdf_reduce(tlist(2,i))) .GT.
     .             0)) THEN
                  esdf_boolean = .TRUE.
                  EXIT
               END IF
               IF (ANY (INDEX (negative, esdf_reduce(tlist(2,i))) .GT.
     .             0)) THEN
                  esdf_boolean = .FALSE.
                  EXIT
               END IF
               CALL esdf_die ('Unable to parse boolean value')

            END IF

         END DO

         RETURN
         END FUNCTION esdf_boolean

c   --------------  esdf_block  ---------------------- 
         FUNCTION esdf_block (label, nlines)

         CHARACTER (len=*), INTENT (in) :: label
         INTEGER (i4b), INTENT (out)    :: nlines
         LOGICAL            :: esdf_block
c Local
         INTEGER (i4b)       :: i
         CHARACTER (len=llength) :: ctemp
         INTEGER (i4b)      :: kw_number

c Check "label" is defined
         ! CALL esdf_lblchk (label, 'B')
         CALL esdf_lablchk (label, 'B',kw_number)

         ctemp = 'Block "'//TRIM (esdf_reduce(label))//
     .           '" not closed correctly '

         esdf_block = .FALSE.

         nlines = 0

         DO i = kw_index(kw_number), nrecords
            IF ((esdf_reduce(tlist(1,i)) .EQ.
     .          esdf_reduce('begin')) .AND.
     .          (esdf_reduce(tlist(2,i)) .EQ. esdf_reduce(label))) THEN
               esdf_block = .TRUE.
               kw_index(kw_number) = i+1
               DO WHILE (esdf_reduce(tlist(1,i+nlines+1)) .NE.
     .                   esdf_reduce('end'))
                  nlines = nlines + 1
                  IF (nlines + i .GT. nrecords) CALL esdf_die (ctemp)
                  block_data(nlines) = llist(i+nlines)
               END DO
               IF (esdf_reduce(tlist(2,i+nlines+1)) .NE.
     .             esdf_reduce(label))
     .            CALL esdf_die (ctemp)
               EXIT
            END IF
         END DO

         RETURN
         END FUNCTION esdf_block

c   --------------  esdf_reduce  ---------------------- 

c Reduce the string to lower case and remove punctuation

         FUNCTION esdf_reduce (string)

         CHARACTER (len=*), INTENT (in) :: string
         CHARACTER (len=llength) :: esdf_reduce
c Local
         INTEGER (i4b), PARAMETER :: npunct = 2
         INTEGER (i4b)       :: ia, iz, ishift, ic, i, ln
         CHARACTER (len=llength) :: ctemp
         CHARACTER (len=1)   :: punct(npunct)
         LOGICAL             :: keep_going

c Define the punctuation to be removed
         DATA punct          / '.', '-' /


c Initialise system dependant bounds in collating sequence

         ia     = ICHAR ('A')
         iz     = ICHAR ('Z')
         ishift = ICHAR ('a') - ia

c Initialise output

         ln = LEN (string)

         IF (ln .LT. llength) THEN
            esdf_reduce(1:ln)  = string(1:ln)
            esdf_reduce(ln+1:) = ' '
         ELSE
            esdf_reduce(1:llength) = string(1:llength)
         END IF

c Drop all upper case characters to lower case

         DO i = 1, llength
            ic = ICHAR (esdf_reduce(i:i))
            IF ((ic .GE. ia) .AND. (ic .LE. iz))
     .         esdf_reduce(i:i) = CHAR (ishift + ic)
         END DO

c Now remove punctuation

         DO i = 1, npunct
            keep_going = .TRUE.
            DO WHILE (keep_going)
               ic = INDEX (esdf_reduce, punct(i))
               IF (ic .GT. 0) THEN
                  ctemp = esdf_reduce
                  esdf_reduce(ic:) = ctemp(ic+1:)
               ELSE
                  keep_going = .FALSE.
               END IF
            END DO
         END DO

         esdf_reduce = TRIM (ADJUSTL (esdf_reduce))

         RETURN
         END FUNCTION esdf_reduce

c   --------------  esdf_convfac  ---------------------- 

c Find the conversion factor between physical units

         FUNCTION esdf_convfac (from, to)

         CHARACTER (len=*), INTENT (in) :: from, to
         REAL (dp)          :: esdf_convfac
c Local
         INTEGER (i4b)       :: i, ifrom, ito
         CHARACTER (len=llength) :: ctemp

c Find the index numbers of the from and to units

         ifrom = 0
         ito   = 0
         DO i = 1, nphys
            IF (esdf_reduce(from) .EQ. esdf_reduce(phy_n(i))) ifrom = i
            IF (esdf_reduce(to) .EQ. esdf_reduce(phy_n(i))) ito = i
         END DO

c Check that the units were recognised

         IF (ifrom .EQ. 0) THEN
            ctemp = 'Units not recognised in input file : '//
     .              TRIM (esdf_reduce(from))
            CALL esdf_die (ctemp)
         END IF

         IF (ito .EQ. 0) THEN
            ctemp = 'Units not recognised in Program : '//
     .              TRIM (esdf_reduce(to))
            CALL esdf_die (ctemp)
         END IF

c Check that from and to are of the same dimensions

         IF (phy_d(ifrom) .NE. phy_d(ito)) THEN
            ctemp = 'Dimensions Do not match : '//
     .              TRIM (esdf_reduce(from))//' vs '//
     .              TRIM (esdf_reduce(to))
            CALL esdf_die (ctemp)
         END IF

c Set the conversion factor

         esdf_convfac = phy_u(ifrom) / phy_u(ito)

         RETURN
         END FUNCTION esdf_convfac

c   --------------  esdf_unit  ---------------------- 

c Find an unused i/o unit

         FUNCTION esdf_unit (ierr)

         INTEGER (i4b), INTENT (out) :: ierr
         INTEGER (i4b)      :: esdf_unit
c Local
         LOGICAL            :: op

         ierr = 0
         DO esdf_unit = 10, 99
            INQUIRE (UNIT = esdf_unit, opened = op, ERR = 10)
            IF (.NOT. op) RETURN
         END DO
         CALL esdf_warn ('Unable to find a free i/o unit using esdf_u'//
     .                   'nit')
         ierr = 1

         RETURN

 10     CONTINUE
         CALL esdf_die ('Error opening files by esdf_unit')

         RETURN
         END FUNCTION esdf_unit

c   --------------  esdf_file  ---------------------- 

c Open an old file

         SUBROUTINE esdf_file (unit, filename, ierr)

         CHARACTER (len=*), INTENT (in) :: filename
         INTEGER (i4b), INTENT (out)    :: unit, ierr
         LOGICAL            :: ex

         unit = esdf_unit(ierr)
         IF (ierr .GT. 0) RETURN
         INQUIRE (FILE = TRIM (filename), EXIST = ex, ERR = 10)
         IF (.NOT. ex) GOTO 10
         OPEN (UNIT = unit, FILE = TRIM (filename), FORM = 'formatted',
     .         STATUS = 'old', ERR = 10)

         RETURN

 10     CONTINUE
         ierr = 1

         RETURN
         END SUBROUTINE esdf_file


c   c   --------------  esdf_lblchk  ---------------------- 
c   
c   ! Check that the label is known, and used correctly
c   
c         SUBROUTINE esdf_lblchk (string, typ)
c         
c         CHARACTER (len=*), INTENT (in) :: string
c         CHARACTER (len=1), INTENT (in) :: typ
c         ! Local
c         CHARACTER (len=llength) :: ctemp
c         CHARACTER (len=1)   :: tp
c         INTEGER (i4b)       :: i
c               
c                                   ! Check if label is recognised
c         i     = COUNT (esdf_reduce(string) .EQ. kw_label)
c         ctemp = 'Label "'//TRIM (esdf_reduce(string))//
c        .     '" not recognised inkeyword list'
c         IF (i .EQ. 0) CALL esdf_die (ctemp)
c         ctemp = 'Label "'//TRIM (esdf_reduce(string))//
c        .     '" is multiply defined'
c         IF (i .GT. 1) CALL esdf_die (ctemp)
c         ctemp = 'Label "'//TRIM (esdf_reduce(string))//
c        .     '" has been used with the wrong type'
c         tp    = ' '
c         i     = 0
c         DO WHILE (tp .EQ. ' ')
c            i = i + 1
c            IF (esdf_reduce(string) .EQ. kw_label(i)) tp = kw_typ(i)
c         END DO
c         IF (typ .NE. tp) CALL esdf_die (ctemp)
c         
c         RETURN
c         END SUBROUTINE esdf_lblchk
c         
      
c   --------------  esdf_lablchk  ---------------------- 
c     addaed by me:
c     =============
      SUBROUTINE esdf_lablchk (string, typ,index)
      
      CHARACTER (len=*), INTENT (in) :: string
      CHARACTER (len=1), INTENT (in) :: typ
      ! Local
      CHARACTER (len=llength) :: ctemp
      CHARACTER (len=1)   :: tp
      INTEGER (i4b)       :: i,index
            
c Check if label is recognised
      
      i     = COUNT (esdf_reduce(string) .EQ. kw_label)
      ctemp = 'Label "'//TRIM (esdf_reduce(string))//
     .     '" not recognised inkeyword list'
      IF (i .EQ. 0) CALL esdf_die (ctemp)
      ctemp = 'Label "'//TRIM (esdf_reduce(string))//
     .     '" is multiply defined'
      IF (i .GT. 1) CALL esdf_die (ctemp)
      ctemp = 'Label "'//TRIM (esdf_reduce(string))//
     .     '" has been used with the wrong type'
      tp    = ' '
      i     = 0
      DO WHILE (tp .EQ. ' ')
         i = i + 1
         IF (esdf_reduce(string) .EQ. kw_label(i)) tp = kw_typ(i)
      END DO
      index = i
      IF (typ .NE. tp) CALL esdf_die (ctemp)
      
      RETURN
      END SUBROUTINE esdf_lablchk
      
c   --------------  esdf_die  ---------------------- 

c Stop execution due to an error cause by esdf

         SUBROUTINE esdf_die (string)

         CHARACTER (len=*), INTENT (in) :: string

         WRITE (*, '(a,a)') ' ESDF ERROR: ', TRIM (string)
         WRITE (*, '(a)') ' Stopping now'

         STOP

         RETURN
         END SUBROUTINE esdf_die

c   --------------  esdf_warn  ---------------------- 

c Warning due to an error cause by esdf

         SUBROUTINE esdf_warn (string)

         CHARACTER (len=*), INTENT (in) :: string

         nwarns = nwarns + 1
         warns(nwarns) = string

         RETURN
         END SUBROUTINE esdf_warn

c   --------------  esdf_close  ---------------------- 

c Deallocate the data arrays --- call this before re-initialising

         subroutine esdf_close
         deallocate(llist,tlist,block_data)
         if (allocated(warns)) deallocate(warns)
         end subroutine esdf_close

      END MODULE esdf

c     ===============================================================
