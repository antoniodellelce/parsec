c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine sets up the superposition of the LOCAL ionic
c     potentials on the grid. This part of the potential changes only
c     when the atoms move. It is fixed throughout the iterations to
c     self-consistency for a given atomic configuration.
c
c     WARNING: this subroutine is not fully parallelized!
c
c     ---------------------------------------------------------------
      subroutine ionpot(clust,grid,p_pot,parallel,vion_d,OldInpFormat)

      use constants
      use cluster_module
      use grid_module
      use pseudo_potential_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(in) :: clust
c     grid related data
      type (grid_data), intent(in) :: grid
c     pseudo_potential related data
      type (pseudo_potential), intent(in) :: p_pot
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel
c     local part of ionic potential
      real(dp), intent(out) :: vion_d(parallel%mydim)
c     type of interpolation flag
      logical, intent(in) :: OldInpFormat
c
c     Work variables:
c
c     temporary storage variables
      real(dp) :: dist, rindex, delr, vtemp, dxx, dyy, dzz
      integer jok
c     counters
      integer iatom, itype, i, iat, igrid
c     allocation check
      integer alcstat
c     work array for the master processor
      real(dp), allocatable :: vion(:)

c     ---------------------------------------------------------------

      if (parallel%iammaster) then

c     initialize potential
         allocate(vion(grid%nwedge),stat=alcstat)
         call alccheck('vion',grid%nwedge,alcstat)
         vion = zero

c     initialize iatom - counter of all atoms in the system
         iatom = 0
c     go over all atom types

         do itype = 1, clust%type_num
c     go over all atoms, superimpose the contribution to the local
c     ionic potential from each atom over each point of the grid
            do iat = 1, clust%natmi(itype)
               iatom = iatom + 1
               do igrid = 1,grid%nwedge
                  dxx = (grid%shift(1) + grid%kx(igrid))*grid%step(1)
     1                 - clust%xatm(iatom)
                  dyy = (grid%shift(2) + grid%ky(igrid))*grid%step(2)
     1                 - clust%yatm(iatom)
                  dzz = (grid%shift(3) + grid%kz(igrid))*grid%step(3)
     1                 - clust%zatm(iatom)
                  dist = sqrt(dxx*dxx+dyy*dyy+dzz*dzz)
c
c     If dist is larger than the largest value of r given in the
c     pseudopotential file, rs(i,itype), use -zion/r as the ionic
c     potential, vtemp. The factor of two in zion is conversion from
c     Hartree to Rydberg.
                  if (dist .ge. p_pot%rs(p_pot%ns(itype)-1,itype)) then
                     vtemp = -two*p_pot%zion(itype)/dist
                  else
c
c     if dist is smaller than the largest value of r, find the index
c     of dist in the pseudopotential by inverting the logarithmic
c     pseudopotential grid, then interpolate the LOCAL
c     pseudopotential to determine the potential and charge 
c     density
c
c     rindex = 1/B(itype)*log(1+1/A(itype)*dist)
c     A = p_pot%par_a
c     B = p_pot%par_b                
                     rindex = one/p_pot%par_b(itype)*log(one+one/
     1                    p_pot%par_a(itype)*dist)
                     jok  = idint(rindex) + 1
                     delr = (dist-p_pot%rs(jok,itype))/
     1                    (p_pot%rs(jok+1,itype)-p_pot%rs(jok,itype))
c     place pseudopotential on the 3d uniform grid by interpolating
c     the 1d pseudopotential given on the logarithmic grid
c     If "old style", interpolate the pseudopotential directly
                     if (OldInpFormat) then
                        vtemp = p_pot%vion(jok,itype) + delr*
     1                       (p_pot%vion(jok+1,itype) -
     2                       p_pot%vion(jok,itype))
c     If "new style", do the interpolation on r*pseudopotential -
c     improves the interpolation accuracy because it is exact beyond
c     pseudopotential cutoff radius.
                     else
                        vtemp = p_pot%vion(jok,itype)*p_pot%rs(jok
     1                       ,itype)/dist + delr*(p_pot%vion(jok+1
     2                       ,itype)*p_pot%rs(jok+1,itype)
     3                       -p_pot%vion(jok,itype)*p_pot%rs(jok
     4                       ,itype))/dist
                        if (jok .eq. 1) vtemp = p_pot%vion(2,itype)
                     endif
                  endif
                  vion(igrid) = vion(igrid) + vtemp  
               enddo            ! igrid = 1,grid%nwedge
            enddo               ! iat = 1, clust%natmi(itype)
         enddo                  ! itype = 1, clust%type_num
         parallel%ftmp = vion
         deallocate(vion)
      endif                     ! parallel%iammaster

c     master PE distributes the ion potential
      call export_function(parallel,vion_d)

      end subroutine ionpot
c     ===============================================================
