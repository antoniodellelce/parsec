c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Adapted by J.L. Martins from the program GROUP
c     written in 1974 by Warren and Worlton,
c     Computer Physics Communications, vol 8, 71-74 (1974)
c     incorporated by Eckold et al into UNISOFT
c     Modified by Alberto Garcia (1990)
c     Cleaned up by Bernd Pfrommer (1996)
c     Abelian group routines added by M. Tiago (2005)
c
c     Let's see if we can get the rotations both in real and
c     in reciprocal space:
c
c     gmtrx : g-space representation
c     rmtrx: r-space representation
c
c     Input:
c
c     a(i,j) is the i-th cartesian component of the j-th primitive
c     translation vector of the direct lattice and thus it is the
c     transpose of the matrix A defined by Jones in 'The Theory
c     of Brillouin Zones and Electronic States in Crystals'
c
c     coorat(i,j,k) is the k-th component (lattice coordinates) of
c     the position of the j-th atom of type i.
c
c     natom(i) is the number of atoms of type i.
c
c     ntype is the total number of types of atoms.
c
c     nops indicates whether or not to count nonzero fractional
c     translations
c
c     internal:
c
c     b contains the reciprocal lattice vectors in the
c     crystallographic usage, that is, WITHOUT the 2pi factor.
c     This matrix IS Jones'.
c
c     na is the number of atoms.
c
c     ity(i) is an integer distinguishing atoms of different type
c     i.e. different atomic species.
c
c     x(j,i) is the j-th cartesian component of the position vector for
c     the i-th atom in the unit cell.
c
c     output:
c
c     ntrans is the number of point group operations.
c
c     gmtrx is the matrix of rotations in g-lattice coordinates.
c     rmtrx is the matrix of rotations in r-lattice coordinates.
c
c     tnp(oper,ilat) is the fractional translation in latt. coor.
c
c     invers_no is the operation number of the inversion (0 if not
c     present). It is used to restore the inversion symmetry by
c     a simple change of origin when possible
c
c     ---------------------------------------------------------------
      subroutine symgen(ipr, a, coorat, natom, ntype, invers_no,nops,
     1     ntrans, gmtrx, tnp, mxdatm, rmtrx, trans, nab, indx, chi)

      use constants
      implicit none  
c
c     Input/Output variables:
c
      integer, intent(in) :: ipr, ! ipr>=2 print
     1     mxdatm                 ! array dimensioning
c
      integer, intent(out) :: ntrans
      real(dp), intent(out) :: tnp(48, 3), trans(3, 3, 48)
c     rotation matrices in cartesian coordinates
      integer, intent(out) ::   rmtrx(48, 3, 3), gmtrx(48, 3, 3)
c     number of symmetry operations in Abelian subgroup
      integer, intent(out) :: nab
c     index of symmetry operations in Abelian subgroup
      integer, intent(out) :: indx(8)
c     character table of Abelian subgroup
      integer, intent(out) :: chi(8,8)
c
      integer, intent(in) :: ntype, nops  
      integer, intent(out) :: invers_no
      real(dp), intent(in) :: a(3, 3), coorat(ntype, mxdatm, 3)
      integer, intent(in) :: natom(ntype)
c
c     Work variables:
c
      real(dp) :: xdum,ydum
      integer :: i, ihg, ind, ipm, j, k, l, li, m, na, nat, ierr,
     1     include_fractional

      real(dp) :: b(3, 3), r(49, 3, 3), r1(3, 3),
     1     rlat(48, 3, 3)
      real(dp), allocatable :: x(:,:)
      integer, allocatable :: ity(:)
      integer :: ib(48), mult(48,48)
      character(len=3) :: id(48), label(8)
      character(len=5) :: name
c
c     External subroutines:
c
      external atftmt, pgl, symm_ident, symchk

c     ---------------------------------------------------------------

      ipm = 1
      include_fractional = nops

      allocate(x(3, ntype * mxdatm))
      allocate(ity(ntype * mxdatm))
c
c     Calculate cartesian coordinates, atom types, and number of
c     atoms.
c
      na = 0
      do i = 1, ntype
         nat = natom(i)
         do j = 1, nat
            ind = na + j
            do k = 1, 3
               x(k, ind) = zero
               do l = 1, 3
                  x(k, ind) = x(k, ind) + a(k, l) * coorat(i, j, l)
               end do
            end do
            ity(ind) = i
         end do
         na = na + natom(i)
      end do
c
c     Determine reciprocal lattice basis vectors.
c     We know that
c
c               T                             T -1         -1
c              B A  = 2pi (1), so   B  = 2pi(A )   = 2pi(a )
c
c     and we can use the linpack (SCILIB version) routines
c     sgefa and sgedi to
c     compute the determinant (celvol) and the inverse.
c     But note that the 2pi factor is NOT needed here.
c
      do i = 1, 3
         do j = 1, 3
            b(i, j) = a(i, j)
         end do
      end do

      call mtrxin(b,xdum,ydum)

      call pgl(a, b, r, ntrans, ib, ihg)

      call atftmt(ipr, a, b, x, r, tnp, trans, ity, na, ib, ihg,
     1     include_fractional, li, ntrans, invers_no, ntype, mxdatm)
c
c     if(li.gt.0) write(7,180)
c 180 format (5x,'the point group of the crystal contains the',
c    1' inversion, therefore,',/,5x,'time reversal invariance will',
c    2' be invoked for all wave vectors.')
c
c     We have the rotations in cartesian coordinates.
c     Transform into lattice coordinates (r-space and g-space)
c
      do l = 1, ntrans
c
c     In terms of the real-space basis vectors:
c                      T
c              y' = ( B R a ) y     ( y are the r-lattice coord.)
c
c                              T
c              watch out: b = B
c
c     Trans * a ...
c
         do j = 1, 3
            do k = 1, 3
               r1(j, k) = zero
               do m = 1, 3
                  r1(j, k) = r1(j, k) + trans(m, j, l) * a(m, k)
               end do
            end do
         end do
c
c     B * Trans * a
c
         do j = 1, 3
            do k = 1, 3
               rlat(l, j, k) = zero
               do m = 1, 3
                  rlat(l, j, k) = rlat(l, j, k) + b(j, m) * r1(m, k)
               end do
               rmtrx(l, j, k) = nint(rlat(l, j, k))
            end do
         end do
      end do

      call symm_ident(ntrans, rmtrx, tnp, id)

      do l = 1, ntrans
c
c     In terms of the g-space basis vectors:
c                      T  T
c              z' = ( a  R  b ) z    ( z are the g-lattice coord.)
c
         do k = 1, 3
            gmtrx(l, :, k) = rmtrx(l, k, :)
         end do
      end do
c
c     write the matrices and fractional translations
c
      write(7, 900)
 900  format(//4x,'Rotation matrices (r-lattice) and fractional',
     1     ' translations (r-lattice)',/)

      do i = 1, ntrans
         write(7, 901) i, ((rmtrx(i, j, k), k = 1, 3), j = 1, 3),
     1        (tnp(i, k), k = 1, 3), id(i)

      end do
 901  format(i5,3(3x,3i3),4x,3f9.5,1x,a5)

      call symchk(ipr,ierr,ntrans,rmtrx,tnp,1,mult)
c      call symchk(ipr,ierr,ntrans,gmtrx,tnp,0,mult)

      call abelian(ntrans, mult, rmtrx, tnp, id, nab, indx, chi,
     1      name, label)
c
c     write symmetry operations for the Abelian subgroup
c
      write(7,*)
      write(7,*) ' Found Abelian subgroup ',name,' with ',nab
     1      ,' symmetry operations'
      write(7,*)
      write(7,*) ' Character table :'
      write(7,'(100a)') ('-',i=1,20 + 5*nab)
      write(7,'(a,8(a5))') ' Representation  | ',
     1      (trim(id(indx(k))),k=1,nab)
      do i = 1, nab
         write(7,'(5x,a3,1x,i5,2x,a,8(2x,i3))') label(i),i,' | ',
     1        (chi(i,k), k = 1, nab)
      enddo
      write(7,'(100a)') ('-',i=1,20 + 5*nab)
      write(7,'(a,8(2x,i3))') ' Operation number: ',
     1      (indx(k), k = 1, nab)

      write(7,*)
c
c     If identity is not a symmetry operation, then the symmetry
c     group is wrong
c
      k = 0
      do i = 1, ntrans
         if (trim(id(i)) .eq. 'E') k = 1
      enddo
      if (k .eq. 0) then
         write(7,*)
         write(7,*) ' WARNING! SYMMETRY GROUP DOES NOT HAVE IDENTITY'
         write(7,*) ' GROUP SYMMETRIES ARE INCORRECT, POSSIBLY BECAUSE'
         write(7,*) ' OF OVERLAPPING ATOMS. STOP...'
         write(7,*)
         ntrans = 0
      endif

      deallocate(x)
      deallocate(ity)

      end subroutine symgen
c     ===============================================================
c
c     Determines the point group of the crystal,
c     the atom transformation table,f0, the fractional translations,
c     tnp, associated with each rotation and finally the
c     multiplication table mt, for the point group of the crystal. ib
c     now contains operations in the p.g. of the crystal and ntrans
c     is the order of this group.
c
c     1997 Bernd Pfrommer, based on a routine from Alberto Garcia's
c     code. I cleaned up some more, put in dynamic memory allocation,
c     explicit typing, fortran 90 style, and more comments. Also,
c     some shortcuts were put in to speed up the case where the
c     symmetry is low. The rdiff array precomputes the rotated
c     vectors to speed up things.
c
c     ---------------------------------------------------------------
      subroutine atftmt(ipr, a, ai, x, r, tnp, trans, ity, na, ib,
     1     ihg, include_fractional, li, nc, invers_no, mxdtyp, mxdatm)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) ::
     1     ipr,                 ! print flag
     2     ihg,                 ! holohedral group number
     3     na,                  ! total number of atoms (of all kinds)
     4     mxdtyp,              ! max number of atom types
     5     mxdatm,              ! max number of atoms of each type
     6     include_fractional   ! -2 means don't include frac translations

      real(dp), intent(in) ::
     1     x(3, mxdtyp * mxdatm), ! compact list of all coordinates (cartesian)
     2     a(3, 3),             ! realspace lattice vectors
     3     ai(3, 3)             ! inverse of lattice vectors (no 2pi)

      integer, intent(inout) ::
     1     ity(mxdtyp * mxdatm), ! compact list of all the types of all atoms
     2     ib(48),              ! index map for symmetry operations
     3     nc                   ! number of symm-ops without/with basis
      real(dp), intent(inout) ::
     1     r(49, 3, 3)          ! the rotation matrices as they come
                                ! out of the pgl subroutine

      integer, intent(out) ::
     1     li,                  ! something to do with inversion?
     2     invers_no            ! which operation is the inversion
      real(dp), intent(out) ::
     1     trans(3, 3, 48),     ! the cartesian realspace transf. matrices
     2     tnp(48, 3)           ! the nonprimitive translations
c
c     Work variables:
c
      real(dp) :: v(3, 48), vr(3), vt(3), xb(3)
      integer :: ia(48), ic(48), mt(48, 48),
     1     ipm                  ! print flag for multiplication table

      real(dp) :: da, dif, ts, vs, difmax
      real(dp), parameter :: eps = 1.0d-8
      integer :: i, il, is, isy, iu, j, k, k1, k2, k3, k4, ks, l,
     1     m, n,n1, n2, n3, nca, ni
      real(dp), allocatable :: rx(:,:), rdiff(:,:,:)
      integer, allocatable :: if0(:,:)
      character(len=12), parameter :: cst(7) =(/ 'triclinic   ',
     1     'monoclinic  ', 'orthorhombic','tetragonal  ',
     2     'cubic       ', 'trigonal    ', 'hexagonal   ' /)

c     ---------------------------------------------------------------

      invers_no = 0
      ipm = 1

      allocate(rx(3, mxdtyp * mxdatm))
      allocate(if0(48, mxdtyp * mxdatm))
      allocate(rdiff(3, na, na))
c
c     eps should be slightly larger than computer precision
c
      nca = 0
      ni = 13
      if (ihg < 6) ni = 25
      li = 0
      do n = 1, nc              ! loop over all lattice symmetry operations
         l = ib(n)              ! get index of symmetry operation
         ic(n) = ib(n)
c
c     operate on all atoms with symmetry operation l, and store in
c     list rx
c
         do k = 1, na
            do i = 1, 3
               rx(i, k) = zero
               do j = 1, 3
                  rx(i, k) = rx(i, k) + r(l, i, j) * x(j, k)
               end do
            end do
         end do
c
c     This piece of code is pretty naive, and scales like order(n**3).
c     It basically checks if for each
c
c     R*x_1 - x_2   there is a matching R*x_3-x4
c
c     excluding the trivial case of x_1 .eq. x_3  and  x_2 .eq. x_4
c
c     precompute the rdiffs first
         do k1 = 1, na
            do k2 = 1, na
               xb(:) = rx(:, k1) - x(:, k2)
c     rdiff = R*x_1 -x_2
               call rlv(ai, xb, rdiff(1, k2, k1), il)
c     subroutine rlv removes a direct lattice vector from xb
c     leaving the remainder in rdiff. if a nonzero lattice vector was
c     removed, il is made nonzero.
            end do
         end do

         difmax = zero
         do k1 = 1, na          ! double loop over compact atom list
            do k2 = 1, na
               if (ity(k1) == ity(k2)) then ! same type atoms?
                  vr = rdiff(:, k2, k1) !vr stands for v-reference.
                  ks = 0
                  do k3 = 1, na
                     do k4 = 1, na
                        if (ity(k3) == ity(k4)) then
                           vt = rdiff(:, k4, k3) ! vt stands for v-test
                           dif = zero
                           do i = 1, 3
                              da = abs(vr(i) - vt(i)) + eps
                              dif = dif + mod(da, one)
                           end do
                           if (dif <= 10.0d0 * eps) then
                              if0(l, k3) = k4
c     if0 is the function defined in maradudin and vosko by eq.(2.35).
c     it defines the atom transformation table
                              ks = ks + k4
c     if (ks == na * (na + 1) / 2) goto 110       ! found
                              if (ks .eq. na*(na+1)/2) then
c     Reject all symops with non-zero fractional translations.  It
c     should be helpful when you have weird fractional translations
c     that confuse adjustfft.fp.  -- David Roundy
                                 if (include_fractional.eq.-1.or.
     1                                vr(1)*vr(1)+vr(2)*vr(2)+ 
     2                                vr(3)*vr(3).eq. 0.0d0)
     3                                go to 110 ! found all
                              endif
                              goto 80
                           end if
                        end if
                     end do
                     exit
 80                  continue
                  end do
c
c     BP put in this shortcut (check carefully). If there
c     is a single R*x_1- x_2 without match, then give up.
c     this is not a symmetry operation
                  if (ks == 0) goto 130
               end if
            end do
         end do
c
c     this was not a symmetry operation
         goto 130
c
c     we found a symmetry operation
 110     continue

         nca = nca + 1
c
c     v(i,l) is the i-th cartesian component of the fractional
c     translation associated with the rotation r(l).
c
         v(1:3, l) = vr(1:3)

         ib(nca) = l
         if (l == ni) then
            li = l
            invers_no = nca
         end if

 130  end do
      deallocate(rdiff)
c
c     ------------ there are no gotos across this line -------------
c
      if (ipr >= 2) then
         if ((ihg == 7 .and. nca == 24) .or. (ihg == 5 .and. nca
     1        == 48)) then
            write(7, 901) trim(cst(ihg))
 901        format      (
     1           /' The point group of the system is the full '
     2           ,a12,'group')
         else
            write(7, 900) trim(cst(ihg)), (ic(i), i = 1, nc)
 900        format      (/' The crystallographic system is ',a
     1           ,' with operations: ',4(/5x,16i3))
         end if
      end if
      vs = zero
      nc = nca
      do n = 1, nc
         l = ib(n)
         vs = sum(abs(v(1:3, l)))
      end do
      if (vs > eps) then
         write(7, 903)
 903     format   (/' The space group is non-symmorphic',/,
     1        ' (Or a non-standard origin of coordinates is used)',/)
         isy = 0
         is = 0
      else
         write(7, 902)
 902     format   (
     1        /' The space group of the crystal is symmorphic',/,
     2        ' (this usually means that symmetry operations do not',/
     3        ,' involve rotations and translations simultaneously)'/)
         isy = 1
         is = 1
      end if
c
c     construct the multiplication table
c
      do n1 = 1, nc
         do n2 = 1, nc
            l = ib(n1)
            m = ib(n2)
            do i = 1, 3
               do j = 1, 3
                  r(49, i, j) = zero
                  do k = 1, 3
                     r(49, i, j) = r(49, i, j) + r(l, i, k) * r(m
     1                    , k, j)
                  end do
               end do
            end do
            do n3 = 1, nc
               n = ib(n3)
               ts = zero
               do i = 1, 3
                  do j = 1, 3
                     ts = ts + abs(r(49, i, j) - r(n, i, j))
                  end do
               end do
               if (ts > 1.0d2 * eps) cycle
               mt(l, m) = n
               exit
            end do
         end do
      end do

      il = 1
      iu = nc
      if (iu > 24) iu = 24
      write(7,*) 'Operation number: '
 280  continue
      write(7, '(5x,12i3)') (ib(i), i = il, iu)
      do i = 1, na
         do j = 1, nc
            l = ib(j)
            ia(j) = if0(l, i)
         end do
      end do
      if (nc .le. iu) goto 320
c      write(7, 905)
c 905  format(//)
      il = 25
      iu = nc

      goto 280
c
c     Print multiplication table and fractional translations.
c
 320  continue
      if (ipm == 0) goto 410
      il = 1
      iu = nc
      if (nc > 24) iu = 24
      if (is .gt. 0) goto 340
      if (ipr >= 2) then
         write(7, 906)
 906     format   (/,3x,'Multiplication table',30x
     1        ,'Fractional translations')
         write(7, 907) (ib(i), i = il, iu)
 907     format   (5x,24i4)
         write(7, 908)
 908     format   (10x,'v(1)      v(2)      v(3)')
      end if

      goto 360

 340  continue
      if (ipr >= 2) then
         write(7, 909)
      end if
 909  format(/,3x,'Multiplication table')
 350  continue
      if (ipr >= 2) then
         write(7, 910) 'x',(ib(i), i = il, iu)
      end if
 910  format(4x,a,24i4)
 360  continue
      do j = 1, nc
         l = ib(j)
         do i = il, iu
            n = ib(i)
            ia(i) = mt(l, n)
         end do
         if (is .gt. 0) goto 390
         if (ipr >= 2) then
            write(7, 912) ib(j), (ia(i), i = il, iu)
            write(7, 911) (v(i, l), i = 1, 3)
 911        format      (10x,3f10.4)
         end if

         goto 400

 390     continue
         if (ipr >= 2) then
            write(7, 912) ib(j), (ia(i), i = il, iu)
         end if
 400  end do
 912  format(i5,24i4)
      if (iu == nc) goto 410
      il = 25
      iu = nc
      is = 1

      goto 350

 410  continue

      do i = 1, nc
         l = ib(i)
         do j = 1, 3
            tnp(i, j) = -v(j, l)
            do k = 1, 3
               trans(j, k, i) = r(l, j, k)
            end do
         end do
      end do

      deallocate(rx)
      deallocate(if0)

      end subroutine atftmt
c     ===============================================================
c
c     Determines the point group of the lattice and
c     the crystal system. The array ib contains the locations of the
c     group operations and nc is the order of the group.
c
c     ---------------------------------------------------------------
      subroutine pgl(a, b, r, nc, ib, ihg)
c
      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(out) :: ihg, nc
      real(dp), intent(in) :: a(3, 3), b(3, 3), r(49, 3, 3)
      integer, intent(out) :: ib(48)
c
c     Work variables:
c
      real(dp) :: tr
c
c     eps should be slightly larger than computer precision
c
      real(dp), parameter :: eps = 1.0d-8
      integer :: i, ihc, j, k, lx, n, nr

      real(dp) :: vr(3), xa(3)

c     ---------------------------------------------------------------

      ihc = 0
c
c     ihc is 0 for hexagonal groups and 1 for cubic groups.
c
      nr = 24
 10   continue
      nc = 0
      call rot(r, nr)
      do n = 1, nr
         ib(n) = 0
         tr = zero
         do k = 1, 3
            do i = 1, 3
               xa(i) = zero
               do j = 1, 3
                  xa(i) = xa(i) + r(n, i, j) * a(j, k)
               end do
            end do
            call rlv(b, xa, vr, lx)
            do i = 1, 3
               tr = tr + abs(vr(i))
            end do
         end do
         if (tr <= 10.0d0 * eps) then
            nc = nc + 1
            ib(nc) = n
         end if
      end do
      if (ihc == 0) then
         if (nc == 12) then
            ihg = 6

            return

         end if
         if (nc > 12) then
            ihg = 7

            return

         end if
         if (nc < 12) then
            nr = 48
            ihc = 1

            goto 10

         end if
      else
         if (nc == 16) then
            ihg = 4

            return

         end if
         if (nc > 16) then
            ihg = 5

            return

         end if
         if (nc < 16) then
            if (nc == 4) then
               ihg = 2

               return

            end if
            if (nc > 4) then
               ihg = 3

               return

            end if
            if (nc < 4) then
               ihg = 1

               return

            end if
         end if
      end if
c
c     ihg stands for holohedral group number.
c
      end subroutine pgl
c     ===============================================================
c
c     Define the generators for the rotation matrices
c
c     ---------------------------------------------------------------
      subroutine rot(r, nr)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) :: nr

      real(dp), intent(out) :: r(49, 3, 3)
c
c     Work variables:
c
      real(dp) :: f
      integer :: i, j, k, n, nv
c     ---------------------------------------------------------------
      do n = 1, nr
         do i = 1, 3
            do j = 1, 3
               r(n, i, j) = zero
            end do
         end do
      end do

      if (nr <= 24) then
c
c                                 --hexagonal group
         f = root3 * half
         r(2, 1, 1) = half
         r(2, 1, 2) = -f
         r(2, 2, 1) = f
         r(2, 2, 2) = half
         r(7, 1, 1) = mhalf
         r(7, 1, 2) = -f
         r(7, 2, 1) = -f
         r(7, 2, 2) = half
         do n = 1, 6
            r(n, 3, 3) = one
            r(n + 18, 3, 3) = one
            r(n + 6, 3, 3) = mone
            r(n + 12, 3, 3) = mone
         end do
c
c     generate the rest of the rotation matrices
c
         do i = 1, 2
            r(1, i, i) = one
            do j = 1, 2
               r(6, i, j) = r(2, j, i)
               do k = 1, 2
                  r(3, i, j) = r(3, i, j) + r(2, i, k) * r(2, k, j)
                  r(8, i, j) = r(8, i, j) + r(2, i, k) * r(7, k, j)
                  r(12, i, j) = r(12, i, j) + r(7, i, k) * r(2, k, j)
               end do
            end do
         end do
         do i = 1, 2
            do j = 1, 2
               r(5, i, j) = r(3, j, i)
               do k = 1, 2
                  r(4, i, j) = r(4, i, j) + r(2, i, k) * r(3, k, j)
                  r(9, i, j) = r(9, i, j) + r(2, i, k) * r(8, k, j)
                  r(10, i, j) = r(10, i, j) + r(12, i, k) * r(3, k, j)
                  r(11, i, j) = r(11, i, j) + r(12, i, k) * r(2, k, j)
               end do
            end do
         end do

         do n = 1, 12
            nv = n + 12
            do i = 1, 2
               do j = 1, 2
                  r(nv, i, j) = -r(n, i, j)
               end do
            end do
         end do
      else
c
c                                          --cubic group
c
         r(9, 1, 3) = one
         r(9, 2, 1) = one
         r(9, 3, 2) = one
         r(19, 1, 1) = one
         r(19, 2, 3) = mone
         r(19, 3, 2) = one
         do i = 1, 3
            r(1, i, i) = one
            do j = 1, 3
               r(20, i, j) = r(19, j, i)
               r(5, i, j) = r(9, j, i)
               do k = 1, 3
                  r(2, i, j) = r(2, i, j) + r(19, i, k) * r(19, k, j)
                  r(16, i, j) = r(16, i, j) + r(9, i, k) * r(19, k, j)
                  r(23, i, j) = r(23, i, j) + r(19, i, k) * r(9, k, j)
               end do
            end do
         end do
         do i = 1, 3
            do j = 1, 3
               do k = 1, 3
                  r(6, i, j) = r(6, i, j) + r(2, i, k) * r(5, k, j)
                  r(7, i, j) = r(7, i, j) + r(16, i, k) * r(23, k, j)
                  r(8, i, j) = r(8, i, j) + r(5, i, k) * r(2, k, j)
                  r(10, i, j) = r(10, i, j) + r(2, i, k) * r(9, k, j)
                  r(11, i, j) = r(11, i, j) + r(9, i, k) * r(2, k, j)
                  r(12, i, j) = r(12, i, j) + r(23, i, k) * r(16, k,j)
                  r(14, i, j) = r(14, i, j) + r(16, i, k) * r(2, k, j)
                  r(15, i, j) = r(15, i, j) + r(2, i, k) * r(16, k, j)
                  r(22, i, j) = r(22, i, j) + r(23, i, k) * r(2, k, j)
                  r(24, i, j) = r(24, i, j) + r(2, i, k) * r(23, k, j)
               end do
            end do
         end do
         do i = 1, 3
            do j = 1, 3
               do k = 1, 3
                  r(3, i, j) = r(3, i, j) + r(5, i, k) * r(12, k, j)
                  r(4, i, j) = r(4, i, j) + r(5, i, k) * r(10, k, j)
                  r(13, i, j) = r(13, i, j) + r(23, i, k) * r(11, k,j)
                  r(17, i, j) = r(17, i, j) + r(16, i, k) * r(12, k,j)
                  r(18, i, j) = r(18, i, j) + r(16, i, k) * r(10, k,j)
                  r(21, i, j) = r(21, i, j) + r(12, i, k) * r(15, k,j)
               end do
            end do
         end do
         do n = 1, 24
            nv = n + 24
            do i = 1, 3
               do j = 1, 3
                  r(nv, i, j) = -r(n, i, j)
               end do
            end do
         end do
      end if

      end subroutine rot
c     ===============================================================
c
c     Removes a direct lattice vector from g by
c     operation p, leaving the remainder in y. If a nonzero lattice
c     vector was removed, l is made nonzero.
c
c     ---------------------------------------------------------------
      subroutine rlv(p, g, y, l)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(in) :: g(3), ! vector to be multiplied
     1     p(3, 3)              ! multiplication matrix, e.g. lattice vectors

      real(dp), intent(out) :: y(3) ! mod(multiplied vector,lattice vector)
      integer, intent(out) :: l ! is nonzero if a lattice vector was removed
c     ---------------------------------------------------------------
      y(1:3) = matmul(p(1:3, 1:3), g(1:3))
      l = sum(nint(abs(y(1:3))))
      y(1:3) = y(1:3) - one * nint(y(1:3))

      end subroutine rlv
c     ===============================================================
c
c     symchk checks if the symmetry operations defined by
c     mtrx and tnp really forms a group.
c
c     Jan 7, 1990: AG/ 2pi factor removed ! /
c
c     ---------------------------------------------------------------
      subroutine symchk(ipr, ierr, ntrans, mtrx, tnp, flag, mult)

      use constants
      implicit none
c
c     Input/Output variables:
c
c     flag: 0 for g-space check, 1 for r-space check
c
      integer, intent(in) :: ntrans, flag, ipr
c
c     ierr: returns 0 if no error, otherwise it returns
c     the number of the suspected operation.
      integer, intent(out) :: ierr

      real(dp), intent(in) :: tnp(48, 3)
      integer, intent(in) :: mtrx(48, 3, 3)
c     for operations i, j such that i * j = k, then mult(i,j) = k
      integer, intent(out) :: mult(48,48)
c
c     Work variables:
c
      character(len=80), parameter :: not_a_grp = 'The symmetry'/
     1     /' operations do not form a group. Check operation no%i4$\n'

      real(dp) :: ttest, xdum
      integer :: i, im, itest, j, k, l, m, maxerr

      integer :: nerr(48)
      integer :: mtest(3, 3)
c     ---------------------------------------------------------------

      mult = 0
      ierr = 0
c
c     trivial group:
c
      if (ntrans == 1) then
         mult(1,1) = 1
         return
      endif
c
c     check for duplicate operations
c
      do i = 2, ntrans
         im = i - 1
         do j = 1, im
            do k = 1, 3
               if (abs(tnp(i, k) - tnp(j, k)) > 1.0d-8) goto 30
               do l = 1, 3
                  if (mtrx(i, k, l) /= mtrx(j, k, l)) goto 30
               end do
            end do
            if (ipr >= 2) then
               write(7, 900) j, i
            end if
 900        format(/' symmetry operations',i3,' and',i3,' are equal')
            ierr = i
            write(7,*) ' ERROR: repeated symmetry operations'
            stop

 30      end do
      end do
c
c     construct muliplication table
c
      do i = 1, ntrans
         nerr(i) = 0
         do j = 1, ntrans
            mult(i, j) = 0

c     multiply i and j

            do k = 1, 3
               do l = 1, 3
                  mtest(k, l) = 0
                  do m = 1, 3
                     mtest(k, l) = mtest(k, l) + mtrx(i, k, m)
     1                    * mtrx(j, m, l)
                  end do
               end do
            end do
c
c     check for match
c
            do k = 1, ntrans
               do l = 1, 3
                  do m = 1, 3
                     if (mtest(l, m) /= mtrx(k, l, m)) goto 100
                  end do
               end do
               mult(i, j) = k
 100        end do
         end do
      end do
c
c     if translations not correct set mult(i,j) to -1
c
c      if (flag == 0) then

c         if (ipr >= 2) then
c            write(7, *) 'Checking in g-space'
c         end if
c         do i = 1, ntrans
c            do j = 1, ntrans
c               k = mult(i, j)
c               if (k == 0) cycle
c               do l = 1, 3
c                  ttest = tnp(j, l)
c                  do m = 1, 3
c                     ttest = ttest + real(mtrx(i, m, l), dp) *
c     1                    (tnp(i, m) - tnp(k, m))
c                  end do
c                  ttest = abs(ttest)
c                  itest = nint(ttest)
c                  if (abs(ttest - real(itest, dp)) < 1.0d-4) cycle
cc     if (abs(ttest-dble(itest)) .lt. 1.d-6) cycle
c                  write(7, *) i, j, l, abs(ttest - real(itest, dp))
c                  mult(i, j) = -1

c                  goto 150

c               end do
c 150        end do
c         end do

c      else

         if (ipr >= 2) then
            write(7, *) 'Checking in r-space'
         end if
         do i = 1, ntrans
            do j = 1, ntrans
               k = mult(i, j)
               if (k == 0) cycle
               do l = 1, 3
cMLT somehow, check of translation (ttest) was incorrect
                  ttest = tnp(k, l) - tnp(j, l)
                  do m = 1, 3
                     ttest = ttest - mtrx(j, m, l) * tnp(i, m)
                  end do
                  itest = nint(ttest)
                  if (abs(ttest - real(itest, dp)) < 1.0d-4) cycle
c     if (abs(ttest-dble(itest)) .lt. 1.d-6) cycle
                  write(7, *) i, j, k, l, abs(ttest - real(itest, dp))
                  mult(i, j) = -1

                  goto 550

               end do
 550        end do
         end do

c      end if
c
c     check multiplication table
c
      do i = 1, ntrans
         do j = 1, ntrans
            if (mult(i, j) > 0) cycle
            nerr(i) = nerr(i) + 1
            nerr(j) = nerr(j) + 1
         end do
      end do
c
c     find element with max error
c
      ierr = 0
      maxerr = 0
      do i = 1, ntrans
         if (nerr(i) <= maxerr) cycle
         maxerr = nerr(i)
         ierr = i
      end do
      if (ipr >= 2) then
         write(7, 901)
 901     format(' Multiplication table',/)
 903     format(1x,48i2)
         do i = 1, ntrans
            write(7, 903) (mult(i, j), j = 1, ntrans)
         end do
      end if
      if (ierr /= 0)
     1     write(7,*) ' ERROR: repeated symmetry operations'

      end subroutine symchk
c     ===============================================================
c
c     Identify the symmetry operations
c
c     ---------------------------------------------------------------
      subroutine symm_ident(ntrans, mtrx, tnp, id)

      use constants
      implicit none
c
c     Input/Output variables:
c
      integer, intent(in) :: ntrans
      integer, intent(in) :: mtrx(48, 3, 3)
      real(dp), intent(in) :: tnp(48, 3)
      character(len=3), intent(out) :: id(48)
c
c     Work variables:
c
      integer :: i, j, oper, ierr, ipt
      logical :: proper, nonsym(48)
      real(dp) :: det, trace, te, tt
      integer :: iv1(3)
      real(dp) :: a (3, 3), z (3, 3), wr (3), wi (3), fv1 (3)
      real(dp) :: t (3), e (3), canon_t (3)
      real(dp) :: rdot
      character(len=2) :: axes(-2:2)
      data axes / 'C2', 'C3', 'C4', 'C6', 'E ' /
c     ---------------------------------------------------------------
      do oper = 1, ntrans
c
c     Copy the matrix to a double precision format for
c     processing with EISPACK. Also, copy the non-primitive
c     translation.
c
         do i = 1, 3
            do j = 1, 3
               a(i, j) = mtrx(oper, i, j)
               t(j) = tnp(48, j)
            end do
         end do
c
c     Compute determinant  and trace
c
         det = a(1, 1) * a(2, 2) * a(3, 3) + a(2, 1) * a(3, 2) * a(1,
     1        3) + a(1, 2) * a(2, 3) * a(3, 1) - a(3, 1) * a(2, 2)
     2        * a(1, 3) - a(2, 1) * a(1, 2) * a(3, 3) - a(1, 1) * a(3
     3        , 2) * a(2, 3)

         proper = (nint(det) == 1)

         trace = a(1, 1) + a(2, 2) + a(3, 3)

         if (proper) then
c
c     Proper operation
c
            id(oper) = axes(nint(trace - 1))

         else
c
c     R = IS , where S is proper
c
c     call sscal(9, mone, a, 1)
            a = a*mone
            id(oper) = 'I'//axes(nint(-trace - 1))

         end if
      end do

      end subroutine symm_ident
c     ===============================================================
c
c     For a given point group, search for the Abelian subgroup with
c     maximum number of elements and REAL characters. This is a
c     search for inspection, and selects only subgroups with real
c     characters. These are the only possibilities: C_1, C_s, C_i,
c     C_2, C_2h, C_2v, D_2, and D_2h. Output is in array indx, with
c     the indices of symmetry operations in the subgroup with respect
c     to the full point group.
c
c     ---------------------------------------------------------------
      subroutine abelian(ntrans, mult, rmtrx, tnp, id, nab, indx,
     1     chi, name, label)

      use constants
      implicit none
c
c     Input/Output variables
c
      integer, intent(in) ::
     1     ntrans,              ! number of operations in point group
     2     rmtrx(48,3,3),       ! rotation matrix
     3     mult(48,48)          ! multiplication table of point group
c     translation vectors
      real(dp), intent(in) :: tnp(48, 3)
c     identity of symmetry operations, as determined by symm_ident
      character(len=3), intent(in) :: id(48)

      integer, intent(out) ::
     1     nab,                 ! number of operations in Abelian group
     2     indx(8),             ! index of operations in Abelian group
     3     chi(8,8)             ! character table
c     name of Abelian subgroup and their representations
      character(len=5), intent(out) :: name
      character(len=3), intent(out) :: label(8)
c
c     Work variables
c
c     flags, indicate wether a particular symmetry is present or not
      logical ::
     1     l_i,                 ! inversion symmetry
     2     l_c2,                ! C2 rotation
     3     l_c22,               ! multiple C2 rotations
     4     l_ic                 ! mirror symmetry
c
c     indices, locate flagged symmetries
      integer i_e, i_i, i_c2, i_c22, i_ic
c     counters
      integer it, jt, ii, jj, ntr, itr(ntrans), tmtrx(3,3)
c     length of translation vectors
      real(dp) :: length
c     tolerance
      real(dp), parameter :: tol = 1.d-10

c     ---------------------------------------------------------------

      l_i = .false.
      l_c2 = .false.
      l_c22 = .false.
      l_ic = .false.
      indx = 0
      nab = 0
c
c     selected allowed transformations
      itr = 0
      ntr = 0
      do ii = 1, ntrans
c     ignore operations with non-zero translation vector
         length = sqrt( dot_product(tnp(ii,:),tnp(ii,:)) )
         if (length .gt. tol) cycle
c     ignore operations where rotation matrix is not diagonal
         tmtrx = rmtrx(ii,:,:)
         do jj = 1, 3
            tmtrx(jj,jj) = 0
         enddo
         if (maxval(abs(tmtrx)) .ne. 0) cycle
         ntr = ntr + 1
         itr(ntr) = ii
      enddo

      do it = 1, ntr
         ii = itr(it)
c     locate identity
         if (trim(id(ii)) .eq. 'E') then
            nab = nab + 1
            i_e = ii
         endif
c     is there inversion?
         if (trim(id(ii)) .eq. 'IE') then
            l_i = .true.
            nab = nab + 1
            i_i = ii
         endif
c     is there C2 rotation?
         if (.not. l_c2 .and. trim(id(ii)) .eq. 'C2') then
            l_c2 = .true.
            nab = nab + 1
            i_c2 = ii
         endif
c     is there IC2 mirror?
         if (.not. l_ic .and. trim(id(ii)) .eq. 'IC2') then
            l_ic = .true.
            nab = nab + 1
            i_ic = ii
         endif
      enddo

c     is there a second C2 that commutes with the first C2?
      do it = 1, ntr
         ii = itr(it)
         if (trim(id(ii)) .ne. 'C2') cycle
         do jt = 1, ntr
            jj = itr(jt)
            if (trim(id(jj)) .ne. 'C2') cycle
            if (mult(ii,jj) == mult(jj,ii) .and. ii /= jj) then
               l_c22 = .true.
               i_c2 = ii
               i_c22 = jj
               nab = nab + 1
               goto 10
            endif
         enddo
      enddo
 10   continue
c
c     is there IC2 mirror? (if there is C2, it must commute with IC2)
      l_ic = .false.
      do it = 1, ntr
         if (trim(id(itr(it))) .eq. 'IC2') l_ic = .true.
      enddo
      if (l_ic .and. l_c2) then
         l_ic = .false.
         nab = nab - 1
      endif
      do it = 1, ntr
         ii = itr(it)
         if (trim(id(ii)) .ne. 'C2') cycle
         do jt = 1, ntr
            jj = itr(jt)
            length = sqrt( dot_product(tnp(jj,:),tnp(jj,:)) )
            if (length .gt. tol) cycle
            if (trim(id(jj)) .ne. 'IC2') cycle
            if (mult(ii,jj) == mult(jj,ii)) then
               l_ic = .true.
               i_c2 = ii
               i_ic = jj
               nab = nab + 1
               goto 20
            endif
         enddo
      enddo
 20   continue
c
c     more than 2 generators (including E) : D_2h, D_2, C_2v, C_2h
c
      if (nab .gt. 2) then
         if (l_i) then
            if (l_c22) then
               name = 'D_2h'
            else
               name = 'C_2h'
            endif
         else
            if (l_c22) then
               name = 'D_2'
            else
               name = 'C_2v'
            endif
         endif
      else
c
c     2 generators or less (including E) : C_2, C_s, C_i, C_1
c
         if (l_c2) then
            name = 'C_2'
         elseif (l_ic) then
            name = 'C_s'
         elseif (l_i) then
            name = 'C_i'
         else
            name = 'C_1'
         endif
      endif
c
c     construct character table and index
c
      select case(trim(name))
      case('D_2h')
         nab = 8
         indx(1) = i_e          ! E
         indx(2) = i_c2         ! C2_x
         indx(3) = i_c22        ! C2_y
         indx(4) = mult(i_c22, i_c2) ! C2_z = C2_y * C2_x
         indx(5) = i_i          ! IE
         indx(6) = mult(indx(2), indx(5)) ! IC2_x
         indx(7) = mult(indx(3), indx(5)) ! IC2_y
         indx(8) = mult(indx(4), indx(5)) ! IC2_z
         chi(1,1:8) = (/ 1, 1, 1, 1, 1, 1, 1, 1/)
         chi(2,1:8) = (/ 1, 1,-1,-1, 1, 1,-1,-1/)
         chi(3,1:8) = (/ 1,-1, 1,-1, 1,-1, 1,-1/)
         chi(4,1:8) = (/ 1,-1,-1, 1, 1,-1,-1, 1/)
         chi(5,1:8) = (/ 1, 1, 1, 1,-1,-1,-1,-1/)
         chi(6,1:8) = (/ 1, 1,-1,-1,-1,-1, 1, 1/)
         chi(7,1:8) = (/ 1,-1, 1,-1,-1, 1,-1, 1/)
         chi(8,1:8) = (/ 1,-1,-1, 1,-1, 1, 1,-1/)
         label(1) = 'Ag '
         label(2) = 'B1g'
         label(3) = 'B2g'
         label(4) = 'B3g'
         label(5) = 'Au '
         label(6) = 'B1u'
         label(7) = 'B2u'
         label(8) = 'B3u'
      case('C_2h')
         nab = 4
         indx(1) = i_e          ! E
         indx(2) = i_c2         ! C2
         indx(3) = i_i          ! IE
         indx(4) = mult(i_c2, i_i) ! IC2 = C2 * IE
         chi(1,1:4) = (/ 1, 1, 1, 1/)
         chi(2,1:4) = (/ 1,-1, 1,-1/)
         chi(3,1:4) = (/ 1, 1,-1,-1/)
         chi(4,1:4) = (/ 1,-1,-1, 1/)
         label(1) = 'Ag '
         label(2) = 'Bg '
         label(3) = 'Au '
         label(4) = 'Bu '
      case('D_2')
         nab = 4
         indx(1) = i_e          ! E
         indx(2) = i_c2         ! C2_x
         indx(3) = i_c22        ! C2_y
         indx(4) = mult(i_c22, i_C2) ! C2_z = C2_y * C2_x
         chi(1,1:4) = (/ 1, 1, 1, 1/)
         chi(2,1:4) = (/ 1, 1,-1,-1/)
         chi(3,1:4) = (/ 1,-1, 1,-1/)
         chi(4,1:4) = (/ 1,-1,-1, 1/)
         label(1) = 'A  '
         label(2) = 'B1 '
         label(3) = 'B2 '
         label(4) = 'B3 '
      case('C_2v')
         nab = 4
         indx(1) = i_e          ! E
         indx(2) = i_c2         ! C2
         indx(3) = i_ic         ! IC2
         indx(4) = mult(i_c2, i_ic) ! IC2' = C2 * IC2
         chi(1,1:4) = (/ 1, 1, 1, 1/)
         chi(2,1:4) = (/ 1, 1,-1,-1/)
         chi(3,1:4) = (/ 1,-1, 1,-1/)
         chi(4,1:4) = (/ 1,-1,-1, 1/)
         label(1) = 'A1 '
         label(2) = 'A2 '
         label(3) = 'B1 '
         label(4) = 'B2 '
      case('C_2')
         nab = 2
         indx(1) = i_e          ! E
         indx(2) = i_c2         ! C2
         chi(1,1:2) = (/ 1, 1/)
         chi(2,1:2) = (/ 1,-1/)
         label(1) = 'A  '
         label(2) = 'B  '
      case('C_s')
         nab = 2
         indx(1) = i_e          ! E
         indx(2) = i_ic         ! IC2
         chi(1,1:2) = (/ 1, 1/)
         chi(2,1:2) = (/ 1,-1/)
         label(1) = 'A1 '
         label(2) = 'A2 '
      case('C_i')
         nab = 2
         indx(1) = i_e          ! E
         indx(2) = i_i          ! IE
         chi(1,1:2) = (/ 1, 1/)
         chi(2,1:2) = (/ 1,-1/)
         label(1) = 'Ag '
         label(2) = 'Au '
      case('C_1')
         nab = 1
         indx(1) = i_e          ! E
         chi(1,1) =  1
         label(1) = 'A  '
      end select

      end subroutine abelian
c     ===============================================================
c
c     Inverts 3x3 matrix m corresponding to a symmetry operation,
c     storing the result in m. It also calculates determinant and
c     trace of the input matrix. Matrix inversion is aborted if
c     det<del.
c
c     ---------------------------------------------------------------
      subroutine mtrxin(m,det,tr)

      use constants
      implicit none
c
c     Input/Output variables:
c
      real(dp), intent(inout) :: m(3,3)
      real(dp), intent(out) :: det,tr
c
c     Work variables:
c
      real(dp) :: a(3,3),del,x
      integer i,j
c     ---------------------------------------------------------------
c
c     compute matrix of cofactors
c
      a(1,1) = m(2,2)*m(3,3) - m(2,3)*m(3,2)
      a(2,1) = -m(2,1)*m(3,3) + m(2,3)*m(3,1)
      a(3,1) = m(2,1)*m(3,2) - m(2,2)*m(3,1)
      a(1,2) = -m(1,2)*m(3,3) + m(1,3)*m(3,2)
      a(2,2) = m(1,1)*m(3,3) - m(1,3)*m(3,1)
      a(3,2) = -m(1,1)*m(3,2) + m(1,2)*m(3,1)
      a(1,3) = m(1,2)*m(2,3) - m(1,3)*m(2,2)
      a(2,3) = -m(1,1)*m(2,3) + m(1,3)*m(2,1)
      a(3,3) = m(1,1)*m(2,2) - m(1,2)*m(2,1)
c
c     compute determinant
c
      det = m(1,1)*a(1,1) + m(1,2)*a(2,1) + m(1,3)*a(3,1)
      tr = m(1,1) + m(2,2) + m(3,3)
      del = 1.0d-05
      if (abs(det) < del) stop 501
c
c     form mi
c
      do i=1,3
         do j=1,3
            x = a(i,j)/det
            m(i,j) = x
c            if (x < 0.0d0) m(i,j) = x - del
c            if (x > 0.0d0) m(i,j) = x + del
         enddo
      enddo

      end subroutine mtrxin
c     ===============================================================
