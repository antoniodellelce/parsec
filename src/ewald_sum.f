c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Computes the Ewald contribution to the total energy and forces
c     by performing two convergent summations, one over lattice
c     vectors and the other over reciprocal lattice vectors.
c
c     Adapted from plane-wave programs written by S. Froyen and
c     J. L. Martins.
c
c     ---------------------------------------------------------------
      subroutine ewald_sum(clust,pbc,eewald,ipr,zv)

      use constants
      use cluster_module
      use pbc_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     pbc related data
      type (pbc_data), intent(inout) :: pbc
c     print flag
      integer, intent(in) :: ipr
c     Ewald summation
      real(dp), intent(out) :: eewald

      real(dp), intent(in) :: zv(clust%type_num)
c
c     Work variables:
c
c     allocation check
      integer alcstat

      integer natot
      integer i,j,k,n,imx,jmx,kmx,ii,jj,ngg,nss
      real(dp) :: vcell,arg,bdot(3)

      real(dp) :: rp1,rp2,rp3,fsub1,fsub2,fsub3
      real(dp) :: eps,seps,sepi,rmax,fpiv,fpiv2
      real(dp) :: esumg,esumr,esum0,ztemp,rmod,exp1,exp2
      real(dp) :: esub,enorm,rmod1,rmod2,sumc,sumr,gdr,cosg
      real(dp) :: sing,expgc,expgr,t1,t2,t3
      real(dp) :: fsumr(3,pbc%atom_num*pbc%type_num)
      real(dp) :: fsumg(9,pbc%atom_num*pbc%type_num) 

      real(dp), allocatable :: expg(:)
      real(dp) :: zz(pbc%atom_num*pbc%type_num)
      real(dp) :: rc(3,pbc%atom_num*pbc%type_num)
      real(dp) :: pi_dev_eps_v
c     constants
      real(dp), parameter :: twopio = 0.5d0/pi
      real(dp), parameter :: small = 1.d-20
      real(dp), parameter :: p25 = 0.25d0
      real(dp), parameter :: tol = 200.d0, small2 = 6.5d0
c
c     External functions:
c
      real(dp), external :: erfc

c     ---------------------------------------------------------------

      bdot = pbc%bdot

      allocate(expg(pbc%nstar),stat=alcstat)
      call alccheck('expg',pbc%nstar,alcstat)

      vcell = pbc%vcell
      eps = p25*two*pbc%ek(pbc%nstar)/tol
      seps = sqrt(eps)
      sepi = two*seps/sqrt(pi)
      rmax = sqrt(tol/eps)
      fpiv = four*pi/vcell
      fpiv2 = two*fpiv

      imx = int(rmax*sqrt(bdot(1))*twopio) + 1
      jmx = int(rmax*sqrt(bdot(2))*twopio) + 1
      kmx = int(rmax*sqrt(bdot(3))*twopio) + 1

c     store data for atoms into new arrays and
c     fold into first cell : prevents errors in sums from
c     too small a real space sum.
      natot = 0
      do i=1,clust%type_num
         do j=1,clust%natmi(i)
            natot = natot + 1
            zz(natot) = zv(i)
            rc(1:3,natot) =  pbc%rat(1:3,j,i)*twopio
            do n = 1, 3
               if (rc(n,natot) .ge. one)
     1                rc(n,natot) = modulo(rc(n,natot) + eight,one)
               if (rc(n,natot) .lt. zero)
     1                rc(n,natot) = modulo(rc(n,natot),one)
            enddo
         enddo
      enddo

      esumg = zero
      esumr = zero

      fsumg(1:3,:) = zero
      fsumr = zero

c     start sum in g space
      esum0 = zero
      ztemp = -p25/eps
      do i=2,pbc%nstar
         expg(i) = exp(ztemp*two*pbc%ek(i))/(two*pbc%ek(i))
         if (expg(i) .lt. small) exit
      enddo
      nss = i - 1
      ngg = pbc%ng 

      ngg = ngg - sum(pbc%mstar(nss+1:pbc%nstar))

      ztemp = half/eps
      do i=2,ngg
         sumc=zero
         sumr=zero

c     start loop over atoms in cell
         do j=1,natot
            gdr =twopi*(real(pbc%kgv(1,i),dp)*rc(1,j)+
     1                  real(pbc%kgv(2,i),dp)*rc(2,j)+
     2                  real(pbc%kgv(3,i),dp)*rc(3,j))
            cosg = zz(j)*cos(gdr)
            sing = zz(j)*sin(gdr)
            sumc = sumc + cosg
            sumr = sumr + sing
            fsumg(4,j) = -real(pbc%kgv(1,i),dp)*cosg
            fsumg(5,j) = -real(pbc%kgv(2,i),dp)*cosg
            fsumg(6,j) = -real(pbc%kgv(3,i),dp)*cosg
            fsumg(7,j) =  real(pbc%kgv(1,i),dp)*sing
            fsumg(8,j) =  real(pbc%kgv(2,i),dp)*sing
            fsumg(9,j) =  real(pbc%kgv(3,i),dp)*sing
         enddo
         expgc = expg(pbc%inds(i))*sumc
         expgr = expg(pbc%inds(i))*sumr
         t1 = expgc*sumc+expgr*sumr
         esumg=esumg+t1
         do j=1,natot
            fsumg(1,j)=fsumg(1,j)+expgc*fsumg(7,j)+
     1           expgr*fsumg(4,j)
            fsumg(2,j)=fsumg(2,j)+expgc*fsumg(8,j)+
     1           expgr*fsumg(5,j)
            fsumg(3,j)=fsumg(3,j)+expgc*fsumg(9,j)+
     1           expgr*fsumg(6,j)
         enddo
      enddo
c
      esumg = fpiv*esumg
      fsumg(1:3,:) = fpiv2*fsumg(1:3,:)

      pi_dev_eps_v =  pi/(eps*vcell)

c     end g sum
c
c     start sum in r space
      esum0 = zero
      do i=-imx,-1
         rmod1 = real(i*i,dp)*pbc%adot(1)
         do j=-jmx,jmx
            rmod2 = rmod1+real(j,dp)*real(j,dp)*pbc%adot(2)
            do k=-kmx,kmx
               rmod = sqrt(rmod2+real(k,dp)*real(k,dp)*pbc%adot(3))
               arg = seps*rmod
               if (arg .lt. small2) then
                  exp1 = erfc(arg) / rmod
                  exp2 = (exp1 + sepi*exp(-arg*arg))/(rmod*rmod)
                  esum0 = esum0 + exp1
               endif
            enddo
         enddo
      enddo
      do i=1,imx
         rmod1 = real(i*i,dp)*pbc%adot(1)
         do j=-jmx,jmx
            rmod2 = rmod1+real(j,dp)*real(j,dp)*pbc%adot(2)
            do k=-kmx,kmx
               rmod = sqrt(rmod2+real(k,dp)*real(k,dp)*pbc%adot(3))
               arg = seps*rmod
               if (arg .lt. small2) then
                  exp1 = erfc(arg) / rmod
                  exp2 = (exp1 + sepi*exp(-arg*arg))/(rmod*rmod)
                  esum0 = esum0 + exp1
               endif
            enddo
         enddo
      enddo
      do j=-jmx,-1
         rmod1 = real(j*j,dp)*pbc%adot(2)
         do k=-kmx,kmx
            rmod = sqrt(rmod1+real(k,dp)*real(k,dp)*pbc%adot(3))
            arg = seps*rmod
            if (arg .lt. small2) then
               exp1 = erfc(arg) / rmod
               exp2 = (exp1 + sepi*exp(-arg*arg))/(rmod*rmod)
               esum0 = esum0 + exp1
            endif
         enddo
      enddo
      do j=1,jmx
         rmod1 = real(j*j,dp)*pbc%adot(2)
         do k=-kmx,kmx
            rmod = sqrt(rmod1+real(k,dp)*real(k,dp)*pbc%adot(3))
            arg = seps*rmod
            if (arg .lt. small2) then
               exp1 = erfc(arg) / rmod
               exp2 = (exp1 + sepi*exp(-arg*arg))/(rmod*rmod)
               esum0 = esum0 + exp1
            endif
         enddo
      enddo
      do k=-1,-kmx,-1
         rmod = sqrt(real(k*k,dp)*pbc%adot(3))
         arg = seps*rmod
         if (arg .gt. small2) exit
         exp1 = erfc(arg) / rmod
         exp2 = (exp1 + sepi*exp(-arg*arg))/(rmod*rmod)
         esum0 = esum0 + exp1
      enddo
      do k=1,kmx
         rmod = real(k,dp)*pbc%adot(3)
         arg = seps*rmod
         if (arg .gt. small2) exit
         exp1 = erfc(arg) / rmod
         exp2 = (exp1 + sepi*exp(-arg*arg))/(rmod*rmod)
         esum0 = esum0 + exp1
      enddo

      esum0 = esum0 - pi/(eps*vcell) - sepi     

c     start loop over atoms in cell
c     term with a=b (atom 1)
      ztemp = zz(1)*zz(1)
      esumr = esumr + ztemp*esum0

      do i=2,natot

c     term with a=b (atom >1)
         ztemp = zz(i)*zz(i)
         esumr = esumr + ztemp*esum0

c     terms with a#b
         do j=1,i-1

c     loop over lattice points
            esub = zero
            fsub1 = zero
            fsub2 = zero
            fsub3 = zero
            do ii=-imx,imx
               rp1 =real(ii,dp) + rc(1,i) - rc(1,j)
               rmod1 = rp1*rp1*pbc%adot(1)
               do jj=-jmx,jmx
                  rp2 =real(jj,dp) + rc(2,i) - rc(2,j)
                  rmod2 = rmod1 + rp2*rp2*pbc%adot(2)
                  do k=-kmx,kmx
                     rp3 =real(k,dp) + rc(3,i) - rc(3,j)
                     rmod = sqrt(rmod2+rp3*rp3*pbc%adot(3))
                     arg = seps*rmod
                     if (arg .lt. small2) then
                        exp1 = erfc(arg) / rmod
                        exp2=(exp1+sepi*exp(-arg*arg))/(rmod*rmod)
                        esub = esub + exp1
                        t1 = rp1 * exp2
                        t2 = rp2 * exp2
                        t3 = rp3 * exp2
                        fsub1 = fsub1 + t1
                        fsub2 = fsub2 + t2
                        fsub3 = fsub3 + t3
                     endif
                  enddo
               enddo
            enddo
            esub = esub - pi_dev_eps_v
            ztemp = two*zz(i)*zz(j)
            esumr = esumr + ztemp*esub
            t1 = ztemp*fsub1
            t2 = ztemp*fsub2
            t3 = ztemp*fsub3
            fsumr(1,i) = fsumr(1,i) + t1
            fsumr(2,i) = fsumr(2,i) + t2
            fsumr(3,i) = fsumr(3,i) + t3
            fsumr(1,j) = fsumr(1,j) - t1
            fsumr(2,j) = fsumr(2,j) - t2
            fsumr(3,j) = fsumr(3,j) - t3
         enddo
      enddo

c     end r sum
      eewald = esumg + esumr

c     force
c     note - returned force is in units of real space lattice vectors
c     printed force is in cartesian coordinates
      do ii=1,3
         clust%force(ii,:)= (fsumr(ii,:)+pbc%bdot(ii)*fsumg(ii,:)
     1        *twopio)*pbc%box_size(ii)
      enddo

      if (allocated(expg)) deallocate(expg)

c     if print flag on, print the ion-ion forces
      if(ipr .ge. 1) then
         write(7,*)
         write(7,*) 'Forces from ion-ion interaction:'
         write(7,*) '================================'
         write(7,22)
         write(7,*)
         do i = 1, clust%atom_num
            write(7,20) i,clust%xatm(i),clust%yatm(i)
     1           ,clust%zatm(i),clust%force(:,i)
         enddo
         write(7,*)
      endif

 20   format(i4,2x,3(f11.6,1x),2x,3(f11.6,1x))
 22   format('-atom-  ----x----   ----y----   ----z-----'
     1     ,'   ----Fx----  ----Fy----   ----Fz---',/
     2     '                       [bohr]           '
     3     ,'                  [Ry/bohr]            ')

      end subroutine ewald_sum
c     ===============================================================
