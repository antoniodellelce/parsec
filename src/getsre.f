c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine calculates and reports the regular and
c     charge -weighted self-consistent residual error (SRE) following
c     each iteration.
c     The charge weighted SRE is the one used to  determine
c     convergence.
c
c     ---------------------------------------------------------------
      subroutine getsre(elec_st,pot,parallel,hcub,imove,iter)

      use constants
      use electronic_struct_module
      use potential_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
c     potential related data
      type (potential), intent(in) :: pot
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

c     (grid spacing)**3
      real(dp), intent(in) :: hcub
c     movement number, self-consistent iteration number
      integer, intent(in) :: imove, iter
c
c     Work variables:
c
c     "regular" SRE
      real(dp) :: sre(elec_st%nspin)
c     temporary data holder
      real(dp) :: sum, dv
c     counters
      integer i,ii,jj
c     spin identifier for printing
      character(len=2) idsp

c     ---------------------------------------------------------------

c     sum potential differences squared - as is, and weighted by
c     the charge density
      sre = zero
      elec_st%sre = zero

c     compute SRE
      do ii = 1, elec_st%nspin
         jj=ii-1+elec_st%nspin
         do i = 1, parallel%mydim
            dv = pot%vnew(i,ii) - pot%vold(i,ii)
            sum = dv*dv
            sre(ii) = sre(ii) + sum
            elec_st%sre(ii)=elec_st%sre(ii)+
     1           elec_st%rho(i,jj)*sum*real(elec_st%nrep,dp)
         enddo
      enddo
      call psum(sre,elec_st%nspin,parallel%procs_num,parallel%comm)
      call psum(elec_st%sre,elec_st%nspin,
     1     parallel%procs_num,parallel%comm)

c     normalize by h^3 for the "regular" SRE, by h^3/(# of electrons)
c     for the charge weighted SRE.
      if (parallel%iammaster) then
         do ii = 1, elec_st%nspin
            sre(ii) = sqrt(sre(ii)*hcub)
            elec_st%sre(ii) = sqrt(hcub*elec_st%sre(ii)/
     1           elec_st%totel(ii))
            write(7,*)
c     set spin identifier
            jj=ii-1+elec_st%nspin
            if (jj .eq. 1) idsp = '  '
            if (jj .eq. 2) idsp = 'up'
            if (jj .eq. 3) idsp = 'dn'

            write(7,10) imove, iter, idsp, sre(ii), elec_st%sre(ii)
         enddo
      endif
 10   format(i3,'-',i2,1x,a2,' SRE of pot. & charge weighted pot = ',
     1     f14.10,2x, f14.10,/)

      end subroutine getsre
c     ===============================================================
