c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This is a machine-dependent subroutine that should be modified
c     when porting to new systems.
c
c     ---------------------------------------------------------------
      subroutine mysecond(t)

      use constants
      implicit none
#ifdef IBM
c     **************** ibm r6000 start ********************
      real(dp), intent(out) :: t
      integer mclock

      t=real(mclock(),dp)/100.d0
c     ************** ibm r6000 end*************************

#else 
c     ************** sgi altix / sun **********************
      real(dp), intent(out) :: t 

      call CPU_TIME(t)
c     ************** sgi altix  / sun ********************* 
#endif
      end subroutine mysecond
c     ===============================================================
