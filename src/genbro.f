c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Authors: M. Jain, L. Kronik
c
c     This subroutine mixes the old and new potential according to
c     the GENERALIZED Broyden method, using p memory steps. 
c
c     NOTE: This is NOT the modified Broyden method of D. D. Johnson.
c     This is an original, unpublished algorithm developed by M. Jain
c     and L. Kronik. The gist of the algorithm is similar to the
c     generalization of the Broyden suggested by Eyert (J. Comp. Phys.
c     124, 271, 1996), but it is based on considerations for updating
c     the inverse Jacobian with respect to that of the previous
c     iteration, not with respect to that of the first iteration. 
c     Eyert's paper also explains why this approach is inherently
c     superior to the modified Broyden algorithm.
c
c     The equations implemented are:
c
c     (a) B_n = B_1 + \Sum_{i=2}^n (u_i*v_i^t)
c     (b) f_i=xout_i-xin_i ;
c         df_i = f_i-f_(i-1);
c         dxin_i = xin_i-xin_(i-1)
c     (c) u_i = B_1*df(i)+dxin_i-\Sum_{k=2}^{i-1} [(u_k*v_k^t*df_i]
c     (d) T_{i,j} = df_i^t*df_j
c     (e) v_i^t = \Sum_{k=1}^p {[T^(-1)]_pk * df_(i-p+k)^t
c     (f) x_new = x_in-B_n*f_n
c
c     where:
c             B is the inverse Jacobian,
c             1 denotes thec first iteration,
c             n denotes the nth iteration,
c             t indicates transposition,
c             T is the "mixing matrix" of dimensions pxp.
c
c     By combining Eqs. (a) and (f), there is never a need to store
c     B_n!! We only need to store the history of the u, v
c     intermediate vectors, and this is what we do. This trick is
c     known as the Srivastava Scheme (G. P. Srivastava, J. Phys. A:
c     Math. Gen. 17, L317, 1984). Even this storage becomes difficult
c     for many iterations. This subroutine is therefore an
c     "out-of-core" one (i.e., it communicates with the hard disk) in
c     order to avoid prohibitive memory requirements.
c
c     ---------------------------------------------------------------
      subroutine genbro(mixer,parallel,iter,nrep,xin,xout,ierr)

      use constants
      use mixer_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     mixer related data
      type (mixer_data), intent(inout) :: mixer
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

c     iteration number
      integer, intent(in) :: iter
c     order of the reduced group
      integer, intent(in) :: nrep
c     input potential
      real(dp), intent(in) :: xin(mixer%ndim * mixer%nspin)
c     output potential on input, mixed potential on output
      real(dp), intent(inout) :: xout(mixer%ndim * mixer%nspin)
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     suffix for temporary files
      character(len=3) :: idstring
c     multiplicity of grid points in irreducible wedge (equal to
c     order of the reduced group)
      real(dp) :: weight
c     counters
      integer i,j,m,nn
c     temporary holder of resid_in
      real(dp) :: dvin
c     accumulators for scalar multiplications
      real(dp) :: vtemp, vfac
c     error flag for matrix inversion calls
      integer info 
c     number of read blocks and size of each read block from
c     bro.* files
      integer nblk, rsize
c     string with mixing history file name
      character(len=6) fnam
c     initial Jacobian
      real(dp) :: pmix
c     size parameter for memory block allocated for the u,v update
c     arrays
      integer blk
c     allocation check
      integer alcstat
c     system dimension
      integer dim
c     Jacobian update history vectors, work arrays
      real(dp), dimension (:,:), allocatable :: u,v
c     history of resid_out-resid_in, where resid_out is out-out_old, 
c     resid_in is in-in_old
      real(dp), dimension (:,:), allocatable :: df
c     work arrays for matrix inversion calls
      integer, dimension(1: mixer%memory) :: ipiv
      real(dp), dimension(1: mixer%memory) :: work
c
c     External functions:
c
      real(dp), external :: distdot

c     ---------------------------------------------------------------
c
c initialize parameters and work arrays
c
      pmix = mixer%param
      weight = real(nrep,dp)
      dim = mixer%ndim * mixer%nspin
      blk = mixer%block
      write(idstring,'(I3.3)') parallel%iam

      allocate(u(dim,2:mixer%block+1),stat=alcstat)
      call alccheck('genbro_u',dim*mixer%block,alcstat)
      allocate(v(dim,2:mixer%block+1),stat=alcstat)
      call alccheck('genbro_v',dim*mixer%block,alcstat)
      allocate(df(dim , 1:mixer%memory),stat=alcstat)
      call alccheck('genbro_df',dim*mixer%memory,alcstat)

      u = zero
      v = zero
      df = zero
      ipiv = 0
      work = zero
c
c     if p=0 the algorithm reduces to trivial linear mixing
c
      if (mixer%memory.eq.0) then
         xout = (1-pmix)*xin + pmix*xout
         goto 99
      endif

c     if p is not 0, there's "real work" to do:
 
c     The first iteration must be treated separately because various
c     things need to be initialized.
 
      if (iter .eq. 1) then

c     initialize inverse Jacobian matrix. Here it is simply a scalar,
c     pmix, but has been given an array because a more sophisticated
c     first guess may be desired in the future.

c     Also, save xin, xout as xinold, xoutold, for the next iteration

         mixer%resid1 = -pmix
         mixer%old1 = xin
         mixer%old2 = xout

c     Perform a Broyden update based on the initial guess for the 
c     inverse Jacobian [Eq. (f) above]
 
         do i = 1, dim
            xout(i) = xin(i)-mixer%resid1(i)*(xout(i) - xin(i))
         enddo
         mixer%T = zero
         mixer%Tinv = zero
         goto 99
      endif
 
c     If this is not the first iteration, we must distinguish whether
c     we are filling up the "memory buffer" of previous iterations
c     until it occupies all p previous memory steps (iter .le. p+1)
c     and the case where we are updating it on a FIFO
c     basis (iter .gt. p+1).
 
      m = iter - 1

      if (iter .le. mixer%memory+1)  then

c     read previous values of u,v vectors from bro.1
c     read previous values of df from bro.df

         open(10,file='bro.1.'//idstring,form='unformatted')
         open(11,file='bro.df.'//idstring,form='unformatted')
 
         do j = 2, m
            read(10) (u(i,j),i=1,dim)
         enddo
         do j = 2, m
            read(10) (v(i,j),i=1,dim)
         enddo
         do j = 1, m-1
            read(11) (df(i,j),i=1,dim)
         enddo
         close(10)
         close(11)
 
c     Compute current df, u, v [Eqs. (c),(d)]
 
         m = iter
 
         do i = 1, dim
            dvin = xin(i) - mixer%old1(i)
            df(i,m-1) = xout(i) - mixer%old2(i) - dvin
            u(i,m) = dvin - mixer%resid1(i)*df(i,m-1)
            v(i,m) = zero
         enddo

         do j = 2, m-1
            vfac = distdot(dim,v(1,j),1,df(1,m-1),1
     1           ,parallel%procs_num,parallel%comm)*weight
            do i = 1, dim
               u(i,m) = u(i,m) - vfac*u(i,j)
            enddo
         enddo
 
c     Update the T matrix (Eq. (d)]
c     First the row and then the column
 
         do j= 2, m
            vtemp = distdot(dim,df(1,m-1),1,df(1,j-1),1
     1           ,parallel%procs_num,parallel%comm)*weight
            mixer%T(m-1,j-1) = vtemp
            mixer%T(j-1,m-1) = vtemp
         enddo

c     Invert the T matrix
         do j = 1, m-1
            do i = 1, m-1
               mixer%Tinv(j,i) = mixer%T(j,i)
            enddo
         enddo
         do j = 1, m-1
            ipiv(j) = 0
         enddo

c     adi
         call dgetrf(m-1,m-1,mixer%Tinv,mixer%memory,ipiv,info)

         if (info .lt. 0) then
            write(7,*) 'ERROR: problem with mixer matrix inversion'
            write(7,*) 'STOP in genbro'
            ierr = 1
            return
         endif
         if (info .gt. 0) then
            write(7,*) 'Warning: T matrix in mixer is Singular'
         endif
c     adi
         call dgetri(m-1,mixer%Tinv,mixer%memory,ipiv,work,m-1,info)
         if (info .lt. 0) then
            write(7,*) 'ERROR: problem with mixer matrix inversion'
            write(7,*) 'STOP in genbro'
            ierr = 1
            return
         endif
         if (info .gt. 0) then
            write(7,*) 'Warning: T matrix in mixer is Singular'
         endif
         
c     update v according to the T matrix [Eq. (e) above]
         do i = 1, dim
            do j = 2,m
               v(i,m) = v(i,m) + mixer%Tinv(m-1,j-1)*df(i,j-1)
            enddo
         enddo
  
c     save xin, xout as xinold, xoutold, for the next iteration

         mixer%old1 = xin
         mixer%old2 = xout

c     perform first part of computation of x_new (put in xout). This
c     is the "B_1*f_n" fragment of Eqs. (a)+(f).

         do i = 1, dim
            xout(i) =  mixer%resid1(i)*(xout(i) - xin(i))
         enddo

c     update x_out according to the update of B_1 to B_n
         do j = 2, m
            vfac = (distdot(dim,v(1,j),1,mixer%old2,1
     1           ,parallel%procs_num,parallel%comm) - 
     2              distdot(dim,v(1,j),1,xin,1
     3           ,parallel%procs_num,parallel%comm) )*weight
            do i = 1, dim
               xout(i) = xout(i) + vfac*u(i,j)
            enddo
         enddo

c     complete the update of x_out

         xout = xin - xout

c     write u,v,df out to disk for the next iteration.
         open(10,file='bro.1.'//idstring,form='unformatted')
         open(11,file='bro.df.'//idstring,form='unformatted')
         rewind (10)
         rewind (11)
         do j = 2, m
            write(10) (u(i,j),i=1,dim)
         enddo
         do j = 2, m
            write(10) (v(i,j),i=1,dim)
         enddo
         if (m. ne. mixer%memory+1) then
            do j = 1, m-1
               write(11) (df(i,j),i=1,dim)
            enddo
         else 
            do j = 2, m-1
               write(11) (df(i,j),i=1,dim)
            enddo
         endif
         close(10)
         close(11)
 
      else
 
c     read the df arrays
         open(11,file='bro.df.'//idstring,form='unformatted')
         do j = 1, mixer%memory-1
            read(11) (df(i,j),i=1,dim)
         enddo
         close(11)

c     Update u,v,df for inverse Jacobian matrix
         do i = 1, dim
            dvin = xin(i) - mixer%old1(i)
            df(i,mixer%memory) = xout(i) - mixer%old2(i) - dvin
            u(i,blk+1) = dvin - mixer%resid1(i)*df(i,mixer%memory)
            v(i,blk+1) = zero
         enddo
c     update xin, xout to xinold, xoutold for next iteration
         mixer%old1 = xin
         mixer%old2 = xout

c     perform the B_1 fragment of Eq. (f) (see similar comment above)
         do i = 1, dim
            xout(i) =  mixer%resid1(i)*(xout(i) - xin(i))
         enddo

c     Shift the T matrix to make room for the new information
 
         do i = 1, mixer%memory-1
            do j = 1,mixer%memory-1
               mixer%T(i,j) = mixer%T(i+1,j+1)
            enddo
         enddo
 
c     Update the T matrix [Eq. (d)]
c     First the row and then the column
 
         do j= 2,mixer%memory+1
            vtemp = distdot(dim,df(1,mixer%memory),1,df(1,j-1),1
     1           ,parallel%procs_num,parallel%comm)*weight
            mixer%T(mixer%memory,j-1) = vtemp
            mixer%T(j-1,mixer%memory) = vtemp
         enddo

c     find the inverse of T
         do j = 1,mixer%memory
            do i = 1,mixer%memory
               mixer%Tinv(j,i) = mixer%T(j,i)
            enddo
         enddo
         ipiv = 0
c     adi
         call dgetrf(mixer%memory,mixer%memory,mixer%Tinv
     1        ,mixer%memory,ipiv,info)
         if (info .lt. 0) then
            write(7,*) 'Exiting: Error in mixer'
            stop
         endif
         if (info .gt. 0) then
            write(7,*) 'Warning: T matrix in mixer is Singular'
         endif
c     adi
         call dgetri(mixer%memory,mixer%Tinv,mixer%memory,ipiv,work,
     1        mixer%memory,info)
         if (info .lt. 0) then
            write(7,*) 'Exiting: Error in mixer'
            stop
         endif
         if (info .gt. 0) then
            write(7,*) 'Warning: T matrix in mixer is Singular'
         endif

c     update current v vector (stored in v(i,blk+1)
         do i = 1, dim
            do j = 1,mixer%memory
               v(i,blk+1) = v(i,blk+1) + mixer%Tinv(mixer%memory,j)
     1              *df(i,j)
            enddo
         enddo

c     find out how many times the u,v blocks of memory will be read,
c     using an m value appropriate for READING (iter-1). Each block
c     is stored in a different file (bro.1, bro.2,...) and read
c     separately. This conserved memory and minimizes writes to disk.

         nblk = (m-1)/(blk-1)+1
c     prepare the m value appropriate for further COMPUTING, iter
         m = iter
 
c     for each of time we access the block:
         do nn = 1, nblk
 
c     create the file name and open it
            write(fnam,20) nn
 20         format('bro.',i1,'.')
            open(10,file=fnam//idstring,form='unformatted')
            rewind(10)

c     read old u.v vectors
            if (nn .ne. nblk) then
               rsize = blk-1
            else
               rsize = iter-1-(nblk-1)*(blk-1)
            endif
            do j = 2, rsize
               read(10) (u(i,j),i=1,dim)
            enddo
            do j = 2, rsize
               read(10) (v(i,j),i=1,dim)
            enddo
            close(10)
 
c     find current u (stored in u(i,blk+1)
            do j = 2, rsize
               vfac = distdot(dim,v(1,j),1,df(1,mixer%memory),1
     1              ,parallel%procs_num,parallel%comm)*weight
               do i = 1, dim
                  u(i,blk+1) = u(i,blk+1) - vfac*u(i,j)
               enddo
            enddo

c     update x_out according to the update of B_1 to B_(n-1) 

            do j = 2, rsize
               vfac = (distdot(dim,v(1,j),1,mixer%old2,1
     1              ,parallel%procs_num,parallel%comm) - 
     2              distdot(dim,v(1,j),1,xin,1
     3              ,parallel%procs_num,parallel%comm) )*weight
               do i = 1, dim
                  xout(i) = xout(i) + vfac*u(i,j)
               enddo
            enddo

c     If this is the last block to be read, write last block of u, v
c     for the next iteration.

            if (nn .eq. nblk) then
               if (rsize .ne. blk-1) then
                  open(10,file=fnam//idstring,form='unformatted')
                  rewind (10)
                  do j = 2, rsize
                     write(10) (u(i,j),i=1,dim)
                  enddo
                  write(10) (u(i,blk+1),i=1,dim)
                  do j = 2, rsize
                     write(10) (v(i,j),i=1,dim)
                  enddo
                  write(10) (v(i,blk+1),i=1,dim)
                  close(10)
               else
                  write(fnam,20) nn+1
                  open(10,file=fnam//idstring,form='unformatted')
                  rewind(10)
                  write(10) (u(i,blk+1),i=1,dim)
                  write(10) (v(i,blk+1),i=1,dim)
                  close(10)
               endif
            endif
         enddo

c     write df vectors for next time
         open(11,file='bro.df.'//idstring,form='unformatted')
         do j = 2,mixer%memory
            write(11) (df(i,j),i=1,dim)
         enddo
         close(11)

c     update x_out according to the update of B_(n-1) to B_n (i.e.,
c     use the current u,v, which weren't in the loop before)

         vfac = (distdot(dim,v(1,blk+1),1,mixer%old2,1
     1        ,parallel%procs_num,parallel%comm) - 
     2        distdot(dim,v(1,blk+1),1,xin,1
     3        ,parallel%procs_num,parallel%comm) )*weight
         do i = 1, dim
            xout(i) = xout(i) + vfac*u(i,blk+1)
         enddo

c     complete the last fragment of Eq. (f)
         do i = 1, dim
            xout(i) = xin(i) - xout(i)
         enddo  
      endif

 99   continue
      deallocate(u)
      deallocate(v)
      deallocate(df)

      end subroutine genbro
c     ===============================================================
