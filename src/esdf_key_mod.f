c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Module to hold keyword list. this must be updated as
c     new keywords are brought into existence.
c
c     The 'label' is the label as used in calling the esdf routines
c     'typ' defines the type, with the following syntax. it is 3
c     characters long.
c     the first indicates:
c          i - integer
c          s - single
c          d - double
c          p - physical
c          t - string (text)
c          e - defined (exists)
c          l - boolean (logical)
c          b - block
c     the second is always a colon (:)
c     the third indicates the "level" of the keyword
c          b - basic
c          i - intermediate
c          e - expert
c          d - dummy
c
c     'Dscrpt' is a description of the variable. it should contain a
c     (short) title enclosed between *! ... !*, and then a more detailed
c     description of the variable.
c
c     ---------------------------------------------------------------
      module esdf_key

      implicit none
c     maximum number of kewords
      integer, parameter :: numkw = 200

      character (len=80)   :: kw_label(numkw)
      character (len=3)    :: kw_typ(numkw)
      character (len=3000) :: kw_dscrpt(numkw)

c     added by me (?)
      integer :: kw_index(numkw)

c now define the keywords

      data kw_label(1)    / 'restart_run' /
      data kw_typ(1)      / 'L:B' /
      data kw_dscrpt(1)   / '*! Inital run flag !*' /
      data kw_label(2)    / 'periodic_system' /
      data kw_typ(2)      / 'L:B' /
      data kw_dscrpt(2)   / '*!System type (confined or periodic)!*'/
      data kw_label(3)    / 'boundary_sphere_radius' /
      data kw_typ(3)      / 'P:B' /
      data kw_dscrpt(3)   / '*! Radius of boundary sphere !*' /
      data kw_label(4)    / 'grid_spacing' /
      data kw_typ(4)      / 'P:E' /
      data kw_dscrpt(4)   / '*! Grid spacing (h) !*' /
      data kw_label(5)    / 'expansion_order' /
      data kw_typ(5)      / 'I:B' /
      data kw_dscrpt(5)   / '*!finite difference expansion order!*'/
      data kw_label(6)    / 'states_num' /
      data kw_typ(6)      / 'I:B' /
      data kw_dscrpt(6)   / '*! number of states !*' /
      data kw_label(7)    / 'net_charges' /
      data kw_typ(7)      / 'P:E' /
      data kw_dscrpt(7)   / '*! net charge !*' / !-?
      data kw_label(8)    / 'fermi_temp' /
      data kw_typ(8)      / 'P:I' /
      data kw_dscrpt(8)   / '*! Fermi temp. !*' /
      data kw_label(9)    / 'max_iter' /
      data kw_typ(9)      / 'I:E' /
      data kw_dscrpt(9)   / '*! max iter. for self-consistency!*'/
      data kw_label(10)   / 'convergence_criterion' /
      data kw_typ(10)     / 'P:E' /
      data kw_dscrpt(10)  / '*! Convergence criterion !*' /
      data kw_label(11)   / 'diag_tolerance' /
      data kw_typ(11)     / 'D:E' /
      data kw_dscrpt(11)  / '*!  Diagonalization tolerance !*' /
      data kw_label(12)   / 'eigensolver' /
      data kw_typ(12)     / 'T:E' /
      data kw_dscrpt(12)  / '*! eigensolver - diagla or arpack !*' /
      data kw_label(13)   / 'Mixing_Method' /
      data kw_typ(13)     / 'T:B' /
      data kw_dscrpt(13)  / '*! Mixer type (Anderson,Broyden) !*' /
      data kw_label(14)   / 'mixing_param' /
      data kw_typ(14)     / 'D:B' /
      data kw_dscrpt(14)  / '*! Mixing parameter !*' /
      data kw_label(15)   / 'memory_param' /
      data kw_typ(15)     / 'I:B' /
      data kw_dscrpt(15)  / '*! Memory parameter (Broyden only) !*' /
      data kw_label(16)   / 'atom_types_num' /
      data kw_typ(16)     / 'I:E' /
      data kw_dscrpt(16)  / '*! Number of atom types !*' /
      data kw_label(17)   / 'atom_type' /
      data kw_typ(17)     / 'T:E' /
      data kw_dscrpt(17)  / '*! Type of the atom !*' /
      data kw_label(18)   / 'core_cutoff_radius' /
      data kw_typ(18)     / 'P:E' /
      data kw_dscrpt(18)  / '*!pseudopotential core cutoff radius!*'/
      data kw_label(19)   / 'potential_num' /
      data kw_typ(19)     / 'I:E' /
      data kw_dscrpt(19)  / '*! Number of potentials !*' /
      data kw_label(20)   / 'local_component' /
      data kw_typ(20)     / 'T:B' /
      data kw_dscrpt(20)  / '*! The local component (s,p or d) !*' /
      data kw_label(21)   / 'move_flag' /
      data kw_typ(21)     / 'I:I' /
      data kw_dscrpt(21)
     1     / '*!Which atom to move(all,some,first n)!*'/
      data kw_label(22)   / 'lattice_vector_scale' /
      data kw_typ(22)     / 'P:D' /
      data kw_dscrpt(22)  / '*! Unit for lattice vectors !*' /
      data kw_label(23)   / 'correlation_type' /
      data kw_typ(23)     / 'T:D' /
      data kw_dscrpt(23)  / '*! Correlation type !*' /
      data kw_label(24)   / 'ion_energy_diff' /
      data kw_typ(24)     / 'P:I' /
      data kw_dscrpt(24)  / '*! Ion energy diff. (only for "cc") !*'/

      data kw_label(26)   / 'Minimization' /
      data kw_typ(26)     / 'T:E' /
      data kw_dscrpt(26)
     1     / '*! Minimization type (none,simple,BFGS) !*'/
      data kw_label(27)   / 'movement_num' /
      data kw_typ(27)     / 'I:E' /
      data kw_dscrpt(27)  / '*! Number of movements !*' /
      data kw_label(28)   / 'force_min' /
      data kw_typ(28)     / 'P:I' /
      data kw_dscrpt(28)  / '*! Force minimum !*' /
      data kw_label(29)   / 'max_step' /
      data kw_typ(29)     / 'P:B' /
      data kw_dscrpt(29)  / '*! Max step!*' /
      data kw_label(30)   / 'min_step' /
      data kw_typ(30)     / 'P:E' /
      data kw_dscrpt(30)  / '*! Min step!*' /
      data kw_label(31)   / 'relax_restart' /
      data kw_typ(31)     / 'L:B' / 
      data kw_dscrpt(31)
     1     / '*! Flag for relaxation restarting from previous run!*' /
      data kw_label(32)   / 'molecular_dynamics' /
      data kw_typ(32)     / 'L:B' /
      data kw_dscrpt(32)  
     1     / '*! Flag for calculating molecular dynamics !*' /
      data kw_label(33)   / 'restart_mode' /
      data kw_typ(33)     / 'L:I' /
      data kw_dscrpt(33)  / '*! Flag for restart mode !*' /
      data kw_label(34)   / 'Cooling_Method' /
      data kw_typ(34)     / 'T:I' /
      data kw_dscrpt(34)  / '*!Cooling type (log,linear,stair)!*'/
      data kw_label(35)   / 'tinit' /
      data kw_typ(35)     / 'P:I' / 
      data kw_dscrpt(35)  / '*! Tinit !*' / ! - ?
      data kw_label(36)   / 't_final' /
      data kw_typ(36)     / 'P:E' / 
      data kw_dscrpt(36)  / '*! tfinal !*' / ! - ?
      data kw_label(37)   / 't_step' /
      data kw_typ(37)     / 'P:I' / 
      data kw_dscrpt(37)  / '*! tstep (for "stairstep" method)!*' / ! - ?
      data kw_label(38)   / 'time_step' /
      data kw_typ(38)     / 'P:I' /
      data kw_dscrpt(38)  / '*! Time step !*' /
      data kw_label(39)   / 'friction_coefficient' /
      data kw_typ(39)     / 'D:I' /
      data kw_dscrpt(39)  / '*! Friction coefficient !*' /
      data kw_label(40)   / 'step_num' /
      data kw_typ(40)     / 'I:E' /
      data kw_dscrpt(40)  / '*! Number of steps !*' /
      data kw_label(41)   / 'polarizability' /
      data kw_typ(41)     / 'L:B' /
      data kw_dscrpt(41)  / '*! Flag for polarizability !*' /
      data kw_label(42)   / 'electric_field' /
      data kw_typ(42)     / 'P:E' /
      data kw_dscrpt(42)  / '*! Applied electric field !*' /
      data kw_label(43)   / 'spin_polarization' /
      data kw_typ(43)     / 'L:E' /
      data kw_dscrpt(43)  / '*! Flag for spin polarization !*' /
      data kw_label(44)   / 'electron_per_orbital' /
      data kw_typ(44)     / 'B:E' /
      data kw_dscrpt(44)  / '*! # electrons per each orbital!*' /
      data kw_label(45)   / 'atom_coord' /
      data kw_typ(45)     / 'B:E' /
      data kw_dscrpt(45)  / '*! coordinates of atom!*'/
      data kw_label(46)   / 'coordinate_unit' /
      data kw_typ(46)     / 'T:D' /
      data kw_dscrpt(46)  / '*! Unit for atomic coordinates !*' /

      data kw_label(47)    / 'Output_Eigenvalues' /
      data kw_typ(47)      / 'L:B' /
      data kw_dscrpt(47)   / '*! Output eigenstate flag !*' /

      data kw_label(48)   / 'Save_Intermediate_Eigenvalues' /
      data kw_typ(48)     / 'L:B' /
      data kw_dscrpt(48)  /
     1     '*! Save info on eigen.dat between SCF interations!
     2      Good for Debug Mode !*'/

      data kw_label(49)   / 'cell_shape' /
      data kw_typ(49)     / 'B:E' /
      data kw_dscrpt(49)  / '*! length of sides of periodic box !*' /

      data kw_label(50)    / 'output_all_states' /
      data kw_typ(50)      / 'L:B' /
      data kw_dscrpt(50)   / '*! output flag for wfn.dat !*' /

      data kw_label(51)    / 'old_pseudopotential_format' /
      data kw_typ(51)      / 'L:B' /
      data kw_dscrpt(51)   / '*! read psp. files without header !*' /

      data kw_label(53)   / 'Save_Intermediate_Charge_Density' /
      data kw_typ(53)     / 'L:B' /
      data kw_dscrpt(53)  /
     1     '*!Save info on wfn.dat between SCF interations!*'/

      data kw_label(54)   / 'BFGS_number_corr' /
      data kw_typ(54)     / 'I:I' /
      data kw_dscrpt(54)  / '*!number of BFGS corrections!*' /

      data kw_label(56)    / 'old_interpolation_format' /
      data kw_typ(56)      / 'L:B' /
      data kw_dscrpt(56)   / 
     1      '*choose between interpolating Vps (old) and r*Vps (new) ' /

      data kw_label(57)   / 'Output_Level' /
      data kw_typ(57)     / 'I:I' /
      data kw_dscrpt(57)  /
     1     '*!level of output data in parsec.out!*' /

      data kw_label(58)   / 'Initial_diag_tolerance' /
      data kw_typ(58)     / 'D:I' /
      data kw_dscrpt(58)  /
     1     '*! Tolerance in initial (guess) diagonalization !*' /

      data kw_label(59)   / 'Subspace_buffer_size' /
      data kw_typ(59)     / 'I:I' /
      data kw_dscrpt(59)  /
     1     '*! Number of additional states for each representation !*' /

      data kw_label(60)   / 'olddat_style' /
      data kw_typ(60)     / 'L:B' /
      data kw_dscrpt(60)  / '*! Output wavefunctions to old.dat !*' /

      data kw_label(61)   / 'alpha_filter' /
      data kw_typ(61)     / 'D:I' /
      data kw_dscrpt(61)  /
     1     '*!alpha parameter in Fourier filtering!*' /

      data kw_label(62)   / 'beta1_filter' /
      data kw_typ(62)     / 'D:I' /
      data kw_dscrpt(62)  /
     1     '*!beta_1 parameter in Fourier filtering!*' /

      data kw_label(63)   / 'core_filter' /
      data kw_typ(63)     / 'D:I' /
      data kw_dscrpt(63)  /
     1     '*!alpha parameter in core charge Fourier filtering!*' /

      end module esdf_key
c     ===============================================================
