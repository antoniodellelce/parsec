c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine calculates the force contribution due to the
c     local component of the local pseudopotential, and adds it to
c     the ion-ion force component. 
c
c     The local pseudopotential force has three components - one 
c     involving it directly - int_{(dvion)/dr)*rho}, the second
c     relating to core-correction - int{(drho_core}/dr*vxc}, the
c     third correcting for deviations from self-consistency - 
c     int{(drho_atomic)/dr *(Vhxc_new-Vhxc_old).
c     The first two terms are given in, e.g., Eq. (9) of Kronik et
c     al., J. Chem. Phys., 115, 4322 (2001). 
c     The third term is explained in Chan et al.,  Phys. Rev. B 47,
c     4771 (1993).
c
c     clust%force(1:3,:) has components of force, on each atom.
c     on input: force due to ion-ion interactions
c               (coming from forceion.f);
c     on output: input force + force due to the local component of 
c               the pseudopotential.
c
c     WARNING: this subroutine is not fully parallelized!
c
c     ---------------------------------------------------------------
      subroutine forcloc(clust,grid,pot,p_pot,parallel
     1     ,rho,ipr,OldInpFormat)

      use constants
      use cluster_module
      use grid_module
      use potential_module
      use pseudo_potential_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     grid related data
      type(grid_data), intent(in) :: grid
c     potential related data
      type (potential), intent(in) :: pot
c     pseudo_potential related data
      type (pseudo_potential), intent(in) :: p_pot
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel

c     charge density on the 3-d grid (total charge only, 
c     even if spin-polarized)
      real(dp), intent(in) :: rho(parallel%mydim)
c     print flag
      integer, intent(in) :: ipr
c     flag for interpolation of pseudopotential
      logical, intent(in) :: OldInpFormat
c
c     Work variables:
c
c     hcub = h**3
      real(dp) :: hcub

c     temporary variables defined at the beginning of the loop over
c     ity
      integer jcore,npoint
      real(dp) ::  zcore,rmin,rlarge,ainv,binv
c     temporary holders of pseudopotential, atomic charge, 
c     core charge derivatives
      real(dp) :: dv,drhoc,drhoat
c     distance between grid point and atom, its square, and
c     projections
      real(dp) :: r1, r1sq, x1, y1, z1, invr1
c     index of radial grid, distance to previous grid point
      integer ind
      real(dp) :: delinv

c     force variables:
c     term explicitly involving the local pseudopotential, and
c     components
      real(dp) :: fl,flx,fly,flz
c     term resulting from core-correction, and components
      real(dp) :: fcore,fcorex,fcorey,fcorez
c     term resulting from non-consistency correction, and components
      real(dp) :: fc,fcx,fcy,fcz
c     counters
      integer i,ia,ity,iat,igrid
c     allocation check
      integer alcstat

c     distributed work array:
      real(dp) :: vtmp(parallel%mydim)

c     work arrays:
c     difference of Vhxc from current iteration and the previous one
c     (average of up and down components, if spin-polarized
c     computation)
      real(dp), allocatable :: dvhxc(:)
c     average of spin-up and spin-down vxc. equal to just vxc in the 
c     absence of spin-polarization; needed only if core-correction is
c     used.
      real(dp), allocatable :: vxcav(:)
c     charge density on the 3-d grid (total charge only, 
c     even if spin-polarized)
      real(dp), allocatable :: rho_t(:)

c     ---------------------------------------------------------------

      hcub = grid%hcub

c     prepare difference of Hartree-exchange-correlation with respect
c     to previous iteration (for accelerating force convergence)
      do i = 1, parallel%mydim
         vtmp(i) = (pot%vhart(i) + pot%vxc(i,1)) - pot%vhxcold(i,1)
      enddo
c     if spin-polarized, average the spin-up and spin-down dvhxc
      if (pot%nspin .eq. 2) then
         do i = 1, parallel%mydim
            vtmp(i)=vtmp(i)+(pot%vhart(i)+pot%vxc(i,2))-
     1           pot%vhxcold(i,2)
            vtmp(i)=half*vtmp(i)
         enddo
      endif
      call collect_function(parallel,vtmp)
      if (parallel%iammaster) then
         allocate(dvhxc(grid%nwedge),stat=alcstat)
         call alccheck('dvhxc',grid%nwedge,alcstat)
         call dcopy(grid%nwedge,parallel%ftmp,1,dvhxc,1)
      endif

c     prepare vxc that is average of up and down vxc
      if (pot%nspin .eq. 1) then
         do i = 1, parallel%mydim
            vtmp(i) = pot%vxc(i,1)
         enddo
      else
         do i = 1, parallel%mydim
            vtmp(i) = half*(pot%vxc(i,1) + pot%vxc(i,2))
         enddo
      endif
      call collect_function(parallel,vtmp)
      if (parallel%iammaster) then
         allocate(vxcav(grid%nwedge),stat=alcstat)
         call alccheck('vxcav',grid%nwedge,alcstat)
         call dcopy(grid%nwedge,parallel%ftmp,1,vxcav,1)
      endif

c     collect charge density
      call collect_function(parallel,rho)
      if (parallel%iammaster) then
         allocate(rho_t(grid%nwedge),stat=alcstat)
         call alccheck('rho_t',grid%nwedge,alcstat)
         call dcopy(grid%nwedge,parallel%ftmp,1,rho_t,1)
      endif

c     from now on, only master PE works
      if (.not. parallel%iammaster) return

c     header, if printing
      if (ipr .ge. 2) then
         write(7,*)
     1        'Format is: atom #, followed by x,y,z components of'
         write(7,*)
     1        'ion, self-consistency, and core-correction forces'
      endif

c     initialize atom counter, ia
      ia = 0
c     for all atom types...
      do ity = 1, clust%type_num
c     define temporary variables
         jcore = p_pot%icore(ity)

         npoint = p_pot%ns(ity)
c     factor "two" comes from unit change (Hartree -> Rydberg)
         zcore  = -two*p_pot%zion(ity)
         rmin = p_pot%rs(2,ity) 
         rlarge = p_pot%rs(npoint-6,ity)

         ainv = one/p_pot%par_a(ity)
         binv = one/p_pot%par_b(ity)
c     for all atoms within each type
         do iat   = 1, clust%natmi(ity)
c     update atom counter
            ia = ia + 1
c     initialize all force accumulators (and core charge derivative)
c     to zero
            flx = zero
            fly = zero
            flz = zero
            fcx = zero
            fcy = zero
            fcz = zero
            fcorex = zero
            fcorey = zero
            fcorez = zero
            drhoc = zero
c     for all grid points...
            do igrid = 1,grid%ndim
c     calculate distance from grid point to atom
               x1 = (grid%shift(1) + grid%fx(igrid))*grid%step(1) -
     1              clust%xatm(ia)
               y1 = (grid%shift(2) + grid%fy(igrid))*grid%step(2) -
     1              clust%yatm(ia)
               z1 = (grid%shift(3) + grid%fz(igrid))*grid%step(3) -
     1              clust%zatm(ia)
               r1sq = x1*x1+y1*y1+z1*z1
               r1 = sqrt(r1sq)
               invr1 = one/r1

c     if distance to atom negligible, all forces are zero all 
c     charge and potential derivatives vanish, so skip.
               if(r1.lt.rmin) cycle
c
c     if distance to atom larger than the largest value in 
c     pseudopotential file, zion/r is used as the potential. 
               if (r1.ge.rlarge) then
                  dv = -zcore/r1sq
                  drhoat = zero 
               else
c     define index variables for interpolation between radial grid
c     points
                  ind = idint(binv*log(one+ainv*r1)) + 1
                  delinv=one/(p_pot%rs(ind+1,ity)-p_pot%rs(ind,ity))

                  if (OldInpFormat ) then
                     dv =((p_pot%rs(ind+1,ity)-r1)*p_pot%dvion(ind,ity)
     1                 +(r1-p_pot%rs(ind,ity))*p_pot%dvion(ind+1,ity))*
     2                 delinv
                  else
                     dv =( (p_pot%rs(ind+1,ity)-r1)
     1                      *p_pot%dvion(ind,ity)*
     2                       p_pot%rs(ind,ity)*p_pot%rs(ind,ity)
     3                  + (r1-p_pot%rs(ind,ity)) 
     4                      *p_pot%dvion(ind+1,ity)
     5                      *p_pot%rs(ind+1,ity)*p_pot%rs(ind+1,ity)
     6                     )*delinv*invr1*invr1
                  endif
                  drhoat = ((p_pot%rs(ind+1,ity)-r1) * 
     1                 p_pot%drhodr(ind,ity) + (r1-p_pot%rs(ind,ity))* 
     2                 p_pot%drhodr(ind+1,ity)) * delinv
               endif
c
c     interpolate core charge...
               if (r1.ge.rlarge) then
                  drhoc = zero
               else
                  ind = idint(binv*log(one+ainv*r1)) + 1
                  delinv = one/(p_pot%rs(ind+1,ity)-p_pot%rs(ind,ity))

                  drhoc = ((p_pot%rs(ind+1,ity)-r1)*
     1                 p_pot%ddenc(ind,ity)+ (r1-p_pot%rs(ind,ity))
     2                 *p_pot%ddenc(ind+1,ity)) * delinv
               endif
c
c     update force components according to the formulas given in the
c     beginning
c
               fl = dv*rho_t(grid%rindex(igrid))/r1
               fc = drhoat*dvhxc(grid%rindex(igrid))/r1
               flx = flx + fl*x1
               fly = fly + fl*y1
               flz = flz + fl*z1
               fcx = fcx + fc*x1
               fcy = fcy + fc*y1
               fcz = fcz + fc*z1
               if (jcore .gt. 0) then
                  fcore = drhoc*vxcav(grid%rindex(igrid))/r1
                  fcorex = fcorex + fcore*x1
                  fcorey = fcorey + fcore*y1
                  fcorez = fcorez + fcore*z1
               endif
            enddo               ! igrid = 1,grid%ndim

c     report forces explicitly, if asked (only for debugging)
            if (ipr .ge. 2) then
               write(7,20) ia,flx*hcub,fly*hcub,flz*hcub,fcx*hcub,
     1              fcy*hcub,fcz*hcub,fcorex*hcub,fcorey*hcub,
     2              fcorez*hcub
            endif

c     update total force arrays (adding the three components to the
c     ion-ion forces)
            clust%force(1,ia) = clust%force(1,ia) + (flx+fcx)*hcub
            clust%force(2,ia) = clust%force(2,ia) + (fly+fcy)*hcub
            clust%force(3,ia) = clust%force(3,ia) + (flz+fcz)*hcub
            if (jcore .gt. 0) then
               clust%force(1,ia) = clust%force(1,ia) + fcorex*hcub
               clust%force(2,ia) = clust%force(2,ia) + fcorey*hcub
               clust%force(3,ia) = clust%force(3,ia) + fcorez*hcub
            endif
         enddo                  ! iat = 1, clust%natmi(ity)
      enddo                     ! ity = 1, clust%type_num

 20   format('at',i4,1x,3(f11.6,1x),1x,3(f11.6,1x),/,7x,3(f11.6,1x))

      if (ipr .ge. 2) write(7,*)

      deallocate(dvhxc, vxcav, rho_t)

      end subroutine forcloc
c     ===============================================================
