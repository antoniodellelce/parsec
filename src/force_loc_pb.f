c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Computes the local potential contribution to the Hellmann-
c     Feynman forces. It uses the previous charge density (vsc_chg)
c     and potential (vsc). Calculations are done in reciprocal
c     space.
c
c     Adapted from plane-wave programs written by S. Froyen and
c     J. L. Martins.
c
c     ---------------------------------------------------------------
      subroutine force_loc_pb(clust,pbc,vsc,vsc_chg)

      use constants
      use cluster_module
      use pbc_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     pbc related data
      type (pbc_data), intent(inout) :: pbc
c     charge density and potential
      complex(dpc), dimension(pbc%nstar), intent(in) :: vsc, vsc_chg
c
c     Work variables:
c
      integer i,j,k,l,natomi,kadd,jj,mstarj,jaa,ntype
      integer natom(clust%type_num)
      real(dp) :: fi,exp1,vdgr,vdgi
      real(dp) :: floc(3,clust%atom_num,clust%type_num)
      real(dp), dimension(clust%atom_num) :: xxatm,yyatm,zzatm
      complex(dpc) :: vds

c     ---------------------------------------------------------------
      natom = clust%natmi     
      ntype = clust%type_num

      floc = zero

c     loop over atomic types
      do i=1,ntype
c     loop over stars
         kadd = pbc%ng + 1
         do jj=2,pbc%nstar
            j = pbc%nstar - jj + 2
            vds =  conjg( pbc%vql(i,j) * vsc_chg(j) 
     1           + vsc(j) * pbc%dnc(i,j))
c     loop over g vectors in star
            mstarj = pbc%mstar(j)
            do k=1,mstarj
               kadd = kadd - 1
               vdgr = real(vds) * real(pbc%phase(kadd)) - 
     1              aimag(vds) * aimag(pbc%phase(kadd))
               vdgi = pbc%conj(kadd) * (aimag(vds)
     1              * real(pbc%phase(kadd)) +real(vds)
     2              * aimag(pbc%phase(kadd)))
c     loop over atoms of same type
               natomi = natom(i)
               do l=1,natomi
                  fi = dot_product(real(pbc%kgv(:,kadd),dp)
     1               ,pbc%rat(:,l,i))
                  exp1 = sin(fi)*vdgr - cos(fi)*vdgi
c     add to forces
                  floc(:,l,i) = floc(:,l,i) +
     1                 real(pbc%kgv(:,kadd),dp) * exp1
               enddo
            enddo
         enddo
      enddo

      jaa = 0
      do i=1,ntype
         natomi = natom(i)
         do j=1,natomi
            jaa = jaa + 1
            do k=1,3
               clust%force(k,jaa) = clust%force(k,jaa) + 
     1              two * floc(k,j,i) * twopi / pbc%box_size(k)
            enddo
         enddo
      enddo

      end subroutine force_loc_pb
c     ===============================================================
