c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c                  Anderson mixing scheme
c
c     This suborutine mixes the old and new potential to create a new
c     guess for the potential in the self-consistent cycle.
c     D. G. Anderson, J. Assoc. Computing Machinery, Vol12, p547(1965).
c     V. Eyert, J. Comput. Phys. Vol124,P271(1996)
c
c     NOTE: the mixed potential is returned as xin, BUT to be
c     consistent with the old anderson.f and parsec, xout is also
c     updated as the new potential.
c
c     Author: Lingzhu Kong(2005)
c    
c     ---------------------------------------------------------------
      subroutine anderson_mix(mixer,parallel,iter,nrep,xin,xout,ierr)

      use constants
      use mixer_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     mixer related data
      type (mixer_data), intent(inout) :: mixer
c     parallel computation related data
      type (parallel_data), intent(in) :: parallel

c     iteration number
      integer, intent(in) :: iter
c     order of the reduced group
      integer,intent(in) :: nrep
c     error flag
      integer,intent(out) :: ierr

c     old potential
      real(dp), intent(inout) :: xin(mixer%ndim * mixer%nspin)

c     new potentials 
      real(dp), intent(inout) :: xout(mixer%ndim * mixer%nspin)
c
c     Work variables:
c
c     Residual vectors F[n] = ( xout - xin ) (Eq(2.1) in Eyert)
      real(dp), allocatable :: f(:,:)

c     Mixed input (Eq(4.1) in Eyert)
      real(dp), allocatable :: aver(:)

c     Mixed residual(Eq(4.2) in Eyert)
      real(dp), allocatable :: faver(:)

c     Coefficient matrix in (4.3)(Eyert)
      real(dp)   b(mixer%memory,mixer%memory)

c     Right hand side(RHS) vector in (4.3)(Eyert)
      real(dp)   b0(mixer%memory,1)
c 
c     dimension of the potential vector
      integer   dim
c     multiplicity of grid points
      real(dp)  weight
c     counters, temporary variables
      integer    i,j,k,anderi,ipiv(mixer%memory),info,alcstat
      real(dp) :: xsum

c     ---------------------------------------------------------------
c
      dim = mixer%ndim * mixer%nspin

      allocate(f (dim,mixer%memory+1),stat=alcstat)
      call alccheck('f ',dim * (mixer%memory+1), alcstat)

      allocate(aver (dim),stat=alcstat)
      call alccheck('aver ',dim , alcstat)

      allocate(faver (dim),stat=alcstat)
      call alccheck('faver ',dim , alcstat)

      anderi = min(iter,mixer%memory+1)

      weight = real(nrep,dp)

      if(anderi .eq. 1)then
c     first iteration - previous information not available yet!
         do k = 1,dim

c     store for next iteration
            mixer%xinold(k,1) = xin(k)
            mixer%xoutold(k,1) = xout(k)

c     next guess obtained by pure linear mixing
            xin(k) = mixer%param* xout(k) + (one - mixer%param) * xin(k)
            xout(k) = xin(k)
         enddo

      else

c     residual from current iteration 
         do k = 1,dim
            f(k,1) = xout(k) - xin(k)
         enddo

c     resudual from previous iterations
         do j = 2,anderi
            do k = 1,dim
               f(k,j) = mixer%xoutold(k,j-1) - mixer%xinold(k,j-1)
            enddo
         enddo
c
c     Coefficient matirx b and RHS vector b0 initialised as zero
         b = zero
         b0 = zero
 
c     update matrix b from redidual vectors f
         do j = 1,anderi-1
            do i = 1,j
               xsum = zero
               do k = 1,dim
                  xsum = xsum+(f(k,1)-f(k,i+1)) * (f(k,1)-f(k,j+1))
               enddo
               call psum(xsum,1,parallel%procs_num,parallel%comm)
               b(i,j) = xsum*weight

               if(i .ne. j)b(j,i) = b(i,j)
            enddo
         enddo
c     End of the update of matrix b

c     update vectors b0
         do i = 1,anderi-1
            xsum = zero
            do k = 1,dim
               xsum = xsum+(f(k,1)-f(k,i+1)) * f(k,1)
            enddo
            call psum(xsum,1,parallel%procs_num,parallel%comm)
            b0(i,1) = xsum * weight
         enddo
c     End  of the update of b0

c     Solve b * X = b0 for X, overwrite b0. vector X is the
c     coefficients of the previous iterations that enter the new
c     guess potential.
         info = 0
         call dgesv( anderi-1,1,b,mixer%memory,ipiv,b0,
     1        mixer%memory, info )
 
         if( info .ne. 0)then
            ierr = 1
            write(7,*)'erro in dgesv'
            return
         endif

c     mixed input and mixed residual 
         do k = 1,dim
            aver(k) = xin(k)
            faver(k) = f(k,1)
            do i = 1,anderi-1
               aver(k) = aver(k) + b0(i,1)*( mixer%xinold(k,i)-xin(k) )
               faver(k) = faver(k) + b0(i,1)*( f(k,i+1) - f(k,1) )
            enddo
         enddo

         do k = 1,dim

c     Store the old information for next mxing
            if( anderi .eq. mixer%memory+1 )then
               if (anderi .gt. 2)then
                  do i = anderi-2,1,-1
                     mixer%xinold(k,i+1) = mixer%xinold(k,i)
                     mixer%xoutold(k,i+1) = mixer%xoutold(k,i)
                  enddo
               endif

            else
               do i = anderi-1,1,-1
                  mixer%xinold(k,i+1) = mixer%xinold(k,i)
                  mixer%xoutold(k,i+1) = mixer%xoutold(k,i)
               enddo
            endif

            mixer%xinold(k,1) = xin(k)
            mixer%xoutold(k,1) = xout(k)
c     end of store

c     Linear mix the mixed input and mixed residual for the new
c     potential
            xin(k) = aver(k) + mixer%param*faver(k)
            xout(k) = xin(k)

         enddo                  ! k = 1,dim

      endif

      deallocate( f , aver , faver)

      end subroutine anderson_mix
c     ===============================================================
