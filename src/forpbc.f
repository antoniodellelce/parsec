c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     Computes the local potential contribution to the Hellman-Feynman
c     forces. this is done in reciprocal space taking advantage of
c     its "short-ranged nature". the charge density and exchange and
c     correlation potential are first transferred to reciprocal
c     space by Fast Fourier Transform.
c
c     WARNING: this subroutine is not fully parallelized!
c
c     ---------------------------------------------------------------
      subroutine forpbc(clust,elec_st,grid,pbc,parallel,rho_d,vxc_d)

      use constants
      use cluster_module
      use electronic_struct_module
      use grid_module
      use pbc_module
      use parallel_data_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(inout) :: clust
c     electronic structure
      type (electronic_struct), intent(inout) :: elec_st
c     grid related data
      type (grid_data), intent(in) :: grid
c     pbc related data
      type (pbc_data), intent(inout) :: pbc
c     parallel computation related data
      type (parallel_data), intent(inout) :: parallel

c     distributed electron density and exchange-correlation potential
c     (passed outside the structure to overcome a bug with the IBM compiler)
      real(dp), intent(in) :: rho_d(parallel%mydim)
      real(dp), intent(in) :: vxc_d(parallel%mydim,elec_st%nspin)
c
c     Work variables:
c
c     allocation check
      integer alcstat
c     exchange-correlation potential collected to master processor
      real(dp), allocatable :: vxc(:)
c     total charge density collected to master processor
      real(dp), allocatable :: rho(:)

      integer i,ii,j,jj,k,iadd,ncount,js
      real(dp) :: tmpr,tmpi
      complex(dpc) :: ztmp,fi_s
      complex(dpc), dimension(:), allocatable :: vsc3,vsc,vsc_chg

c     ---------------------------------------------------------------

c     Collect vxc over all processors and add spin components
      if (parallel%iammaster) then
         allocate(vxc(grid%nwedge),stat=alcstat)
         call alccheck('vxc',grid%nwedge,alcstat)
         vxc = zero
         allocate(rho(grid%nwedge),stat=alcstat)
         call alccheck('rho',grid%nwedge,alcstat)
         rho = zero
      endif

      do ii = 1, elec_st%nspin
         call collect_function(parallel,vxc_d(1,ii))
         if (parallel%iammaster) 
     1        call daxpy(grid%nwedge,one,parallel%ftmp,1,vxc,1)
      enddo
c     Collect charge density over all processors
      do ii = 1, elec_st%nspin
         call collect_function(parallel,rho_d)
         if (parallel%iammaster) 
     1        call dcopy(grid%nwedge,parallel%ftmp,1,rho,1)
      enddo

c     from now on, only master PE works
      if (.not. parallel%iammaster) return

      allocate(vsc3(pbc%maxdfft),stat=alcstat)
      call alccheck('vsc3',pbc%maxdfft,alcstat)
      allocate(vsc(pbc%nstar),stat=alcstat)
      call alccheck('vsc',pbc%nstar,alcstat)
      allocate(vsc_chg(pbc%nstar),stat=alcstat)
      call alccheck('vsc_chg',pbc%nstar,alcstat)

      vsc3 = zero
      pbc%vscr4 = zero

c     remove the mapping from charge density and exchange-correlation 
c     potential in order to perform the FFTs
      do k = pbc%mz, pbc%mz + pbc%n3 - 1
         do j = pbc%my, pbc%my + pbc%n2 - 1
            do i = pbc%mx, pbc%mx + pbc%n1 - 1
               iadd = ((k-pbc%mz)*pbc%n2 + j-pbc%my)*pbc%n1 + i-pbc%mx+1
               jj = grid%rindex( grid%indexg(i,j,k) )
               pbc%vscr4(iadd) = vxc(jj)/real(2*elec_st%nspin,dp)
               vsc3(iadd) = rho(jj) * pbc%vcell
            enddo
         enddo
      enddo

c     perform the FFTs
      call cfftw (pbc%vscr4,pbc%n1,pbc%n2,pbc%n3,1)
      call cfftw (vsc3,pbc%n1,pbc%n2,pbc%n3,1)

c     do some wraparound
      vsc = cmplx(zero,zero,dp)
      vsc_chg = cmplx(zero,zero,dp)
      do jj=1,pbc%ng
         js = pbc%inds(jj)
         i = 1 + pbc%kgv(1,jj)
         if (i .le. 0) i=pbc%n1+i
         j = 1 + pbc%kgv(2,jj)
         if (j .le. 0) j=pbc%n2+j
         k = 1 + pbc%kgv(3,jj)
         if (k .le. 0) k=pbc%n3+k
         call get_address(pbc,i,j,k,iadd,fi_s)

         ztmp = pbc%vscr4(iadd) * conjg(fi_s)
         tmpr = real(ztmp,dp)
         tmpi = pbc%conj(jj)*aimag(ztmp)
         ztmp = cmplx(tmpr,tmpi,dp)/real(pbc%mstar(js),dp)
         vsc(js) = vsc(js) + ztmp

         ztmp = vsc3(iadd) * conjg(fi_s)
         tmpr = real(ztmp,dp)
         tmpi = pbc%conj(jj)*aimag(ztmp)
         ztmp = cmplx(tmpr,tmpi,dp)/real(pbc%mstar(js),dp)
         vsc_chg(js) = vsc_chg(js) + ztmp * pbc%phase(jj)
      enddo

c     finally, calculate the local force on ions
      call force_loc_pb(clust,pbc,vsc,vsc_chg)

      if (allocated(vsc3)) deallocate(vsc3)
      if (allocated(vsc)) deallocate(vsc)
      if (allocated(vsc_chg)) deallocate(vsc_chg)
      if (allocated(vxc)) deallocate(vxc)
      if (allocated(rho)) deallocate(rho)

      end subroutine forpbc
c     ===============================================================
