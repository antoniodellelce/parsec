c     ===============================================================
c
c     Copyright (C) 2005 Finite Difference Research Group
c     This file is part of parsec, http://www.ices.utexas.edu/parsec/
c
c     This subroutine calculates various quantities associated with
c     the non-local components of the pseudopotential. They are used
c     later for determining this non-local contribution to the
c     Hamiltonian matrix and to the calculation of non-local forces.
c
c     This subroutine handles both the case of a periodic system
c     and a confined system.
c
c     ---------------------------------------------------------------
      subroutine matnloc(clust,grid,p_pot,nloc_p_pot,pbc,ierr)

      use constants
      use cluster_module
      use grid_module
      use pseudo_potential_module
      use non_local_psp_module
      use pbc_module
      implicit none
c
c     Input/Output variables:
c
c     the cluster
      type (cluster), intent(in) :: clust
c     grid related data
      type (grid_data), intent(in) :: grid
c     pseudo_potential related data
      type (pseudo_potential), intent(in) :: p_pot
c     non local pseudo_potential related data
      type (nonloc_pseudo_potential), intent(inout) :: nloc_p_pot
c     periodic boundary conditions data
      type (pbc_data), intent(inout) :: pbc
c     error flag
      integer, intent(out) :: ierr
c
c     Work variables:
c
c     spherical harmonics coefficients
      real(dp) :: c0,c1,c21,c22,c23, c31, c32, c33, c34, c35
c     temporary storage variables
      integer npoint,npt,n1
      real(dp) :: fact1,rinv,vwr1,x,y,z,xa,ya,za
      real(dp), dimension(clust%type_num) :: rc, ainv, binv, rrmin
c     number of replicas of the periodic cell to be used in the
c     construction of non-local spheres around each atom
c     nreplica = 0 if .not. pbc%is_on
      integer nreplica
c     temporary counter of number of non-local points around each
c     atom
      integer nloc
c     loop counters 
      integer i,iat,ity,ja,lm,lp,mg,icellx,icelly,icellz
      real(dp) :: rr_tmp

c     ---------------------------------------------------------------
c
c     Define parameters for periodic boundary conditions.
      if (pbc%is_on) then
         nreplica = 1
      else
         nreplica = 0
      endif
c
c     Define spherical harmonics coefficients - for s, p, and d
c     formulae, see, e.g., pp. 11-12 in W. A. Harrison, "Electronic
c     Structure and the Properties of Solids: The Physics of the
c     Chemical Bond". For f formulae, see, e.g., 
c     S. F. A. Kettle, "Physical Inorganic Chemistry", p. 244.
c
c     1/(2*sqrt(pi)) for s
      c0  = half / sqrt(pi)
c     sqrt(3/(4*pi)), for px, py, pz 
      c1  = sqrt(0.75d0/pi)
c     sqrt(15/(4*pi)), dxy, dzy, dzx 
      c21 = sqrt(3.75d0/pi)
c     sqrt(5/(16*pi)), for dz^2 
      c22 = sqrt(0.3125d0/pi)
c     sqrt(15/(16*pi)), for dx^2-y^2 
      c23 = sqrt(0.9375d0/pi)
c     sqrt(7/(16*pi)), for f_z(5z^2-3r^2)
      c31 = sqrt(0.4375d0/pi)
c     sqrt(21/(32*pi)), for f_x(5z^2-r^2),f_y(5z^2-r^2)
      c32 = sqrt(0.65625d0/pi)
c     sqrt(105/(16*pi)), for f_z(x^2-y^2)
      c33 = sqrt(6.5625d0/pi)
c     sqrt(105/(4*pi)), for f_xyz
      c34 = sqrt(26.25d0/pi)
c     sqrt(35/(32*pi)), for f_x(x^2-3y^2),f_y(y^2-3x^2)
      c35 = sqrt(1.09375d0/pi)
c
c     Determine the spatial extent of the non-local contribution on
c     the radial grid and translate it to the size of the non-local
c     block around each atom.
c
c     initialize the atom counter, ja.
      ja = 0
c     for each atom type 
      do ity = 1, clust%type_num
c     update temporary variables for number of points in
c     pseudopotential, npoint, (inverse) radial grid parameters, ainv
c     and binv, smallest distance from the atom recorded in
c     psedupotential file (rrmin) and the smaller between the core
c     -cutoff radius and the largest radius still available in the
c     pseudopotential file, rc. 
         npoint = p_pot%ns(ity)
         ainv(ity) = one / p_pot%par_a(ity)
         binv(ity) = one / p_pot%par_b(ity)
         rrmin(ity) = p_pot%rs(2, ity)
         rc(ity) = p_pot%rcore(ity)
         if (p_pot%rs(npoint-1, ity) .lt .rc(ity)) then
            rc(ity) = p_pot%rs(npoint-1, ity)
         endif
c
c     for each atom inside a given type...
c
c     update atom # counter, ja, and temporary variables for storing
c     the atomic position, xa, ya, za.
         do iat = 1, clust%natmi(ity)
            ja = ja + 1
            xa = clust%xatm(ja)
            ya = clust%yatm(ja)
            za = clust%zatm(ja)
c
c     Check if the grid point is within the core-cutoff radius around
c     the atom. If it isn't, it has no non-local contribution because
c     outside the core all pseudopotential components are equal.
c     If we are dealing with periodic boundary conditions, we
c     also need to see whether a given grid point is inside the
c     core-cutoff radius if shifted by the size of the box in the
c     +x,-x,+y,-y,+z,-z directions (and any combination thereof). In
c     theory, one would need to check for shifts of twice the size of
c     the box, three times the size of the box, etc. In practice this
c     is NOT needed, because it would imply a core cutoff radius
c     larger than the box - which makes for a very bad calculation
c     anyway.

c     reset counter of non-local points per given atom
            nloc = 0
c     for each grid point in the full grid...
            do mg = 1, grid%ndim
c     allow each point to be shifted by the length of the box in each
c     direction
               do icellx = -nreplica,nreplica
                  do icelly = -nreplica,nreplica
                     do icellz = -nreplica,nreplica
c     compute distance between point (or its replica if PBC) and atom
c     and increase nloc if it is inside the non-local range.
                        x = (grid%shift(1) + grid%fx(mg))*grid%step(1)
     1                       - xa + real(icellx,dp)*pbc%box_size(1)
                        y = (grid%shift(2) + grid%fy(mg))*grid%step(2)
     1                       - ya + real(icelly,dp)*pbc%box_size(2)
                        z = (grid%shift(3) + grid%fz(mg))*grid%step(3)
     1                       - za + real(icellz,dp)*pbc%box_size(3)
                        rr_tmp = sqrt(x*x+y*y+z*z)
                        if (rr_tmp .le. rc(ity)) nloc = nloc + 1
                     enddo
                  enddo
               enddo
            enddo
c     update array of total number of non-local points per atom and
c     maximum size of non-local block associated with any atom
            nloc_p_pot%nlatom(ja) = nloc
         enddo                  ! iat = 1, clust%natmi(ity)
      enddo                     ! ity = 1, clust%type_num
      call nonloc_pseudo_pot_set_maxnloc (nloc_p_pot)

      ja = 0
c     for each atom type
      do ity = 1, clust%type_num
         do iat = 1, clust%natmi(ity)
            ja = ja + 1
            xa = clust%xatm(ja)
            ya = clust%yatm(ja)
            za = clust%zatm(ja)
            nloc = 0
            do mg = 1, grid%ndim
               do icellx = -nreplica,nreplica
                  do icelly = -nreplica,nreplica
                     do icellz = -nreplica,nreplica
c     Compute distance between point (or its replica if PBC) and atom.
c     If the point is inside non-local range:
c     1. Increase the non-local point counter, nloc, by one.
c     Update the four output arrays, indw, ratom, indr, and ffcc,
c     with the grid point, its distance from the atom, the closest
c     radial grid point, and the fractional distance to the next grid
c     point (for extrapolation), respectively.
c     2. Store the cartesian coordinates of this point in pbc%rnloc,
c     without bringing it back to the initial cell (if PBC).
                        x = (grid%shift(1) + grid%fx(mg))*grid%step(1)
     1                       - xa + real(icellx,dp)*pbc%box_size(1)
                        y = (grid%shift(2) + grid%fy(mg))*grid%step(2)
     1                       - ya + real(icelly,dp)*pbc%box_size(2)
                        z = (grid%shift(3) + grid%fz(mg))*grid%step(3)
     1                       - za + real(icellz,dp)*pbc%box_size(3)
                        rr_tmp = sqrt(x*x+y*y+z*z)
                        if (rr_tmp .le. rc(ity)) then
                           nloc = nloc + 1
                           npt = idint(binv(ity) * 
     1                          log(one+ainv(ity)*rr_tmp))+1
                           fact1 = (rr_tmp - p_pot%rs(npt,ity)) / 
     1                          (p_pot%rs(npt+1,ity) - 
     2                          p_pot%rs(npt,ity))
                           nloc_p_pot%ratom(ja, nloc) = rr_tmp
c     in order to avoid dividing by zero:
                           if (rr_tmp .lt. rrmin(ity))  then
                              nloc_p_pot%ratom(ja, nloc) = rrmin(ity)
                           endif
                           nloc_p_pot%indr(ja,nloc) = npt
                           nloc_p_pot%indw(ja,nloc) = grid%rindex(mg)
                           nloc_p_pot%tran(ja,nloc) = grid%rtrans(mg)
                           nloc_p_pot%ffcc(ja,nloc) = fact1
                           nloc_p_pot%rnloc(:,ja,nloc) = (/ x,y,z /)
                        endif
                     enddo
                  enddo
               enddo
            enddo
            if (nloc .ne. nloc_p_pot%nlatom(ja)) then
               write(7,*) ' ERROR in matnloc ',nloc,iat,ja,ity
     1              ,nloc_p_pot%nlatom(ja)
               ierr = 1
               return
            endif
         enddo                  ! iat = 1, clust%natmi(ity)
      enddo                     ! ity = 1, clust%type_num
c
c     Report non-local block sizes to file.
c
      write(7,*)
      write(7,*) 'Non-local pseudopotential messages:'
      write(7,*) '-----------------------------------'
      write(7,*)'Max # of nonlocal points for one atom = ', 
     1     nloc_p_pot%maxnloc
      write(7,*)
      write(7,*) 'Sizes of all non-local blocks '
      write(7,'(12(1x,i5))') (nloc_p_pot%nlatom(i),i=1,clust%atom_num)
      write(7,*)

c     initialize anloc and skbi to zero
      nloc_p_pot%skbi = zero
      nloc_p_pot%anloc = zero
c
c     Calculate the non-local potential vector for each angular
c     component. The result is stored in anloc.
c     The non-local pseudopotential to the hamiltonian is
c     subsequently calculated as the outer product of these vectors,
c     summed over all angular components l,m.
c
      ja = 0
c
c     for each atom type, for each atom within each type
      do ity = 1, clust%type_num
         do iat = 1, clust%natmi(ity)

c     again update temporary storage of atom # and position

            ja = ja + 1

            nloc = nloc_p_pot%nlatom(ja)

c     initialize angular momentum (l,m) count
            lm = 0
c
c     loop over each angular momentum component, l (and within
c     the loop calculate for different m's too).

            do lp = 1, p_pot%nlocp(ity)
c     skip the local component of the pseudopotential!
               if (lp .ne. p_pot%loc(ity)) then
c     Calculate the multiplicand - (non-local pseudopotential)*
c     (radial pseudowavefunction)*(angular wave function), put the
c     results in anloc. That is, calculate explicitly |Vl-Vloc|phi_lm>.

c     ---- s orbitals
                  if (lp.eq.1) then
                     lm = lm + 1
                     nloc_p_pot%skbi(lm,ja) = p_pot%ekbi(1,ity)
                     do i = 1, nloc
                        n1    = nloc_p_pot%indr(ja, i)
                        fact1 = nloc_p_pot%ffcc(ja, i)
                        vwr1  = p_pot%vw(n1, ity, 1) + fact1*
     1                        (p_pot%vw(n1+1,ity,1)-p_pot%vw(n1,ity,1))
                        nloc_p_pot%anloc(i, lm, ja) = c0*vwr1
                     enddo
c     ---- p orbitals
                  else if (lp.eq.2) then
                     lm = lm + 3
                     nloc_p_pot%skbi(lm-2:lm,ja) = p_pot%ekbi(2,ity)
                     do i = 1, nloc
                        n1    = nloc_p_pot%indr(ja,i)
                        fact1 = nloc_p_pot%ffcc(ja,i)
                        rinv = one/nloc_p_pot%ratom(ja,i)
                        x = nloc_p_pot%rnloc(1,ja,i)*rinv 
                        y = nloc_p_pot%rnloc(2,ja,i)*rinv 
                        z = nloc_p_pot%rnloc(3,ja,i)*rinv
                        vwr1  = p_pot%vw(n1, ity, 2) + fact1*
     1                        (p_pot%vw(n1+1,ity,2)-p_pot%vw(n1,ity,2))
                        nloc_p_pot%anloc(i, lm-2, ja) = c1*vwr1*x
                        nloc_p_pot%anloc(i, lm-1, ja) = c1*vwr1*y
                        nloc_p_pot%anloc(i, lm,   ja) = c1*vwr1*z
                     enddo      
c     ---- d orbitals
                  else if (lp.eq.3) then
                     lm = lm + 5
                     nloc_p_pot%skbi(lm-4:lm,ja) = p_pot%ekbi(3,ity)
                     do i = 1, nloc
                        n1    = nloc_p_pot%indr(ja, i)
                        fact1 = nloc_p_pot%ffcc(ja, i)
                        rinv = one/nloc_p_pot%ratom(ja,i)
                        x = nloc_p_pot%rnloc(1,ja,i)*rinv
                        y = nloc_p_pot%rnloc(2,ja,i)*rinv
                        z = nloc_p_pot%rnloc(3,ja,i)*rinv
                        vwr1  = p_pot%vw(n1, ity, 3) + fact1*
     1                        (p_pot%vw(n1+1,ity,3)-p_pot%vw(n1,ity,3))
                        nloc_p_pot%anloc(i, lm-4, ja)=c21*vwr1*x*y 
                        nloc_p_pot%anloc(i, lm-3, ja)=c21*vwr1*z*y 
                        nloc_p_pot%anloc(i, lm-2, ja)=c21*vwr1*x*z
                        nloc_p_pot%anloc(i, lm-1, ja)=c22*vwr1*
     1                       (three*z*z-one)
                        nloc_p_pot%anloc(i, lm,   ja)=c23*vwr1*(x*x-y*y)
                     enddo    
c     ---- f orbitals
                  else if (lp.eq.4) then
                     lm = lm + 7
                     nloc_p_pot%skbi(lm-6:lm,ja) = p_pot%ekbi(4,ity)
                     do i = 1, nloc
                        n1    = nloc_p_pot%indr(ja, i)
                        fact1 = nloc_p_pot%ffcc(ja, i)
                        rinv = one/nloc_p_pot%ratom(ja,i)
                        x = nloc_p_pot%rnloc(1,ja,i)*rinv
                        y = nloc_p_pot%rnloc(2,ja,i)*rinv
                        z = nloc_p_pot%rnloc(3,ja,i)*rinv
                        vwr1  = p_pot%vw(n1, ity, 4) + fact1*
     1                        (p_pot%vw(n1+1,ity,4)-p_pot%vw(n1,ity,4))
                        nloc_p_pot%anloc(i,lm-6,ja)=
     1                       c31*vwr1*z*(five*z*z-three)
                        nloc_p_pot%anloc(i,lm-5,ja)=c32*vwr1*x*
     1                       (five*z*z-one)
                        nloc_p_pot%anloc(i,lm-4,ja)=c32*vwr1*y*
     1                       (five*z*z-one)
                        nloc_p_pot%anloc(i,lm-3,ja)=c33*vwr1*z*(x*x-y*y)
                        nloc_p_pot%anloc(i,lm-2,ja)=c34*vwr1*x*y*z
                        nloc_p_pot%anloc(i,lm-1,ja)=c35*vwr1*x*
     1                       (x*x-three*y*y)
                        nloc_p_pot%anloc(i,lm,  ja)=c35*vwr1*y*
     1                       (y*y-three*x*x)
                     enddo
                  endif
               endif
            enddo               ! lp = 1, p_pot%nlocp(ity)
c     update array with number of lm components for each atom
            nloc_p_pot%nlmatm(ja) = lm
         enddo                  ! iat = 1, clust%natmi(ity)
      enddo                     ! ity = 1, clust%type_num

      end subroutine matnloc
c     ===============================================================
