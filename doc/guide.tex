\documentclass[10pt]{book}
\usepackage{html}
\usepackage{htmllist}
\usepackage{makeidx}
\usepackage{hyperref}

\title{\htmladdnormallinkfoot{Parsec 1.1 
User's Guide
}
{http://www.ices.utexas.edu/parsec/}\footnote{this text written by M. Tiago, mtiago@ices.utexas.edu}
}
\author{
\htmladdnormallinkfoot{Prof. J. R. Chelikowsky's Research Group}
{http://www.ices.utexas.edu/ccm}
}

\date{\today}

\makeindex

\begin{document}

\maketitle

\tableofcontents


\chapter{What it is}

Parsec is a computer code that solves the Kohn-Sham equations by expressing
electron wave-functions directly in real space, without use of explicit
basis set. The name stands for "Pseudopotential Algorithm for Real-Space
Electronic Calculations". It handles periodic boundary conditions (on all
three directions) and confined-system boundary conditions. In the latter,
the charge density and wave-functions
are required to vanish outside a spherical shell enclosing the electronic
system. A finite-difference approach is used for the calculation of spatial
derivatives. Norm-conserving pseudopotentials are used to describe the
interaction between valence electrons and ionic cores (core electrons+nuclei).
Owing to the sparsity of the Hamiltonian matrix, the Kohn-Sham
equations are solved by direct diagonalization, with the use of extremely
efficient sparse-matrix eigensolvers. Besides
standard DFT calculations, parsec is able to perform structural relaxation,
simulated annealing, Langevin molecular dynamics, polarizability calculations
(confined-system boundary conditions only). It can also be used to provide
input for TDLDA (time-dependent density functional theory, in adiabatic
local-density approximation), GW approximation, and solution of the 
Bethe-Salpeter equation for optical excitations.

The development of parsec started in the early 1990's with the work of J.R. Chelikowsky, N. Troullier,
 and Y. Saad ( Phys. Rev. Lett {\bf 72}, 1240 ; Phys. Rev. B {\bf 50},
11355). Later, many people have contributed with additional features and
performance improvement:

\begin{itemize}
\item A. Stathopolous : mathematical subroutines;
\item H. Kim : core correction, multipole expansion;
\item I. Vasiliev : Polarizability, asymptotically-corrected functionals;
\item M. Jain and L. Kronik : GGA, generalized Broyden mixer;
\item K. Wu : improvements in iterative diagonalizer and Hartree solver;
\item R. Burdick : major rewrite and improvement of multipole expansion;
\item M. Alemany and M. Jain : periodic boundary conditions, parallel
implementation;
\item M. Alemany and E. Lorin : ARPACK solver;
\item A. Makmal: dynamical memory allocation;
\item M. Tiago : parallelization, symmetry operations, interface with GW and
BSE, this user's guide.
\end{itemize}

\chapter{Getting the code}

Public versions of the code can be downloaded from
\htmladdnormallinkfoot{Prof. Chelikowsky's group page} 
{http://www.ices.utexas.edu/parsec/}. There is also a development
version, with implementations that are being tested for stability and
performance. If you are interested in contributing to development,
contact the developers.

Parsec is distributed under the General Public License (GPL). In a
nutshell, these are the conditions implied in using it:

\begin{itemize}
\item It should be used for non-commercial purposes only.

\item The authors give no warranty regarding its reliability or
  usefulness to your particular application.

\item The authors offer no technical support.

\end{itemize}

\chapter{Compiling Parsec}

At the beginning of the {\tt Makefile} file, edit the include line with
the correct machine-dependent compilation file. Existing compilation files
are inside subdirectory {\tt config}. Some of the existing files are:

\begin{itemize}
\item {\tt make.ibmsp3 make.ibmsp3\_mpi} : sp.msi.umn.edu, with/without MPI support.

\item {\tt make.intel} : plain i686 machine running linux.
\end{itemize}

This code has been ported and tested on a variety of different
platforms: IBM SP3 and SP4 (AIX compiler), SGI (intel and f90
compilers), Intel/Linux (g95, intel compilers).

Usually, the executable is named {\tt parsec.ser} (no MPI support) or
{\tt parsec.mpi} (with MPI support).

\section{Porting to New Platforms}

Besides creating a new {\tt make.*} file for the machine you want to use,
these are the files you may have to modify in
order to port it to other platforms: myflush.F, timing.F, cfftw.F

Parsec uses the following external libraries:

\begin{itemize}
\item
  \htmladdnormallinkfoot{ARPACK}
  {http://www.caam.rice.edu/software/ARPACK/}, diagonalization of
  sparse matrices.

\item \htmladdnormallinkfoot{FFTW} {http://www.fftw.org}, versions 2.x or 3.x,
used for 3-dimensional FFT of real functions.

\item \htmladdnormallinkfoot{LAPACK/BLAS} {http://www.netlib.org/lapack/},
linear algebra. Alternatively, support for libraries 
ESSL (IBM) and \htmladdnormallinkfoot{MKL}
{http://www.intel.com/cd/software/products/asmo-na/eng/index.htm}
(Intel Math Kernel Library) is also available.

\item MPI : message passing interface, for parallel machines.

\end{itemize}

\chapter{Running Parsec}

\section{Basic input}

\begin{itemize}
\item {\tt parsec.in} : input file with user-provided parameters.

\item {\tt *\_POTRE.DAT} : pseudopotential files, in format
  consistent with Prof. J.L. Martins'
  \htmladdnormallinkfoot{pseudopotential generator}
  {http://bohr.inesc-mn.pt/~jlm/pseudo.html}. The distributors of Parsec have a version of Prof. Martins' code with minor but important modifications, so that files in the proper format are created. For the moment, only
  norm-conserving pseudopotentials are implemented. For
  back-compatibility, files in "old format" are also accepted, see
  below \htmlref{\tt Old\_Pseudopotential\_Format}{OldPseudopotentialFormat}
 flag. Pseudopotential files used in Parsec are compatible with the \htmladdnormallinkfoot{"Siesta"}{http://www.uam.es/siesta/} DFT program, but the opposite is not true: pseudo-wavefunctions are appended to {\tt *\_POTRE.DAT} files, but the same does not necessarily happen to formatted pseudopotential files used in Siesta.

\end{itemize}

%\section{ The {\tt parsec.in} file}
\section{ The parsec.in file}

The user-provided input uses the electronic structure data format (ESDF),
implemented as a module written by Chris J. Pickard. Comments can be added
on any line, and are preceded by any of the symbols "{\tt \# ; !}". Keywords and
blockwords are case insensitive. Characters "{\tt . : =}" and multiple spaces
are ignored. Most parameters have default values. All parameters with
true/false value have default value false. Parameter values can be
written anywhere in the file, with the exception that information pertinent
to the same chemical element should be given in consecutive lines (sometimes,
data from different elements can be mixed, but this should be avoided for
the sake of clarity). Keywords are always followed by their corresponding
value on the same line. Input values with intrinsic physical unit can
be given in different units, with name following the value. Default
unit is used if none is specified. For
example, the line {\tt grid\_spacing 0.5 ang} sets a grid spacing of
0.5 angstroms. Blocks have the syntax:

\begin{verbatim}
begin Blockword
.
.
.
end Blockword
\end{verbatim}

WARNING: parsec does not watch out for misprints in {\tt parsec.in}. Lines
with unrecognized keywords or blockwords are usually ignored.

\section{Basic output}

\begin{itemize}
\item {\tt parsec.out} : unified output file.

\item {\tt out.*} : standard output. It contains some warnings
  and error messages.

\item {\tt wfn.dat} : contains eigenvalues, density function,
  effective potential,
  wave-functions and other output data, in binary (unformatted)
  form. See \htmlref{\tt Olddat\_Style}{OlddatStyle}.
\end{itemize}

\chapter{Input options}

\section{The real-space grid}

The following parameters define the region of space where the
Kohn-Sham equations will be solved, and the appropriate boundary
conditions. In parallel environment, grid points are distributed among
processors. Symmetry operations are used to reduce the sampled region
to an irreducible wedge.

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Periodic\_System 
\index{Periodic\_System@{\tt Periodic\_System}} 
\label{PeriodicSystem}]
({\it boolean}),
default value = {\tt false }

This flag enables the use of periodic boundary conditions on all three
directions. If set true, the input grid spacing may be increased so as to
make it commensurable with the given size of periodic cell. Also, atomic
coordinates are used as given, with no initial repositioning around the origin
of real-space grid. If atoms are found to be outside the cell, they are 
replaced by their periodic image inside the cell.

\item[\tt Cell\_Shape 
\index{Cell\_Shape@{\tt Cell\_Shape}}
\label{CellShape}]
({\it block}),
default value = {\tt no default }

Length of periodic cell along the x, y, z coordinates.
Relevant only in periodic systems. 

\item[\tt Lattice\_Vector\_Scale 
\index{Lattice\_Vector\_Scale@{\tt Lattice\_Vector\_Scale}}
\label{Latticevectorscale}]
({\it real}),
default value = {\tt 1 },
default unit = {\it bohr} (Bohr radii)

Constant that multiplies the length of all three unit lattice
vectors. This is useful for volumetric expansions or contractions in
periodic systems. Relevant only in periodic systems.

\item[\tt Boundary\_Sphere\_Radius 
\index{Boundary\_Sphere\_Radius@{\tt Boundary\_Sphere\_Radius}}
\label{BoundarySphereRadius}]
({\it real}),
default value = {\tt no default },
default unit = {\it bohr} (Bohr radii)

Radius of enclosing spherical shell. Boundary conditions in
non-periodic systems are such that all wave-functions vanish beyond a spherical
shell centered in the origin and with given radius. The Hartree potential is
also set to vanish on the shell (internally, this is done by solving Poisson's
equation with a compensating charge outside the shell). Calculation stops if any
atom is found outside the shell. The amount of extra space that should be given
between the shell and any inner atom is strongly system-dependent and should be
carefully tested for convergence.

\item[\tt Grid\_Spacing 
\index{Grid\_Spacing@{\tt Grid\_Spacing}}
\label{GridSpacing}]
({\it real}),
default value = {\tt no default },
default unit = {\it bohr} (Bohr radii)

Distance between neighbor points in the real-space grid.
This is a critical parameter in the calculation and it should be carefully
tested for convergence. The grid is assumed to be regular and orthogonal.

\item[\tt Expansion\_Order 
\index{Expansion\_Order@{\tt Expansion\_Order}}
\label{ExpansionOrder}]
({\it integer}),
default value = {\tt 12 }

Order used in the finite-difference expansion of gradients and Laplacians. It
corresponds to twice the number of neighbor points used on each side and for
each direction. It must be an even number.

\item[\tt Olddat\_Style 
\index{Olddat\_Style@{\tt Olddat\_Style}}
\label{OlddatStyle}]
({\it boolean}),
default value = {\tt false}

Compatibility flag. Removes the grid shift and ignores symmetry
operations, so that the resulting grid is equal to the one used in
older versions of this code. Output wave-functions and density are
written to file {\tt old.dat} instead of {\tt wfn.dat}. Notice that
these two files have different format!
\end{htmllist}

\section{Diagonalization parameters}

Many DFT codes solve the Kohn-Sham equations by minimizing
the total energy functional. Parsec uses a different approach: it
performs direct diagonalization of the Kohn-Sham
equations. Expressed in real space, the Hamiltonian is a sparse
matrix, and efficient algorithms can be used to obtain the low-energy
eigenstates with minimal memory usage and parallel communication.

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Eigensolver 
\index{Eigensolver@{\tt Eigensolver}}
\label{Eigensolver}]
({\it word}),
default value = {\tt arpack }

Implemented eigensolvers are: {\tt arpack} (implicitly restarted
Lanczos/Arnoldi) and {\tt diagla} (block preconditioned Lanczos). The last solver
uses significantly less memory, but it occasionally fails to converge in
periodic systems.

\item[\tt Diag\_Tolerance 
\index{Diag\_Tolerance@{\tt Diag\_Tolerance}}
\label{DiagTolerance}]
({\it real}),
default value = {\tt 1.d-4 }

Tolerance in the residual norm of eigen-pairs.

\item[\tt Initial\_Diag\_Tolerance 
\index{Initial\_Diag\_Tolerance@{\tt Initial\_Diag\_Tolerance}}
\label{InitialDiagTolerance}]
({\it real}),
default value = {\tt 100.0 }

Tolerance in the residual norm used for the trial diagonalization. This can be
a fairly large number, since the trial diagonalization does not need to be
very accurate.

\item[\tt Subspace\_Buffer\_Size 
\index{Subspace\_Buffer\_Size@{\tt Subspace\_Buffer\_Size}}
\label{SubspaceBufferSize}]
({\it integer}),
default value = {\tt 1 }

Number of additional eigenvalues to be computed for each representation. Must
be some positive value, so that the full set of eigenvalues can be
reconstructed from the sum of representations without missing levels.

\end{htmllist}

\section{Self-consistency and mixing parameters}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Max\_Iter 
\index{Max\_Iter@{\tt Max\_Iter}}
\label{MaxIter}]
({\it integer}),
default value = {\tt integer }

Maximum number of iterations in the self-consistent loop.

\item[\tt Convergence\_Criterion 
\index{Convergence\_Criterion@{\tt Convergence\_Criterion}}
\label{ConvergenceCriterion}]
({\it real}),
default value = {\tt 2.d-4 },
default unit = {\it Ry} (rydbergs)

The density function is considered converged when the self-consistent
residual error (SRE) falls below this value. The SRE is an integral of the
square of the difference between the last two self-consistent potentials,
weighted by electron density and taken squared root. This parameter should
not be chosen less than the typical diagonalization accuracy.

\item[\tt Mixing\_Method 
\index{Mixing\_Method@{\tt Mixing\_Method}}
\label{MixingMethod}]
({\it word}),
default value = {\tt Anderson }

Defines the scheme used in the mixing of old an new potentials. Implemented
schemes are {\tt Broyden} and {\tt Anderson} (which were proved to be equivalent by
V. Eyert, 1996). In practice, there is no significant difference between
these two schemes. Broyden mixing uses disk for input/output of internal data.

\item[\tt Mixing\_Param 
\index{Mising\_Param@{\tt Mixing\_Param}}
\label{MixingParam}]
({\it real}),
default value = {\tt 0.30 }

Choice for the diagonal Jacobian used in the mixing of potentials. Small values reduce the amount
of mixing and result in slower but more controlled convergence. For improved
performance, the user may try reducing this parameter in cases where
the energy landscape is too corrugated (e.g.: magnetic systems, or systems
with many degrees of freedom).

\item[\tt Memory\_Param 
\index{Memory\_Param@{\tt Memory\_Param}}
\label{MemoryParam}]
({\it integer}),
default value = {\tt 2 }

Number of previous SCF steps used in mixing. Ideal values range from 4 to 6.
Larger values may actually worsen convergence because it includes steps far
from the real solution.

\end{htmllist}

\section{Global atom and pseudopotential parameters}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Atom\_Types\_Num 
\index{Atom\_Types\_Num@{\tt Atom\_Types\_Num}}
\label{AtomTypesNum}]
({\it integer}),
default value = {\tt no default }

Number of different chemical elements present in the system.

\item[\tt Coordinate\_Unit 
\index{Coordinate\_Unit@{\tt Coordinate\_Unit}}
\label{CoordinateUnit}]
({\it word}),
default value = {\tt Cartesian\_Bohr }

By default, positions of atoms are given in cartesian coordinates and
units of Bohr radius. Other implemented options are:

\begin{itemize}
\item {\tt Cartesian\_Ang} atomic coordinates given in angstroms, cartesian.

\item {\tt Lattice\_Vectors} atomic coordinates given in units of lattice
  vectors. Available only with periodic boundary conditions.
\end{itemize}

\item[\tt Old\_Pseudopotential\_Format 
\index{Old\_Pseudopotential\_Format@{\tt Old\_Pseudopotential\_Format}} 
\label{OldPseudopotentialFormat}]
({\it boolean}),
default value = {\tt false }

By default, pseudopotential files have atomic parameters (cut-off radii,
etc. ) included. For back-compatibility, this flag enables the reading of
old-format files, which do not have atomic parameters included. Notice that
the naming convention of old-format files is {\tt *\_POTR.DAT} instead of 
{\tt *\_POTRE.DAT}. The use of mixed formats is not allowed: parsec reads all files
in new format or all files in old format.

\item[\tt Old\_Interpolation\_Format 
\index{Old\_Interpolation\_Format@{\tt Old\_Interpolation\_Format}}
\label{OldInterpolationFormat}]
({\it boolean}),
default value = {\tt false }

This is a compatibility flag. Normally, the local component of pseudopotentials
{\it V} at some arbitrary point {\it r} is evaluated by linear interpolation of the
function {\it r*V}. Due to the Coulomb-like behavior of {\it V} far away from
the atom, this interpolation is exact beyond the cut-off radius. This
flag reverts to the old method: linear interpolation of {\it V} itself.
It should be used for debugging purposes only.

\end{htmllist}

\section{Parameters specific to each chemical element}

These parameters should be given in sequence in {\tt parsec.in}, and
they refer to a particular chemical element in the system.

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Atom\_Type 
\index{Atom\_Type@{\tt Atom\_Type}}
\label{AtomType}]
({\it word}),
default value = {\tt no default }

Chemical symbol of the current element. THIS IS CASE SENSITIVE!

\item[\tt Potential\_Num 
\index{Potential\_Num@{\tt Potential\_Num}}
\label{PotentialNum}]
({\it integer}),
default value = {\tt no default }

Number of available angular components. Relevant only if old-format
pseudopotentials are used.

\item[\tt Core\_Cutoff\_Radius 
\index{Core\_Cutoff\_Radius@{\tt Core\_Cutoff\_Radius}}
\label{CoreCutoffRadius}]
({\it real}),
default value = {\tt no default },
default unit = {\it bohr} (Bohr radii)

Maximum cut-off radius used in the generation of pseudopotential.
Relevant only if old-format pseudopotentials are used.

\item[\tt Electron\_Per\_Orbital 
\index{Electron\_Per\_Orbital@{\tt Electron\_Per\_Orbital}}
\label{ElectronPerOrbital}]
({\it block}),
default value = {\tt no default }

Number of electrons per atomic orbital used in the generation of
pseudopotential. Relevant only if old-format pseudopotentials are used.

\item[\tt Local\_Component 
\index{Local\_Component@{\tt Local\_Component}}
\label{LocalComponent}]
({\it integer}),
default value = {\tt no default }

Choice of angular component to be considered local in the Kleinman-Bylander
transformation. No ghost-state check is done in parsec. Therefore, check if
the chosen component does not have ghost states.

\item[\tt Move\_Flag 
\index{Move\_Flag@{\tt Move\_Flag}}
\label{MoveFlag}]
({\it integer}),
default value = {\tt 0 }

Type of movement to be applied to atoms. Relevant only if structural relaxation
is performed. Value {\tt 0} makes all atoms movable in all directions. Value
{\it n}
positive makes the first {\it n} atoms movable. A negative value makes selected atoms
movable (the selection is made inside block \htmlref{\tt Atom\_Coord}
{AtomCoord}).

\item[\tt Atom\_Coord 
\index{Atom\_Coord@{\tt Atom\_Coord}}
\label{AtomCoord}]
({\it block}),
default value = {\tt no default }

Coordinates of atoms belonging to the current chemical species. See
\htmlref{\tt Coordinate\_Unit} {CoordinateUnit}. If structural relaxation is done with selected atoms
(\htmlref{\tt Move\_Flag} {MoveFlag} < 0), each triplet of coordinates
must be followed by an integer: 0 if this atom is fixed, 1 if it is movable.

\item[\tt Alpha\_Filter 
\index{Alpha\_Filter@{\tt Alpha\_Filter}}
\label{AlphaFilter}]
({\it real}),
default value = {\tt 0.0 }

Cut-off parameter in Fourier filtering of pseudopotentials, in reciprocal
space. If value is non-positive, pseudopotentials are not
filtered. Fourier filtering is implemented according to Briggs {\it et
  al.} Phys. Rev. B {\bf 54}, 14362 (1996), but no second filtering in
real space has been implemented so far.

\item[\tt Beta1\_Filter 
\index{Beta1\_Filter@{\tt Beta1\_Filter}}
\label{Beta1Filter}]
({\it real}),
default value = {\tt 0.0 }

Cut-off parameter in Fourier filtering of pseudopotentials, in reciprocal
space. Referenced only if {\tt alpha\_filter} is positive.

\item[\tt Core\_Filter 
\index{Core\_Filter@{\tt Core\_Filter}}
\label{CoreFilter}]
({\it  real}),
default value = equal to {\tt Alpha\_Filter}

Cut-off parameter in Fourier filtering of non-linear core density. If
non-positive, then core density is not filtered.

\end{htmllist}

\section{Electronic parameters}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Restart\_Run 
\index{Restart\_Run@{\tt Restart\_Run}}
\label{RestartRun}]
({\it boolean}),
default value = {\tt false}

By default, the initial charge density in a parsec run is built from
the superposition of atomic charge densities. If you want instead to
start from a previous run, choose value true.
For confined systems, having {\tt restart\_run = true} prevents the initial
centering of atoms. If this flag is used but an adequate file {\tt
  wfn.dat} is not found, the calculation is interrupted.

\item[\tt States\_Num 
\index{States\_Num@{\tt States\_Num}}
\label{StatesNum}]
({\it integer}),
default value = {\tt no default }

Number of energy eigenvalues to be computed. In spin-polarized calculations,
this is the number of eigenvalues per spin orientation.

\item[\tt Net\_Charges 
\index{Net\_Charges@{\tt Net\_Charges}}
\label{NetCharges}]
({\it real}),
default value = {\tt 0.0 },
default unit = {\it e} (electron charge)

Net electric charge in the system, in units of electron charge. It can also
be used in periodic systems. In that case, a background charge is used
and the divergent component of the Hartree potential is ignored.

\item[\tt Fermi\_Temp 
\index{Fermi\_Temp@{\tt Fermi\_Temp}}
\label{FermiTemp}]
({\it real}),
default value = {\tt 80.0 },
default unit = {\it K} (kelvins)

Fermi temperature used to smear out the Fermi energy. If a
negative value is given, then the occupancy of Kohn-Sham orbitals is read from
file {\tt occup.in} and kept fixed throughout the calculation.

\item[\tt Correlation\_Type 
\index{Correlation\_Type@{\tt Correlation\_Type}}
\label{CorrelationType}]
({\it word}),
default value = {\tt CA }

Choice of exchange-Correlation functional to be used. Available
choices and their variants are:

\begin{itemize}
\item {\tt CA, PZ }: Ceperley-Alder (LDA), with spin-polarized
  option. D.M. Ceperley and B.J. Alder, Phys. Rev. Lett. {\bf 45},
  566 (1980);  J.P. Perdew and A. Zunger, Phys. Rev. B {\bf 23}, 
5048 (1981).

\item {\tt XA}: Slater's x-alpha (LDA). J.C. Slater, Phys. Rev. {\bf 81} 385 (1951).

\item {\tt WI}: Wigner's interpolation (LDA). E. Wigner, Phys. Rev. {\bf 46},
  1002 (1934).

\item {\tt HL}: Hedin-Lundqvist (LDA). L. Hedin and B.I. Lundqvist,
  J. Phys. C {\bf 4}, 2064 (1971).

\item {\tt LB}: Ceperley-Alder with Leeuwen-Baerends correction
  (asymptotically corrected). R. van Leeuwen and E. J. Baerends,
  Phys. Rev. A {\bf 49}, 2421 (1994).

\item {\tt CS}: Ceperley-Alder with Casida-Salahub correction
  (asymptotically corrected). M.E. Casida and D.R. Salahub,
  J. Chem. Phys. {\bf 113}, 8918 (2000).

\item {\tt PW, PW91}: Perdew-Wang (GGA), with spin-polarized
  option. J.P. Perdew and Y. Wang, Phys. Rev. B {\bf 45}, 13244 (1992).

\item {\tt PB, PBE}: Perdew-Burke-Ernzerhof (GGA), with spin-polarized
  option. J.P. Perdew, K. Burke, and M. Ernzerhof, Phys. Rev. Lett.
  {\bf 77}, 3865 (1996).
\end{itemize}

\item[\tt Ion\_Energy\_Diff 
\index{Ion\_Energy\_Diff@{\tt Ion\_Energy\_Diff}}
\label{IonEnergyDiff}]
({\it real}),
default value = {\tt 0.0},
default unit = {\it Ry} (rydbergs)

Energy difference used in the construction of the Casida-Salahub functional.
Relevant only if that functional is used.

\item[\tt Spin\_Polarization: 
\index{Spin\_Polarization@{\tt Spin\_Polarization:}}
\label{SpinPolarization}]
({\it boolean}),
default value = {\tt false }

Enables spin-polarized densities and functional.

\end{htmllist}

\section{Structural relaxation}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Minimization 
\index{Minimization@{\tt Minimization}}
\label{Minimization}]
({\it word}),
default value = {\tt none }

Choice of method for structural relaxation. Available options are:
{\tt none} (no
relaxation), {\tt simple} (steepest-descent algorithm), {\tt BFGS}
(Broyden-Fletcher-Goldfarb-Shanno, the best), and {\tt manual} (manual movement). Steepest-descent should be
used as last resort. With manual movement, the sequence of atomic coordinates
must be provided by the user in file {\tt manual.dat} and it is used
"as is". The last choice is useful for debugging. Symmetry operations
are ignored if manual movement is performed

\item[\tt Movement\_Num 
\index{Movement\_Num@{\tt Movement\_Num}}
\label{MovementNum}]
({\it integer}),
default value = {\tt 500 }

Maximum number of iterations during structural relaxation.

\item[\tt Force\_Min 
\index{Force\_Min@{\tt Force\_Min}}
\label{ForceMin}]
({\it real}),
default value = {\tt 1.5d-3 },
default unit = {\it Ry/bohr} (rydbergs/Bohr radii)

Maximum magnitude of the force on any atom in the relaxed structure.
This should not be set less than about 0.01 {\it Ry/a.u.}, which is the
usual accuracy in the calculation of forces.

\item[\tt Max\_Step 
\index{Max\_Step@{\tt Max\_Step}}
\label{MaxStep}]
({\it real}),
default value = {\tt 0.05} (steepest descent), {\tt -1} (BFGS),
default unit = {\it bohr} (Bohr radii)

If steepest-descent minimization is performed, this parameter sets the
maximum displacement of atoms between two steps in relaxation.
With BFGS, this defines the maximum displacement of atoms along
each Cartesian component from their original position (if negative, no
constraint in the movement of atoms is imposed). Ignored in all other
circumstances.

\item[\tt Min\_Step 
\index{Min\_Step@{\tt Min\_Step}}
\label{MinStep}]
({\it real}),
default value = {\tt 1.d-4 },
default unit = {\it bohr} (Bohr radii)

Minimum displacement of atoms between two steps in steepest-descent 
relaxation. Relevant only if steepest descent is performed.

\item[\tt BFGS\_Number\_Corr 
\index{BFGS\_Number\_Corr@{\tt BFGS\_Number\_Corr}}
\label{BFGSNumberCorr}]
({\it integer}),
default value = {\tt 7 }

Number of variable metric corrections used to defined the limited-memory
matrix. Relevant only if BFGS relaxation is performed.

\item[\tt Relax\_Restart 
\index{Relax\_Restart@{\tt Relax\_Restart}}
\label{RelaxRestart}]
({\it boolean}),
default value = {\tt false }

Flag for the restart of a relaxation run. If true, the user must provide
information about previous run in file {\tt relax\_restart.dat}. No need to input
file {\tt wfn.dat}. If this option is used, the atomic coordinates in
{\tt parsec.in}
are replaced with the ones in {\tt relax\_restart.dat}. Appropriate warnings
are written to output. If the restart file is not found, this flag is ignored
and no information from previous relaxations is used.

\end{htmllist}

\section{Molecular dynamics}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Molecular\_Dynamics 
\index{Molecular\_Dynamics@{\tt Molecular\_Dynamics}}
\label{MolecularDynamics}]
({\it boolean}),
default value = {\tt false }

Enables molecular dynamics. This feature can be used both for
simulated annealing and for direct simulation of hot systems. Both
canonical and micro-canonical ensembles are implemented. Symmetry
operations are ignored if molecular dynamics is being performed.

\item[\tt Cooling\_Method 
\index{Cooling\_Method@{\tt Cooling\_Method}}
\label{CoolingMethod}]
({\it word}),
default value = {\tt stair }

Type of cooling (i.e.: change of ensemble temperature) used during molecular
dynamics. Possible choices are: {\tt stair} (stair-case), {\tt log}
(logarithmic cooling), and {\tt linear} (linear cooling). As the
molecular dynamics simulation proceeds, the
ensemble temperature changes according to the cooling method and to the
choices of initial and final temperatures. At each step, the actual temperature
is calculated from equipartition theorem and the kinetic energy (difference
between the total energy and potential energy). In the first steps, the
actual and ensemble temperatures may be very different, particularly if there
are few atoms, when thermal fluctuations are enormous.

\item[\tt Tinit 
\index{Tinit@{\tt Tinit}}
\label{Tinit}]
({\it real}),
default value = {\tt 500.0 },
default unit = {\it K} (kelvins)

Initial ensemble temperature in the system.

\item[\tt T\_final 
\index{T\_final@{\tt T\_final}}
\label{Tfinal}]
({\it real}),
default value = {\tt 500.0 },
default unit = {\it K} (kelvins)

Final ensemble temperature.

\item[\tt T\_Step 
\index{T\_Step@{\tt T\_Step}}
\label{TStep}]
({\it real}),
default value = {\tt 150.0 },
default unit = {\it K} (kelvins)

Step temperature used in the cooling.

\item[\tt Time\_Step 
\index{Time\_Step@{\tt Time\_Step}}
\label{TimeStep}]
({\it real}),
default value = {\tt 150.0 },
default unit = {\it ps} ($10^{-12}$ seconds)

Time interval between two steps in molecular dynamics.
The dynamical equations of motion are numerically integrated using a modified
Verlet algorithm. 

\item[\tt Friction\_Coefficient 
\index{Friction\_Coefficient@{\tt Friction\_Coefficient}}
\label{FrictionCoefficient}]
({\it real}),
default value = {\tt 0.0 }

Viscosity parameter in molecular dynamics, in atomic units (inverse time).
If less than 1.d-6
{\it a.u.}, then a micro-canonical ensemble is used: total energy is assumed
constant, and the atoms start moving with a velocity distribution consistent
with the initial temperature. If more, then a canonical ensemble is used and
true Langevin molecular dynamics is performed.

\item[\tt Step\_num 
\index{Step\_num@{\tt Step\_num}}
\label{Stepnum}]
({\it integer}),
default value = {\tt 250 }

Number of steps in molecular dynamics. A successful simulation ends only after
reaching the requested number of steps.

\item[\tt Restart\_mode 
\index{Restart\_mode@{\tt Restart\_mode}}
\label{Restartmode}]
({\it boolean}),
default value = {\tt false }

Enables the restart of an aborted molecular dynamics simulation.
With this feature, the last two
sets of positions, velocities and accelerations stored in input file
{\tt mdinit.dat} are used. The number of steps in that file is included in the
current run as well.

\end{htmllist}

\section{Polarizability}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Polarizability 
\index{Polarizability@{\tt Polarizability}}
\label{Polarizability}]
({\it boolean}),
default value = {\tt false }

Enables calculation of polarizability using a finite-field method. Available
only with non-periodic boundary conditions. An uniform electric field is
applied successively along the +x,-x,+y,-y,+z,-z directions, and dipole moments
are calculated and collected and printed in output file {\tt polar.dat}. From
dipole moments, the diagonal part of polarizability tensor is
obtained. Symmetry operations are ignored if this flag is used.

\item[\tt Electric\_Field 
\index{Electric\_Field@{\tt Electric\_Field}}
\label{ElectricField}]
({\it real}),
default value = {\tt 0.001},
{\it default unit = ry/bohr/e} (rydbergs/Bohr radii/electron charge)

Magnitude of external electric field to be applied.

\end{htmllist}

\section{Flags for additional input/output}

\begin{htmllist}
\htmlitemmark{WhiteBall}

\item[\tt Skip\_force 
\index{Skip\_force@{\tt Skip\_force}}
\label{Skipforce}]
({\it boolean}),
default value = {\tt false }

Suppresses calculation of forces. Since forces are easily obtained, this
flag is useless.

\item[\tt Output\_All\_States 
\index{Output\_All\_States@{\tt Output\_All\_States}}
\label{OutputAllStates}]
({\it boolean}),
default value = {\tt false }

Forces output of all wave-functions up to requested number of states to file
{\tt wfn.dat}, instead of only the occupied ones (default).

\item[\tt Save\_Intermediate\_Charge\_Density 
\index{Save\_Intermediate\_Charge\_Density@{\tt Save\_Intermediate\_Charge\_Density}}
\label{SaveIntermediateChargeDensity}]
({\it boolean}),
default value = {\tt false }

Causes wave-functions to be written to output after each self-consistent
iteration, instead of only after self-consistency is reached. Previous
wave-functions are over-written. This is useful for debugging purposes or
if diagonalization is too demanding.

\item[\tt Output\_Eigenvalues 
\index{Output\_Eigenvalues@{\tt Output\_Eigenvalues}}
\label{OutputEigenvalues}]
({\it boolean}),
default value = {\tt false }

Saves eigenvalues to file {\tt eigen.dat} after self-consistency in density
function is achieved. This is useful for debugging purposes.

\item[\tt Save\_Intermediate\_Eigenvalues 
\index{Save\_Intermediate\_Eigenstates@{\tt Save\_Intermediate\_Eigenvalues}}
\label{SaveIntermediateEigenvalues}]
({\it boolean}),
default value = {\tt false }

Saves eigenvalues after each SCF iteration, instead of after the last one.
Useful for debugging purposes.

\item[\tt Output\_Level 
\index{Output\_Level@{\tt Output\_Level}}
\label{OutputLevel}]
({\it integer}),
default value = {\tt 0 }

Output flag for additional info in parsec.out. With default value,
only the basic output is written. Positive values cause additional
information to be printed out:

\begin{verbatim}
> 0 : print out local/non-local components of force
> 1 : print out all symmetry data and additional arpack output
> 2 : print out more BFGS output
> 3 : print out all BFGS output
> 4 : print out all g-stars (warning: _lots_ of data)
> 5 : print out local potentials (warning: _lots_ of data)
\end{verbatim}

\end{htmllist}

\chapter{Post-processing tools}

\section{PVOX}

PVOX (parsec visualization toolbox) is a set of matlab/python scripts,
with graphical user interface. It was developed in 2005 by Michael
Frasca, Lee Ballard, and Nick Voshell. It reads
parsec output directly and extracts the density of states, makes plots of
electron charge density, self-consistent potential, and selected
wave-functions. It has been tested with \htmladdnormallinkfoot{Matlab 7.0}
{http://www.mathworks.com/products/matlab/} and Python 2.4.1.

\section{Other tools}

There are additional tools that read parsec output and perform specific
tasks. Some of them are: {\tt angproj}, for calculation of angle-projected
density of states; {\tt plotdx}, interface with
\htmladdnormallinkfoot{Data Explorer}{http://www.opendx.org} for visualization
of charge density, wave-functions, etc.

\chapter{Problems, unfinished work, etc.}

\section{Troubleshooting}

\begin{enumerate}
\item Diagla occasionally shows bad performance if periodic boundary
  conditions are used. If you get error messages related to the
  eigensolver, try Arpack.

\item Missing symmetry operations: only symmetry operations belonging
  to the cubic system are tested for existence. That is because the
  finite grid spacing breaks all other symmetries (for example, the
  five-fold rotational symmetry in icosahedral molecules). In the
  Hamiltonian, only some of the present operations are used, and the
  selection is very stringent. If you feel not all possible symmetry
  operations are included in the Hamiltonian, try rotating or
  translating the whole system.

\item Unrecognized chemical element: Parsec has an internal periodic
  table (file {\tt ptable.f}), and it checks for the existence of
  input elements. If the
  calculation stops with "unknown element", edit the periodic table.

\item Current pseudopotential generator does not support asymptotically
   corrected functionals (LB, CS).
\end{enumerate}

\section{Future implementations}

\begin{enumerate}
\item Add k-point sampling in periodic systems.

\item Allow for a non-orthorhombic periodic cell if periodic boundary
  conditions are used.
\end{enumerate}

\addcontentsline{toc}{section}{Index}
\printindex

\end{document}
